package com.dd.payhub.paydefinition.model;

import com.dd.payhub.paycommon.model.RoleFunction;
import com.dd.payhub.paycommon.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRole {

    private String officialEmailId;
    private String password;
    private String status;
    private String firstName;
    private String lastName;
    private String middleName;
    private long phoneNumber;
    private String bankId;
    private long lastPasswordChange;
    private double lastLoginDate;
    private String approvalRequired; //true/false
    private int consecutiveBadLoging;
    private RoleFunction bankRole;

    public String getOfficialEmailId() {
        return officialEmailId;
    }

    public void setOfficialEmailId(String officialEmailId) {
        this.officialEmailId = officialEmailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public long getLastPasswordChange() {
        return lastPasswordChange;
    }

    public void setLastPasswordChange(long lastPasswordChange) {
        this.lastPasswordChange = lastPasswordChange;
    }

    public double getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(double lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public String getApprovalRequired() {
        return approvalRequired;
    }

    public void setApprovalRequired(String approvalRequired) {
        this.approvalRequired = approvalRequired;
    }

    public int getConsecutiveBadLoging() {
        return consecutiveBadLoging;
    }

    public void setConsecutiveBadLoging(int consecutiveBadLoging) {
        this.consecutiveBadLoging = consecutiveBadLoging;
    }

    public RoleFunction getBankRole() {
        return bankRole;
    }

    public void setBankRole(RoleFunction bankRole) {
        this.bankRole = bankRole;
    }
}
