package com.dd.payhub.paydefinition.dal;

import com.dd.payhub.paycommon.model.AlternateId;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.entity.AlternateIdDB;
import com.dd.payhub.paydefinition.entity.AlternateIdUnapprovedDB;

public interface AlternateIdDAL {

    ReactTableRecord getAllAlternateIdByPagination(ReactTableQuery tableQuery);
    NewReturnObject addAlternateId(AlternateId alternateId, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject getEditableAlternateId(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language);
    NewReturnObject updateAlternateId(AlternateId alternateId, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject approveAlternateId(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject rejectByAlternateId(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject alternateIdStatusChanger(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language, String setStatus);
    NewReturnObject viewByAlternateId(ReactTableQuery tableQuery,NewReturnObject newReturnObject,String language);
    AlternateIdDB findByAlternateId(String alternateId);
    AlternateIdUnapprovedDB findUnapprovedByAlternateId(String alternateId);
    long countByAlternateId(String alternateId);
    long countByUnapprovedAlternateId(String alternateId);
}
