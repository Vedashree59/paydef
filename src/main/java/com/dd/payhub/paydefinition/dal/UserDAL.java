package com.dd.payhub.paydefinition.dal;

import com.dd.payhub.paycommon.model.User;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paydefinition.entity.UserDB;

import javax.servlet.http.HttpServletRequest;

public interface UserDAL {
    UserDB findByOfficialEmailId(String EmailId);
    NewReturnObject verifyUser(User user,NewReturnObject newReturnObject, String language);
    NewReturnObject checkUserLoginCredentials(User user, HttpServletRequest request,NewReturnObject newReturnObject, String language);
    NewReturnObject listOfUserIdsByTenantId(String tenantId, NewReturnObject newReturnObject, String language) ;

    UserDB findTenantIDByOfficialEmailId(String userID);
}
