package com.dd.payhub.paydefinition.dal;

import com.dd.payhub.paycommon.model.Customer;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.entity.CustomerDB;
import com.dd.payhub.paydefinition.entity.CustomerUnapprovedDB;

public interface CustomerDAL {


    ReactTableRecord getAllCustomerByPagination(ReactTableQuery tableQuery);
    NewReturnObject addCustomer(Customer customer, String userId, NewReturnObject newReturnObject, String language,Boolean approvalRequired);
    NewReturnObject getEditableCustomer(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language);
    NewReturnObject updateCustomer(Customer customer, String userId, NewReturnObject newReturnObject, String language,Boolean approvalRequired);
    NewReturnObject approveCustomer(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject rejectByTenantIdCustomerId(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject viewByTenantIdCustomerId(ReactTableQuery tableQuery,NewReturnObject newReturnObject,String language);
    NewReturnObject customerStatusChanger(ReactTableQuery tableQuery, String headerUserId, NewReturnObject newReturnObject, String language, String setStatus,Boolean approvalRequired);
    CustomerDB findByTenantIdCustomerId(String tenantId,String customerId);
    CustomerUnapprovedDB findUnapprovedByTenantIdCustomerId(String tenantId,String customerId);
    long countByTenantIdCustomerId(String tenantId,String customerId);
    long countUnapprovedByTenantIdCustomerId(String tenantId,String customerId);

    void listOfAllCustomerIdsByRegx(String keyword,String tenantId,NewReturnObject newReturnObject,String language);
}
