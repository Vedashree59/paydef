package com.dd.payhub.paydefinition.dal;



import com.dd.payhub.paycommon.model.Country;
import com.dd.payhub.paycommon.model.Country;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.entity.CountryDB;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paydefinition.entity.CountryUnapprovedDB;
import com.dd.payhub.paydefinition.entity.CountryDB;
import com.dd.payhub.paydefinition.entity.CountryUnapprovedDB;

public interface CountryDAL {

    ReactTableRecord getAllCountryByPagination(ReactTableQuery tableQuery);
    NewReturnObject addCountry(Country country, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject getEditableCountry(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language);
    NewReturnObject updateCountry(Country country, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject approveCountry(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject rejectByCountryCode(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject countryStatusChanger(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language, String setStatus);
    NewReturnObject viewByCountryCode(ReactTableQuery tableQuery,NewReturnObject newReturnObject,String language);
    CountryDB findByCountryCode(String countryCode);
    CountryUnapprovedDB findUnapprovedByCountryCode(String countryCode);
    long countByCountryCode(String countryCode);
    long countByUnapprovedCountryCode(String countryCode);
    

    CountryDB findByCountryName(String countryName);

    NewReturnObject listOfAllCountriesCodeAndNamesByRegx(String keyword, com.dd.payhub.paycommon.view.NewReturnObject newReturnObject, String language);
}

