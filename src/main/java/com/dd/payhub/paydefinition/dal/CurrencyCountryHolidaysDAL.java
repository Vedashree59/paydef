package com.dd.payhub.paydefinition.dal;

import com.dd.payhub.paycommon.model.CountryHolidays;
import com.dd.payhub.paycommon.model.CurrencyCountryHolidays;
import com.dd.payhub.paycommon.model.CurrencyCountryHolidays;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.entity.*;

public interface CurrencyCountryHolidaysDAL {

    NewReturnObject addCurrencyCountryHolidays(CurrencyCountryHolidays currencyCountryHolidays, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired);

    ReactTableRecord getAllCurrencyCountryHolidaysByPagination(ReactTableQuery tableQuery);

    CurrencyCountryHolidaysDB findByTenantIdCurrencyCountryCodeYear(String tenantId, String countryCode,String currencyCode, String year);

    CurrencyCountryHolidaysUnapprovedDB findUnapprovedByTenantIdCurrencyCountryCodeYear(String tenantId, String countryCode,String currencyCode, String year);
    //
    NewReturnObject getEditableCurrencyCountryHolidays(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language);

    //
    NewReturnObject updateCurrencyCountryHolidays(CurrencyCountryHolidays currencyCountryHolidaysDB, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired);

    NewReturnObject approveCurrencyCountryHolidays(ReactTableQuery reactTableQuery, String userId, NewReturnObject newReturnObject, String language);

    //
    NewReturnObject deleteByTenantIdCurrencyCountryCodeYear(ReactTableQuery reactTableQuery, String userId, NewReturnObject newReturnObject, String language);

    //
    NewReturnObject viewByTableQuery(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language);
}
