package com.dd.payhub.paydefinition.dal;

import com.dd.payhub.paycommon.model.FunctionDefinition;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.entity.FunctionListDB;
import com.dd.payhub.paydefinition.entity.FunctionListUnapprovedDB;

public interface FunctionListDAL {
    ReactTableRecord getAllFunctionListByPagination(ReactTableQuery tableQuery);
<<<<<<< HEAD

    NewReturnObject addFunctionList(FunctionDefinition functionDefinition, String userId, NewReturnObject newReturnObject, String language);

    FunctionListDB findByFunctionId(String functionId);

    FunctionListUnapprovedDB findUnapprovedByFunctionId(String functionId);

    NewReturnObject getEditableFunctionList(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language);

    NewReturnObject updateFunctionList(FunctionDefinition functionDefinition, String userId, NewReturnObject newReturnObject, String language);

=======
    NewReturnObject addFunctionList(FunctionDefinition functionDefinition, String userId, NewReturnObject newReturnObject, String language);
    FunctionListDB findByFunctionId(String functionId);
    FunctionListUnapprovedDB findUnapprovedByFunctionId(String functionId);

    NewReturnObject getEditableFunctionList(String functionId, NewReturnObject newReturnObject, String language);

    NewReturnObject updateFunctionList(FunctionDefinition functionDefinition, String userId, NewReturnObject newReturnObject, String language);
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
    NewReturnObject approveFunctionList(String functionId, String userId, NewReturnObject newReturnObject, String language);

    NewReturnObject deleteByFunctionId(String functionId, String userId, NewReturnObject newReturnObject, String language);

<<<<<<< HEAD
    NewReturnObject viewByFunctionId(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language);

    void getListOfFunction(String keyword, NewReturnObject newReturnObject, String language);
=======
    NewReturnObject viewByFunctionId(ReactTableQuery tableQuery,NewReturnObject newReturnObject,String language);

>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

}
