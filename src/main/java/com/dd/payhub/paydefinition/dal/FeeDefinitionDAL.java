package com.dd.payhub.paydefinition.dal;

import com.dd.payhub.paycommon.model.FeeDefinition;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.entity.FeeDefinitionDB;
import com.dd.payhub.paydefinition.entity.FeeDefinitionUnapprovedDB;

public interface FeeDefinitionDAL {
    ReactTableRecord getAllFeeDefinitionByPagination(ReactTableQuery tableQuery);
    NewReturnObject addFeeDefinition(FeeDefinition feeDefinition, String userId, NewReturnObject newReturnObject, String language,boolean approvalRequired);
    NewReturnObject getEditableFeeDefinition(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language);
    NewReturnObject updateFeeDefinition(FeeDefinition feeDefinition, String userId, NewReturnObject newReturnObject, String language,boolean approvalRequired);
    NewReturnObject approveFeeDefinition(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject rejectByFeeIdTenantId(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject feeDefinitionStatusChanger(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language, String setStatus,boolean approvalRequired);
    NewReturnObject viewByFeeIdTenantId(ReactTableQuery tableQuery,NewReturnObject newReturnObject,String language);
    FeeDefinitionDB findByFeeIdTenantId(String feeId, String tenantId);
    FeeDefinitionUnapprovedDB findUnapprovedByFeeIdTenantId(String feeId, String tenantId);
    long countByFeeIdTenantId(String feeId, String tenantId);
    long countByUnapprovedFeeIdTenantId(String feeId, String tenantId);
}
