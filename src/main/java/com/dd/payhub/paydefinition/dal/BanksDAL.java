package com.dd.payhub.paydefinition.dal;

import com.dd.payhub.paycommon.model.Banks;
import com.dd.payhub.paycommon.model.Banks;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.entity.BanksDB;
import com.dd.payhub.paydefinition.entity.BanksUnapprovedDB;
import com.dd.payhub.paydefinition.entity.BanksDB;
import com.dd.payhub.paydefinition.entity.BanksUnapprovedDB;

import java.util.List;

public interface BanksDAL {


    ReactTableRecord getAllBanksByPagination(ReactTableQuery tableQuery);
    NewReturnObject addBank(Banks banks, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject getEditableBank(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language);
    NewReturnObject updateBank(Banks banks, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject approveBank(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject rejectByBankListId(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject banksStatusChanger(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language, String setStatus);
    NewReturnObject viewByBankListId(ReactTableQuery tableQuery,NewReturnObject newReturnObject,String language);
    BanksDB findByBankListId(String banksId);
    BanksUnapprovedDB findUnapprovedByBankListId(String banksId);
    long countByBankListId(String banksId);
    long countByUnapprovedBankListId(String banksId);





    NewReturnObject listOfIds(java.lang.String keyword, NewReturnObject newReturnObject, java.lang.String language);

}
