package com.dd.payhub.paydefinition.dal;


import com.dd.payhub.paycommon.model.ProductDefinition;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.entity.ProductDefinitionDB;
import com.dd.payhub.paydefinition.entity.ProductDefinitionUnapprovedDB;

public interface ProductDefinitionDAL {

    ReactTableRecord getAllProductDefinitionByPagination(ReactTableQuery tableQuery);
    NewReturnObject addProductDefinition(ProductDefinition productDefinition, String userId, NewReturnObject newReturnObject, String language,Boolean approvalRequired);
    NewReturnObject getEditableProductDefinition(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language);
    NewReturnObject updateProductDefinition(ProductDefinition productDefinition, String userId, NewReturnObject newReturnObject, String language,Boolean approvalRequired);
    NewReturnObject approveProductDefinition(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject rejectByProductId(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject viewByProductId(ReactTableQuery tableQuery,NewReturnObject newReturnObject,String language);
    NewReturnObject productDefinitionStatusChanger(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language, String setStatus,Boolean approvalRequired);
    ProductDefinitionDB findByProductId(String productId);
    ProductDefinitionUnapprovedDB findUnapprovedByProductId(String productId);
    long countByProductId(String productId);
    long countByUnapprovedProductId(String productId);
}
