package com.dd.payhub.paydefinition.dal;

import com.dd.payhub.paycommon.model.EventDefinition;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.entity.EventDefinitionDB;
import com.dd.payhub.paydefinition.entity.EventDefinitionUnapprovedDB;

public interface EventDefinitionDAL {

    ReactTableRecord getAllEventDefinitionByPagination(ReactTableQuery tableQuery);
    NewReturnObject addEventDefinition(EventDefinition eventDefinition, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject getEditableEventDefinition(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language);
    NewReturnObject updateEventDefinition(EventDefinition eventDefinition, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject approveEventDefinition(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject rejectByEventId(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject eventDefinitionStatusChanger(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language, String setStatus);
    NewReturnObject viewByEventId(ReactTableQuery tableQuery,NewReturnObject newReturnObject,String language);
    EventDefinitionDB findByEventId(String eventId);
    EventDefinitionUnapprovedDB findUnapprovedByEventId(String eventId);
    long countByEventId(String eventId);
    long countByUnapprovedEventId(String eventId);

}
