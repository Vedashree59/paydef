package com.dd.payhub.paydefinition.dal;

<<<<<<< HEAD
public interface CurrencyPairDAL {
=======
import com.dd.payhub.paycommon.model.CurrencyPair;
import com.dd.payhub.paycommon.model.CurrencyPair;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.entity.CurrencyPairDB;
import com.dd.payhub.paydefinition.entity.CurrencyPairUnapprovedDB;

public interface CurrencyPairDAL {
    ReactTableRecord getAllCurrencyPairByPagination(ReactTableQuery tableQuery);

    NewReturnObject addCurrencyPair(CurrencyPair currencyPair, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired);

    CurrencyPairDB findByTenantIdCountryCodeCurrencyCode(String tenantId, String countryCode, String CurrencyCode);

    CurrencyPairUnapprovedDB findUnapprovedByTenantIdCountryCodeCurrencyCode(String tenantId, String countryCode, String CurrencyCode);

    NewReturnObject getEditableCurrencyPair(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language);

    NewReturnObject updateCurrencyPair(CurrencyPair currencyPairDB, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired);


    NewReturnObject approveCurrencyPair(ReactTableQuery reactTableQuery, String userId, NewReturnObject newReturnObject, String language);

    NewReturnObject deleteByTenantIdCountryCodeCurrencyCode(ReactTableQuery reactTableQuery, String userId, NewReturnObject newReturnObject, String language);

    NewReturnObject viewByTenantIdCountryCodeCurrencyCode(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language);


>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
}
