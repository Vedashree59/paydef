package com.dd.payhub.paydefinition.dal;

import com.dd.payhub.paycommon.model.CountryHolidays;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.entity.CountryHolidaysDB;
import com.dd.payhub.paydefinition.entity.CountryHolidaysUnapprovedDB;

public interface CountryHolidaysDAL {

    ReactTableRecord getAllCountryHolidaysByPagination(ReactTableQuery tableQuery);

    NewReturnObject addCountryHolidays(CountryHolidays countryHolidays, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired);

    NewReturnObject getEditableCountryHolidays(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language);

    NewReturnObject updateCountryHolidays(CountryHolidays countryHolidaysDB, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired);

    NewReturnObject approveCountryHolidays(ReactTableQuery reactTableQuery, String userId, NewReturnObject newReturnObject, String language);

    NewReturnObject rejectByTenantIdCountryCodeYear(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language);

    NewReturnObject viewByTenantIdCountryCodeYear(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language);

    NewReturnObject countryHolidaysStatusChanger(ReactTableQuery tableQuery, String headerUserId, NewReturnObject newReturnObject, String language, String setStatus, Boolean approvalRequired);


    CountryHolidaysDB findByTenantIdCountryCodeYear(String tenantId, String countryCode, String year);

    CountryHolidaysUnapprovedDB findUnapprovedByTenantIdCountryCodeYear(String tenantId, String countryCode, String year);

    long countByTenantIdCountryCodeYear(String tenantId, String countryCode, String year);

    long countByUnapprovedTenantIdCountryCodeYear(String tenantId, String countryCode, String year);
}