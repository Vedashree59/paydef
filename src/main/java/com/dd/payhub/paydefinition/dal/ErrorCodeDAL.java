package com.dd.payhub.paydefinition.dal;

import com.dd.payhub.paycommon.view.AppError;

import java.util.List;

public interface ErrorCodeDAL {
    AppError findByErrorCodeAndLanguage(String errorCode, String language);

    List<AppError> findByErrorCodeAndLanguage(String[] errorCode, String language);
}
