package com.dd.payhub.paydefinition.dal;

import com.dd.payhub.paycommon.model.Country;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.model.Tenant;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.entity.TenantDB;
import com.dd.payhub.paydefinition.entity.TenantUnapprovedDB;



public interface TenantDAL {
    ReactTableRecord getAllTenantByPagination(ReactTableQuery tableQuery);
<<<<<<< HEAD
    NewReturnObject addTenant(Tenant tenant, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject updateTenant(Tenant tenant, String userId, NewReturnObject newReturnObject, String language);
    TenantDB findByTenantId(String tenantid);
    TenantDB findApprovalRequiredTenantId(String tenantid);
    long countByTenantId(String tenantid);
    long countByUnapprovedTenantId(String tenantid);
    TenantUnapprovedDB findUnapprovedByTenantId(String tenantid);
    NewReturnObject getEditableTenant(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language);
    NewReturnObject rejectByTenantId(String tenantId, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject approveTenant(String tenantId, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject viewByTenantId(ReactTableQuery tableQuery,NewReturnObject newReturnObject,String language);
    NewReturnObject tenantStatusChanger(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language, String setStatus);
=======

    NewReturnObject addTenant(Tenant tenant, String userId, NewReturnObject newReturnObject, String language);

    NewReturnObject updateTenant(Tenant tenant, String userId, NewReturnObject newReturnObject, String language);

    TenantDB findByTenantId(String tenantid);

    TenantDB findApprovalRequiredTenantId(String tenantid);

    long countByTenantId(String tenantid);

    TenantUnapprovedDB findUnapprovedByTenantId(String tenantid);

    NewReturnObject getEditableTenant(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language);

    NewReturnObject deleteByTenantId(String tenantId, String userId, NewReturnObject newReturnObject, String language);

    NewReturnObject approveTenant(String tenantId, String userId, NewReturnObject newReturnObject, String language);

    NewReturnObject viewByTenantId(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language);

>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
    NewReturnObject listOfAllTenantIdsByRegx(String keyword, NewReturnObject newReturnObject, String language);
}
