package com.dd.payhub.paydefinition.dal;

import com.dd.payhub.paycommon.model.*;
import com.dd.payhub.paycommon.model.Currency;
import com.dd.payhub.paycommon.model.Currency;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.entity.*;

public interface CurrencyDAL {
    ReactTableRecord getAllCurrencyByPagination(ReactTableQuery tableQuery);
    NewReturnObject addCurrency(Currency currency, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject getEditableCurrency(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language);
    NewReturnObject updateCurrency(Currency currency, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject approveCurrency(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject rejectByCurrencyCode(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language);
    NewReturnObject currencyStatusChanger(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language, String setStatus);
    NewReturnObject viewByCurrencyCode(ReactTableQuery tableQuery,NewReturnObject newReturnObject,String language);
    CurrencyDB findByCurrencyCode(String currencyCode);
    CurrencyUnapprovedDB findUnapprovedByCurrencyCode(String currencyCode);
    long countByCurrencyCode(String currencyCode);
    long countByUnapprovedCurrencyCode(String currencyCode);
<<<<<<< HEAD
    NewReturnObject listOfAllCurrenciesCodeAndNamesByRegx(String keyword, com.dd.payhub.paycommon.view.NewReturnObject newReturnObject, String language);
=======


    NewReturnObject listOfAllCurrencyCodeAndNamesByRegx(String keyword, com.dd.payhub.paycommon.view.NewReturnObject newReturnObject, String language);
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
}
