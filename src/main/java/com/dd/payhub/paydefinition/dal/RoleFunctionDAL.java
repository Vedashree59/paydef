package com.dd.payhub.paydefinition.dal;

import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.model.RoleFunction;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.entity.RoleFunctionDB;
import com.dd.payhub.paydefinition.entity.RoleFunctionUnapprovedDB;

public interface RoleFunctionDAL {
    ReactTableRecord getAllRoleFunctionByPagination(ReactTableQuery tableQuery);

    void addRoleFunction(RoleFunction roleFunction, String userId, NewReturnObject newReturnObject, String language);

    RoleFunctionDB findByRoleId(String roleId);

    RoleFunctionUnapprovedDB findUnapprovedByRoleId(String roleId);

    long countByRoleId(String roleId);

    long countUnapprovedByRoleId(String roleId);

    void getEditableRoleFunction(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language);

    void updateRoleFunction(RoleFunction roleFunction, String userId, NewReturnObject newReturnObject, String language);

    void deleteRoleFunction(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language);

    void approveRoleFunction(String roleId, String userId, NewReturnObject newReturnObject, String language);

    void deleteByRoleId(String roleId, String userId, NewReturnObject newReturnObject, String language);

    void viewByRoleId(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language);
}
