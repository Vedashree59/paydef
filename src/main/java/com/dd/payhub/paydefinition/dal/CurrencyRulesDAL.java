package com.dd.payhub.paydefinition.dal;

import com.dd.payhub.paycommon.model.CurrencyRules;
import com.dd.payhub.paycommon.model.CurrencyRules;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.entity.*;
import com.dd.payhub.paydefinition.entity.CurrencyRulesDB;
import com.dd.payhub.paydefinition.entity.CurrencyRulesUnapprovedDB;

public interface CurrencyRulesDAL {

    ReactTableRecord getAllCurrencyRulesByPagination(ReactTableQuery tableQuery);

    NewReturnObject addCurrencyRules(CurrencyRules currencyRules, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired);

    CurrencyRulesDB findByTenantIdCountryCodeCurrencyCode(String tenantId, String countryCode, String CurrencyCode);

    CurrencyRulesUnapprovedDB findUnapprovedByTenantIdCountryCodeCurrencyCode(String tenantId, String countryCode, String CurrencyCode);

    NewReturnObject getEditableCurrencyRules(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language);

    NewReturnObject updateCurrencyRules(CurrencyRules currencyRulesDB, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired);

    NewReturnObject approveCurrencyRules(ReactTableQuery reactTableQuery, String userId, NewReturnObject newReturnObject, String language);

    NewReturnObject deleteByTenantIdCountryCodeCurrencyCode(ReactTableQuery reactTableQuery, String userId, NewReturnObject newReturnObject, String language);

    NewReturnObject viewByTenantIdCountryCodeCurrencyCode(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language);


    CurrencyRulesDB findApprovalRequiredTenantIdCountryCodeCurrencyCode(String tenantId, String countryCode, String currencyCode);

    long countByTenantIdCountryCodeCurrencyCode(String tenantId, String countryCode, String currencyCode);

}
