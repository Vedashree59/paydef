package com.dd.payhub.paydefinition.dal;

import com.dd.payhub.paycommon.model.CurrencyHolidays;
import com.dd.payhub.paycommon.model.CurrencyHolidays;
import com.dd.payhub.paycommon.model.CurrencyHolidays;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.entity.*;
import com.dd.payhub.paydefinition.entity.CurrencyHolidaysDB;
import com.dd.payhub.paydefinition.entity.CurrencyHolidaysUnapprovedDB;

public interface CurrencyHolidaysDAL {

    ReactTableRecord getAllCurrencyHolidaysByPagination(ReactTableQuery tableQuery);

    NewReturnObject addCurrencyHolidays(CurrencyHolidays currencyHolidays, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired);
    NewReturnObject getEditableCurrencyHolidays(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language);

    NewReturnObject updateCurrencyHolidays(CurrencyHolidays currencyHolidaysDB, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired);

    NewReturnObject approveCurrencyHolidays(ReactTableQuery reactTableQuery, String userId, NewReturnObject newReturnObject, String language);
//
    NewReturnObject rejectByTenantIdCurrencyCodeYear(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language);
//
    NewReturnObject viewByTenantIdCurrencyCodeYear(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language);
//
    NewReturnObject currencyHolidaysStatusChanger(ReactTableQuery tableQuery, String headerUserId, NewReturnObject newReturnObject, String language, String setStatus, Boolean approvalRequired);


    CurrencyHolidaysDB findByTenantIdCurrencyCodeYear(String tenantId, String currencyCode, String year);

    CurrencyHolidaysUnapprovedDB findUnapprovedByTenantIdCurrencyCodeYear(String tenantId, String currencyCode, String year);
//
    long countByTenantIdCurrencyCodeYear(String tenantId, String currencyCode, String year);
//
    long countByUnapprovedTenantIdCurrencyCodeYear(String tenantId, String currencyCode, String year);



}
