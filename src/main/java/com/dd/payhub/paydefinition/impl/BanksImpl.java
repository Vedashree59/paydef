package com.dd.payhub.paydefinition.impl;

import com.dd.payhub.paycommon.model.*;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.dal.BanksDAL;
import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
import com.dd.payhub.paydefinition.entity.*;
import com.dd.payhub.paydefinition.entity.BanksDB;
import com.dd.payhub.paydefinition.entity.BanksUnapprovedDB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Collation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;


@Repository
public class BanksImpl implements BanksDAL {
    private static final Logger logger = LogManager.getLogger(BanksImpl.class);

    private final MongoTemplate mongoTemplate;
    private ErrorCodeDAL errorCodeDAL;

    @Autowired

    public BanksImpl(MongoTemplate mongoTemplate, ErrorCodeDAL errorCodeDAL) {
        this.mongoTemplate = mongoTemplate;
        this.errorCodeDAL = errorCodeDAL;
    }

    @Override
    public ReactTableRecord getAllBanksByPagination(ReactTableQuery tableQuery) {
        int page = 0;
        int pageSize = 5;
        int totalPage;
        long totalRecord = 0;
        ReactTableRecord tableRecord = new ReactTableRecord();
        String sortField = tableQuery.getSortField().isEmpty() ? "sortDate" : tableQuery.getSortField();

        Query query = new Query();//if
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.

        try {
            if (tableQuery.getPage() != 0) {
                page = tableQuery.getPage();
            }
            if (tableQuery.getPageSize() != 0) {
                pageSize = tableQuery.getPageSize();
            }
            //sorting order code.
            if (!tableQuery.getSortOrder().equals("true")) {//false means order by asc.
                query.with(Sort.by(Sort.Direction.DESC, sortField));
            } else {
                query.with(Sort.by(Sort.Direction.ASC, sortField));
            }
            Criteria statusCriteria = Criteria.where("bank.status").is(tableQuery.getStatus());
            query.addCriteria(Criteria.where("bank.status").is(tableQuery.getStatus()));

            if (!tableQuery.getKeyword().isEmpty()) {
                Criteria bankNameCriteria = Criteria.where("bank.bankName").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria bankListIdCriteria = Criteria.where("bank.bankListId").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria banksFullNameCriteria = Criteria.where("bank.banksShortName").regex(String.valueOf(tableQuery.getKeyword()), "i");

                query.addCriteria(new Criteria().orOperator(bankNameCriteria, bankListIdCriteria, banksFullNameCriteria).andOperator(statusCriteria));
            }
            if (tableQuery.getCollection().equals("approved")) {
                totalRecord = mongoTemplate.count(query, BanksDB.class);

                tableRecord.setTableRecord(mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), BanksDB.class));
            } else {
                totalRecord = mongoTemplate.find(query, BanksUnapprovedDB.class).size();

                tableRecord.setTableRecord((mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), BanksUnapprovedDB.class)));
            }
            totalPage = (int) Math.ceil(totalRecord / (float) pageSize);
            tableRecord.setTotalPage(totalPage);
            tableRecord.setTotalRecord(totalRecord);
            return tableRecord;
        } catch (Exception ex) {
            tableRecord.setTotalPage(0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord(new ArrayList<>());
            logger.error(ex.getMessage(),"getAllBanksByPagination");
            return tableRecord;
        }
    }

    @Override
    public NewReturnObject addBank(Banks banks, String userId, NewReturnObject newReturnObject, String language) {
        try {

            BanksDB banksDB = new BanksDB();
            banksDB.setBank(banks);
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            banksDB.addAuditInfo(audit);
            banksDB.getBank().setVersion(0);//setting the version.
            AuditHistory auditHistory = new AuditHistory(audit, "tm_banks_unapproved");

            banksDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
            //            inserting in the database.
            auditHistory.addContent(mongoTemplate.insert(new BanksUnapprovedDB(banksDB)));
            auditHistory.addLastStatus(null);

            mongoTemplate.insert(auditHistory);
            newReturnObject.PerformReturnObject("Success");
            return newReturnObject;
        } catch (Exception ex) {
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MBS002", language));//error code
            newReturnObject.setReturncode(0);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            return newReturnObject;
        }
    }

    @Override
    public NewReturnObject updateBank(Banks bank, String userId, NewReturnObject newReturnObject, String language) {
        try {
            BanksDB banksDB = new BanksDB();
            banksDB.setBank(bank);

            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            //
            BanksUnapprovedDB banksUnapprovedDB = findUnapprovedByBankListId(banksDB.getBank().getBankListId());
            BanksDB banksDBResult = findByBankListId(banksDB.getBank().getBankListId());
            if (banksDBResult != null || banksUnapprovedDB != null) {//record must exist to update

                AuditHistory auditHistory = new AuditHistory(audit, "tm_banks_unapproved");

                if (banksUnapprovedDB == null) {
                    boolean approveStatus = banksDBResult != null ? banksDBResult.getBank().getStatus().equals(banksDB.getBank().getStatus()) : false;
                    if (approveStatus) {//status is not changed
                        banksDB.setAuditInfo(banksDBResult.getAuditInfo());
                        banksDB.addAuditInfo(audit);
                        banksDB.getBank().setVersion(banksDBResult.getBank().getVersion() + 1);//version gets increased during update
                        banksDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.

                        auditHistory.addLastStatus(banksDBResult);//last version record.
                        auditHistory.addContent(mongoTemplate.save(new BanksUnapprovedDB(banksDB)));
                        mongoTemplate.insert(auditHistory);
                        newReturnObject.PerformReturnObject("Success");
                        newReturnObject.setReturncode(1);
                    } else {
                        //cannot change the status during edit.
                        newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MBS009", language));
                    }
                } else {
                    boolean approveStatus = banksUnapprovedDB != null ? banksUnapprovedDB.getBank().getStatus().equals(banksDB.getBank().getStatus()) : false;
                    if (approveStatus) {//status is not changed
                        banksDB.setAuditInfo(banksUnapprovedDB.getAuditInfo());
                        banksDB.addAuditInfo(audit);
                        banksDB.set_id(banksUnapprovedDB.get_id());
                        banksDB.getBank().setVersion(banksUnapprovedDB.getBank().getVersion() + 1);//version gets increased during update
                        banksDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
                        banksDB.set_id(banksUnapprovedDB.get_id());//setting the mongo ID.

                        auditHistory.addLastStatus(banksUnapprovedDB);//old version record.
                        auditHistory.addContent(mongoTemplate.save(new BanksUnapprovedDB(banksDB)));
                        mongoTemplate.insert(auditHistory);
                        newReturnObject.PerformReturnObject("Success");
                        newReturnObject.setReturncode(1);
                    } else {
                        //cannot change the status during edit.
                        newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MBS009", language));
                    }

                }

            } else {
                //error record does not exist to update.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MBS006", language));
            }
        } catch (Exception ex) {

            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MBS002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject approveBank(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            String bankListId=tableQuery.getPrimaryKey();
            BanksUnapprovedDB unApprovedResult = findUnapprovedByBankListId(bankListId);
            BanksDB banksDB = findByBankListId(bankListId);


            Audit audit = audit = getLastAuditUnapproved(bankListId, newReturnObject, language);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            audit.setApproverId(userId);
            if (!audit.getCreatorId().equals(userId)) {//creator and approver cannot be same.

                unApprovedResult.addAuditInfo(audit); //setting the audit.
                if (unApprovedResult != null) {//record exist to approve.
                    AuditHistory auditHistory = new AuditHistory(audit, "tm_banks");

                    if (banksDB == null) {
                        // data is not present in main collection, create new record
                        auditHistory.addLastStatus(null);
                        auditHistory.addContent(mongoTemplate.insert(new BanksDB(unApprovedResult)));
                    } else {
                        String mongoId = banksDB.get_id();
                        auditHistory.addLastStatus(banksDB);//old version
                        banksDB = new BanksDB(unApprovedResult);
                        banksDB.set_id(mongoId);

                        auditHistory.addContent(mongoTemplate.save(banksDB));
                    }
                    newReturnObject.PerformReturnObject(null);


                    mongoTemplate.insert(auditHistory);
                    newReturnObject = rejectByBankListId(tableQuery, userId, newReturnObject, language);
                } else {//no record to approve.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MBS004", language));
                }
            } else {//approverID and creatorID are same.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MBS008", language));
            }

        } catch (Exception ex) {

            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MBS002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    public NewReturnObject rejectByBankListId(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            String bankListId=tableQuery.getPrimaryKey();
            BanksUnapprovedDB bankUnapprovedDB = findUnapprovedByBankListId(bankListId);
            Audit audit = getLastAuditUnapproved(bankListId, newReturnObject, language);
            audit.setApproverId(userId);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            AuditHistory auditHistory = new AuditHistory(audit, "tm_banks_unapproved");
            auditHistory.addContent(bankUnapprovedDB);

            Query query = new Query();
            query.addCriteria(Criteria.where("bank.bankListId").is(bankListId));
            mongoTemplate.remove(query, BanksUnapprovedDB.class);

            mongoTemplate.insert(auditHistory);

            newReturnObject.setReturncode(1);
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MBS002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject getEditableBank(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language) {
        try {
            //gives approved record only when there are no unapproved records.
            if (reactTableQuery.getCollection().equals("approved")) {

                if (countByUnapprovedBankListId(reactTableQuery.getPrimaryKey()) == 0) {
                    newReturnObject.PerformReturnObject(findByBankListId(reactTableQuery.getPrimaryKey()));
                } else {
                    newReturnObject.PerformReturnObject(new ArrayList<>());
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MBS007", language));
                }

            } else if (reactTableQuery.getCollection().equals("unapproved")) {
                newReturnObject.PerformReturnObject(findUnapprovedByBankListId(reactTableQuery.getPrimaryKey()));
            }
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MBS002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }


    @Override
    public NewReturnObject viewByBankListId(ReactTableQuery tableQuery, NewReturnObject newReturnObject , String language) {
        try {
            String bankListId=tableQuery.getPrimaryKey();
            Criteria criteriabankListId= Criteria.where("bank.bankListId").is(bankListId);
            if (tableQuery.getCollection().equals("approved")) {
                BanksDB banksDB = mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriabankListId)), BanksDB.class);
                newReturnObject.PerformReturnObject(banksDB);
            } else if (tableQuery.getCollection().equals("unapproved")) {
                BanksUnapprovedDB banksUnapprovedDB = mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriabankListId)), BanksUnapprovedDB.class);
                newReturnObject.PerformReturnObject(banksUnapprovedDB);
            } else {
                newReturnObject.setReturncode(0);
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MBS010", language));
            }

        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MBS002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());

        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject banksStatusChanger(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language, String setStatus)
    {
        try {
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            String bankListId=tableQuery.getPrimaryKey();
            BanksDB banksDBResult = findByBankListId(bankListId);
            BanksUnapprovedDB banksUnapprovedDB = findUnapprovedByBankListId(bankListId);
            String collection=tableQuery.getCollection();



            AuditHistory auditHistory = new AuditHistory(audit, "tm_banks_unapproved");
            BanksDB banksDB=null;

            if(collection.equals("approved")){
                banksDB=banksDBResult!=null?banksDBResult:null;
            }
            if(collection.equals("unapproved")){
                banksDB=banksUnapprovedDB!=null?new BanksDB(banksUnapprovedDB):null;
            }
            if(banksDB!=null) { //record must exist to alter status.
                banksDB.getBank().setVersion(banksDB.getBank().getVersion() + 1);
                banksDB.addAuditInfo(audit);
                auditHistory.addLastStatus(banksDBResult);//last version record.
                String status = banksDB.getBank().getStatus();
                banksDB.getBank().setStatus(setStatus);//changing the status.
                auditHistory.addContent(mongoTemplate.save(new BanksUnapprovedDB(banksDB)));


                mongoTemplate.insert(auditHistory);
                newReturnObject.PerformReturnObject("Success");
                newReturnObject.setReturncode(1);
            }else{
//                  error record does not exist to update.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MBS006", language));
            }


        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MBS002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }
<<<<<<< HEAD

    @Override
    public BanksUnapprovedDB findUnapprovedByBankListId(String bankListId) {
        try{
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));
            query.addCriteria(Criteria.where("bank.bankListId").is(bankListId));
            return mongoTemplate.findOne(query, BanksUnapprovedDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "findByBankListId");
            return null;
        }
=======
    
    @Override
    public BanksUnapprovedDB findUnapprovedByBankListId(String bankListId) {
        try{
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));
        query.addCriteria(Criteria.where("bank.bankListId").is(bankListId));
        return mongoTemplate.findOne(query, BanksUnapprovedDB.class);
    } catch (Exception ex) {
        logger.error(ex.getMessage(), "findByBankListId");
        return null;
    }
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
    }

    @Override
    public BanksDB findByBankListId(String name) {
        try{
<<<<<<< HEAD
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));
            query.addCriteria(Criteria.where("bank.bankListId").is(name));
            return mongoTemplate.findOne(query, BanksDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "findByBankListId");
            return null;
        }
=======
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));
        query.addCriteria(Criteria.where("bank.bankListId").is(name));
        return mongoTemplate.findOne(query, BanksDB.class);
    } catch (Exception ex) {
        logger.error(ex.getMessage(), "findByBankListId");
        return null;
    }
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
    }

    @Override
    public long countByBankListId(String bankListId) {
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));
        query.addCriteria(Criteria.where("bank.bankListId").is(bankListId));
        return mongoTemplate.count(query, BanksDB.class);
    }

    @Override
    public long countByUnapprovedBankListId(String bankListId) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("bank.bankListId").is(bankListId));
            return mongoTemplate.count(query, BanksUnapprovedDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByUnapprovedBankListId");
            return 0;
        }
    }



    @Override
    public NewReturnObject listOfIds(String keyword, NewReturnObject newReturnObject,String language) {
<<<<<<< HEAD
        try {
            Query query=new Query();
            query.fields().include("bank.bankListId");
            query.with(Sort.by(Sort.Direction.ASC,"bank.bankListId"));
            Criteria statusCriteria=Criteria.where("bank.status").is("active");

            if(keyword.isEmpty()||keyword.equals(" ")){
                newReturnObject.PerformReturnObject(mongoTemplate.find(query.addCriteria(statusCriteria).limit(100),BanksDB.class));
            }else {
                Criteria bankListIdCriteria = Criteria.where("bank.bankListId").regex(keyword, "i");
                query.addCriteria(new Criteria().andOperator(statusCriteria,bankListIdCriteria));
                newReturnObject.PerformReturnObject(mongoTemplate.find(query,BanksDB.class));
            }


        }
        catch (Exception ex) {
            logger.error((Marker) ex,newReturnObject.getAppErrorString());
            newReturnObject.PerformReturnObject(new ArrayList<>());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MBS002", language));
        }
        return newReturnObject;
=======
       try {
           Query query=new Query();
           query.fields().include("bank.bankListId");
           query.with(Sort.by(Sort.Direction.ASC,"bank.bankListId"));
           Criteria statusCriteria=Criteria.where("bank.status").is("active");

           if(keyword.isEmpty()||keyword.equals(" ")){
               newReturnObject.PerformReturnObject(mongoTemplate.find(query.addCriteria(statusCriteria).limit(100),BanksDB.class));
           }else {
               Criteria bankListIdCriteria = Criteria.where("bank.bankListId").regex(keyword, "i");
               query.addCriteria(new Criteria().andOperator(statusCriteria,bankListIdCriteria));
               newReturnObject.PerformReturnObject(mongoTemplate.find(query,BanksDB.class));
           }


       }
       catch (Exception ex) {
           logger.error((Marker) ex,newReturnObject.getAppErrorString());
           newReturnObject.PerformReturnObject(new ArrayList<>());
           newReturnObject.setReturncode(0);
           newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MBS002", language));
       }
       return newReturnObject;
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
    }
    public Audit getLastAuditUnapproved(String bankListId, NewReturnObject newReturnObject, String language) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("bank.bankListId").is(bankListId));

            List<Audit> audit = mongoTemplate.findOne(query, BanksUnapprovedDB.class).getAuditInfo();

            return audit.get(audit.size() - 1);
        } catch (Exception ex) {
            logger.error((Marker) ex,newReturnObject.getAppErrorString());
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MBS002", language));
            return null;
        }
    }

<<<<<<< HEAD
}
=======
}
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
