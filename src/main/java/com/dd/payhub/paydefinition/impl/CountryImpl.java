package com.dd.payhub.paydefinition.impl;

import com.dd.payhub.paycommon.model.AuditHistory;
import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.Country;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.dal.CountryDAL;
import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
import com.dd.payhub.paydefinition.entity.*;
import com.dd.payhub.paydefinition.entity.CountryDB;
import com.dd.payhub.paydefinition.entity.CountryUnapprovedDB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Collation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.*;

@Repository
public class CountryImpl implements CountryDAL {
    private static final Logger logger = LogManager.getLogger(CountryImpl.class);

    private final MongoTemplate mongoTemplate;
        private ErrorCodeDAL errorCodeDAL;

    @Autowired
    public CountryImpl(MongoTemplate mongoTemplate,ErrorCodeDAL errorCodeDAL) {
        this.mongoTemplate = mongoTemplate;
        this.errorCodeDAL = errorCodeDAL;

    }

    @Override
    public ReactTableRecord getAllCountryByPagination(ReactTableQuery tableQuery) {
        int page = 0;
        int pageSize = 5;
        int totalPage;
        long totalRecord = 0;
        ReactTableRecord tableRecord = new ReactTableRecord();
        String sortField = tableQuery.getSortField().isEmpty() ? "sortDate" : tableQuery.getSortField();

        Query query = new Query();//if
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.

        try {
            if (tableQuery.getPage() != 0) {
                page = tableQuery.getPage();
            }
            if (tableQuery.getPageSize() != 0) {
                pageSize = tableQuery.getPageSize();
            }
            //sorting order code.
            if (!tableQuery.getSortOrder().equals("true")) {//false means order by asc.
                query.with(Sort.by(Sort.Direction.DESC, sortField));
            } else {
                query.with(Sort.by(Sort.Direction.ASC, sortField));
            }
            Criteria statusCriteria = Criteria.where("country.status").is(tableQuery.getStatus());
            query.addCriteria(Criteria.where("country.status").is(tableQuery.getStatus()));

            if (!tableQuery.getKeyword().isEmpty()) {
                Criteria CountryCodeCriteria = Criteria.where("country.countryCode").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria countryShortNameCriteria = Criteria.where("country.countryShortName").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria countryFullNameCriteria = Criteria.where("country.countryName").regex(String.valueOf(tableQuery.getKeyword()), "i");

                query.addCriteria(new Criteria().orOperator(CountryCodeCriteria, countryShortNameCriteria, countryFullNameCriteria).andOperator(statusCriteria));
            }
            if (tableQuery.getCollection().equals("approved")) {
                totalRecord = mongoTemplate.count(query, CountryDB.class);

                tableRecord.setTableRecord(mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), CountryDB.class));
            } else {
                totalRecord = mongoTemplate.find(query, CountryUnapprovedDB.class).size();

                tableRecord.setTableRecord((mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), CountryUnapprovedDB.class)));
            }
            totalPage = (int) Math.ceil(totalRecord / (float) pageSize);
            tableRecord.setTotalPage(totalPage);
            tableRecord.setTotalRecord(totalRecord);
            return tableRecord;
        } catch (Exception ex) {
            tableRecord.setTotalPage(0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord(new ArrayList<>());
            logger.error(ex.getMessage(),"getAllCountryByPagination");
            return tableRecord;
        }
    }


    @Override
        public NewReturnObject addCountry(Country country, String userId, NewReturnObject newReturnObject, String language) {
        try {

            CountryDB countryDB = new CountryDB();
            countryDB.setCountry(country);
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            countryDB.addAuditInfo(audit);
            countryDB.getCountry().setVersion(0);//setting the version.
            AuditHistory auditHistory = new AuditHistory(audit, "tm_country_unapproved");

            countryDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
            //            inserting in the database.
            auditHistory.addContent(mongoTemplate.insert(new CountryUnapprovedDB(countryDB)));
            auditHistory.addLastStatus(null);

            mongoTemplate.insert(auditHistory);
            newReturnObject.PerformReturnObject("Success");
            return newReturnObject;
        } catch (Exception ex) {
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));//error code
            newReturnObject.setReturncode(0);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            return newReturnObject;
        }
    }

    @Override
    public NewReturnObject getEditableCountry(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language) {
        try {
            //gives approved record only when there are no unapproved records.
            if (reactTableQuery.getCollection().equals("approved")) {

                if (countByUnapprovedCountryCode(reactTableQuery.getPrimaryKey()) == 0) {
                    newReturnObject.PerformReturnObject(findByCountryCode(reactTableQuery.getPrimaryKey()));
                } else {
                    newReturnObject.PerformReturnObject(new ArrayList<>());
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT007", language));
                }

            } else if (reactTableQuery.getCollection().equals("unapproved")) {
                newReturnObject.PerformReturnObject(findUnapprovedByCountryCode(reactTableQuery.getPrimaryKey()));
            }
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject updateCountry(Country country, String userId, NewReturnObject newReturnObject, String language) {
        try {
            CountryDB countryDB = new CountryDB();
            countryDB.setCountry(country);

            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
//
            CountryUnapprovedDB countryUnapprovedDB = findUnapprovedByCountryCode(countryDB.getCountry().getCountryCode());
            CountryDB countryDBResult = findByCountryCode(countryDB.getCountry().getCountryCode());
            if (countryDBResult != null || countryUnapprovedDB != null) {//record must exist to update

                AuditHistory auditHistory = new AuditHistory(audit, "tm_country_unapproved");

                if (countryUnapprovedDB == null) {
                    boolean approveStatus = countryDBResult != null ? countryDBResult.getCountry().getStatus().equals(countryDB.getCountry().getStatus()) : false;
                    if (approveStatus) {//status is not changed
                        countryDB.setAuditInfo(countryDBResult.getAuditInfo());
                        countryDB.addAuditInfo(audit);
                        countryDB.getCountry().setVersion(countryDBResult.getCountry().getVersion() + 1);//version gets increased during update
                        countryDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.

                        auditHistory.addLastStatus(countryDBResult);//last version record.
                        auditHistory.addContent(mongoTemplate.save(new CountryUnapprovedDB(countryDB)));
                        mongoTemplate.insert(auditHistory);
                        newReturnObject.PerformReturnObject("Success");
                        newReturnObject.setReturncode(1);
                    } else {
                        //cannot change the status during edit.
                        newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT009", language));
                    }
                } else {
                    boolean approveStatus = countryUnapprovedDB != null ? countryUnapprovedDB.getCountry().getStatus().equals(countryDB.getCountry().getStatus()) : false;
                    if (approveStatus) {//status is not changed
                        countryDB.setAuditInfo(countryUnapprovedDB.getAuditInfo());
                        countryDB.addAuditInfo(audit);
                        countryDB.set_id(countryUnapprovedDB.get_id());
                        countryDB.getCountry().setVersion(countryUnapprovedDB.getCountry().getVersion() + 1);//version gets increased during update
                        countryDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
                        countryDB.set_id(countryUnapprovedDB.get_id());//setting the mongo ID.

                        auditHistory.addLastStatus(countryUnapprovedDB);//old version record.
                        auditHistory.addContent(mongoTemplate.save(new CountryUnapprovedDB(countryDB)));
                        mongoTemplate.insert(auditHistory);
                        newReturnObject.PerformReturnObject("Success");
                        newReturnObject.setReturncode(1);
                    } else {
                        //cannot change the status during edit.
                        newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT009", language));
                    }

                }

            } else {
                //error record does not exist to update.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT006", language));
            }
        } catch (Exception ex) {

            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject approveCountry(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            String countryCode=tableQuery.getPrimaryKey();
            CountryUnapprovedDB unApprovedResult = findUnapprovedByCountryCode(countryCode);
            CountryDB countryDB = findByCountryCode(countryCode);


            Audit audit = audit = getLastAuditUnapproved(countryCode, newReturnObject, language);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            audit.setApproverId(userId);
            if (!audit.getCreatorId().equals(userId)) {//creator and approver cannot be same.

                unApprovedResult.addAuditInfo(audit); //setting the audit.
                if (unApprovedResult != null) {//record exist to approve.
                    AuditHistory auditHistory = new AuditHistory(audit, "tm_country");

                    if (countryDB == null) {
                        // data is not present in main collection, create new record
                        auditHistory.addLastStatus(null);
                        auditHistory.addContent(mongoTemplate.insert(new CountryDB(unApprovedResult)));
                    } else {
                        String mongoId = countryDB.get_id();
                        auditHistory.addLastStatus(countryDB);//old version
                        countryDB = new CountryDB(unApprovedResult);
                        countryDB.set_id(mongoId);

                        auditHistory.addContent(mongoTemplate.save(countryDB));
                    }
                    newReturnObject.PerformReturnObject(null);


                    mongoTemplate.insert(auditHistory);
                    newReturnObject = rejectByCountryCode(tableQuery, userId, newReturnObject, language);
                } else {//no record to approve.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT004", language));
                }
            } else {//approverID and creatorID are same.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT008", language));
            }

        } catch (Exception ex) {

            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject rejectByCountryCode(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            String countryCode=tableQuery.getPrimaryKey();
            CountryUnapprovedDB countryUnapprovedDB = findUnapprovedByCountryCode(countryCode);
            Audit audit = getLastAuditUnapproved(countryCode, newReturnObject, language);
            audit.setApproverId(userId);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            AuditHistory auditHistory = new AuditHistory(audit, "tm_country_unapproved");
            auditHistory.addContent(countryUnapprovedDB);

            Query query = new Query();
            query.addCriteria(Criteria.where("country.countryCode").is(countryCode));
            mongoTemplate.remove(query, CountryUnapprovedDB.class);

            mongoTemplate.insert(auditHistory);

            newReturnObject.setReturncode(1);
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject viewByCountryCode(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language) {
        try {
            String countryCode=tableQuery.getPrimaryKey();
            Criteria criteriacountryCode= Criteria.where("country.countryCode").is(countryCode);
            if (tableQuery.getCollection().equals("approved")) {
                CountryDB countryDB = mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriacountryCode)), CountryDB.class);
                newReturnObject.PerformReturnObject(countryDB);
            } else if (tableQuery.getCollection().equals("unapproved")) {
                CountryUnapprovedDB countryUnapprovedDB = mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriacountryCode)), CountryUnapprovedDB.class);
                newReturnObject.PerformReturnObject(countryUnapprovedDB);
            } else {
                newReturnObject.setReturncode(0);
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT010", language));
            }

        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());

        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject countryStatusChanger(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language, String setStatus)
    {
        try {
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            String countryCode=tableQuery.getPrimaryKey();
            CountryDB countryDBResult = findByCountryCode(countryCode);
            CountryUnapprovedDB countryUnapprovedDB = findUnapprovedByCountryCode(countryCode);
            String collection=tableQuery.getCollection();



            AuditHistory auditHistory = new AuditHistory(audit, "tm_country_unapproved");
            CountryDB countryDB=null;

            if(collection.equals("approved")){
                countryDB=countryDBResult!=null?countryDBResult:null;
            }
            if(collection.equals("unapproved")){
                countryDB=countryUnapprovedDB!=null?new CountryDB(countryUnapprovedDB):null;
            }
            if(countryDB!=null) { //record must exist to alter status.
                countryDB.getCountry().setVersion(countryDB.getCountry().getVersion() + 1);
                countryDB.addAuditInfo(audit);
                auditHistory.addLastStatus(countryDBResult);//last version record.
                String status = countryDB.getCountry().getStatus();
                countryDB.getCountry().setStatus(setStatus);//changing the status.
                auditHistory.addContent(mongoTemplate.save(new CountryUnapprovedDB(countryDB)));


                mongoTemplate.insert(auditHistory);
                newReturnObject.PerformReturnObject("Success");
                newReturnObject.setReturncode(1);
            }else{
//                  error record does not exist to update.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT006", language));
            }


        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }


    @Override
    public CountryDB findByCountryCode(String name) {
        try {

            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("country.countryCode").is(name));
            return mongoTemplate.findOne(query, CountryDB.class);
  
    } catch (Exception ex) {
        logger.error(ex.getMessage(), "findByCountryCode");
        return null;
    }
    }

    @Override
        public CountryUnapprovedDB findUnapprovedByCountryCode(String countryCode) {
        try{
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("country.countryCode").is(countryCode));
        return mongoTemplate.findOne(query, CountryUnapprovedDB.class);
      
    } catch (Exception ex) {
        logger.error(ex.getMessage(), "CountryUnapprovedDB");
        return null;
    }

    }

    
    @Override
    public CountryDB findByCountryName(String countryName) {
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        query.addCriteria(Criteria.where("country.countryName").is(countryName));
        return mongoTemplate.findOne(query, CountryDB.class);
    }


    @Override
    public long countByCountryCode(String countryCode) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("country.countryCode").is(countryCode));
            return mongoTemplate.count(query, CountryDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByCountryCode");
            return 0;
        }
    }

    @Override
    public long countByUnapprovedCountryCode(String countryCode) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("country.countryCode").is(countryCode));
            return mongoTemplate.count(query, CountryUnapprovedDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByUnapprovedCountryCode");
            return 0;
        }
    }
<<<<<<< HEAD

    @Override
    public NewReturnObject listOfAllCountriesCodeAndNamesByRegx(String keyword, NewReturnObject newReturnObject, String language) {
        try {
            Query query=new Query();
            query.fields().include("country.countryName").include("country.countryCode");//only two fields are searched.
            query.with(Sort.by(Sort.Direction.ASC,"country.countryCode"));

            Criteria statusCriteria=Criteria.where("country.status").is("active");

            if(keyword.isEmpty()||keyword.equals(" ")){
                newReturnObject.PerformReturnObject(mongoTemplate.find(query.addCriteria(statusCriteria),CountryDB.class));
=======

    @Override
    public NewReturnObject listOfAllCountriesCodeAndNamesByRegx(String keyword, NewReturnObject newReturnObject, String language) {
        try {Query query=new Query();
            query.fields().include("country.countryName").include("country.countryCode");//only two fields are searched.
            query.with(Sort.by(Sort.Direction.ASC,"country.countryCode"));

            Criteria statusCriteria=Criteria.where("country.status").is("active");

            if(keyword.isEmpty()||keyword.equals(" ")){
                newReturnObject.PerformReturnObject(mongoTemplate.find(query.addCriteria(statusCriteria),CountryDB.class));
            }
            else{
                Criteria countryCodeCriteria=Criteria.where("country.countryCode").regex(keyword, "i");
                Criteria countryNameCriteria = Criteria.where("country.countryName").regex(keyword, "i");

                query.addCriteria(new Criteria().orOperator(countryNameCriteria,countryCodeCriteria).andOperator(statusCriteria));
                newReturnObject.PerformReturnObject(mongoTemplate.find(query, CountryDB.class));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            }
            else{
                Criteria countryCodeCriteria=Criteria.where("country.countryCode").regex(keyword, "i");
                Criteria countryNameCriteria = Criteria.where("country.countryName").regex(keyword, "i");

<<<<<<< HEAD
                query.addCriteria(new Criteria().orOperator(countryNameCriteria,countryCodeCriteria).andOperator(statusCriteria));
                newReturnObject.PerformReturnObject(mongoTemplate.find(query, CountryDB.class));
            }
=======

>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        }
        catch (Exception ex) {
            logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
            newReturnObject.PerformReturnObject(new ArrayList<>());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));
        }
        return newReturnObject;
    }

    public Audit getLastAuditUnapproved(String countryCode, NewReturnObject newReturnObject, String language) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("country.countryCode").is(countryCode));

            List<Audit> audit = mongoTemplate.findOne(query, CountryUnapprovedDB.class).getAuditInfo();

            return audit.get(audit.size() - 1);
        } catch (Exception ex) {
            logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));
            return null;
        }
    }

}


