package com.dd.payhub.paydefinition.impl;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.AuditHistory;
import com.dd.payhub.paycommon.model.FeeDefinition;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
import com.dd.payhub.paydefinition.dal.FeeDefinitionDAL;
import com.dd.payhub.paydefinition.entity.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Collation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Repository
public class FeeDefinitionImpl implements FeeDefinitionDAL {

    private static final Logger logger = LogManager.getLogger(FeeDefinitionImpl.class);
    private final MongoTemplate mongoTemplate;
    private ErrorCodeDAL errorCodeDAL;

    @Autowired
    public FeeDefinitionImpl(MongoTemplate mongoTemplate, ErrorCodeDAL errorCodeDAL) {
        this.mongoTemplate = mongoTemplate;
        this.errorCodeDAL = errorCodeDAL;
    }

    @Override
    public ReactTableRecord getAllFeeDefinitionByPagination(ReactTableQuery tableQuery) {
        int page = 0;
        int pageSize = 5;
        int totalPage;
        long totalRecord = 0;
        ReactTableRecord tableRecord = new ReactTableRecord();
        String sortField = tableQuery.getSortField().isEmpty() ? "sortDate" : tableQuery.getSortField();

        Query query = new Query();//if
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.

        try {
            if (tableQuery.getPage() != 0) {
                page = tableQuery.getPage();
            }
            if (tableQuery.getPageSize() != 0) {
                pageSize = tableQuery.getPageSize();
            }
            //sorting order code.
            if (!tableQuery.getSortOrder().equals("true")) {//false means order by asc.
                query.with(Sort.by(Sort.Direction.DESC, sortField));
            } else {
                query.with(Sort.by(Sort.Direction.ASC, sortField));
            }
            Criteria statusCriteria = Criteria.where("feeDefinition.status").is(tableQuery.getStatus());
            query.addCriteria(Criteria.where("feeDefinition.status").is(tableQuery.getStatus()));

            if (!tableQuery.getKeyword().isEmpty()) {
                Criteria feeIdCriteria = Criteria.where("feeDefinition.feeId").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria feeDefinitiontenantIdCriteria = Criteria.where("feeDefinition.tenantId").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria feeDefinitionfeeTypeCriteria = Criteria.where("feeDefinition.feeType").regex(String.valueOf(tableQuery.getKeyword()), "i");

                query.addCriteria(new Criteria().orOperator(feeIdCriteria, feeDefinitiontenantIdCriteria, feeDefinitionfeeTypeCriteria).andOperator(statusCriteria));
            }
            if (tableQuery.getCollection().equals("approved")) {
                totalRecord = mongoTemplate.count(query, FeeDefinitionDB.class);

                tableRecord.setTableRecord(mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), FeeDefinitionDB.class));
            } else {
                totalRecord = mongoTemplate.find(query, FeeDefinitionUnapprovedDB.class).size();

                tableRecord.setTableRecord((mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), FeeDefinitionUnapprovedDB.class)));
            }
            totalPage = (int) Math.ceil(totalRecord / (float) pageSize);
            tableRecord.setTotalPage(totalPage);
            tableRecord.setTotalRecord(totalRecord);
            return tableRecord;
        } catch (Exception ex) {
            tableRecord.setTotalPage(0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord(new ArrayList<>());
            logger.error(ex.getMessage(),"getAllEventDefinitionByPagination");
            return tableRecord;
        }
    }

    @Override
    public NewReturnObject addFeeDefinition(FeeDefinition feeDefinition, String userId, NewReturnObject newReturnObject, String language,boolean approvalRequired) {
        try {
            FeeDefinitionDB feeDefinitionDB = new FeeDefinitionDB();
            feeDefinitionDB.setFeeDefinition(feeDefinition);
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            feeDefinitionDB.addAuditInfo(audit);
            feeDefinitionDB.getFeeDefinition().setVersion(0);//setting the version.
            AuditHistory auditHistory;

            feeDefinitionDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
            //            inserting in the database.
            if (approvalRequired) {
                //4eyeRequired = true  add to unapproved collection.
                auditHistory = new AuditHistory(audit, "tm_fee_definition_unapproved");
                auditHistory.addContent(mongoTemplate.insert(new FeeDefinitionUnapprovedDB(feeDefinitionDB)));
            } else {
                // 4eyeRequired = false adding to the main collection.
                auditHistory = new AuditHistory(audit, "tm_fee_definition");
                auditHistory.addContent(mongoTemplate.insert(feeDefinitionDB));
            }
            auditHistory.addLastStatus(null);
            mongoTemplate.insert(auditHistory);
            newReturnObject.PerformReturnObject("Success");
            return newReturnObject;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFD002", language));
            newReturnObject.setReturncode(0);
            return newReturnObject;
        }
    }

    @Override
    public NewReturnObject getEditableFeeDefinition(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language) {
        try {
            //gives approved record only when there are no unapproved records.
            if (reactTableQuery.getCollection().equals("approved")) {
                if (countByUnapprovedFeeIdTenantId(reactTableQuery.getQueryFields().get("feeId").toString(),reactTableQuery.getQueryFields().get("tenantId").toString()) == 0) {
                    newReturnObject.PerformReturnObject(findByFeeIdTenantId(reactTableQuery.getQueryFields().get("feeId").toString(),reactTableQuery.getQueryFields().get("tenantId").toString()));
                } else {
                    newReturnObject.PerformReturnObject(new ArrayList<>());
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFD007", language));
                }
            } else if (reactTableQuery.getCollection().equals("unapproved")) {
                newReturnObject.PerformReturnObject(findUnapprovedByFeeIdTenantId(reactTableQuery.getQueryFields().get("feeId").toString(),reactTableQuery.getQueryFields().get("tenantId").toString()));
            }
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFD002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject updateFeeDefinition(FeeDefinition feeDefinition, String userId, NewReturnObject newReturnObject, String language,boolean approvalRequired) {
        try {
            FeeDefinitionDB feeDefinitionDB = new FeeDefinitionDB();
            feeDefinitionDB.setFeeDefinition(feeDefinition);
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            FeeDefinitionDB feeDefinitionDBResult =findByFeeIdTenantId(feeDefinition.getFeeId(),feeDefinition.getTenantId());
            FeeDefinitionUnapprovedDB feeDefinitionUnapprovedDB = findUnapprovedByFeeIdTenantId(feeDefinition.getFeeId(),feeDefinition.getTenantId());
            if (feeDefinitionDBResult != null || feeDefinitionUnapprovedDB != null) {//record must exist to update.

                if (approvalRequired) {//4eyeRequired = true  add to unapproved collection.

                    AuditHistory auditHistory = new AuditHistory(audit, "tm_fee_definition_unapproved");

                    if (feeDefinitionUnapprovedDB == null) {//fresh record in unapprove collection.
                        boolean approveStatus = feeDefinitionDBResult.getFeeDefinition().getStatus().equals(feeDefinitionDB.getFeeDefinition().getStatus());
                        if (approveStatus) { //status has not changed .
                            feeDefinitionDB.setAuditInfo(feeDefinitionDBResult.getAuditInfo());//adding the auditInfo
                            feeDefinitionDB.addAuditInfo(audit);
                            feeDefinitionDB.getFeeDefinition().setVersion(feeDefinitionDBResult.getFeeDefinition().getVersion() + 1);
                            auditHistory.addLastStatus(feeDefinitionDBResult);//last version record.
                        } else {
                            //cannot change the status during edit.
                            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFD009", language));
                        }
                    } else {//updating the previous record in unapprove collection.
                        boolean unapproveStatus = feeDefinitionUnapprovedDB.getFeeDefinition().getStatus().equals(feeDefinitionDB.getFeeDefinition().getStatus());
                        if (unapproveStatus) { //status has not changed .
                            feeDefinitionDB.setAuditInfo(feeDefinitionUnapprovedDB.getAuditInfo());
                            feeDefinitionDB.addAuditInfo(audit);
                            feeDefinitionDB.getFeeDefinition().setVersion(feeDefinitionUnapprovedDB.getFeeDefinition().getVersion() + 1);
                            feeDefinitionDB.set_id(feeDefinitionUnapprovedDB.get_id());//setting the mongo ID.
                            auditHistory.addLastStatus(feeDefinitionUnapprovedDB);//old version record.
                        } else {
                            //cannot change the status during edit.
                            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFD009", language));
                        }
                    }
                    auditHistory.addContent(mongoTemplate.save(new FeeDefinitionUnapprovedDB(feeDefinitionDB)));
                    mongoTemplate.insert(auditHistory);
                } else { // 4eyeRequired = false adding to the main collection.

                    if (feeDefinitionDBResult != null) {//record must exist to update.
                        boolean approveStatus = feeDefinitionDBResult.getFeeDefinition().getStatus().equals(feeDefinitionDB.getFeeDefinition().getStatus());
                        if (approveStatus) { //status has not changed .
                            AuditHistory auditHistory = new AuditHistory(audit, "tm_fee_definition");

                            feeDefinitionDB.setAuditInfo(feeDefinitionDBResult.getAuditInfo());
                            feeDefinitionDB.addAuditInfo(audit);
                            feeDefinitionDB.getFeeDefinition().setVersion(feeDefinitionDBResult.getFeeDefinition().getVersion() + 1);
                            feeDefinitionDB.set_id(feeDefinitionDBResult.get_id());//setting the mongo ID.

                            auditHistory.addLastStatus(feeDefinitionDBResult);//last version record.
                            auditHistory.addContent(mongoTemplate.save(feeDefinitionDB));
                            mongoTemplate.save(auditHistory);
                        }else {
                            //cannot change the status during edit.
                            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFD009", language));
                        }
                    } else {
                        //error record does not exist to update.
                        newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFD006", language));
                    }
                }
                newReturnObject.PerformReturnObject(null);
                newReturnObject.setReturncode(1);
            }else {
                //error record does not exist to update.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFD006", language));
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFD002", language));
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject approveFeeDefinition(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            String feeId = tableQuery.getQueryFields().get("feeId").toString();
            String tenantId=tableQuery.getQueryFields().get("tenantId").toString();
            FeeDefinitionUnapprovedDB unApprovedResult = findUnapprovedByFeeIdTenantId(feeId,tenantId);
            FeeDefinitionDB feeDefinitionDB = findByFeeIdTenantId(feeId,tenantId);

            Audit audit  = getLastAuditUnapproved(feeId,tenantId, newReturnObject, language);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            audit.setApproverId(userId);
            if (!audit.getCreatorId().equals(userId)) {//creator and approver cannot be same.

                unApprovedResult.addAuditInfo(audit);
                if (unApprovedResult != null) {//record exist to update.
                    AuditHistory auditHistory = new AuditHistory(audit, "tm_fee_definition");

                    if (feeDefinitionDB == null) {
                        //  data is not present, create new record
                        auditHistory.addLastStatus(null);
                        
                        unApprovedResult.addAuditInfo(audit);
                        auditHistory.addContent(mongoTemplate.insert(new FeeDefinitionDB(unApprovedResult)));
                    } else {
                        String mongoId = feeDefinitionDB.get_id();
                        auditHistory.addLastStatus(feeDefinitionDB);//old version
                        feeDefinitionDB = new FeeDefinitionDB(unApprovedResult);
                        feeDefinitionDB.set_id(mongoId);//setting the mongoID
                        feeDefinitionDB.addAuditInfo(audit);//setting the audit.
                        auditHistory.addContent(mongoTemplate.save(feeDefinitionDB));
                    }
                    mongoTemplate.insert(auditHistory);
                    newReturnObject = rejectByFeeIdTenantId(tableQuery, userId, newReturnObject, language);
                } else {//no record to approve.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFD004", language));
                }
            } else {//approverID and creatorID are same.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFD008", language));
            }

        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFD002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject rejectByFeeIdTenantId(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            String feeId = tableQuery.getQueryFields().get("feeId").toString();
            String tenantId=tableQuery.getQueryFields().get("tenantId").toString();
            FeeDefinitionUnapprovedDB countryUnapprovedDB = findUnapprovedByFeeIdTenantId(feeId,tenantId);
            Audit audit = getLastAuditUnapproved(feeId,tenantId, newReturnObject, language);
            audit.setApproverId(userId);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            AuditHistory auditHistory = new AuditHistory(audit, "tm_fee_definition_unapproved");
            auditHistory.addContent(countryUnapprovedDB);

            Query query = new Query();
            Criteria feeIdCriteria = Criteria.where("feeDefinition.feeId").is(feeId);
            Criteria tenantIdCriteria = Criteria.where("feeDefinition.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(feeIdCriteria,tenantIdCriteria));
            mongoTemplate.remove(query, FeeDefinitionUnapprovedDB.class);//deleting the record from the unapproved collection.
            mongoTemplate.insert(auditHistory);//recording to the auditHistory.

            newReturnObject.setReturncode(1);
            newReturnObject.PerformReturnObject("Success");
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFD002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());

        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject feeDefinitionStatusChanger(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language, String setStatus,boolean approvalRequired) {
        try {
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            String feeId = tableQuery.getQueryFields().get("feeId").toString();
            String tenantId=tableQuery.getQueryFields().get("tenantId").toString();
            FeeDefinitionDB feeDefinitionDBResult = findByFeeIdTenantId(feeId,tenantId);
            FeeDefinitionUnapprovedDB feeDefinitionUnapprovedDB = findUnapprovedByFeeIdTenantId(feeId,tenantId);
            if (approvalRequired) {//4EyeRequired=true .

                if (feeDefinitionDBResult != null || feeDefinitionUnapprovedDB != null) {//record must exist to alter status.

                    AuditHistory auditHistory = new AuditHistory(audit, "tm_feeDefinition_unapproved");
                    FeeDefinitionDB eventDefinitionDB = feeDefinitionDBResult != null ? feeDefinitionDBResult : new FeeDefinitionDB(feeDefinitionUnapprovedDB);//take the existing record.
                    eventDefinitionDB.getFeeDefinition().setVersion(eventDefinitionDB.getFeeDefinition().getVersion() + 1);
                    eventDefinitionDB.addAuditInfo(audit);
                    auditHistory.addLastStatus(feeDefinitionDBResult);//last version record.
                    String status = eventDefinitionDB.getFeeDefinition().getStatus();
                    eventDefinitionDB.getFeeDefinition().setStatus(setStatus);
                    auditHistory.addContent(mongoTemplate.save(new FeeDefinitionUnapprovedDB(eventDefinitionDB)));
                    mongoTemplate.insert(auditHistory);
                    newReturnObject.PerformReturnObject("Success");
                    newReturnObject.setReturncode(1);
                } else {
                    //error record does not exist to update.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFD006", language));

                }
            } else {
                //approval not required inserting to the main collection
                if (feeDefinitionDBResult != null) {//record must exist to alter status.
                    AuditHistory auditHistory = new AuditHistory(audit, "tm_feeDefinition");
                    FeeDefinitionDB eventDefinitionDB = feeDefinitionDBResult;
                    eventDefinitionDB.getFeeDefinition().setVersion(eventDefinitionDB.getFeeDefinition().getVersion() + 1);
                    eventDefinitionDB.addAuditInfo(audit);
                    auditHistory.addLastStatus(feeDefinitionDBResult);//last version record.
                    eventDefinitionDB.getFeeDefinition().setStatus(setStatus);
                    auditHistory.addContent(mongoTemplate.save(eventDefinitionDB));
                    mongoTemplate.insert(auditHistory);
                    newReturnObject.PerformReturnObject("Success");
                    newReturnObject.setReturncode(1);
                } else {
                    //error record does not exist to update.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFD006", language));
                }
            }
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFD002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject viewByFeeIdTenantId(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language) {
        try{
            String feeId = tableQuery.getQueryFields().get("feeId").toString();
            String tenantId=tableQuery.getQueryFields().get("tenantId").toString();
            Criteria feeIdCriteria = Criteria.where("feeDefinition.feeId").is(feeId);
            Criteria tenantIdCriteria = Criteria.where("feeDefinition.tenantId").is(tenantId);
            if (tableQuery.getCollection().equals("approved")) {
                FeeDefinitionDB feeDefinitionDB = mongoTemplate.findOne(Query.query(new Criteria().andOperator(feeIdCriteria,tenantIdCriteria)), FeeDefinitionDB.class);
                newReturnObject.PerformReturnObject(feeDefinitionDB);
            } else if (tableQuery.getCollection().equals("unapproved")) {
                FeeDefinitionUnapprovedDB feeDefinitionUnapprovedDB = mongoTemplate.findOne(Query.query(new Criteria().andOperator(feeIdCriteria,tenantIdCriteria)), FeeDefinitionUnapprovedDB.class);
                newReturnObject.PerformReturnObject(feeDefinitionUnapprovedDB);
            } else {
                newReturnObject.setReturncode(0);
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFD010", language));
            }

        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFD002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());

        }
        return newReturnObject;
    }

    @Override
    public FeeDefinitionDB findByFeeIdTenantId(String feeId, String tenantId) {

        try{
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            Criteria feeIdCriteria = Criteria.where("feeDefinition.feeId").is(feeId);
            Criteria tenantIdCriteria = Criteria.where("feeDefinition.tenantId").is(tenantId);
            return mongoTemplate.findOne(query.addCriteria(new Criteria().andOperator(feeIdCriteria,tenantIdCriteria)),FeeDefinitionDB.class);
        }catch (Exception ex){
            logger.error(ex.getMessage(),"findByFeeIdTenantId");
            return null;
        }
    }

    @Override
    public FeeDefinitionUnapprovedDB findUnapprovedByFeeIdTenantId(String feeId, String tenantId) {
        try{
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            Criteria feeIdCriteria = Criteria.where("feeDefinition.feeId").is(feeId);
            Criteria tenantIdCriteria = Criteria.where("feeDefinition.tenantId").is(tenantId);
            return mongoTemplate.findOne(query.addCriteria(new Criteria().andOperator(feeIdCriteria,tenantIdCriteria)),FeeDefinitionUnapprovedDB.class);
        }catch (Exception ex){
            logger.error(ex.getMessage(),"findUnapprovedByFeeIdTenantId");
            return null;
        }
    }

    @Override
    public long countByFeeIdTenantId(String feeId, String tenantId) {
       try{
           Query query = new Query();
           query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
           Criteria feeIdCriteria = Criteria.where("feeDefinition.feeId").is(feeId);
           Criteria tenantIdCriteria = Criteria.where("feeDefinition.tenantId").is(tenantId);
           return mongoTemplate.count(query.addCriteria(new Criteria().andOperator(feeIdCriteria,tenantIdCriteria)),FeeDefinitionDB.class);
       }catch (Exception ex){
           logger.error(ex.getMessage(),"countByFeeIdTenantId");
        return 0;
       }


    }

    @Override
    public long countByUnapprovedFeeIdTenantId(String feeId, String tenantId) {
        try{
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            Criteria feeIdCriteria = Criteria.where("feeDefinition.feeId").is(feeId);
            Criteria tenantIdCriteria = Criteria.where("feeDefinition.tenantId").is(tenantId);
            return mongoTemplate.count(query.addCriteria(new Criteria().andOperator(feeIdCriteria,tenantIdCriteria)),FeeDefinitionUnapprovedDB.class);
        }catch (Exception ex){
            logger.error(ex.getMessage(),"countByUnapprovedFeeIdTenantId");
            return 0;
        }
    }

    private Audit getLastAuditUnapproved(String feeId, String tenantId, NewReturnObject newReturnObject, String language) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            Criteria feeIdCriteria = Criteria.where("feeDefinition.feeId").is(feeId);
            Criteria tenantIdCriteria = Criteria.where("feeDefinition.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(feeIdCriteria,tenantIdCriteria));

            List<Audit> audit = mongoTemplate.findOne(query, FeeDefinitionUnapprovedDB.class).getAuditInfo();

            return audit.get(audit.size() - 1);
        }catch (Exception ex) {
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFD002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            return null;
        }
       
    }
   

    
}
