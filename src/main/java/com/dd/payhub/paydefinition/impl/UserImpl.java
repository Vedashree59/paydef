package com.dd.payhub.paydefinition.impl;

import com.dd.payhub.paycommon.model.User;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
import com.dd.payhub.paydefinition.dal.UserDAL;
import com.dd.payhub.paydefinition.controller.UserController;
import com.dd.payhub.paydefinition.entity.TenantDB;
import com.dd.payhub.paydefinition.entity.UserDB;
import io.jsonwebtoken.Jwts;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

@Repository
public class UserImpl implements UserDAL {
    private static final Logger logger = LogManager.getLogger(UserImpl.class);

    private final MongoTemplate mongoTemplate;
    private ErrorCodeDAL errorCodeDAL;

    @Autowired
    public UserImpl(MongoTemplate mongoTemplate, ErrorCodeDAL errorCodeDAL) {
        this.mongoTemplate = mongoTemplate;
        this.errorCodeDAL = errorCodeDAL;
    }

    @Override
    public UserDB findByOfficialEmailId(String EmailId) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("user.officialEmailId").is(EmailId));
            return mongoTemplate.findOne(query, UserDB.class);
        } catch (Exception ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public NewReturnObject verifyUser(User user, NewReturnObject newReturnObject, String language) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("user.officialEmailId").is(user.getOfficialEmailId()).andOperator(Criteria.where("user.tenantId").is(user.getTenantId())));
            long userCount = mongoTemplate.count(query, UserDB.class);
            if (userCount == 0) {
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MUS003", language));
            } else {
                newReturnObject.PerformReturnObject(null);
            }
        } catch (Exception ex) {
            logger.error(ex);
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject checkUserLoginCredentials(User user, HttpServletRequest request, NewReturnObject newReturnObject, String language) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("user.officialEmailId").is(user.getOfficialEmailId()).andOperator(Criteria.where("user.tenantId").is(user.getTenantId()), Criteria.where("user.password").is(user.getPassword())));
            long userCount = mongoTemplate.count(query, UserDB.class);
            if (userCount == 0) {
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MUS012", language));
            } else {
                String jwsString = Jwts.builder().setIssuer(request.getHeader("origin")).setExpiration(new Date(Calendar.getInstance().getTimeInMillis() + (1000 * 60 * 1))).setSubject("authentification").claim("userId", user.getOfficialEmailId()).claim("tenantId", user.getTenantId()).signWith(UserController.key).compact();
                newReturnObject.PerformReturnObject(jwsString);
            }
        } catch (Exception ex) {
            logger.error(ex);
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject listOfUserIdsByTenantId(String tenantId, NewReturnObject newReturnObject, String language) {
        try {
            Criteria statusCriteria = Criteria.where("user.status").is("active");
            Criteria tenantIdCriteria = Criteria.where("user.tenantId").is(tenantId);
            Query query = new Query();
            query.fields().include("user.officialEmailId").include("user").getFieldsObject();
            query.addCriteria(new Criteria().orOperator(tenantIdCriteria).andOperator(statusCriteria));
            newReturnObject.PerformReturnObject(mongoTemplate.find(query, UserDB.class));
        } catch (Exception ex) {
            logger.error((Marker) ex, newReturnObject.getAppErrorString());
            newReturnObject.PerformReturnObject(new ArrayList<>());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MTN002", language));
        }
        return newReturnObject;
    }

    @Override
    public UserDB findTenantIDByOfficialEmailId(String userID) {
        try {

            Query query = new Query();
            query.fields().include("user.tenantId").getFieldsObject();
            query.addCriteria(Criteria.where("user.status").is("active").andOperator(Criteria.where("user.officialEmailId").is(userID)));
            return (mongoTemplate.findOne(query, UserDB.class));
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "findTenantIDByOfficialEmailId");
            return null;
        }
    }
}


