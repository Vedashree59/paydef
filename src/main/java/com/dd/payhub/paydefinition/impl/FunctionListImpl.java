package com.dd.payhub.paydefinition.impl;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.AuditHistory;
import com.dd.payhub.paycommon.model.FunctionDefinition;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
import com.dd.payhub.paydefinition.dal.FunctionListDAL;
import com.dd.payhub.paydefinition.entity.FunctionListDB;
import com.dd.payhub.paydefinition.entity.FunctionListUnapprovedDB;
<<<<<<< HEAD
import com.dd.payhub.paydefinition.entity.TenantDB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Collation;
=======
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Repository
public class FunctionListImpl implements FunctionListDAL {
    private static final Logger logger = LogManager.getLogger(FunctionListImpl.class);
    private final MongoTemplate mongoTemplate;
    private ErrorCodeDAL errorCodeDAL;

    @Autowired
    public FunctionListImpl(MongoTemplate mongoTemplate, ErrorCodeDAL errorCodeDAL) {
        this.mongoTemplate = mongoTemplate;
        this.errorCodeDAL = errorCodeDAL;
    }

    @Override
    public ReactTableRecord getAllFunctionListByPagination(ReactTableQuery tableQuery) {
        int page = 0;
        int pageSize = 5;
        int totalPage;
        long totalRecord = 0;
        ReactTableRecord tableRecord = new ReactTableRecord();
<<<<<<< HEAD
        String sortField=tableQuery.getSortField().isEmpty()?"sortDate":tableQuery.getSortField();

        Query query = new Query();//if
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
=======
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

        try {
            if (tableQuery.getPage() != 0) {
                page = (int) tableQuery.getPage();
            }
            if (tableQuery.getPageSize() != 0) {
                pageSize = (int) tableQuery.getPageSize();
            }
<<<<<<< HEAD
            //sorting order code.
            if(!tableQuery.getSortOrder().equals("true")){//false means order by asc.
                query.with(Sort.by(Sort.Direction.DESC,sortField));
            }
            else {
                query.with(Sort.by(Sort.Direction.ASC,sortField));
            }
=======
            Query query = new Query();//if
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            Criteria statusCriteria = Criteria.where("function.status").is(tableQuery.getStatus());
            query.addCriteria(Criteria.where("function.status").is(tableQuery.getStatus()));

            if (!tableQuery.getKeyword().isEmpty()) {
                Criteria functionNameCriteria = Criteria.where("function.functionName").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria functionIdCriteria = Criteria.where("function.functionId").regex(String.valueOf(tableQuery.getKeyword()), "i");
                query.addCriteria(new Criteria().orOperator(functionIdCriteria, functionNameCriteria).andOperator(statusCriteria));
            }
            if (tableQuery.getCollection().equals("approved")) {
                totalRecord = mongoTemplate.count(query, FunctionListDB.class);
                tableRecord.setTableRecord(mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), FunctionListDB.class));
            } else {
                totalRecord = mongoTemplate.find(query, FunctionListUnapprovedDB.class).size();
                tableRecord.setTableRecord((mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), FunctionListUnapprovedDB.class)));
            }
            totalPage = (int) Math.ceil(totalRecord / (float) pageSize);
            tableRecord.setTotalPage(totalPage);
            tableRecord.setTotalRecord(totalRecord);
            return tableRecord;
        } catch (Exception ex) {
            logger.error(ex);
           logger.error(ex.getMessage());
            tableRecord.setTotalPage(0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord(new ArrayList<>());
            return tableRecord;
        }
    }

    @Override
    public NewReturnObject addFunctionList(FunctionDefinition functionDefinition, String userId, NewReturnObject newReturnObject, String language) {
        try {
            FunctionListDB functionListDb = new FunctionListDB();
            functionListDb.setFunction(functionDefinition);
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            functionListDb.addAuditInfo(audit);
<<<<<<< HEAD
            functionListDb.getFunction().setVersion(0);//setting the version.
            AuditHistory auditHistory = new AuditHistory(audit, "tm_function_list_unapproved");

            functionListDb.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
            // inserting in the database.
=======
            functionListDb.getFunction().setVersion(0);
            AuditHistory auditHistory = new AuditHistory(audit, "tm_function_list_unapproved");

>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            FunctionListUnapprovedDB functionListUnapprovedDB = mongoTemplate.insert(new FunctionListUnapprovedDB(functionListDb));
            auditHistory.addContent(functionListUnapprovedDB);
            auditHistory.addLastStatus(null);
            mongoTemplate.insert(auditHistory);
            newReturnObject.PerformReturnObject(functionListUnapprovedDB);
            return newReturnObject;
        } catch (Exception ex) {
            logger.error(ex);
           logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
<<<<<<< HEAD
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFN002", language));
=======
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            newReturnObject.setReturncode(0);
            return newReturnObject;
        }
    }

    @Override
    public FunctionListDB findByFunctionId(String functionId) {
        Query query = new Query();
<<<<<<< HEAD
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
=======
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        query.addCriteria(Criteria.where("function.functionId").is(functionId));
        return mongoTemplate.findOne(query, FunctionListDB.class);
    }

    @Override
    public FunctionListUnapprovedDB findUnapprovedByFunctionId(String functionId) {
        Query query = new Query();
<<<<<<< HEAD
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        query.addCriteria(Criteria.where("function.functionId").is(functionId));
        return mongoTemplate.findOne(query, FunctionListUnapprovedDB.class);
    }
    public long countUnapprovedByFunctionId(String functionId) {
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        query.addCriteria(Criteria.where("function.functionId").is(functionId));
        return mongoTemplate.count(query, FunctionListUnapprovedDB.class);
    }
    @Override
    public NewReturnObject getEditableFunctionList(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language) {
        try {

            //gives approved record only when there are no unapproved records.
            if(reactTableQuery.getCollection().equals("approved")){

                if( countUnapprovedByFunctionId(reactTableQuery.getPrimaryKey())==0){
                    newReturnObject.PerformReturnObject(findByFunctionId(reactTableQuery.getPrimaryKey()));
                }else{
                    newReturnObject.PerformReturnObject(new ArrayList<>());
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFN007", language));
                }

            }else if(reactTableQuery.getCollection().equals("unapproved")){
                newReturnObject.PerformReturnObject(findUnapprovedByFunctionId(reactTableQuery.getPrimaryKey()));
            }

=======
        query.addCriteria(Criteria.where("function.functionId").is(functionId));
        return mongoTemplate.findOne(query, FunctionListUnapprovedDB.class);
    }

    @Override
    public NewReturnObject getEditableFunctionList(String functionId, NewReturnObject newReturnObject, String language) {
        try {

            FunctionListUnapprovedDB unApprovedResult = findUnapprovedByFunctionId(functionId);
            if (unApprovedResult == null) {
                FunctionListDB approvedResult = findByFunctionId(functionId);
                newReturnObject.PerformReturnObject(approvedResult);
            } else {
                newReturnObject.PerformReturnObject(unApprovedResult);
            }
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
<<<<<<< HEAD
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFN002", language));
=======
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject updateFunctionList(FunctionDefinition functionDefinition, String userId, NewReturnObject newReturnObject, String language) {
        try {
            FunctionListDB functionListDB = new FunctionListDB();
            functionListDB.setFunction(functionDefinition);

            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
//
            FunctionListUnapprovedDB functionListUnapprovedDB = findUnapprovedByFunctionId(functionListDB.getFunction().getFunctionId());
            FunctionListDB functionListDBResult = findByFunctionId(functionListDB.getFunction().getFunctionId());
            if (functionListDBResult != null || functionListUnapprovedDB != null) {//record must exist to update

                AuditHistory auditHistory = new AuditHistory(audit, "tm_function_list_unapproved");

                if (functionListUnapprovedDB == null) {
                    functionListDB.setAuditInfo(functionListDBResult.getAuditInfo());
                    functionListDB.addAuditInfo(audit);
<<<<<<< HEAD
                    functionListDB.getFunction().setVersion(functionListDBResult.getFunction().getVersion() + 1);//version gets increased during update.
                    functionListDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.

                    auditHistory.addLastStatus(functionListDBResult);//last version record.

=======
                    functionListDB.getFunction().setVersion(functionListDBResult.getFunction().getVersion() + 1);

                    auditHistory.addLastStatus(functionListDBResult);//last version record.


>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                } else {
                    functionListDB.setAuditInfo(functionListUnapprovedDB.getAuditInfo());
                    functionListDB.addAuditInfo(audit);
                    functionListDB.set_id(functionListUnapprovedDB.get_id());
<<<<<<< HEAD
                    functionListDB.getFunction().setVersion(functionListUnapprovedDB.getFunction().getVersion() + 1);//version gets increased during update.
                    functionListDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
=======
                    functionListDB.getFunction().setVersion(functionListUnapprovedDB.getFunction().getVersion() + 1);
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                    functionListDB.set_id(functionListUnapprovedDB.get_id());//setting the mongo ID.

                    auditHistory.addLastStatus(functionListUnapprovedDB);//old version record.


                }
                auditHistory.addContent(mongoTemplate.save(new FunctionListUnapprovedDB(functionListDB)));
                mongoTemplate.insert(auditHistory);

            }
            newReturnObject.PerformReturnObject(null);
            newReturnObject.setReturncode(1);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
<<<<<<< HEAD
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFN002", language));
=======
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject approveFunctionList(String functionId, String userId, NewReturnObject newReturnObject, String language) {
        try {

            FunctionListUnapprovedDB unApprovedResult = findUnapprovedByFunctionId(functionId);
            FunctionListDB functionListDB = findByFunctionId(functionId);


            Audit audit = audit = getLastAuditUnapproved(functionId, newReturnObject, language);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            audit.setApproverId(userId);
<<<<<<< HEAD
            if(!audit.getCreatorId().equals(userId)){//creator and approver cannot be same.

                unApprovedResult.addAuditInfo(audit);//setting the audit.
=======

            unApprovedResult.addAuditInfo(audit);
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            if(unApprovedResult!=null){//record exist to update.
                AuditHistory auditHistory = new AuditHistory(audit,"tm_function_list");

                if (functionListDB == null) {
                    // tm_Function_list data is not present, create new record
                    auditHistory.addLastStatus(null);
                    unApprovedResult.addAuditInfo(audit);
<<<<<<< HEAD
=======
//                    FunctionListDB approveRecord=new FunctionListDB();
//                    approveRecord.setFunction(unApprovedResult.getFunction());
//                    approveRecord.setAuditInfo(unApprovedResult.getAuditInfo());
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                    auditHistory.addContent(mongoTemplate.insert(new FunctionListDB(unApprovedResult)));
                } else {
                    String mongoId = functionListDB.get_id();
                    auditHistory.addLastStatus(functionListDB);//old version
                    functionListDB = new FunctionListDB(unApprovedResult);
                    functionListDB.set_id(mongoId);

<<<<<<< HEAD
=======

>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                    functionListDB.addAuditInfo(audit);//setting the audit.
                    auditHistory.addContent(mongoTemplate.save(functionListDB));
                }
                newReturnObject.PerformReturnObject(null);

<<<<<<< HEAD
                mongoTemplate.insert(auditHistory);
                newReturnObject = deleteByFunctionId(functionId, userId, newReturnObject, language);//delete the un approved after approving.
            }else{//no record to approve.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFN004", language));
            }
            }else{//approverID and creatorID are same.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFN008", language));
=======

                mongoTemplate.insert(auditHistory);
                newReturnObject = deleteByFunctionId(functionId, userId, newReturnObject, language);
            }else{//no record to approve.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT004", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            }

        } catch (Exception ex) {
            logger.error(ex);
           logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
<<<<<<< HEAD
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFN002", language));
=======
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject deleteByFunctionId(String functionId, String userId, NewReturnObject newReturnObject, String language) {
        try {
            FunctionListUnapprovedDB functionListUnapprovedDB = findUnapprovedByFunctionId(functionId);
            Audit audit = getLastAuditUnapproved(functionId, newReturnObject, language);
            audit.setApproverId(userId);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            AuditHistory auditHistory = new AuditHistory(audit, "tm_function_list_unapproved");
            auditHistory.addContent(functionListUnapprovedDB);

            Query query = new Query();
            query.addCriteria(Criteria.where("function.functionId").is(functionId));
            mongoTemplate.remove(query, FunctionListUnapprovedDB.class);

            mongoTemplate.insert(auditHistory);

            newReturnObject.setReturncode(1);
        } catch (Exception ex) {
            logger.error(ex);
          logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
<<<<<<< HEAD
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFN002", language));
=======
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        }
        return newReturnObject;
    }

    private Audit getLastAuditUnapproved(String functionId, NewReturnObject newReturnObject, String language) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("function.functionId").is(functionId));

            List<Audit> audit = mongoTemplate.findOne(query, FunctionListUnapprovedDB.class).getAuditInfo();

            return audit.get(audit.size() - 1);
        } catch (Exception ex) {
            logger.error(ex);
           logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
<<<<<<< HEAD
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFN002", language));
=======
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            return null;
        }

    }

    @Override
    public NewReturnObject viewByFunctionId(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language) {
        try {

            Criteria criteriaFunctionId=Criteria.where("function.functionId").is(tableQuery.getPrimaryKey());
            if(tableQuery.getCollection().equals("approved")){
                FunctionListDB functionListDB =mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriaFunctionId)), FunctionListDB.class);
                newReturnObject.PerformReturnObject(functionListDB);
            }else if(tableQuery.getCollection().equals("unapproved")){
                FunctionListUnapprovedDB functionListUnapprovedDB =mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriaFunctionId)), FunctionListUnapprovedDB.class);
                newReturnObject.PerformReturnObject(functionListUnapprovedDB);
            }
            else {
                newReturnObject.setReturncode(0);
<<<<<<< HEAD
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFN005", language));
=======
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT005", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            }

        }
        catch (Exception ex) {
            logger.error(ex);
           logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
<<<<<<< HEAD
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFN002", language));
        }
        return newReturnObject;
    }

    @Override
    public void getListOfFunction(String keyword, NewReturnObject newReturnObject, String language) {
        try{
            Query query = new Query();
            query.fields().exclude("auditInfo").getFieldsObject();
            if(keyword.isEmpty()||keyword.equals(" ")){
                query.addCriteria(Criteria.where("function.status").is("active"));
                newReturnObject.PerformReturnObject(mongoTemplate.find(query, FunctionListDB.class));
            }
            else{
                Criteria statusCriteria=Criteria.where("function.status").is("active");
                Criteria functionIdCriteria=Criteria.where("function.functionId").regex(keyword, "i");
                query.addCriteria(new Criteria().orOperator(functionIdCriteria).andOperator(statusCriteria));
                newReturnObject.PerformReturnObject(mongoTemplate.find(query, FunctionListDB.class));
            }

        }catch (Exception ex){
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFN002", language));
            logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
        }
    }
=======
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));
        }
        return newReturnObject;
    }
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
}
