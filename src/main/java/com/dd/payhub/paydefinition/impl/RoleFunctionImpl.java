package com.dd.payhub.paydefinition.impl;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.AuditHistory;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.model.RoleFunction;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
import com.dd.payhub.paydefinition.dal.RoleFunctionDAL;
import com.dd.payhub.paydefinition.entity.FunctionListDB;
import com.dd.payhub.paydefinition.entity.FunctionListUnapprovedDB;
import com.dd.payhub.paydefinition.entity.RoleFunctionDB;
import com.dd.payhub.paydefinition.entity.RoleFunctionUnapprovedDB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.ArrayList;

@Repository
public class RoleFunctionImpl implements RoleFunctionDAL {

    private static final Logger logger = LogManager.getLogger(RoleFunctionImpl.class);
    private final MongoTemplate mongoTemplate;
    private ErrorCodeDAL errorCodeDAL;

    @Autowired
    public RoleFunctionImpl(MongoTemplate mongoTemplate, ErrorCodeDAL errorCodeDAL) {
        this.mongoTemplate = mongoTemplate;
        this.errorCodeDAL = errorCodeDAL;
    }


    @Override
    public ReactTableRecord getAllRoleFunctionByPagination(ReactTableQuery tableQuery) {
        int page = 0;
        int pageSize = 5;
        int totalPage;
        long totalRecord = 0;
        ReactTableRecord tableRecord = new ReactTableRecord();

        try {
            if (tableQuery.getPage() != 0) {
                page = (int) tableQuery.getPage();
            }
            if (tableQuery.getPageSize() != 0) {
                pageSize = (int) tableQuery.getPageSize();
            }
            Query query = new Query();//if
            Criteria statusCriteria = Criteria.where("role.status").is(tableQuery.getStatus());
            query.addCriteria(Criteria.where("role.status").is(tableQuery.getStatus()));

            if (!tableQuery.getKeyword().isEmpty()) {
                Criteria functionNameCriteria = Criteria.where("role.roleName").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria functionIdCriteria = Criteria.where("role.roleId").regex(String.valueOf(tableQuery.getKeyword()), "i");
                query.addCriteria(new Criteria().orOperator(functionIdCriteria, functionNameCriteria).andOperator(statusCriteria));
            }
            if (tableQuery.getCollection().equals("approved")) {
                totalRecord = mongoTemplate.count(query, RoleFunctionDB.class);
                tableRecord.setTableRecord(mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), RoleFunctionDB.class));
            } else {
                totalRecord = mongoTemplate.count(query, RoleFunctionUnapprovedDB.class);
                tableRecord.setTableRecord((mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), RoleFunctionUnapprovedDB.class)));
            }
            totalPage = (int) Math.ceil(totalRecord / (float) pageSize);
            tableRecord.setTotalPage(totalPage);
            tableRecord.setTotalRecord(totalRecord);
            return tableRecord;
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage());
            tableRecord.setTotalPage(0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord(new ArrayList<>());
            return tableRecord;
        }
    }

    @Override
    public void addRoleFunction(RoleFunction roleFunction, String userId, NewReturnObject newReturnObject, String language) {
        try {
            RoleFunctionDB roleFunctionDb = new RoleFunctionDB();
            roleFunctionDb.setRole(roleFunction);
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            roleFunctionDb.addAuditInfo(audit);
            roleFunctionDb.getRole().setVersion(0);
            AuditHistory auditHistory = new AuditHistory(audit, "tm_role_function_unapproved");

            RoleFunctionUnapprovedDB unapprovedDB = mongoTemplate.insert(new RoleFunctionUnapprovedDB(roleFunctionDb));
            auditHistory.addContent(unapprovedDB);
            auditHistory.addLastStatus(null);
            mongoTemplate.insert(auditHistory);
            newReturnObject.PerformReturnObject(unapprovedDB);
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MRF002", language));
            newReturnObject.setReturncode(0);
        }

    }

    @Override
    public RoleFunctionDB findByRoleId(String roleId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("role.roleId").is(roleId));
        return mongoTemplate.findOne(query, RoleFunctionDB.class);
    }

    @Override
    public RoleFunctionUnapprovedDB findUnapprovedByRoleId(String roleId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("role.roleId").is(roleId));
        return mongoTemplate.findOne(query, RoleFunctionUnapprovedDB.class);
    }

    @Override
    public long countByRoleId(String roleId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("role.roleId").is(roleId));
        return mongoTemplate.count(query, RoleFunctionDB.class);
    }

    @Override
    public long countUnapprovedByRoleId(String roleId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("role.roleId").is(roleId));
        return mongoTemplate.count(query, RoleFunctionUnapprovedDB.class);
    }

    @Override
    public void getEditableRoleFunction(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language) {
        try {
            //gives approved record only when there are no unapproved records.
            if (reactTableQuery.getCollection().equals("approved")) {
                if (countUnapprovedByRoleId(reactTableQuery.getPrimaryKey()) == 0) {
                    newReturnObject.PerformReturnObject(findByRoleId(reactTableQuery.getPrimaryKey()));
                } else {
                    newReturnObject.PerformReturnObject(new ArrayList<>());
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MRF007", language));
                }
            } else if (reactTableQuery.getCollection().equals("unapproved")) {
                newReturnObject.PerformReturnObject(findUnapprovedByRoleId(reactTableQuery.getPrimaryKey()));
            }
        } catch (Exception ex) {
            logger.error(ex);
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MRF002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
    }

    @Override
    public void updateRoleFunction(RoleFunction roleFunction, String userId, NewReturnObject newReturnObject, String language) {
        try {
            RoleFunctionDB approvedDB = new RoleFunctionDB();
            approvedDB.setRole(roleFunction);

            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
//
            RoleFunctionUnapprovedDB unapprovedDB = findUnapprovedByRoleId(approvedDB.getRole().getRoleId());
            RoleFunctionDB roleFunctionDBResult = findByRoleId(approvedDB.getRole().getRoleId());
            if (roleFunctionDBResult != null || unapprovedDB != null) {//record must exist to update

                AuditHistory auditHistory = new AuditHistory(audit, "tm_function_list_unapproved");
                if (unapprovedDB == null) {
                    approvedDB.setAuditInfo(roleFunctionDBResult.getAuditInfo());
                    approvedDB.addAuditInfo(audit);
                    approvedDB.getRole().setVersion(roleFunctionDBResult.getRole().getVersion() + 1);//version gets increased during update.
                    approvedDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.

                    auditHistory.addLastStatus(roleFunctionDBResult);//last version record.

                } else {
                    approvedDB.setAuditInfo(unapprovedDB.getAuditInfo());
                    approvedDB.addAuditInfo(audit);
                    approvedDB.set_id(unapprovedDB.get_id());
                    approvedDB.getRole().setVersion(unapprovedDB.getRole().getVersion() + 1);//version gets increased during update.
                    approvedDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
                    approvedDB.set_id(unapprovedDB.get_id());//setting the mongo ID.

                    auditHistory.addLastStatus(unapprovedDB);//old version record.
                }
                auditHistory.addContent(mongoTemplate.save(new RoleFunctionUnapprovedDB(approvedDB)));
                mongoTemplate.insert(auditHistory);
            }
            newReturnObject.PerformReturnObject(null);
            newReturnObject.setReturncode(1);

        } catch (Exception ex) {
            logger.error(ex);
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MRF002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }

    }

    @Override
    public void deleteRoleFunction(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language) {

    }

    @Override
    public void approveRoleFunction(String roleId, String userId, NewReturnObject newReturnObject, String language) {

    }

    @Override
    public void deleteByRoleId(String roleId, String userId, NewReturnObject newReturnObject, String language) {

    }

    @Override
    public void viewByRoleId(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language) {
        try {
            Criteria criteriaFunctionId = Criteria.where("role.roleId").is(tableQuery.getPrimaryKey());
            if (tableQuery.getCollection().equals("approved")) {
                newReturnObject.PerformReturnObject(mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriaFunctionId)), RoleFunctionDB.class));
            } else if (tableQuery.getCollection().equals("unapproved")) {
                newReturnObject.PerformReturnObject(mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriaFunctionId)), RoleFunctionUnapprovedDB.class));
            } else {
                newReturnObject.setReturncode(0);
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MFN005", language));
            }
        } catch (Exception ex) {
            logger.error(ex);
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MRF002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
    }

}
