package com.dd.payhub.paydefinition.impl;

import com.dd.payhub.paycommon.model.*;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.dal.CurrencyDAL;
import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
import com.dd.payhub.paydefinition.entity.*;
import com.dd.payhub.paydefinition.entity.CurrencyDB;
import com.dd.payhub.paydefinition.entity.CurrencyUnapprovedDB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Collation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CurrencyImpl implements CurrencyDAL {

    private static final Logger logger = LogManager.getLogger(CurrencyImpl.class);
    private final MongoTemplate mongoTemplate;
    private ErrorCodeDAL errorCodeDAL;

    @Autowired

    public CurrencyImpl(MongoTemplate mongoTemplate, ErrorCodeDAL errorCodeDAL) {
        this.mongoTemplate = mongoTemplate;
        this.errorCodeDAL = errorCodeDAL;
    }

    @Override
    public ReactTableRecord getAllCurrencyByPagination(ReactTableQuery tableQuery) {
        int page = 0;
        int pageSize = 5;
        int totalPage;
        long totalRecord = 0;
        ReactTableRecord tableRecord = new ReactTableRecord();
        String sortField = tableQuery.getSortField().isEmpty() ? "sortDate" : tableQuery.getSortField();

        Query query = new Query();//if
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.

        try {
            if (tableQuery.getPage() != 0) {
                page = tableQuery.getPage();
            }
            if (tableQuery.getPageSize() != 0) {
                pageSize = tableQuery.getPageSize();
            }
            //sorting order code.
            if (!tableQuery.getSortOrder().equals("true")) {//false means order by asc.
                query.with(Sort.by(Sort.Direction.DESC, sortField));
            } else {
                query.with(Sort.by(Sort.Direction.ASC, sortField));
            }
            Criteria statusCriteria = Criteria.where("currency.status").is(tableQuery.getStatus());
            query.addCriteria(Criteria.where("currency.status").is(tableQuery.getStatus()));

            if (!tableQuery.getKeyword().isEmpty()) {
                Criteria CurrencyCodeCriteria = Criteria.where("currency.currencyCode").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria currencyShortNameCriteria = Criteria.where("currency.currencyShortName").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria currencyFullNameCriteria = Criteria.where("currency.currencyName").regex(String.valueOf(tableQuery.getKeyword()), "i");

                query.addCriteria(new Criteria().orOperator(CurrencyCodeCriteria, currencyShortNameCriteria, currencyFullNameCriteria).andOperator(statusCriteria));
            }
            if (tableQuery.getCollection().equals("approved")) {
                totalRecord = mongoTemplate.count(query, CurrencyDB.class);

                tableRecord.setTableRecord(mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), CurrencyDB.class));
            } else {
                totalRecord = mongoTemplate.find(query, CurrencyUnapprovedDB.class).size();

                tableRecord.setTableRecord((mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), CurrencyUnapprovedDB.class)));
            }
            totalPage = (int) Math.ceil(totalRecord / (float) pageSize);
            tableRecord.setTotalPage(totalPage);
            tableRecord.setTotalRecord(totalRecord);
            return tableRecord;
        } catch (Exception ex) {
            tableRecord.setTotalPage(0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord(new ArrayList<>());
            logger.error(ex.getMessage(),"getAllCurrencyByPagination");
            return tableRecord;
        }
    }

    @Override
    public NewReturnObject addCurrency(Currency currency, String userId, NewReturnObject newReturnObject, String language) {
        try {

            CurrencyDB currencyDB = new CurrencyDB();
            currencyDB.setCurrency(currency);
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            currencyDB.addAuditInfo(audit);
            currencyDB.getCurrency().setVersion(0);//setting the version.
            AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_unapproved");

            currencyDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
            //            inserting in the database.
            auditHistory.addContent(mongoTemplate.insert(new CurrencyUnapprovedDB(currencyDB)));
            auditHistory.addLastStatus(null);

            mongoTemplate.insert(auditHistory);
            newReturnObject.PerformReturnObject("Success");
            return newReturnObject;
        } catch (Exception ex) {
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCY002", language));//error code
            newReturnObject.setReturncode(0);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            return newReturnObject;
        }
    }

    @Override
    public NewReturnObject getEditableCurrency(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language) {
        try {
            //gives approved record only when there are no unapproved records.
            if (reactTableQuery.getCollection().equals("approved")) {

                if (countByUnapprovedCurrencyCode(reactTableQuery.getPrimaryKey()) == 0) {
                    newReturnObject.PerformReturnObject(findByCurrencyCode(reactTableQuery.getPrimaryKey()));
                } else {
                    newReturnObject.PerformReturnObject(new ArrayList<>());
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCY007", language));
                }

            } else if (reactTableQuery.getCollection().equals("unapproved")) {
                newReturnObject.PerformReturnObject(findUnapprovedByCurrencyCode(reactTableQuery.getPrimaryKey()));
            }
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCY002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject updateCurrency(Currency currency, String userId, NewReturnObject newReturnObject, String language) {
            try {
                CurrencyDB currencyDB = new CurrencyDB();
                currencyDB.setCurrency(currency);

                Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
    //
                CurrencyUnapprovedDB currencyUnapprovedDB = findUnapprovedByCurrencyCode(currencyDB.getCurrency().getCurrencyCode());
                CurrencyDB currencyDBResult = findByCurrencyCode(currencyDB.getCurrency().getCurrencyCode());
                if (currencyDBResult != null || currencyUnapprovedDB != null) {//record must exist to update

                    AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_unapproved");

                    if (currencyUnapprovedDB == null) {
                        boolean approveStatus = currencyDBResult != null ? currencyDBResult.getCurrency().getStatus().equals(currencyDB.getCurrency().getStatus()) : false;
                        if (approveStatus) {//status is not changed
                            currencyDB.setAuditInfo(currencyDBResult.getAuditInfo());
                            currencyDB.addAuditInfo(audit);
                            currencyDB.getCurrency().setVersion(currencyDBResult.getCurrency().getVersion() + 1);//version gets increased during update
                            currencyDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.

                            auditHistory.addLastStatus(currencyDBResult);//last version record.
                            auditHistory.addContent(mongoTemplate.save(new CurrencyUnapprovedDB(currencyDB)));
                            mongoTemplate.insert(auditHistory);
                            newReturnObject.PerformReturnObject("Success");
                            newReturnObject.setReturncode(1);
                        } else {
                            //cannot change the status during edit.
                            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCY009", language));
                        }
                    } else {
                        boolean approveStatus = currencyUnapprovedDB != null ? currencyUnapprovedDB.getCurrency().getStatus().equals(currencyDB.getCurrency().getStatus()) : false;
                        if (approveStatus) {//status is not changed
                            currencyDB.setAuditInfo(currencyUnapprovedDB.getAuditInfo());
                            currencyDB.addAuditInfo(audit);
                            currencyDB.set_id(currencyUnapprovedDB.get_id());
                            currencyDB.getCurrency().setVersion(currencyUnapprovedDB.getCurrency().getVersion() + 1);//version gets increased during update
                            currencyDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
                            currencyDB.set_id(currencyUnapprovedDB.get_id());//setting the mongo ID.

                            auditHistory.addLastStatus(currencyUnapprovedDB);//old version record.
                            auditHistory.addContent(mongoTemplate.save(new CurrencyUnapprovedDB(currencyDB)));
                            mongoTemplate.insert(auditHistory);
                            newReturnObject.PerformReturnObject("Success");
                            newReturnObject.setReturncode(1);
                        } else {
                            //cannot change the status during edit.
                            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCY009", language));
                        }

                    }

                } else {
                    //error record does not exist to update.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCY006", language));
                }
            } catch (Exception ex) {

                newReturnObject.setReturncode(0);
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCY002", language));
                logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            }
            return newReturnObject;
        }

    @Override
    public NewReturnObject approveCurrency(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            String currencyCode=tableQuery.getPrimaryKey();
            CurrencyUnapprovedDB unApprovedResult = findUnapprovedByCurrencyCode(currencyCode);
            CurrencyDB currencyDB = findByCurrencyCode(currencyCode);


            Audit audit = audit = getLastAuditUnapproved(currencyCode, newReturnObject, language);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            audit.setApproverId(userId);
            if (!audit.getCreatorId().equals(userId)) {//creator and approver cannot be same.

                unApprovedResult.addAuditInfo(audit); //setting the audit.
                if (unApprovedResult != null) {//record exist to approve.
                    AuditHistory auditHistory = new AuditHistory(audit, "tm_currency");

                    if (currencyDB == null) {
                        // data is not present in main collection, create new record
                        auditHistory.addLastStatus(null);
                        auditHistory.addContent(mongoTemplate.insert(new CurrencyDB(unApprovedResult)));
                    } else {
                        String mongoId = currencyDB.get_id();
                        auditHistory.addLastStatus(currencyDB);//old version
                        currencyDB = new CurrencyDB(unApprovedResult);
                        currencyDB.set_id(mongoId);

                        auditHistory.addContent(mongoTemplate.save(currencyDB));
                    }
                    newReturnObject.PerformReturnObject(null);


                    mongoTemplate.insert(auditHistory);
                    newReturnObject = rejectByCurrencyCode(tableQuery, userId, newReturnObject, language);
                } else {//no record to approve.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCY004", language));
                }
            } else {//approverID and creatorID are same.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCY008", language));
            }

        } catch (Exception ex) {

            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCY002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject rejectByCurrencyCode(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            String currencyCode=tableQuery.getPrimaryKey();
            CurrencyUnapprovedDB currencyUnapprovedDB = findUnapprovedByCurrencyCode(currencyCode);
            Audit audit = getLastAuditUnapproved(currencyCode, newReturnObject, language);
            audit.setApproverId(userId);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_unapproved");
            auditHistory.addContent(currencyUnapprovedDB);

            Query query = new Query();
            query.addCriteria(Criteria.where("currency.currencyCode").is(currencyCode));
            mongoTemplate.remove(query, CurrencyUnapprovedDB.class);

            mongoTemplate.insert(auditHistory);

            newReturnObject.setReturncode(1);
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCY002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject viewByCurrencyCode(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language) {
        try {
            String currencyCode=tableQuery.getPrimaryKey();
            Criteria criteriacurrencyCode= Criteria.where("currency.currencyCode").is(currencyCode);
            if (tableQuery.getCollection().equals("approved")) {
                CurrencyDB currencyDB = mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriacurrencyCode)), CurrencyDB.class);
                newReturnObject.PerformReturnObject(currencyDB);
            } else if (tableQuery.getCollection().equals("unapproved")) {
                CurrencyUnapprovedDB currencyUnapprovedDB = mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriacurrencyCode)), CurrencyUnapprovedDB.class);
                newReturnObject.PerformReturnObject(currencyUnapprovedDB);
            } else {
                newReturnObject.setReturncode(0);
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCY010", language));
            }

        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCY002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());

        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject currencyStatusChanger(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language, String setStatus) 
        {
            try {
                Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
                String currencyCode=tableQuery.getPrimaryKey();
                CurrencyDB currencyDBResult = findByCurrencyCode(currencyCode);
                CurrencyUnapprovedDB currencyUnapprovedDB = findUnapprovedByCurrencyCode(currencyCode);
                String collection=tableQuery.getCollection();



                AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_unapproved");
                CurrencyDB currencyDB=null;

                if(collection.equals("approved")){
                    currencyDB=currencyDBResult!=null?currencyDBResult:null;
                }
                if(collection.equals("unapproved")){
                    currencyDB=currencyUnapprovedDB!=null?new CurrencyDB(currencyUnapprovedDB):null;
                }
                if(currencyDB!=null) { //record must exist to alter status.
                    currencyDB.getCurrency().setVersion(currencyDB.getCurrency().getVersion() + 1);
                    currencyDB.addAuditInfo(audit);
                    auditHistory.addLastStatus(currencyDBResult);//last version record.
                    String status = currencyDB.getCurrency().getStatus();
                    currencyDB.getCurrency().setStatus(setStatus);//changing the status.
                    auditHistory.addContent(mongoTemplate.save(new CurrencyUnapprovedDB(currencyDB)));


                    mongoTemplate.insert(auditHistory);
                    newReturnObject.PerformReturnObject("Success");
                    newReturnObject.setReturncode(1);
                }else{
//                  error record does not exist to update.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCY006", language));
                }


            } catch (Exception ex) {
                newReturnObject.setReturncode(0);
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCY002", language));
                logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            }
            return newReturnObject;
        }

    @Override
    public CurrencyDB findByCurrencyCode(String currencyCode) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("currency.currencyCode").is(currencyCode));
            return mongoTemplate.findOne(query, CurrencyDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "findByCurrencyCode");
            return null;
        }
    }

    @Override
    public CurrencyUnapprovedDB findUnapprovedByCurrencyCode(String currencyCode) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("currency.currencyCode").is(currencyCode));
            return mongoTemplate.findOne(query, CurrencyUnapprovedDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "CurrencyUnapprovedDB");
            return null;
        }
    }

    @Override
    public long countByCurrencyCode(String currencyCode) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("currency.currencyCode").is(currencyCode));
            return mongoTemplate.count(query, CurrencyDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByCurrencyCode");
            return 0;
        }
    }

    @Override
    public long countByUnapprovedCurrencyCode(String currencyCode) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("currency.currencyCode").is(currencyCode));
            return mongoTemplate.count(query, CurrencyUnapprovedDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByUnapprovedCurrencyCode");
            return 0;
        }
    }

    public Audit getLastAuditUnapproved(String currencyCode, NewReturnObject newReturnObject, String language) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("currency.currencyCode").is(currencyCode));

            List<Audit> audit = mongoTemplate.findOne(query, CurrencyUnapprovedDB.class).getAuditInfo();

            return audit.get(audit.size() - 1);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCY002", language));
            return null;
        }
    }


    @Override
<<<<<<< HEAD
    public NewReturnObject listOfAllCurrenciesCodeAndNamesByRegx(String keyword, NewReturnObject newReturnObject, String language) {
=======
    public NewReturnObject listOfAllCurrencyCodeAndNamesByRegx(String keyword, NewReturnObject newReturnObject, String language) {
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        try {Query query=new Query();
            query.fields().include("currency.currencyName").include("currency.currencyCode");//only two fields are searched.
            query.with(Sort.by(Sort.Direction.ASC,"currency.currencyCode"));

            Criteria statusCriteria=Criteria.where("currency.status").is("active");

            if(keyword.isEmpty()||keyword.equals(" ")){
                newReturnObject.PerformReturnObject(mongoTemplate.find(query.addCriteria(statusCriteria),CurrencyDB.class));
            }
            else{
                Criteria currencyCodeCriteria=Criteria.where("currency.currencyCode").regex(keyword, "i");
                Criteria currencyNameCriteria = Criteria.where("currency.currencyName").regex(keyword, "i");

                query.addCriteria(new Criteria().orOperator(currencyNameCriteria,currencyCodeCriteria).andOperator(statusCriteria));
                newReturnObject.PerformReturnObject(mongoTemplate.find(query, CurrencyDB.class));
            }


        }
        catch (Exception ex) {
            logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
            newReturnObject.PerformReturnObject(new ArrayList<>());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCY002", language));
        }
        return newReturnObject;
    }
    
}
