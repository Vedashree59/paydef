<<<<<<< HEAD
//package com.dd.payhub.paydefinition.impl;
//
//import com.dd.payhub.paycommon.model.*;
//import com.dd.payhub.paycommon.view.NewReturnObject;
//import com.dd.payhub.paycommon.view.ReactTableRecord;
//import com.dd.payhub.paydefinition.dal.CurrencyPairDAL;
//import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
//import com.dd.payhub.paydefinition.entity.*;
//import com.dd.payhub.paydefinition.entity.CurrencyPairDB;
//import com.dd.payhub.paydefinition.entity.CurrencyPairUnapprovedDB;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Sort;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.core.query.Collation;
//import org.springframework.data.mongodb.core.query.Criteria;
//import org.springframework.data.mongodb.core.query.Query;
//import org.springframework.stereotype.Repository;
//
//import java.time.Instant;
//import java.util.ArrayList;
//import java.util.List;
//
//@Repository
//public class CurrencyPairImpl implements CurrencyPairDAL {
//    private static final Logger logger = LogManager.getLogger(CurrencyHolidaysImpl.class);
//    private final MongoTemplate mongoTemplate;
//    private ErrorCodeDAL errorCodeDAL;
//
//    @Autowired
//
//    public CurrencyPairImpl(MongoTemplate mongoTemplate, ErrorCodeDAL errorCodeDAL) {
//        this.mongoTemplate = mongoTemplate;
//        this.errorCodeDAL = errorCodeDAL;
//    }
//
//    @Override
//    public ReactTableRecord getAllCurrencyPairByPagination(ReactTableQuery tableQuery) {
//        int page = 0;
//        int pageSize = 5;
//        int totalPage;
//        long totalRecord = 0;
//        ReactTableRecord tableRecord = new ReactTableRecord();
//        String sortField = tableQuery.getSortField().isEmpty() ? "sortDate" : tableQuery.getSortField();
//
//        Query query = new Query();//if
//        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
//
//        try {
//            if (tableQuery.getPage() != 0) {
//                page = (int) tableQuery.getPage();
//            }
//            if (tableQuery.getPageSize() != 0) {
//                pageSize = (int) tableQuery.getPageSize();
//            }
//            //sorting order code.
//            if (!tableQuery.getSortOrder().equals("true")) {//false means order by asc.
//                query.with(Sort.by(Sort.Direction.DESC, sortField));
//            } else {
//                query.with(Sort.by(Sort.Direction.ASC, sortField));
//            }
//            Criteria statusCriteria = Criteria.where("currencyPair.status").is(tableQuery.getStatus());
//          //  query.addCriteria(Criteria.where("currencyPair.status").is(tableQuery.getStatus()));
//
//            if (!tableQuery.getKeyword().isEmpty()) {
//                Criteria CCYCode1Criteria = Criteria.where("currencyPair.CCYCode1").regex(String.valueOf(tableQuery.getKeyword()), "i");
//                Criteria CCYCode2Criteria = Criteria.where("currencyPair.CCYCode2").regex(String.valueOf(tableQuery.getKeyword()), "i");
//                Criteria TenantIdCriteria = Criteria.where("currencyPair.tenantId").regex(String.valueOf(tableQuery.getKeyword()), "i");
//                query.addCriteria(new Criteria().orOperator(TenantIdCriteria, CCYCode2Criteria, CCYCode1Criteria).andOperator(statusCriteria));
//            }
//            if (tableQuery.getCollection().equals("approved")) {
//                totalRecord = mongoTemplate.count(query, CurrencyPairDB.class);
//
//                tableRecord.setTableRecord(mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), CurrencyPairDB.class));
//            } else {
//                totalRecord = mongoTemplate.find(query, CurrencyPairUnapprovedDB.class).size();
//
//                tableRecord.setTableRecord((mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), CurrencyPairUnapprovedDB.class)));
//            }
//            totalPage = (int) Math.ceil(totalRecord / (float) pageSize);
//            tableRecord.setTotalPage(totalPage);
//            tableRecord.setTotalRecord(totalRecord);
//            return tableRecord;
//        } catch (Exception ex) {
//            logger.error(ex);
//            logger.error(ex.getMessage());
//            tableRecord.setTotalPage(0);
//            tableRecord.setTotalRecord(0);
//            tableRecord.setTableRecord(new ArrayList<>());
//            return tableRecord;
//        }
//    }
//
//
//    @Override
//    public NewReturnObject addCurrencyPair(CurrencyPair currencyPair, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired) {
//
//
//        try {
//            CurrencyPairDB currencyPairDB = new CurrencyPairDB();
//            currencyPairDB.setCurrencyPair(currencyPair);
//            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
//            currencyPairDB.addAuditInfo(audit);
//            //  currencyPairDB.getCurrencyPair().setVersion(0);
//            AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_pair_unapproved");
//
//            currencyPairDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
//            //            inserting in the database.
//            CurrencyPairUnapprovedDB currencyPairUnapprovedDB = mongoTemplate.insert(new CurrencyPairUnapprovedDB(currencyPairDB));
//            auditHistory.addContent(currencyPairUnapprovedDB);
//            auditHistory.addLastStatus(null);
//
//            mongoTemplate.insert(auditHistory);
//            newReturnObject.PerformReturnObject(currencyPairUnapprovedDB);
//            return newReturnObject;
//        } catch (Exception ex) {
//            logger.error(ex);
//            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
//            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));//error code
//            newReturnObject.setReturncode(0);
//            return newReturnObject;
//        }
//    }
//
//    @Override
//    public CurrencyPairDB findByTenantIdCountryCodeCurrencyCode(String tenantId, String countryCode, String CurrencyCode) {
//        Query query = new Query();
//        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
//        Criteria currencyCodeCriteria = Criteria.where("currencyPair.CCYCode2").is(CurrencyCode);
//        Criteria countryCodeCriteria = Criteria.where("currencyPair.CCYCode1").is(countryCode);
//        Criteria tenantIdCriteria = Criteria.where("currencyPair.tenantId").is(tenantId);
//        query.addCriteria(new Criteria().andOperator(tenantIdCriteria, currencyCodeCriteria, countryCodeCriteria));
//        return mongoTemplate.findOne(query, CurrencyPairDB.class);
//    }
//
//    @Override
//    public CurrencyPairUnapprovedDB findUnapprovedByTenantIdCountryCodeCurrencyCode(String tenantId, String countryCode, String CurrencyCode) {
//        Query query = new Query();
//        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
//        Criteria currencyCodeCriteria = Criteria.where("currencyPair.CCYCode2").is(CurrencyCode);
//        Criteria countryCodeCriteria = Criteria.where("currencyPair.CCYCode1").is(countryCode);
//        Criteria tenantIdCriteria = Criteria.where("currencyPair.tenantId").is(tenantId);
//        query.addCriteria(new Criteria().andOperator(tenantIdCriteria, currencyCodeCriteria, countryCodeCriteria));
//        return mongoTemplate.findOne(query, CurrencyPairUnapprovedDB.class);
//    }
//
//
//    @Override
//    public NewReturnObject getEditableCurrencyPair(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language) {
//        try {
//            String tenantId = reactTableQuery.getQueryFields().get("tenantId").toString();
//            String countryCode = reactTableQuery.getQueryFields().get("CCYCode1").toString();
//            String currencyCode = reactTableQuery.getQueryFields().get("CCYCode2").toString();
//            //gives approved record only when there are no unapproved records.
//
//            if (reactTableQuery.getCollection().equals("approved")) {
//
//                //gives approved record only when there are no unapproved records.
//                CurrencyPairUnapprovedDB unApprovedResult = findUnapprovedByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, currencyCode);
//                if (unApprovedResult == null) {
//                    newReturnObject.PerformReturnObject(findByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, currencyCode));
//                } else {
//                    newReturnObject.PerformReturnObject(new ArrayList<>());
//                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR07", language));
//                }
//
//            } else if (reactTableQuery.getCollection().equals("unapproved")) {
//                newReturnObject.PerformReturnObject(findUnapprovedByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, currencyCode));
//            }
//
//        } catch (Exception ex) {
//            logger.error(ex);
//            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
//            newReturnObject.setReturncode(0);
//            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR02", language));
//        }
//        return newReturnObject;
//    }
//
//
//    @Override
//    public NewReturnObject updateCurrencyPair(CurrencyPair currencyPair, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired) {
//        try {
//            CurrencyPairDB currencyPairDB = new CurrencyPairDB();
//            currencyPairDB.setCurrencyPair(currencyPair);
//
//            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
////
//            CurrencyPairDB currencyPairDBResult = findByTenantIdCountryCodeCurrencyCode(currencyPairDB.getCurrencyPair().getTenantId(), currencyPairDB.getCurrencyPair().getCCYCode1(), currencyPairDB.getCurrencyPair().getCCYCode2());
//            CurrencyPairUnapprovedDB currencyPairUnapprovedDB = findUnapprovedByTenantIdCountryCodeCurrencyCode(currencyPairDB.getCurrencyPair().getTenantId(), currencyPairDB.getCurrencyPair().getCCYCode1(), currencyPairDB.getCurrencyPair().getCCYCode2());
//            if (currencyPairDBResult != null || currencyPairUnapprovedDB != null) {//record must exist to update
//
//                AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_pair_unapproved");
//
//                if (currencyPairUnapprovedDB == null) {
//
//                    currencyPairDB.setAuditInfo(currencyPairDBResult.getAuditInfo());
//                    currencyPairDB.addAuditInfo(audit);
//                    //  currencyPairDB.getCurrencyPair().setVersion(currencyPairDBResult.getCurrencyPair().getVersion()+1);
//                    auditHistory.addLastStatus(currencyPairDBResult);//last version record.
//                } else {
//                    currencyPairDB.setAuditInfo(currencyPairUnapprovedDB.getAuditInfo());
//                    currencyPairDB.addAuditInfo(audit);
//                    currencyPairDB.set_id(currencyPairUnapprovedDB.get_id());
//                    //   currencyPairDB.getCurrencyPair().setVersion(currencyPairUnapprovedDB.getCurrencyPair().getVersion() + 1);
//                    currencyPairDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
//                    currencyPairDB.set_id(currencyPairUnapprovedDB.get_id());//setting the mongo ID.
//
//                    auditHistory.addLastStatus(currencyPairUnapprovedDB);//old version record.
//                }
//                auditHistory.addContent(mongoTemplate.save(new CurrencyPairUnapprovedDB(currencyPairDB)));
//                mongoTemplate.insert(auditHistory);
//
//            }
//            newReturnObject.PerformReturnObject(null);
//            newReturnObject.setReturncode(1);
//        } catch (Exception ex) {
//            logger.error(ex);
//            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
//            newReturnObject.setReturncode(0);
//            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCUR02", language));
//        }
//        return newReturnObject;
//    }
//
//
//
//    @Override
//    public NewReturnObject deleteByTenantIdCountryCodeCurrencyCode(ReactTableQuery reactTableQuery, String userid, NewReturnObject newReturnObject, String language) {
//        try {
//            String tenantId = reactTableQuery.getQueryFields().get("tenantId").toString();
//            String countryCode = reactTableQuery.getQueryFields().get("CCYCode1").toString();
//            String currencyCode = reactTableQuery.getQueryFields().get("CCYCode2").toString();
//            CurrencyPairUnapprovedDB currencyPairUnapprovedDB = findUnapprovedByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, currencyCode);
//            Audit audit = getLastAuditUnapproved(tenantId, countryCode, currencyCode, newReturnObject, language);
//            audit.setApproverId(userid);
//            audit.setApprovedDate(Instant.now().toEpochMilli());
//            AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_pair_unapproved");
//            auditHistory.addContent(currencyPairUnapprovedDB);
//
//            Query query = new Query();
//            Criteria currencyCodeCriteria = Criteria.where("currencyPair.CCYCode2").is(currencyCode);
//            Criteria countryCodeCriteria = Criteria.where("currencyPair.CCYCode1").is(countryCode);
//            Criteria tenantIdCriteria = Criteria.where("currencyPair.tenantId").is(tenantId);
//            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, currencyCodeCriteria, countryCodeCriteria));
//            mongoTemplate.remove(query, CurrencyPairUnapprovedDB.class);
//
//            mongoTemplate.insert(auditHistory);
//
//            newReturnObject.setReturncode(1);
//        } catch (Exception ex) {
//            logger.error(ex);
//            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
//            newReturnObject.setReturncode(0);
//            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR02", language));
//        }
//        return newReturnObject;
//    }
//
//
//    @Override
//    public NewReturnObject viewByTenantIdCountryCodeCurrencyCode(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language) {
//        try {
//            String tenantId = tableQuery.getQueryFields().get("tenantId").toString();
//            String countryCode = tableQuery.getQueryFields().get("CCYCode2").toString();
//            String CurrencyCode = tableQuery.getQueryFields().get("CCYCode1").toString();
//
//            //          Criteria criteriaStatus = Criteria.where("currencyPair.status").is("active");
//            if (tableQuery.getCollection().equals("approved")) {
//                CurrencyPairDB currencyPairDB = findByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, CurrencyCode);//mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriaTenantId)), CurrencyPairDB.class);
//                newReturnObject.PerformReturnObject(currencyPairDB);
//            } else if (tableQuery.getCollection().equals("unapproved")) {
//                CurrencyPairUnapprovedDB currencyPairUnapprovedDB = findUnapprovedByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, CurrencyCode);//mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriaTenantId)), CurrencyPairUnapprovedDB.class);
//                newReturnObject.PerformReturnObject(currencyPairUnapprovedDB);
//            } else {
//                newReturnObject.setReturncode(0);
//                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR06", language));
//            }
//
//        } catch (Exception ex) {
//            logger.error(ex);
//            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
//            newReturnObject.setReturncode(0);
//            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR02", language));
//        }
//        return newReturnObject;
//    }
//
//    @Override
//    public NewReturnObject approveCurrencyPair(ReactTableQuery reactTableQuery, String userId, NewReturnObject newReturnObject, String language) {
//        try {
//            String tenantId=reactTableQuery.getQueryFields().get("tenantId").toString();
//            String countryCode=reactTableQuery.getQueryFields().get("CCYCode2").toString();
//            String currencyCode=reactTableQuery.getQueryFields().get("CCYCode1").toString();
//            newReturnObject = newReturnObject;
//            CurrencyPairUnapprovedDB unApprovedResult = findUnapprovedByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, currencyCode);
//            CurrencyPairDB currencyPairDB = findByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, currencyCode);
//
//
//            Audit audit = audit = getLastAuditUnapproved(tenantId, countryCode, currencyCode, newReturnObject, language);
//            audit.setApprovedDate(Instant.now().toEpochMilli());
//            audit.setApproverId(userId);
//            if(!audit.getCreatorId().equals(userId)){//creator and approver cannot be same.
//
//                unApprovedResult.addAuditInfo(audit);
//                if (unApprovedResult != null) {//record exist to update.
//                    AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_pair");
//
//                    if (currencyPairDB == null) {
//                        // tm_country_holidays data is not present, create new record
//                        auditHistory.addLastStatus(null);
//                        auditHistory.addContent( mongoTemplate.insert(new CurrencyPairDB(unApprovedResult)));
//                    } else {
//                        String mongoId = currencyPairDB.get_id();
//                        auditHistory.addLastStatus(currencyPairDB);//old version
//                        currencyPairDB = new CurrencyPairDB(unApprovedResult);
//                        currencyPairDB.set_id(mongoId);
//
//                        auditHistory.addContent( mongoTemplate.save(currencyPairDB));
//                    }
//                    newReturnObject.PerformReturnObject(null);
//
//
//                    mongoTemplate.insert(auditHistory);
//                    newReturnObject = deleteByTenantIdCountryCodeCurrencyCode(reactTableQuery, userId, newReturnObject, language);
//                } else {//no record to approve.
//                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR05", language));
//                }}else{//approverID and creatorID are same.
//                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR08", language));
//            }
//
//
//        } catch (Exception ex) {
//            logger.error(ex);
//            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
//            newReturnObject.setReturncode(0);
//            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR02", language));
//        }
//        return newReturnObject;
//    }
//
//
//
//    public Audit getLastAuditUnapproved(String tenantId, String countryCode, String currencyCode, NewReturnObject newReturnObject, String language) {
//        try {
//            Query query = new Query();
//            Criteria currencyCodeCriteria = Criteria.where("currencyPair.CCYCode1").is(currencyCode);
//            Criteria countryCodeCriteria = Criteria.where("currencyPair.CCYCode2").is(countryCode);
//            Criteria tenantIdCriteria = Criteria.where("currencyPair.tenantId").is(tenantId);
//            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, countryCodeCriteria, currencyCodeCriteria));
//
//            List<Audit> audit = mongoTemplate.findOne(query, CurrencyPairUnapprovedDB.class).getAuditInfo();
//
//            return audit.get(audit.size() - 1);
//        } catch (Exception ex) {
//            logger.error(ex);
//            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
//            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR02", language));
//            return null;
//        }
//    }
//
//
//
//}
=======
package com.dd.payhub.paydefinition.impl;

import com.dd.payhub.paycommon.model.*;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.dal.CurrencyPairDAL;
import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
import com.dd.payhub.paydefinition.entity.*;
import com.dd.payhub.paydefinition.entity.CurrencyPairDB;
import com.dd.payhub.paydefinition.entity.CurrencyPairUnapprovedDB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Collation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CurrencyPairImpl implements CurrencyPairDAL {
    private static final Logger logger = LogManager.getLogger(CurrencyHolidaysImpl.class);
    private final MongoTemplate mongoTemplate;
    private ErrorCodeDAL errorCodeDAL;

    @Autowired

    public CurrencyPairImpl(MongoTemplate mongoTemplate, ErrorCodeDAL errorCodeDAL) {
        this.mongoTemplate = mongoTemplate;
        this.errorCodeDAL = errorCodeDAL;
    }

    @Override
    public ReactTableRecord getAllCurrencyPairByPagination(ReactTableQuery tableQuery) {
        int page = 0;
        int pageSize = 5;
        int totalPage;
        long totalRecord = 0;
        ReactTableRecord tableRecord = new ReactTableRecord();
        String sortField = tableQuery.getSortField().isEmpty() ? "sortDate" : tableQuery.getSortField();

        Query query = new Query();//if
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.

        try {
            if (tableQuery.getPage() != 0) {
                page = (int) tableQuery.getPage();
            }
            if (tableQuery.getPageSize() != 0) {
                pageSize = (int) tableQuery.getPageSize();
            }
            //sorting order code.
            if (!tableQuery.getSortOrder().equals("true")) {//false means order by asc.
                query.with(Sort.by(Sort.Direction.DESC, sortField));
            } else {
                query.with(Sort.by(Sort.Direction.ASC, sortField));
            }
            Criteria statusCriteria = Criteria.where("currencyPair.status").is(tableQuery.getStatus());
          //  query.addCriteria(Criteria.where("currencyPair.status").is(tableQuery.getStatus()));

            if (!tableQuery.getKeyword().isEmpty()) {
                Criteria CCYCode1Criteria = Criteria.where("currencyPair.CCYCode1").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria CCYCode2Criteria = Criteria.where("currencyPair.CCYCode2").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria TenantIdCriteria = Criteria.where("currencyPair.tenantId").regex(String.valueOf(tableQuery.getKeyword()), "i");
                query.addCriteria(new Criteria().orOperator(TenantIdCriteria, CCYCode2Criteria, CCYCode1Criteria).andOperator(statusCriteria));
            }
            if (tableQuery.getCollection().equals("approved")) {
                totalRecord = mongoTemplate.count(query, CurrencyPairDB.class);

                tableRecord.setTableRecord(mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), CurrencyPairDB.class));
            } else {
                totalRecord = mongoTemplate.find(query, CurrencyPairUnapprovedDB.class).size();

                tableRecord.setTableRecord((mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), CurrencyPairUnapprovedDB.class)));
            }
            totalPage = (int) Math.ceil(totalRecord / (float) pageSize);
            tableRecord.setTotalPage(totalPage);
            tableRecord.setTotalRecord(totalRecord);
            return tableRecord;
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage());
            tableRecord.setTotalPage(0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord(new ArrayList<>());
            return tableRecord;
        }
    }


    @Override
    public NewReturnObject addCurrencyPair(CurrencyPair currencyPair, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired) {


        try {
            CurrencyPairDB currencyPairDB = new CurrencyPairDB();
            currencyPairDB.setCurrencyPair(currencyPair);
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            currencyPairDB.addAuditInfo(audit);
            //  currencyPairDB.getCurrencyPair().setVersion(0);
            AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_pair_unapproved");

            currencyPairDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
            //            inserting in the database.
            CurrencyPairUnapprovedDB currencyPairUnapprovedDB = mongoTemplate.insert(new CurrencyPairUnapprovedDB(currencyPairDB));
            auditHistory.addContent(currencyPairUnapprovedDB);
            auditHistory.addLastStatus(null);

            mongoTemplate.insert(auditHistory);
            newReturnObject.PerformReturnObject(currencyPairUnapprovedDB);
            return newReturnObject;
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));//error code
            newReturnObject.setReturncode(0);
            return newReturnObject;
        }
    }

    @Override
    public CurrencyPairDB findByTenantIdCountryCodeCurrencyCode(String tenantId, String countryCode, String CurrencyCode) {
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        Criteria currencyCodeCriteria = Criteria.where("currencyPair.CCYCode2").is(CurrencyCode);
        Criteria countryCodeCriteria = Criteria.where("currencyPair.CCYCode1").is(countryCode);
        Criteria tenantIdCriteria = Criteria.where("currencyPair.tenantId").is(tenantId);
        query.addCriteria(new Criteria().andOperator(tenantIdCriteria, currencyCodeCriteria, countryCodeCriteria));
        return mongoTemplate.findOne(query, CurrencyPairDB.class);
    }

    @Override
    public CurrencyPairUnapprovedDB findUnapprovedByTenantIdCountryCodeCurrencyCode(String tenantId, String countryCode, String CurrencyCode) {
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        Criteria currencyCodeCriteria = Criteria.where("currencyPair.CCYCode2").is(CurrencyCode);
        Criteria countryCodeCriteria = Criteria.where("currencyPair.CCYCode1").is(countryCode);
        Criteria tenantIdCriteria = Criteria.where("currencyPair.tenantId").is(tenantId);
        query.addCriteria(new Criteria().andOperator(tenantIdCriteria, currencyCodeCriteria, countryCodeCriteria));
        return mongoTemplate.findOne(query, CurrencyPairUnapprovedDB.class);
    }


    @Override
    public NewReturnObject getEditableCurrencyPair(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language) {
        try {
            String tenantId = reactTableQuery.getQueryFields().get("tenantId").toString();
            String countryCode = reactTableQuery.getQueryFields().get("CCYCode1").toString();
            String currencyCode = reactTableQuery.getQueryFields().get("CCYCode2").toString();
            //gives approved record only when there are no unapproved records.

            if (reactTableQuery.getCollection().equals("approved")) {

                //gives approved record only when there are no unapproved records.
                CurrencyPairUnapprovedDB unApprovedResult = findUnapprovedByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, currencyCode);
                if (unApprovedResult == null) {
                    newReturnObject.PerformReturnObject(findByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, currencyCode));
                } else {
                    newReturnObject.PerformReturnObject(new ArrayList<>());
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR07", language));
                }

            } else if (reactTableQuery.getCollection().equals("unapproved")) {
                newReturnObject.PerformReturnObject(findUnapprovedByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, currencyCode));
            }

        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR02", language));
        }
        return newReturnObject;
    }


    @Override
    public NewReturnObject updateCurrencyPair(CurrencyPair currencyPair, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired) {
        try {
            CurrencyPairDB currencyPairDB = new CurrencyPairDB();
            currencyPairDB.setCurrencyPair(currencyPair);

            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
//
            CurrencyPairDB currencyPairDBResult = findByTenantIdCountryCodeCurrencyCode(currencyPairDB.getCurrencyPair().getTenantId(), currencyPairDB.getCurrencyPair().getCCYCode1(), currencyPairDB.getCurrencyPair().getCCYCode2());
            CurrencyPairUnapprovedDB currencyPairUnapprovedDB = findUnapprovedByTenantIdCountryCodeCurrencyCode(currencyPairDB.getCurrencyPair().getTenantId(), currencyPairDB.getCurrencyPair().getCCYCode1(), currencyPairDB.getCurrencyPair().getCCYCode2());
            if (currencyPairDBResult != null || currencyPairUnapprovedDB != null) {//record must exist to update

                AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_pair_unapproved");

                if (currencyPairUnapprovedDB == null) {

                    currencyPairDB.setAuditInfo(currencyPairDBResult.getAuditInfo());
                    currencyPairDB.addAuditInfo(audit);
                    //  currencyPairDB.getCurrencyPair().setVersion(currencyPairDBResult.getCurrencyPair().getVersion()+1);
                    auditHistory.addLastStatus(currencyPairDBResult);//last version record.
                } else {
                    currencyPairDB.setAuditInfo(currencyPairUnapprovedDB.getAuditInfo());
                    currencyPairDB.addAuditInfo(audit);
                    currencyPairDB.set_id(currencyPairUnapprovedDB.get_id());
                    //   currencyPairDB.getCurrencyPair().setVersion(currencyPairUnapprovedDB.getCurrencyPair().getVersion() + 1);
                    currencyPairDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
                    currencyPairDB.set_id(currencyPairUnapprovedDB.get_id());//setting the mongo ID.

                    auditHistory.addLastStatus(currencyPairUnapprovedDB);//old version record.
                }
                auditHistory.addContent(mongoTemplate.save(new CurrencyPairUnapprovedDB(currencyPairDB)));
                mongoTemplate.insert(auditHistory);

            }
            newReturnObject.PerformReturnObject(null);
            newReturnObject.setReturncode(1);
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCUR02", language));
        }
        return newReturnObject;
    }



    @Override
    public NewReturnObject deleteByTenantIdCountryCodeCurrencyCode(ReactTableQuery reactTableQuery, String userid, NewReturnObject newReturnObject, String language) {
        try {
            String tenantId = reactTableQuery.getQueryFields().get("tenantId").toString();
            String countryCode = reactTableQuery.getQueryFields().get("CCYCode1").toString();
            String currencyCode = reactTableQuery.getQueryFields().get("CCYCode2").toString();
            CurrencyPairUnapprovedDB currencyPairUnapprovedDB = findUnapprovedByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, currencyCode);
            Audit audit = getLastAuditUnapproved(tenantId, countryCode, currencyCode, newReturnObject, language);
            audit.setApproverId(userid);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_pair_unapproved");
            auditHistory.addContent(currencyPairUnapprovedDB);

            Query query = new Query();
            Criteria currencyCodeCriteria = Criteria.where("currencyPair.CCYCode2").is(currencyCode);
            Criteria countryCodeCriteria = Criteria.where("currencyPair.CCYCode1").is(countryCode);
            Criteria tenantIdCriteria = Criteria.where("currencyPair.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, currencyCodeCriteria, countryCodeCriteria));
            mongoTemplate.remove(query, CurrencyPairUnapprovedDB.class);

            mongoTemplate.insert(auditHistory);

            newReturnObject.setReturncode(1);
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR02", language));
        }
        return newReturnObject;
    }


    @Override
    public NewReturnObject viewByTenantIdCountryCodeCurrencyCode(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language) {
        try {
            String tenantId = tableQuery.getQueryFields().get("tenantId").toString();
            String countryCode = tableQuery.getQueryFields().get("CCYCode2").toString();
            String CurrencyCode = tableQuery.getQueryFields().get("CCYCode1").toString();

            //          Criteria criteriaStatus = Criteria.where("currencyPair.status").is("active");
            if (tableQuery.getCollection().equals("approved")) {
                CurrencyPairDB currencyPairDB = findByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, CurrencyCode);//mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriaTenantId)), CurrencyPairDB.class);
                newReturnObject.PerformReturnObject(currencyPairDB);
            } else if (tableQuery.getCollection().equals("unapproved")) {
                CurrencyPairUnapprovedDB currencyPairUnapprovedDB = findUnapprovedByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, CurrencyCode);//mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriaTenantId)), CurrencyPairUnapprovedDB.class);
                newReturnObject.PerformReturnObject(currencyPairUnapprovedDB);
            } else {
                newReturnObject.setReturncode(0);
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR06", language));
            }

        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR02", language));
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject approveCurrencyPair(ReactTableQuery reactTableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            String tenantId=reactTableQuery.getQueryFields().get("tenantId").toString();
            String countryCode=reactTableQuery.getQueryFields().get("CCYCode2").toString();
            String currencyCode=reactTableQuery.getQueryFields().get("CCYCode1").toString();
            newReturnObject = newReturnObject;
            CurrencyPairUnapprovedDB unApprovedResult = findUnapprovedByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, currencyCode);
            CurrencyPairDB currencyPairDB = findByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, currencyCode);


            Audit audit = audit = getLastAuditUnapproved(tenantId, countryCode, currencyCode, newReturnObject, language);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            audit.setApproverId(userId);
            if(!audit.getCreatorId().equals(userId)){//creator and approver cannot be same.

                unApprovedResult.addAuditInfo(audit);
                if (unApprovedResult != null) {//record exist to update.
                    AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_pair");

                    if (currencyPairDB == null) {
                        // tm_country_holidays data is not present, create new record
                        auditHistory.addLastStatus(null);
                        auditHistory.addContent( mongoTemplate.insert(new CurrencyPairDB(unApprovedResult)));
                    } else {
                        String mongoId = currencyPairDB.get_id();
                        auditHistory.addLastStatus(currencyPairDB);//old version
                        currencyPairDB = new CurrencyPairDB(unApprovedResult);
                        currencyPairDB.set_id(mongoId);

                        auditHistory.addContent( mongoTemplate.save(currencyPairDB));
                    }
                    newReturnObject.PerformReturnObject(null);


                    mongoTemplate.insert(auditHistory);
                    newReturnObject = deleteByTenantIdCountryCodeCurrencyCode(reactTableQuery, userId, newReturnObject, language);
                } else {//no record to approve.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR05", language));
                }}else{//approverID and creatorID are same.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR08", language));
            }


        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR02", language));
        }
        return newReturnObject;
    }



    public Audit getLastAuditUnapproved(String tenantId, String countryCode, String currencyCode, NewReturnObject newReturnObject, String language) {
        try {
            Query query = new Query();
            Criteria currencyCodeCriteria = Criteria.where("currencyPair.CCYCode1").is(currencyCode);
            Criteria countryCodeCriteria = Criteria.where("currencyPair.CCYCode2").is(countryCode);
            Criteria tenantIdCriteria = Criteria.where("currencyPair.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, countryCodeCriteria, currencyCodeCriteria));

            List<Audit> audit = mongoTemplate.findOne(query, CurrencyPairUnapprovedDB.class).getAuditInfo();

            return audit.get(audit.size() - 1);
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR02", language));
            return null;
        }
    }



}
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
