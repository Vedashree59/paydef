package com.dd.payhub.paydefinition.impl;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.AuditHistory;
import com.dd.payhub.paycommon.model.CountryHolidays;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.dal.CountryHolidaysDAL;
import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
import com.dd.payhub.paydefinition.entity.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Collation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CountryHolidaysImpl implements CountryHolidaysDAL {

    private static final Logger logger = LogManager.getLogger(CountryHolidaysImpl.class);
    private final MongoTemplate mongoTemplate;
    private ErrorCodeDAL errorCodeDAL;

    @Autowired
    public CountryHolidaysImpl(MongoTemplate mongoTemplate, ErrorCodeDAL errorCodeDAL) {
        this.mongoTemplate = mongoTemplate;
        this.errorCodeDAL = errorCodeDAL;
    }

    @Override
    public ReactTableRecord getAllCountryHolidaysByPagination(ReactTableQuery tableQuery) {
        int page = 0;
        int pageSize = 5;
        int totalPage;
        long totalRecord = 0;
        ReactTableRecord tableRecord = new ReactTableRecord();
        String sortField=tableQuery.getSortField().isEmpty()?"sortDate":tableQuery.getSortField();

        Query query = new Query();//if
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.

        try {
            if (tableQuery.getPage() != 0) {
                page = tableQuery.getPage();
            }
            if (tableQuery.getPageSize() != 0) {
                pageSize = tableQuery.getPageSize();
            }
            //sorting order code.
            if(!tableQuery.getSortOrder().equals("true")){//false means order by asc.
                query.with(Sort.by(Sort.Direction.DESC,sortField));
            }
            else {
                query.with(Sort.by(Sort.Direction.ASC,sortField));
            }
            Criteria statusCriteria = Criteria.where("countryHolidays.status").is(tableQuery.getStatus());
            query.addCriteria(Criteria.where("countryHolidays.status").is(tableQuery.getStatus()));

            if (!tableQuery.getKeyword().isEmpty()) {
                Criteria yearCriteria = Criteria.where("countryHolidays.year").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria countryCodeCriteria = Criteria.where("countryHolidays.countryCode").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria TenantIdCriteria = Criteria.where("countryHolidays.tenantId").regex(String.valueOf(tableQuery.getKeyword()), "i");
                query.addCriteria(new Criteria().orOperator(TenantIdCriteria, countryCodeCriteria, yearCriteria).andOperator(statusCriteria));
            }
            if (tableQuery.getCollection().equals("approved")) {
                totalRecord = mongoTemplate.count(query, CountryHolidaysDB.class);

                tableRecord.setTableRecord(mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), CountryHolidaysDB.class));
            } else {
                totalRecord = mongoTemplate.find(query, CountryHolidaysUnapprovedDB.class).size();

                tableRecord.setTableRecord((mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), CountryHolidaysUnapprovedDB.class)));
            }
            totalPage = (int) Math.ceil(totalRecord / (float) pageSize);
            tableRecord.setTotalPage(totalPage);
            tableRecord.setTotalRecord(totalRecord);
            return tableRecord;
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage());
            tableRecord.setTotalPage(0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord(new ArrayList<>());
            return tableRecord;
        }
    }

    @Override
    public NewReturnObject addCountryHolidays(CountryHolidays countryHolidays, String userId, NewReturnObject newReturnObject, String language,Boolean approvalRequired) {

        try {
            CountryHolidaysDB countryHolidaysDB = new CountryHolidaysDB();
            countryHolidaysDB.setCountryHolidays(countryHolidays);
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            countryHolidaysDB.addAuditInfo(audit);
            countryHolidaysDB.getCountryHolidays().setVersion(0);//setting the version.
            AuditHistory auditHistory;

            countryHolidaysDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
            //            inserting in the database.
            if (approvalRequired) {
                //4eyeRequired = true  add to unapproved collection.
                auditHistory = new AuditHistory(audit, "tm_country_holidays_unapproved");
                auditHistory.addContent(mongoTemplate.insert(new CountryHolidaysUnapprovedDB(countryHolidaysDB)));
            } else {
                // 4eyeRequired = false adding to the main collection.
                auditHistory = new AuditHistory(audit, "tm_country_holidays");
                auditHistory.addContent(mongoTemplate.insert(countryHolidaysDB));
            }
            auditHistory.addLastStatus(null);
            mongoTemplate.insert(auditHistory);
            newReturnObject.PerformReturnObject("Success");
            return newReturnObject;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
<<<<<<< HEAD
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCUH02", language));
=======
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU002", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            newReturnObject.setReturncode(0);
            return newReturnObject;
        }
    }

    @Override
    public NewReturnObject getEditableCountryHolidays(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language) {
        try {
            String tenantId=reactTableQuery.getQueryFields().get("tenantId").toString();
            String countryCode=reactTableQuery.getQueryFields().get("countryCode").toString();
            String year=reactTableQuery.getQueryFields().get("year").toString();
            //gives approved record only when there are no unapproved records.

            //gives approved record only when there are no unapproved records.
            if (reactTableQuery.getCollection().equals("approved")) {
                if ((countByTenantIdCountryCodeYear(tenantId,countryCode,year)) == 0) {
                    newReturnObject.PerformReturnObject(findByTenantIdCountryCodeYear(tenantId,countryCode,year));
                } else {
                    newReturnObject.PerformReturnObject(new ArrayList<>());
<<<<<<< HEAD
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCUH07", language));
=======
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU007", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                }
            } else if (reactTableQuery.getCollection().equals("unapproved")) {
                newReturnObject.PerformReturnObject(findUnapprovedByTenantIdCountryCodeYear(tenantId,countryCode,year));
            }
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
<<<<<<< HEAD
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCUH02", language));
=======
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU002", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject updateCountryHolidays(CountryHolidays countryHolidays, String userId, NewReturnObject newReturnObject, String language,Boolean approvalRequired) {
        try {
            CountryHolidaysDB countryHolidaysDB=new CountryHolidaysDB();
            countryHolidaysDB.setCountryHolidays(countryHolidays);

            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
//
            CountryHolidaysDB countryHolidaysDBResult = findByTenantIdCountryCodeYear(countryHolidaysDB.getCountryHolidays().getTenantId(), countryHolidaysDB.getCountryHolidays().getCountryCode(), countryHolidaysDB.getCountryHolidays().getYear());
            CountryHolidaysUnapprovedDB countryHolidaysUnapprovedDB = findUnapprovedByTenantIdCountryCodeYear(countryHolidaysDB.getCountryHolidays().getTenantId(), countryHolidaysDB.getCountryHolidays().getCountryCode(), countryHolidaysDB.getCountryHolidays().getYear());
            if (countryHolidaysDBResult != null || countryHolidaysUnapprovedDB != null) {//record must exist to update.

                if (approvalRequired) {//4eyeRequired = true  add to unapproved collection.

                    AuditHistory auditHistory = new AuditHistory(audit, "tm_country_holidays_unapproved");

                    if (countryHolidaysUnapprovedDB == null) {//fresh record in unapprove collection.
                        boolean approveStatus = countryHolidaysDBResult.getCountryHolidays().getStatus().equals(countryHolidaysDB.getCountryHolidays().getStatus());
                        if (approveStatus) { //status has not changed .
                            countryHolidaysDB.setAuditInfo(countryHolidaysDBResult.getAuditInfo());//adding the auditInfo
                            countryHolidaysDB.addAuditInfo(audit);
                            countryHolidaysDB.getCountryHolidays().setVersion(countryHolidaysDBResult.getCountryHolidays().getVersion() + 1);
                            auditHistory.addLastStatus(countryHolidaysDBResult);//last version record.
                        } else {
                            //cannot change the status during edit.
<<<<<<< HEAD
                            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCUH09", language));
=======
                            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU009", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                        }
                    } else {//updating the previous record in unapprove collection.
                        boolean unapproveStatus = countryHolidaysUnapprovedDB.getCountryHolidays().getStatus().equals(countryHolidaysDB.getCountryHolidays().getStatus());
                        if (unapproveStatus) { //status has not changed .
                            countryHolidaysDB.setAuditInfo(countryHolidaysUnapprovedDB.getAuditInfo());
                            countryHolidaysDB.addAuditInfo(audit);
                            countryHolidaysDB.getCountryHolidays().setVersion(countryHolidaysUnapprovedDB.getCountryHolidays().getVersion() + 1);
                            countryHolidaysDB.set_id(countryHolidaysUnapprovedDB.get_id());//setting the mongo ID.
                            auditHistory.addLastStatus(countryHolidaysUnapprovedDB);//old version record.
                        } else {
                            //cannot change the status during edit.
<<<<<<< HEAD
                            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCUH09", language));
=======
                            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU009", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                        }
                    }
                    auditHistory.addContent(mongoTemplate.save(new CountryHolidaysUnapprovedDB(countryHolidaysDB)));
                    mongoTemplate.insert(auditHistory);
                } else { // 4eyeRequired = false adding to the main collection.

                    if (countryHolidaysDBResult != null) {//record must exist to update.
                        boolean approveStatus = countryHolidaysDBResult.getCountryHolidays().getStatus().equals(countryHolidaysDB.getCountryHolidays().getStatus());
                        if (approveStatus) { //status has not changed .
                            AuditHistory auditHistory = new AuditHistory(audit, "tm_country_holidays");

                            countryHolidaysDB.setAuditInfo(countryHolidaysDBResult.getAuditInfo());
                            countryHolidaysDB.addAuditInfo(audit);
                            countryHolidaysDB.getCountryHolidays().setVersion(countryHolidaysDBResult.getCountryHolidays().getVersion() + 1);
                            countryHolidaysDB.set_id(countryHolidaysDBResult.get_id());//setting the mongo ID.

                            auditHistory.addLastStatus(countryHolidaysDBResult);//last version record.
                            auditHistory.addContent(mongoTemplate.save(countryHolidaysDB));
                            mongoTemplate.save(auditHistory);
                        }else {
                            //cannot change the status during edit.
<<<<<<< HEAD
                            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCUH09", language));
                        }
                    } else {
                        //error record does not exist to update.
                        newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCUH06", language));
=======
                            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU009", language));
                        }
                    } else {
                        //error record does not exist to update.
                        newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU006", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                    }
                }
                newReturnObject.PerformReturnObject(null);
                newReturnObject.setReturncode(1);
            }else {
                //error record does not exist to update.
<<<<<<< HEAD
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCUH06", language));
=======
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU006", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
<<<<<<< HEAD
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCUH02", language));
=======
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU002", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject approveCountryHolidays(ReactTableQuery reactTableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            String tenantId=reactTableQuery.getQueryFields().get("tenantId").toString();
            String countryCode=reactTableQuery.getQueryFields().get("countryCode").toString();
            String year=reactTableQuery.getQueryFields().get("year").toString();
            CountryHolidaysUnapprovedDB unApprovedResult = findUnapprovedByTenantIdCountryCodeYear(tenantId, countryCode, year);
            CountryHolidaysDB countryHolidaysDB = findByTenantIdCountryCodeYear(tenantId, countryCode, year);


            Audit audit = audit = getLastAuditUnapproved(tenantId, countryCode, year, newReturnObject, language);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            audit.setApproverId(userId);
            if (!audit.getCreatorId().equals(userId)) {//creator and approver cannot be same.

                unApprovedResult.addAuditInfo(audit);
                if (unApprovedResult != null) {//record exist to update.
                    AuditHistory auditHistory = new AuditHistory(audit, "tm_country_holidays");

                    if (countryHolidaysDB == null) {
                        //  data is not present, create new record
                        auditHistory.addLastStatus(null);
                        unApprovedResult.addAuditInfo(audit);
                        auditHistory.addContent(mongoTemplate.insert(new CountryHolidaysDB(unApprovedResult)));
                    } else {
                        String mongoId = countryHolidaysDB.get_id();
                        auditHistory.addLastStatus(countryHolidaysDB);//old version
                        countryHolidaysDB = new CountryHolidaysDB(unApprovedResult);
                        countryHolidaysDB.set_id(mongoId);//setting the mongoID
                        countryHolidaysDB.addAuditInfo(audit);//setting the audit.
                        auditHistory.addContent(mongoTemplate.save(countryHolidaysDB));
                    }
                    mongoTemplate.insert(auditHistory);
                    newReturnObject = rejectByTenantIdCountryCodeYear(reactTableQuery, userId, newReturnObject, language);
                } else {//no record to approve.
<<<<<<< HEAD
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCUH04", language));
=======
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU004", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                }
            } else {//approverID and creatorID are same.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MTN008", language));
            }

        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
<<<<<<< HEAD
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCUH02", language));
=======
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU002", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject rejectByTenantIdCountryCodeYear(ReactTableQuery reactTableQuery, String userid, NewReturnObject newReturnObject, String language) {
        try {
            String tenantId=reactTableQuery.getQueryFields().get("tenantId").toString();
            String countryCode=reactTableQuery.getQueryFields().get("countryCode").toString();
            String year=reactTableQuery.getQueryFields().get("year").toString();
            CountryHolidaysUnapprovedDB countryHolidaysUnapprovedDB = findUnapprovedByTenantIdCountryCodeYear(tenantId, countryCode, year);
            Audit audit = getLastAuditUnapproved(tenantId, countryCode, year, newReturnObject, language);
            audit.setApproverId(userid);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            AuditHistory auditHistory = new AuditHistory(audit, "tm_country_holidays_unapproved");
            auditHistory.addContent(countryHolidaysUnapprovedDB);

            Query query = new Query();
            Criteria yearCriteria = Criteria.where("countryHolidays.year").is(year);
            Criteria countryHolidaysCodeCriteria = Criteria.where("countryHolidays.countryCode").is(countryCode);
            Criteria tenantIdCriteria = Criteria.where("countryHolidays.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, countryHolidaysCodeCriteria, yearCriteria));
            mongoTemplate.remove(query, CountryHolidaysUnapprovedDB.class);
            mongoTemplate.insert(auditHistory);

            newReturnObject.setReturncode(1);
<<<<<<< HEAD
            newReturnObject.PerformReturnObject("Success"); 
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCUH02", language));
=======
            newReturnObject.PerformReturnObject("Success");
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU002", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject viewByTenantIdCountryCodeYear(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language) {
        try {
            String tenantId=tableQuery.getQueryFields().get("tenantId").toString();
            String countryCode=tableQuery.getQueryFields().get("countryCode").toString();
            String year=tableQuery.getQueryFields().get("year").toString();

            Criteria criteriaTenant = Criteria.where("countryHolidays.tenantId").is(tenantId);
            Criteria criteriaCountryCode = Criteria.where("countryHolidays.countryCode").is(countryCode);
            Criteria criteriaYear = Criteria.where("countryHolidays.year").is(year);
            if (tableQuery.getCollection().equals("approved")) {
                CountryHolidaysDB countryHolidaysDB = mongoTemplate.findOne(Query.query(new Criteria().andOperator(criteriaTenant,criteriaCountryCode,criteriaYear)), CountryHolidaysDB.class);
                newReturnObject.PerformReturnObject(countryHolidaysDB);
            } else if (tableQuery.getCollection().equals("unapproved")) {
<<<<<<< HEAD
                CountryHolidaysUnapprovedDB countryHolidaysUnapprovedDB = mongoTemplate.findOne(Query.query(new Criteria().andOperator(criteriaTenant,criteriaCountryCode,criteriaYear)), CountryHolidaysUnapprovedDB.class);
                newReturnObject.PerformReturnObject(countryHolidaysUnapprovedDB);
            } else {
                newReturnObject.setReturncode(0);
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCUH10", language));
=======
                CountryHolidaysUnapprovedDB countryHolidaysUnapprovedDB = mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriaTenant,criteriaCountryCode,criteriaYear)), CountryHolidaysUnapprovedDB.class);
                newReturnObject.PerformReturnObject(countryHolidaysUnapprovedDB);
            } else {
                newReturnObject.setReturncode(0);
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU010", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            }

        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCUH02", language));
        }
        return newReturnObject;
    }
    @Override
    public NewReturnObject countryHolidaysStatusChanger(ReactTableQuery tableQuery, String headerUserId, NewReturnObject newReturnObject, String language, String setStatus, Boolean approvalRequired) {

        try {
            Audit audit = new Audit(headerUserId, Instant.now().toEpochMilli(), "", 0.0);

            String tenantId=tableQuery.getQueryFields().get("tenantId").toString();
            String countryCode=tableQuery.getQueryFields().get("countryCode").toString();
            String year=tableQuery.getQueryFields().get("year").toString();
            CountryHolidaysDB countryHolidaysDBResult = findByTenantIdCountryCodeYear(tenantId,countryCode,year);
            CountryHolidaysUnapprovedDB countryHolidaysUnapprovedDB = findUnapprovedByTenantIdCountryCodeYear(tenantId,countryCode,year);
            if (approvalRequired) {//4EyeRequired=true .

                if (countryHolidaysDBResult != null || countryHolidaysUnapprovedDB != null) {//record must exist to alter status.

                    AuditHistory auditHistory = new AuditHistory(audit, "tm_country_holidays_unapproved");
                    CountryHolidaysDB eventDefinitionDB = countryHolidaysDBResult != null ? countryHolidaysDBResult : new CountryHolidaysDB(countryHolidaysUnapprovedDB);//take the existing record.
                    eventDefinitionDB.getCountryHolidays().setVersion(eventDefinitionDB.getCountryHolidays().getVersion() + 1);
                    eventDefinitionDB.addAuditInfo(audit);
                    auditHistory.addLastStatus(countryHolidaysDBResult);//last version record.
                    String status = eventDefinitionDB.getCountryHolidays().getStatus();
                    eventDefinitionDB.getCountryHolidays().setStatus(setStatus);
                    auditHistory.addContent(mongoTemplate.save(new CountryHolidaysUnapprovedDB(eventDefinitionDB)));
                    mongoTemplate.insert(auditHistory);
                    newReturnObject.PerformReturnObject("Success");
                    newReturnObject.setReturncode(1);
                } else {
                    //error record does not exist to update.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED006", language));

                }
            } else {
                //approval not required inserting to the main collection
                if (countryHolidaysDBResult != null) {//record must exist to alter status.
                    AuditHistory auditHistory = new AuditHistory(audit, "tm_country_holidays");
                    CountryHolidaysDB countryHolidaysDB = countryHolidaysDBResult;
                    countryHolidaysDB.getCountryHolidays().setVersion(countryHolidaysDB.getCountryHolidays().getVersion() + 1);
                    countryHolidaysDB.addAuditInfo(audit);
                    auditHistory.addLastStatus(countryHolidaysDBResult);//last version record.
                    countryHolidaysDB.getCountryHolidays().setStatus(setStatus);
                    auditHistory.addContent(mongoTemplate.save(countryHolidaysDB));
                    mongoTemplate.insert(auditHistory);
                    newReturnObject.PerformReturnObject("Success");
                    newReturnObject.setReturncode(1);
                } else {
                    //error record does not exist to update.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED006", language));
                }
            }
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public CountryHolidaysDB findByTenantIdCountryCodeYear(String tenantId, String countryCode, String year) {
        try {
<<<<<<< HEAD
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        Criteria yearCriteria = Criteria.where("countryHolidays.year").is(year);
        Criteria countryHolidaysCodeCriteria = Criteria.where("countryHolidays.countryCode").is(countryCode);
        Criteria tenantIdCriteria = Criteria.where("countryHolidays.tenantId").is(tenantId);
        query.addCriteria(new Criteria().andOperator(tenantIdCriteria, countryHolidaysCodeCriteria, yearCriteria));
        return mongoTemplate.findOne(query, CountryHolidaysDB.class);
    } catch (Exception ex) {
        logger.error(ex.getMessage(), "countByCountryHolidaysCode");
        return null;
    }
=======
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            Criteria yearCriteria = Criteria.where("countryHolidays.year").is(year);
            Criteria countryHolidaysCodeCriteria = Criteria.where("countryHolidays.countryCode").is(countryCode);
            Criteria tenantIdCriteria = Criteria.where("countryHolidays.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, countryHolidaysCodeCriteria, yearCriteria));
            return mongoTemplate.findOne(query, CountryHolidaysDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByCountryHolidaysCode");
            return null;
        }
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
    }

    @Override
    public CountryHolidaysUnapprovedDB findUnapprovedByTenantIdCountryCodeYear(String tenantId, String countryCode, String year) {
        try{
<<<<<<< HEAD
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        Criteria yearCriteria = Criteria.where("countryHolidays.year").is(year);
        Criteria countryHolidaysCodeCriteria = Criteria.where("countryHolidays.countryCode").is(countryCode);
        Criteria tenantIdCriteria = Criteria.where("countryHolidays.tenantId").is(tenantId);
        query.addCriteria(new Criteria().andOperator(tenantIdCriteria, countryHolidaysCodeCriteria, yearCriteria));
        return mongoTemplate.findOne(query, CountryHolidaysUnapprovedDB.class);
    } catch (Exception ex) {
        logger.error(ex.getMessage(), "countByCountryHolidaysCode");
        return null;
    }
=======
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            Criteria yearCriteria = Criteria.where("countryHolidays.year").is(year);
            Criteria countryHolidaysCodeCriteria = Criteria.where("countryHolidays.countryCode").is(countryCode);
            Criteria tenantIdCriteria = Criteria.where("countryHolidays.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, countryHolidaysCodeCriteria, yearCriteria));
            return mongoTemplate.findOne(query, CountryHolidaysUnapprovedDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByCountryHolidaysCode");
            return null;
        }
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
    }


    public long countByTenantIdCountryCodeYear(String tenantId, String countryCode, String year) {
        try{
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            Criteria yearCriteria = Criteria.where("countryHolidays.year").is(year);
            Criteria countryHolidaysCodeCriteria = Criteria.where("countryHolidays.countryCode").is(countryCode);
            Criteria tenantIdCriteria = Criteria.where("countryHolidays.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, countryHolidaysCodeCriteria, yearCriteria));
            return mongoTemplate.count(query, CountryHolidaysUnapprovedDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByCountryHolidaysCode");
            return 0;
        }
    }

    @Override
    public long countByUnapprovedTenantIdCountryCodeYear(String tenantId, String countryCode, String year) {
        try{
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            Criteria yearCriteria = Criteria.where("countryHolidays.year").is(year);
            Criteria countryHolidaysCodeCriteria = Criteria.where("countryHolidays.countryCode").is(countryCode);
            Criteria tenantIdCriteria = Criteria.where("countryHolidays.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, countryHolidaysCodeCriteria, yearCriteria));
            return mongoTemplate.count(query, CountryHolidaysUnapprovedDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByCountryHolidaysCode");
            return 0;
        }
    }


    public Audit getLastAuditUnapproved(String tenantId, String countryCode, String year, NewReturnObject newReturnObject, String language) {
        try {
            Query query = new Query();
            Criteria yearCriteria = Criteria.where("countryHolidays.year").is(year);
            Criteria countryHolidaysCodeCriteria = Criteria.where("countryHolidays.countryCode").is(countryCode);
            Criteria tenantIdCriteria = Criteria.where("countryHolidays.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, countryHolidaysCodeCriteria, yearCriteria));

            List<Audit> audit = mongoTemplate.findOne(query, CountryHolidaysUnapprovedDB.class).getAuditInfo();

            return audit.get(audit.size() - 1);
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCUH02", language));
            return null;
        }
    }

}
