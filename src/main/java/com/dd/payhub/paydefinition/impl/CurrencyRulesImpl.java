package com.dd.payhub.paydefinition.impl;

import com.dd.payhub.paycommon.model.*;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.dal.CurrencyRulesDAL;
import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
import com.dd.payhub.paydefinition.entity.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Collation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CurrencyRulesImpl implements CurrencyRulesDAL {

    private static final Logger logger = LogManager.getLogger(CurrencyHolidaysImpl.class);
    private final MongoTemplate mongoTemplate;
    private ErrorCodeDAL errorCodeDAL;

    @Autowired

    public CurrencyRulesImpl(MongoTemplate mongoTemplate, ErrorCodeDAL errorCodeDAL) {
        this.mongoTemplate = mongoTemplate;
        this.errorCodeDAL = errorCodeDAL;
    }

    @Override
    public ReactTableRecord getAllCurrencyRulesByPagination(ReactTableQuery tableQuery) {
        int page = 0;
        int pageSize = 5;
        int totalPage;
        long totalRecord = 0;
        ReactTableRecord tableRecord = new ReactTableRecord();
        String sortField = tableQuery.getSortField().isEmpty() ? "sortDate" : tableQuery.getSortField();

        Query query = new Query();//if
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.

        try {
            if (tableQuery.getPage() != 0) {
                page = (int) tableQuery.getPage();
            }
            if (tableQuery.getPageSize() != 0) {
                pageSize = (int) tableQuery.getPageSize();
            }
            //sorting order code.
            if (!tableQuery.getSortOrder().equals("true")) {//false means order by asc.
                query.with(Sort.by(Sort.Direction.DESC, sortField));
            } else {
                query.with(Sort.by(Sort.Direction.ASC, sortField));
            }
            Criteria statusCriteria = Criteria.where("currencyRules.status").is(tableQuery.getStatus());
            query.addCriteria(Criteria.where("currencyRules.status").is(tableQuery.getStatus()));

            if (!tableQuery.getKeyword().isEmpty()) {
                Criteria currencyCodeCriteria = Criteria.where("currencyRules.currencyCode").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria countryCodeCriteria = Criteria.where("currencyRules.countryCode").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria TenantIdCriteria = Criteria.where("currencyRules.tenantId").regex(String.valueOf(tableQuery.getKeyword()), "i");
                query.addCriteria(new Criteria().orOperator(TenantIdCriteria, countryCodeCriteria, currencyCodeCriteria).andOperator(statusCriteria));
            }
            if (tableQuery.getCollection().equals("approved")) {
                totalRecord = mongoTemplate.count(query, CurrencyRulesDB.class);

                tableRecord.setTableRecord(mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), CurrencyRulesDB.class));
            } else {
                totalRecord = mongoTemplate.find(query, CurrencyRulesUnapprovedDB.class).size();

                tableRecord.setTableRecord((mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), CurrencyRulesUnapprovedDB.class)));
            }
            totalPage = (int) Math.ceil(totalRecord / (float) pageSize);
            tableRecord.setTotalPage(totalPage);
            tableRecord.setTotalRecord(totalRecord);
            return tableRecord;
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage());
            tableRecord.setTotalPage(0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord(new ArrayList<>());
            return tableRecord;
        }
    }


    @Override
    public NewReturnObject addCurrencyRules(CurrencyRules currencyRules, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired) {


        try {
            CurrencyRulesDB currencyRulesDB = new CurrencyRulesDB();
            currencyRulesDB.setCurrencyRules(currencyRules);
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            currencyRulesDB.addAuditInfo(audit);
            //  currencyRulesDB.getCurrencyRules().setVersion(0);
            AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_rules_unapproved");

            currencyRulesDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
            //            inserting in the database.
            CurrencyRulesUnapprovedDB currencyRulesUnapprovedDB = mongoTemplate.insert(new CurrencyRulesUnapprovedDB(currencyRulesDB));
            auditHistory.addContent(currencyRulesUnapprovedDB);
            auditHistory.addLastStatus(null);

            mongoTemplate.insert(auditHistory);
            newReturnObject.PerformReturnObject(currencyRulesUnapprovedDB);
            return newReturnObject;
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));//error code
            newReturnObject.setReturncode(0);
            return newReturnObject;
        }
    }

    @Override
    public CurrencyRulesDB findByTenantIdCountryCodeCurrencyCode(String tenantId, String countryCode, String CurrencyCode) {
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        Criteria currencyCodeCriteria = Criteria.where("currencyRules.currencyCode").is(CurrencyCode);
        Criteria countryCodeCriteria = Criteria.where("currencyRules.countryCode").is(countryCode);
        Criteria tenantIdCriteria = Criteria.where("currencyRules.tenantId").is(tenantId);
        query.addCriteria(new Criteria().andOperator(tenantIdCriteria, currencyCodeCriteria, countryCodeCriteria));
        return mongoTemplate.findOne(query, CurrencyRulesDB.class);
    }

    @Override
    public CurrencyRulesUnapprovedDB findUnapprovedByTenantIdCountryCodeCurrencyCode(String tenantId, String countryCode, String CurrencyCode) {
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        Criteria currencyCodeCriteria = Criteria.where("currencyRules.currencyCode").is(CurrencyCode);
        Criteria countryCodeCriteria = Criteria.where("currencyRules.countryCode").is(countryCode);
        Criteria tenantIdCriteria = Criteria.where("currencyRules.tenantId").is(tenantId);
        query.addCriteria(new Criteria().andOperator(tenantIdCriteria, currencyCodeCriteria, countryCodeCriteria));
        return mongoTemplate.findOne(query, CurrencyRulesUnapprovedDB.class);
    }


    @Override
    public NewReturnObject getEditableCurrencyRules(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language) {
        try {
            String tenantId = reactTableQuery.getQueryFields().get("tenantId").toString();
            String countryCode = reactTableQuery.getQueryFields().get("countryCode").toString();
            String currencyCode = reactTableQuery.getQueryFields().get("currencyCode").toString();
            //gives approved record only when there are no unapproved records.

            if (reactTableQuery.getCollection().equals("approved")) {

                //gives approved record only when there are no unapproved records.
                CurrencyRulesUnapprovedDB unApprovedResult = findUnapprovedByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, currencyCode);
                if (unApprovedResult == null) {
                    newReturnObject.PerformReturnObject(findByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, currencyCode));
                } else {
                    newReturnObject.PerformReturnObject(new ArrayList<>());
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR07", language));
                }

            } else if (reactTableQuery.getCollection().equals("unapproved")) {
                newReturnObject.PerformReturnObject(findUnapprovedByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, currencyCode));
            }

        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR02", language));
        }
        return newReturnObject;
    }


    @Override
    public NewReturnObject updateCurrencyRules(CurrencyRules currencyRules, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired) {
        try {
            CurrencyRulesDB currencyRulesDB = new CurrencyRulesDB();
            currencyRulesDB.setCurrencyRules(currencyRules);

            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
//
            CurrencyRulesDB currencyRulesDBResult = findByTenantIdCountryCodeCurrencyCode(currencyRulesDB.getCurrencyRules().getTenantId(), currencyRulesDB.getCurrencyRules().getCountryCode(), currencyRulesDB.getCurrencyRules().getCurrencyCode());
            CurrencyRulesUnapprovedDB currencyRulesUnapprovedDB = findUnapprovedByTenantIdCountryCodeCurrencyCode(currencyRulesDB.getCurrencyRules().getTenantId(), currencyRulesDB.getCurrencyRules().getCountryCode(), currencyRulesDB.getCurrencyRules().getCurrencyCode());
            if (currencyRulesDBResult != null || currencyRulesUnapprovedDB != null) {//record must exist to update

                AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_rules_unapproved");

                if (currencyRulesUnapprovedDB == null) {

                    currencyRulesDB.setAuditInfo(currencyRulesDBResult.getAuditInfo());
                    currencyRulesDB.addAuditInfo(audit);
                    //  currencyRulesDB.getCurrencyRules().setVersion(currencyRulesDBResult.getCurrencyRules().getVersion()+1);
                    auditHistory.addLastStatus(currencyRulesDBResult);//last version record.
                } else {
                    currencyRulesDB.setAuditInfo(currencyRulesUnapprovedDB.getAuditInfo());
                    currencyRulesDB.addAuditInfo(audit);
                    currencyRulesDB.set_id(currencyRulesUnapprovedDB.get_id());
                    //   currencyRulesDB.getCurrencyRules().setVersion(currencyRulesUnapprovedDB.getCurrencyRules().getVersion() + 1);
                    currencyRulesDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
                    currencyRulesDB.set_id(currencyRulesUnapprovedDB.get_id());//setting the mongo ID.

                    auditHistory.addLastStatus(currencyRulesUnapprovedDB);//old version record.
                }
                auditHistory.addContent(mongoTemplate.save(new CurrencyRulesUnapprovedDB(currencyRulesDB)));
                mongoTemplate.insert(auditHistory);

            }
            newReturnObject.PerformReturnObject(null);
            newReturnObject.setReturncode(1);
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCUR02", language));
        }
        return newReturnObject;
    }


    @Override
    public NewReturnObject deleteByTenantIdCountryCodeCurrencyCode(ReactTableQuery reactTableQuery, String userid, NewReturnObject newReturnObject, String language) {
        try {
            String tenantId = reactTableQuery.getQueryFields().get("tenantId").toString();
            String countryCode = reactTableQuery.getQueryFields().get("countryCode").toString();
            String currencyCode = reactTableQuery.getQueryFields().get("currencyCode").toString();
            CurrencyRulesUnapprovedDB currencyRulesUnapprovedDB = findUnapprovedByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, currencyCode);
            Audit audit = getLastAuditUnapproved(tenantId, countryCode, currencyCode, newReturnObject, language);
            audit.setApproverId(userid);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_rules_unapproved");
            auditHistory.addContent(currencyRulesUnapprovedDB);

            Query query = new Query();
            Criteria currencyCodeCriteria = Criteria.where("currencyRules.currencyCode").is(currencyCode);
            Criteria countryCodeCriteria = Criteria.where("currencyRules.countryCode").is(countryCode);
            Criteria tenantIdCriteria = Criteria.where("currencyRules.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, currencyCodeCriteria, countryCodeCriteria));
            mongoTemplate.remove(query, CurrencyRulesUnapprovedDB.class);

            mongoTemplate.insert(auditHistory);

            newReturnObject.setReturncode(1);
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR02", language));
        }
        return newReturnObject;
    }


    @Override
    public NewReturnObject viewByTenantIdCountryCodeCurrencyCode(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language) {
        try {
            String tenantId = tableQuery.getQueryFields().get("tenantId").toString();
            String countryCode = tableQuery.getQueryFields().get("countryCode").toString();
            String CurrencyCode = tableQuery.getQueryFields().get("currencyCode").toString();

            //          Criteria criteriaStatus = Criteria.where("currencyRules.status").is("active");
            if (tableQuery.getCollection().equals("approved")) {
                CurrencyRulesDB currencyRulesDB = findByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, CurrencyCode);//mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriaTenantId)), CurrencyRulesDB.class);
                newReturnObject.PerformReturnObject(currencyRulesDB);
            } else if (tableQuery.getCollection().equals("unapproved")) {
                CurrencyRulesUnapprovedDB currencyRulesUnapprovedDB = findUnapprovedByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, CurrencyCode);//mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriaTenantId)), CurrencyRulesUnapprovedDB.class);
                newReturnObject.PerformReturnObject(currencyRulesUnapprovedDB);
            } else {
                newReturnObject.setReturncode(0);
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR06", language));
            }

        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR02", language));
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject approveCurrencyRules(ReactTableQuery reactTableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            String tenantId=reactTableQuery.getQueryFields().get("tenantId").toString();
            String countryCode=reactTableQuery.getQueryFields().get("countryCode").toString();
            String currencyCode=reactTableQuery.getQueryFields().get("currencyCode").toString();
            newReturnObject = newReturnObject;
            CurrencyRulesUnapprovedDB unApprovedResult = findUnapprovedByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, currencyCode);
            CurrencyRulesDB currencyRulesDB = findByTenantIdCountryCodeCurrencyCode(tenantId, countryCode, currencyCode);


            Audit audit = audit = getLastAuditUnapproved(tenantId, countryCode, currencyCode, newReturnObject, language);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            audit.setApproverId(userId);
            if(!audit.getCreatorId().equals(userId)){//creator and approver cannot be same.

                unApprovedResult.addAuditInfo(audit);
                if (unApprovedResult != null) {//record exist to update.
                    AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_rules");

                    if (currencyRulesDB == null) {
                        // tm_country_holidays data is not present, create new record
                        auditHistory.addLastStatus(null);
                        auditHistory.addContent( mongoTemplate.insert(new CurrencyRulesDB(unApprovedResult)));
                    } else {
                        String mongoId = currencyRulesDB.get_id();
                        auditHistory.addLastStatus(currencyRulesDB);//old version
                        currencyRulesDB = new CurrencyRulesDB(unApprovedResult);
                        currencyRulesDB.set_id(mongoId);

                        auditHistory.addContent( mongoTemplate.save(currencyRulesDB));
                    }
                    newReturnObject.PerformReturnObject(null);


                    mongoTemplate.insert(auditHistory);
                    newReturnObject = deleteByTenantIdCountryCodeCurrencyCode(reactTableQuery, userId, newReturnObject, language);
                } else {//no record to approve.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR05", language));
                }}else{//approverID and creatorID are same.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR08", language));
            }


        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR02", language));
        }
        return newReturnObject;
    }



    public Audit getLastAuditUnapproved(String tenantId, String countryCode, String currencyCode, NewReturnObject newReturnObject, String language) {
        try {
            Query query = new Query();
            Criteria currencyCodeCriteria = Criteria.where("currencyRules.currencyCode").is(currencyCode);
            Criteria countryCodeCriteria = Criteria.where("currencyRules.countryCode").is(countryCode);
            Criteria tenantIdCriteria = Criteria.where("currencyRules.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, countryCodeCriteria, currencyCodeCriteria));

            List<Audit> audit = mongoTemplate.findOne(query, CurrencyRulesUnapprovedDB.class).getAuditInfo();

            return audit.get(audit.size() - 1);
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYR02", language));
            return null;
        }
    }



    @Override
    public long countByTenantIdCountryCodeCurrencyCode(String tenantId,String countryCode, String currencyCode) {
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        Criteria currencyCodeCriteria = Criteria.where("currencyRules.currencyCode").is(currencyCode);
        Criteria countryCodeCriteria = Criteria.where("currencyRules.currencyCode").is(countryCode);
        Criteria tenantIdCriteria = Criteria.where("currencyRules.tenantId").is(tenantId);
        query.addCriteria(new Criteria().andOperator(tenantIdCriteria, countryCodeCriteria, currencyCodeCriteria));
        return mongoTemplate.count(query, CurrencyRulesDB.class);
    }


    @Override
    public CurrencyRulesDB findApprovalRequiredTenantIdCountryCodeCurrencyCode(String tenantId, String countryCode, String currencyCode) {
        Query query = new Query();
        //   query.fields().include("currencyRules.fourEyeRequired");
        Criteria currencyCodeCriteria = Criteria.where("currencyRules.currencyCode").is(currencyCode);
        Criteria countryCodeCriteria = Criteria.where("currencyRules.countryCode").is(countryCode);
        Criteria tenantIdCriteria = Criteria.where("currencyRules.tenantId").is(tenantId);
        query.addCriteria(new Criteria().andOperator(tenantIdCriteria, countryCodeCriteria, currencyCodeCriteria));
        return mongoTemplate.findOne(query, CurrencyRulesDB.class);
    }


}