package com.dd.payhub.paydefinition.impl;

<<<<<<< HEAD
import com.dd.payhub.paycommon.model.*;
=======
import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.AuditHistory;
import com.dd.payhub.paycommon.model.EventDefinition;
import com.dd.payhub.paycommon.model.ReactTableQuery;
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
import com.dd.payhub.paydefinition.dal.EventDefinitionDAL;
<<<<<<< HEAD
=======
import com.dd.payhub.paydefinition.entity.*;
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
import com.dd.payhub.paydefinition.entity.EventDefinitionDB;
import com.dd.payhub.paydefinition.entity.EventDefinitionUnapprovedDB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Collation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Repository
public class EventDefinitionImpl implements EventDefinitionDAL {

    private static final Logger logger = LogManager.getLogger(EventDefinitionImpl.class);
    private final MongoTemplate mongoTemplate;
    private ErrorCodeDAL errorCodeDAL;

    @Autowired
    public EventDefinitionImpl(MongoTemplate mongoTemplate, ErrorCodeDAL errorCodeDAL) {
        this.mongoTemplate = mongoTemplate;
        this.errorCodeDAL = errorCodeDAL;
    }

    @Override
    public ReactTableRecord getAllEventDefinitionByPagination(ReactTableQuery tableQuery) {
        int page = 0;
        int pageSize = 5;
        int totalPage;
        long totalRecord = 0;
        ReactTableRecord tableRecord = new ReactTableRecord();
        String sortField = tableQuery.getSortField().isEmpty() ? "sortDate" : tableQuery.getSortField();

        Query query = new Query();//if
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.

        try {
            if (tableQuery.getPage() != 0) {
<<<<<<< HEAD
                page = tableQuery.getPage();
=======
                page =  tableQuery.getPage();
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            }
            if (tableQuery.getPageSize() != 0) {
                pageSize = tableQuery.getPageSize();
            }
            //sorting order code.
            if (!tableQuery.getSortOrder().equals("true")) {//false means order by asc.
                query.with(Sort.by(Sort.Direction.DESC, sortField));
            } else {
                query.with(Sort.by(Sort.Direction.ASC, sortField));
            }
            Criteria statusCriteria = Criteria.where("eventDefinition.status").is(tableQuery.getStatus());
            query.addCriteria(Criteria.where("eventDefinition.status").is(tableQuery.getStatus()));

            if (!tableQuery.getKeyword().isEmpty()) {
<<<<<<< HEAD
                Criteria eventIdCriteria = Criteria.where("eventDefinition.eventId").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria eventDefinitionShortNameCriteria = Criteria.where("eventDefinition.shortName").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria eventDefinitionFullNameCriteria = Criteria.where("eventDefinition.fullName").regex(String.valueOf(tableQuery.getKeyword()), "i");

                query.addCriteria(new Criteria().orOperator(eventIdCriteria, eventDefinitionShortNameCriteria, eventDefinitionFullNameCriteria).andOperator(statusCriteria));
=======
                Criteria EventIdCriteria = Criteria.where("eventDefinition.eventId").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria eventDefinitionShortNameCriteria = Criteria.where("eventDefinition.shortName").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria eventDefinitionFullNameCriteria = Criteria.where("eventDefinition.fullName").regex(String.valueOf(tableQuery.getKeyword()), "i");

                query.addCriteria(new Criteria().orOperator(EventIdCriteria, eventDefinitionShortNameCriteria, eventDefinitionFullNameCriteria).andOperator(statusCriteria));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            }
            if (tableQuery.getCollection().equals("approved")) {
                totalRecord = mongoTemplate.count(query, EventDefinitionDB.class);

                tableRecord.setTableRecord(mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), EventDefinitionDB.class));
            } else {
                totalRecord = mongoTemplate.find(query, EventDefinitionUnapprovedDB.class).size();

                tableRecord.setTableRecord((mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), EventDefinitionUnapprovedDB.class)));
            }
            totalPage = (int) Math.ceil(totalRecord / (float) pageSize);
            tableRecord.setTotalPage(totalPage);
            tableRecord.setTotalRecord(totalRecord);
            return tableRecord;
        } catch (Exception ex) {
            tableRecord.setTotalPage(0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord(new ArrayList<>());
            logger.error(ex.getMessage(),"getAllEventDefinitionByPagination");
            return tableRecord;
        }
    }

    @Override
    public NewReturnObject addEventDefinition(EventDefinition eventDefinition, String userId, NewReturnObject newReturnObject, String language) {
        try {

            EventDefinitionDB eventDefinitionDB = new EventDefinitionDB();
            eventDefinitionDB.setEventDefinition(eventDefinition);
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            eventDefinitionDB.addAuditInfo(audit);
            eventDefinitionDB.getEventDefinition().setVersion(0);//setting the version.
            AuditHistory auditHistory = new AuditHistory(audit, "tm_event_definition_unapproved");

            eventDefinitionDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
            //            inserting in the database.
            auditHistory.addContent(mongoTemplate.insert(new EventDefinitionUnapprovedDB(eventDefinitionDB)));
            auditHistory.addLastStatus(null);

            mongoTemplate.insert(auditHistory);
            newReturnObject.PerformReturnObject("Success");
            return newReturnObject;
        } catch (Exception ex) {
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED002", language));//error code
            newReturnObject.setReturncode(0);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            return newReturnObject;
        }
    }

    @Override
    public NewReturnObject getEditableEventDefinition(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language) {
        try {
            //gives approved record only when there are no unapproved records.
            if (reactTableQuery.getCollection().equals("approved")) {
<<<<<<< HEAD
=======

>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                if (countByUnapprovedEventId(reactTableQuery.getPrimaryKey()) == 0) {
                    newReturnObject.PerformReturnObject(findByEventId(reactTableQuery.getPrimaryKey()));
                } else {
                    newReturnObject.PerformReturnObject(new ArrayList<>());
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED007", language));
                }

            } else if (reactTableQuery.getCollection().equals("unapproved")) {
                newReturnObject.PerformReturnObject(findUnapprovedByEventId(reactTableQuery.getPrimaryKey()));
            }
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject updateEventDefinition(EventDefinition eventDefinition, String userId, NewReturnObject newReturnObject, String language) {
        try {
            EventDefinitionDB eventDefinitionDB = new EventDefinitionDB();
            eventDefinitionDB.setEventDefinition(eventDefinition);

            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
//
            EventDefinitionUnapprovedDB eventDefinitionUnapprovedDB = findUnapprovedByEventId(eventDefinitionDB.getEventDefinition().getEventId());
            EventDefinitionDB eventDefinitionDBResult = findByEventId(eventDefinitionDB.getEventDefinition().getEventId());
            if (eventDefinitionDBResult != null || eventDefinitionUnapprovedDB != null) {//record must exist to update

                AuditHistory auditHistory = new AuditHistory(audit, "tm_event_definition_unapproved");

                if (eventDefinitionUnapprovedDB == null) {
                    boolean approveStatus = eventDefinitionDBResult != null ? eventDefinitionDBResult.getEventDefinition().getStatus().equals(eventDefinitionDB.getEventDefinition().getStatus()) : false;
                    if (approveStatus) {//status is not changed
                        eventDefinitionDB.setAuditInfo(eventDefinitionDBResult.getAuditInfo());
                        eventDefinitionDB.addAuditInfo(audit);
                        eventDefinitionDB.getEventDefinition().setVersion(eventDefinitionDBResult.getEventDefinition().getVersion() + 1);//version gets increased during update
                        eventDefinitionDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.

                        auditHistory.addLastStatus(eventDefinitionDBResult);//last version record.
                        auditHistory.addContent(mongoTemplate.save(new EventDefinitionUnapprovedDB(eventDefinitionDB)));
                        mongoTemplate.insert(auditHistory);
                        newReturnObject.PerformReturnObject("Success");
                        newReturnObject.setReturncode(1);
                    } else {
                        //cannot change the status during edit.
                        newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED009", language));
                    }
                } else {
                    boolean approveStatus = eventDefinitionUnapprovedDB != null ? eventDefinitionUnapprovedDB.getEventDefinition().getStatus().equals(eventDefinitionDB.getEventDefinition().getStatus()) : false;
                    if (approveStatus) {//status is not changed
                        eventDefinitionDB.setAuditInfo(eventDefinitionUnapprovedDB.getAuditInfo());
                        eventDefinitionDB.addAuditInfo(audit);
                        eventDefinitionDB.set_id(eventDefinitionUnapprovedDB.get_id());
                        eventDefinitionDB.getEventDefinition().setVersion(eventDefinitionUnapprovedDB.getEventDefinition().getVersion() + 1);//version gets increased during update
                        eventDefinitionDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
                        eventDefinitionDB.set_id(eventDefinitionUnapprovedDB.get_id());//setting the mongo ID.

                        auditHistory.addLastStatus(eventDefinitionUnapprovedDB);//old version record.
                        auditHistory.addContent(mongoTemplate.save(new EventDefinitionUnapprovedDB(eventDefinitionDB)));
                        mongoTemplate.insert(auditHistory);
                        newReturnObject.PerformReturnObject("Success");
                        newReturnObject.setReturncode(1);
                    } else {
                        //cannot change the status during edit.
                        newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED009", language));
                    }

                }

            } else {
                //error record does not exist to update.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED006", language));
            }
        } catch (Exception ex) {

            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject approveEventDefinition(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            String eventID=tableQuery.getPrimaryKey();
            EventDefinitionUnapprovedDB unApprovedResult = findUnapprovedByEventId(eventID);
            EventDefinitionDB eventDefinitionDB = findByEventId(eventID);


            Audit audit = audit = getLastAuditUnapproved(eventID, newReturnObject, language);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            audit.setApproverId(userId);
            if (!audit.getCreatorId().equals(userId)) {//creator and approver cannot be same.

                unApprovedResult.addAuditInfo(audit); //setting the audit.
                if (unApprovedResult != null) {//record exist to approve.
                    AuditHistory auditHistory = new AuditHistory(audit, "tm_event_definition");

                    if (eventDefinitionDB == null) {
                        // data is not present in main collection, create new record
                        auditHistory.addLastStatus(null);
                        auditHistory.addContent(mongoTemplate.insert(new EventDefinitionDB(unApprovedResult)));
                    } else {
                        String mongoId = eventDefinitionDB.get_id();
                        auditHistory.addLastStatus(eventDefinitionDB);//old version
                        eventDefinitionDB = new EventDefinitionDB(unApprovedResult);
                        eventDefinitionDB.set_id(mongoId);

                        auditHistory.addContent(mongoTemplate.save(eventDefinitionDB));
                    }
                    newReturnObject.PerformReturnObject(null);


                    mongoTemplate.insert(auditHistory);
                    newReturnObject = rejectByEventId(tableQuery, userId, newReturnObject, language);
                } else {//no record to approve.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED004", language));
                }
            } else {//approverID and creatorID are same.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED008", language));
            }

        } catch (Exception ex) {

            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject rejectByEventId(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            String eventID=tableQuery.getPrimaryKey();
            EventDefinitionUnapprovedDB eventDefinitionUnapprovedDB = findUnapprovedByEventId(eventID);
            Audit audit = getLastAuditUnapproved(eventID, newReturnObject, language);
            audit.setApproverId(userId);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            AuditHistory auditHistory = new AuditHistory(audit, "tm_event_definition_unapproved");
            auditHistory.addContent(eventDefinitionUnapprovedDB);

            Query query = new Query();
            query.addCriteria(Criteria.where("eventDefinition.eventId").is(eventID));
            mongoTemplate.remove(query, EventDefinitionUnapprovedDB.class);

            mongoTemplate.insert(auditHistory);

            newReturnObject.setReturncode(1);
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject viewByEventId(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language) {
        try {
            String eventID=tableQuery.getPrimaryKey();
            Criteria criteriaEventID= Criteria.where("eventDefinition.eventId").is(eventID);
            if (tableQuery.getCollection().equals("approved")) {
                EventDefinitionDB eventDefinitionDB = mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriaEventID)), EventDefinitionDB.class);
                newReturnObject.PerformReturnObject(eventDefinitionDB);
            } else if (tableQuery.getCollection().equals("unapproved")) {
                EventDefinitionUnapprovedDB eventDefinitionUnapprovedDB = mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriaEventID)), EventDefinitionUnapprovedDB.class);
                newReturnObject.PerformReturnObject(eventDefinitionUnapprovedDB);
            } else {
                newReturnObject.setReturncode(0);
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED010", language));
            }

        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());

        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject eventDefinitionStatusChanger(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language, String setStatus) {
        try {
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            String eventID=tableQuery.getPrimaryKey();
            EventDefinitionDB eventDefinitionDBResult = findByEventId(eventID);
            EventDefinitionUnapprovedDB eventDefinitionUnapprovedDB = findUnapprovedByEventId(eventID);
            String collection=tableQuery.getCollection();

<<<<<<< HEAD


                AuditHistory auditHistory = new AuditHistory(audit, "tm_event_definition_unapproved");
                EventDefinitionDB eventDefinitionDB=null;
=======
            if (eventDefinitionDBResult != null || eventDefinitionUnapprovedDB != null) {//record must exist to alter status.

                AuditHistory auditHistory = new AuditHistory(audit, "tm_event_definition_unapproved");
                EventDefinitionDB eventDefinitionDB =null;
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

                if(collection.equals("approved")){
                    eventDefinitionDB=eventDefinitionDBResult!=null?eventDefinitionDBResult:null;
                }
                if(collection.equals("unapproved")){
                    eventDefinitionDB=eventDefinitionUnapprovedDB!=null?new EventDefinitionDB(eventDefinitionUnapprovedDB):null;
                }
<<<<<<< HEAD
                if(eventDefinitionDB!=null) { //record must exist to alter status.
                    eventDefinitionDB.getEventDefinition().setVersion(eventDefinitionDB.getEventDefinition().getVersion() + 1);
                    eventDefinitionDB.addAuditInfo(audit);
                    auditHistory.addLastStatus(eventDefinitionDBResult);//last version record.
                    eventDefinitionDB.getEventDefinition().setStatus(setStatus);//changing the status.
                    auditHistory.addContent(mongoTemplate.save(new EventDefinitionUnapprovedDB(eventDefinitionDB)));


                    mongoTemplate.insert(auditHistory);
                    newReturnObject.PerformReturnObject("Success");
                    newReturnObject.setReturncode(1);
                }else{
//                  error record does not exist to update.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED006", language));
                }

=======

                eventDefinitionDB.getEventDefinition().setVersion(eventDefinitionDB.getEventDefinition().getVersion() + 1);
                eventDefinitionDB.addAuditInfo(audit);
                auditHistory.addLastStatus(eventDefinitionDBResult);//last version record.
                String status = eventDefinitionDB.getEventDefinition().getStatus();
                eventDefinitionDB.getEventDefinition().setStatus(setStatus);//changing the status.
                auditHistory.addContent(mongoTemplate.save(new EventDefinitionUnapprovedDB(eventDefinitionDB)));


                mongoTemplate.insert(auditHistory);
                newReturnObject.PerformReturnObject("Success");
                newReturnObject.setReturncode(1);
            } else {
                //error record does not exist to update.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED006", language));
            }
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public EventDefinitionDB findByEventId(String eventId) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("eventDefinition.eventId").is(eventId));
            return mongoTemplate.findOne(query, EventDefinitionDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "findByEventId");
            return null;
        }
    }

    @Override
    public EventDefinitionUnapprovedDB findUnapprovedByEventId(String eventId) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("eventDefinition.eventId").is(eventId));
            return mongoTemplate.findOne(query, EventDefinitionUnapprovedDB.class);
        } catch (Exception ex) {
<<<<<<< HEAD
            logger.error(ex.getMessage(), "findUnapprovedByEventId");
=======
            logger.error(ex.getMessage(), "EventDefinitionUnapprovedDB");
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            return null;
        }
    }

    @Override
    public long countByEventId(String eventId) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("eventDefinition.eventId").is(eventId));
            return mongoTemplate.count(query, EventDefinitionDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByEventId");
            return 0;
        }
    }

    @Override
    public long countByUnapprovedEventId(String eventId) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("eventDefinition.eventId").is(eventId));
            return mongoTemplate.count(query, EventDefinitionUnapprovedDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByUnapprovedEventId");
            return 0;
        }
    }

    public Audit getLastAuditUnapproved(String eventId, NewReturnObject newReturnObject, String language) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("eventDefinition.eventId").is(eventId));

            List<Audit> audit = mongoTemplate.findOne(query, EventDefinitionUnapprovedDB.class).getAuditInfo();

            return audit.get(audit.size() - 1);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MED002", language));
            return null;
        }
    }
}
