//
//
//import com.dd.payhub.paycommon.model.CountryHolidays;
//import com.dd.payhub.paycommon.model.CountryHolidays;
//import com.dd.payhub.paycommon.view.NewReturnObject;
//import com.dd.payhub.paydefinition.config.FnOptConfig;
//import com.dd.payhub.paydefinition.entity.CountryHolidaysDB;
//import com.dd.payhub.paydefinition.entity.CountryHolidaysUnapprovedDB;
//import org.springframework.context.i18n.LocaleContextHolder;
//import org.springframework.web.bind.annotation.RequestAttribute;
//import org.springframework.web.bind.annotation.RequestBody;
//
//public class ch {
    //
    //    private boolean validate(Countryholiday ch,String language) {
    //        boolean valid = false;
    //        //mandatory fields.
    //        try {
    //            if ((!ch.getid().isEmpty()) && (!ch.getCountryHolidaysCode().isEmpty()) && (!ch.gettid().isEmpty()) && ((countryHolidays.getYear().length())==4) && (!ch.gethlist().isEmpty()))  {
    //                valid = true;
    //            } else {
    //                this.newReturnObject.addError(utilityService.singleError("MCY004", language));//error code
    //
    //            }
    //        } catch (Exception ex) {
    //            this.newReturnObject.addError(utilityService.singleError("MCY002", language));//error code
    //
    //        }
    //        return valid;
    //    }
//
//    public String newCountryHolidays(@RequestBody CountryHolidays CountryHolidaysDB, @RequestAttribute(value = "userid") String headerUserId) {
//        //validation.
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//            //change for token in future.
//            CountryHolidaysDB countryHolidaysDBRecord = null;
//            CountryHolidaysUnapprovedDB countryHolidaysUnapprovedDB = null;
//
//            if (validateCountryHolidays(countryHolidaysDB, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCY001", language, FnOptConfig.fIdCountryHolidaysController, FnOptConfig.opFNew)) {//error code.
//                //valid data to insert new record.
//                countryHolidaysDBRecord = countryHolidaysDAL.findByCountryHolidaysCode(countryHolidaysDB.getCountryHolidaysCode());
//                countryHolidaysUnapprovedDB = countryHolidaysDAL.findUnapprovedByCountryHolidaysCode(countryHolidaysDB.getCountryHolidaysCode());
//                if (countryHolidaysDBRecord == null && countryHolidaysUnapprovedDB == null) {//record does not exist so insertion can be done.
//                    newReturnObject = countryHolidaysDAL.addCountryHolidays(countryHolidaysDB, headerUserId, newReturnObject, language);//
//                } else {//Error record exist duplication not allowed.
//                    this.newReturnObject.addError(utilityService.singleError("MCY003", language));
//                    this.newReturnObject.setReturncode(0);
//                }
//            }
//
//        } catch (Exception ex) {
//            this.newReturnObject.setReturncode(0);
//            this.newReturnObject.addError(utilityService.singleError("MCY002", language));//error code
//            logger.error("Exception in CountryHolidaysController.addCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
//
//        }
//        return this.newReturnObject.setReturnJSON();
//
//    }
//}
