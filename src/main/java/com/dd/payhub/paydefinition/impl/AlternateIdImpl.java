package com.dd.payhub.paydefinition.impl;

import com.dd.payhub.paycommon.model.*;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
import com.dd.payhub.paydefinition.dal.AlternateIdDAL;
import com.dd.payhub.paydefinition.entity.*;
import com.dd.payhub.paydefinition.entity.AlternateIdDB;
import com.dd.payhub.paydefinition.entity.AlternateIdUnapprovedDB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Collation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AlternateIdImpl implements AlternateIdDAL {

    private static final Logger logger = LogManager.getLogger(AlternateIdImpl.class);
    private final MongoTemplate mongoTemplate;
    private ErrorCodeDAL errorCodeDAL;

    @Autowired
    public AlternateIdImpl(MongoTemplate mongoTemplate, ErrorCodeDAL errorCodeDAL) {
        this.mongoTemplate = mongoTemplate;
        this.errorCodeDAL = errorCodeDAL;
    }

    @Override
    public ReactTableRecord getAllAlternateIdByPagination(ReactTableQuery tableQuery) {
        int page = 0;
        int pageSize = 5;
        int totalPage;
        long totalRecord = 0;
        ReactTableRecord tableRecord = new ReactTableRecord();
        String sortField = tableQuery.getSortField().isEmpty() ? "sortDate" : tableQuery.getSortField();

        Query query = new Query();//if
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.

        try {
            if (tableQuery.getPage() != 0) {
                page = tableQuery.getPage();
            }
            if (tableQuery.getPageSize() != 0) {
                pageSize = tableQuery.getPageSize();
            }
            //sorting order code.
            if (!tableQuery.getSortOrder().equals("true")) {//false means order by asc.
                query.with(Sort.by(Sort.Direction.DESC, sortField));
            } else {
                query.with(Sort.by(Sort.Direction.ASC, sortField));
            }
            Criteria statusCriteria = Criteria.where("alternateId.status").is(tableQuery.getStatus());
            query.addCriteria(Criteria.where("alternateId.status").is(tableQuery.getStatus()));

            if (!tableQuery.getKeyword().isEmpty()) {
                Criteria alternateIdCriteria = Criteria.where("alternateId.alternateId").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria alternateIdRoutingIdCriteria = Criteria.where("alternateId.routingId").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria alternateIdCodeTypeCriteria = Criteria.where("alternateId.codeType").regex(String.valueOf(tableQuery.getKeyword()), "i");

                query.addCriteria(new Criteria().orOperator(alternateIdCriteria, alternateIdRoutingIdCriteria, alternateIdCodeTypeCriteria).andOperator(statusCriteria));
            }
            if (tableQuery.getCollection().equals("approved")) {
                totalRecord = mongoTemplate.count(query, AlternateIdDB.class);

                tableRecord.setTableRecord(mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), AlternateIdDB.class));
            } else {
                totalRecord = mongoTemplate.find(query, AlternateIdUnapprovedDB.class).size();

                tableRecord.setTableRecord((mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), AlternateIdUnapprovedDB.class)));
            }
            totalPage = (int) Math.ceil(totalRecord / (float) pageSize);
            tableRecord.setTotalPage(totalPage);
            tableRecord.setTotalRecord(totalRecord);
            return tableRecord;
        } catch (Exception ex) {
            tableRecord.setTotalPage(0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord(new ArrayList<>());
            logger.error(ex.getMessage(), "getAllAlternateIdByPagination");
            return tableRecord;
        }
    }

    @Override
    public NewReturnObject addAlternateId(AlternateId alternateId, String userId, NewReturnObject newReturnObject, String language) {
        try {

            AlternateIdDB alternateIdDB = new AlternateIdDB();
            alternateIdDB.setAlternateId(alternateId);
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            alternateIdDB.addAuditInfo(audit);
            alternateIdDB.getAlternateId().setVersion(0);//setting the version.
            AuditHistory auditHistory = new AuditHistory(audit, "tm_alternate_id_unapproved");

            alternateIdDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
            //            inserting in the database.
            auditHistory.addContent(mongoTemplate.insert(new AlternateIdUnapprovedDB(alternateIdDB)));
            auditHistory.addLastStatus(null);

            mongoTemplate.insert(auditHistory);
            newReturnObject.PerformReturnObject("Success");
            return newReturnObject;
        } catch (Exception ex) {
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MAI002", language));//error code
            newReturnObject.setReturncode(0);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            return newReturnObject;
        }
    }

    @Override
    public NewReturnObject getEditableAlternateId(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language) {
        try {
            //gives approved record only when there are no unapproved records.
            if (reactTableQuery.getCollection().equals("approved")) {
                if (countByUnapprovedAlternateId(reactTableQuery.getPrimaryKey()) == 0) {
                    newReturnObject.PerformReturnObject(findByAlternateId(reactTableQuery.getPrimaryKey()));
                } else {
                    newReturnObject.PerformReturnObject(new ArrayList<>());
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MAI007", language));
                }

            } else if (reactTableQuery.getCollection().equals("unapproved")) {
                newReturnObject.PerformReturnObject(findUnapprovedByAlternateId(reactTableQuery.getPrimaryKey()));
            }
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MAI002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject updateAlternateId(AlternateId alternateId, String userId, NewReturnObject newReturnObject, String language) {
        try {
            AlternateIdDB alternateIdDB = new AlternateIdDB();
            alternateIdDB.setAlternateId(alternateId);

            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
//
            AlternateIdUnapprovedDB alternateIdUnapprovedDB = findUnapprovedByAlternateId(alternateIdDB.getAlternateId().getAlternateId());
            AlternateIdDB alternateIdDBResult = findByAlternateId(alternateIdDB.getAlternateId().getAlternateId());
            if (alternateIdDBResult != null || alternateIdUnapprovedDB != null) {//record must exist to update

                AuditHistory auditHistory = new AuditHistory(audit, "tm_alternate_id_unapproved");

                if (alternateIdUnapprovedDB == null) {
                    boolean approveStatus = alternateIdDBResult != null ? alternateIdDBResult.getAlternateId().getStatus().equals(alternateIdDB.getAlternateId().getStatus()) : false;
                    if (approveStatus) {//status is not changed
                        alternateIdDB.setAuditInfo(alternateIdDBResult.getAuditInfo());
                        alternateIdDB.addAuditInfo(audit);
                        alternateIdDB.getAlternateId().setVersion(alternateIdDBResult.getAlternateId().getVersion() + 1);//version gets increased during update
                        alternateIdDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.

                        auditHistory.addLastStatus(alternateIdDBResult);//last version record.
                        auditHistory.addContent(mongoTemplate.save(new AlternateIdUnapprovedDB(alternateIdDB)));
                        mongoTemplate.insert(auditHistory);
                        newReturnObject.PerformReturnObject("Success");
                        newReturnObject.setReturncode(1);
                    } else {
                        //cannot change the status during edit.
                        newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MAI009", language));
                    }
                } else {
                    boolean approveStatus = alternateIdUnapprovedDB != null ? alternateIdUnapprovedDB.getAlternateId().getStatus().equals(alternateIdDB.getAlternateId().getStatus()) : false;
                    if (approveStatus) {//status is not changed
                        alternateIdDB.setAuditInfo(alternateIdUnapprovedDB.getAuditInfo());
                        alternateIdDB.addAuditInfo(audit);
                        alternateIdDB.set_id(alternateIdUnapprovedDB.get_id());
                        alternateIdDB.getAlternateId().setVersion(alternateIdUnapprovedDB.getAlternateId().getVersion() + 1);//version gets increased during update
                        alternateIdDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
                        alternateIdDB.set_id(alternateIdUnapprovedDB.get_id());//setting the mongo ID.

                        auditHistory.addLastStatus(alternateIdUnapprovedDB);//old version record.
                        auditHistory.addContent(mongoTemplate.save(new AlternateIdUnapprovedDB(alternateIdDB)));
                        mongoTemplate.insert(auditHistory);
                        newReturnObject.PerformReturnObject("Success");
                        newReturnObject.setReturncode(1);
                    } else {
                        //cannot change the status during edit.
                        newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MAI009", language));
                    }

                }

            } else {
                //error record does not exist to update.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MAI006", language));
            }
        } catch (Exception ex) {

            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MAI002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject approveAlternateId(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            String alternateId=tableQuery.getPrimaryKey();
            AlternateIdUnapprovedDB unApprovedResult = findUnapprovedByAlternateId(alternateId);
            AlternateIdDB alternateIdDB = findByAlternateId(alternateId);


            Audit audit = getLastAuditUnapproved(alternateId, newReturnObject, language);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            audit.setApproverId(userId);
            if (!audit.getCreatorId().equals(userId)) {//creator and approver cannot be same.

                unApprovedResult.addAuditInfo(audit); //setting the audit.
                if (unApprovedResult != null) {//record exist to approve.
                    AuditHistory auditHistory = new AuditHistory(audit, "tm_alternate_id");

                    if (alternateIdDB == null) {
                        // data is not present in main collection, create new record
                        auditHistory.addLastStatus(null);
                        auditHistory.addContent(mongoTemplate.insert(new AlternateIdDB(unApprovedResult)));
                    } else {
                        String mongoId = alternateIdDB.get_id();
                        auditHistory.addLastStatus(alternateIdDB);//old version
                        alternateIdDB = new AlternateIdDB(unApprovedResult);
                        alternateIdDB.set_id(mongoId);

                        auditHistory.addContent(mongoTemplate.save(alternateIdDB));
                    }
                    newReturnObject.PerformReturnObject(null);


                    mongoTemplate.insert(auditHistory);
                    newReturnObject = rejectByAlternateId(tableQuery, userId, newReturnObject, language);
                } else {//no record to approve.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MAI004", language));
                }
            } else {//approverID and creatorID are same.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MAI008", language));
            }

        } catch (Exception ex) {

            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MAI002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject rejectByAlternateId(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            String alternateId=tableQuery.getPrimaryKey();
            AlternateIdUnapprovedDB alternateIdUnapprovedDB = findUnapprovedByAlternateId(alternateId);
            Audit audit = getLastAuditUnapproved(alternateId, newReturnObject, language);
            audit.setApproverId(userId);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            AuditHistory auditHistory = new AuditHistory(audit, "tm_alternate_id_unapproved");
            auditHistory.addContent(alternateIdUnapprovedDB);

            Query query = new Query();
            query.addCriteria(Criteria.where("alternateId.alternateId").is(alternateId));
            mongoTemplate.remove(query, AlternateIdUnapprovedDB.class);

            mongoTemplate.insert(auditHistory);

            newReturnObject.setReturncode(1);
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MAI002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject viewByAlternateId(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language) {
        try {
            String alternateId=tableQuery.getPrimaryKey();
            Criteria criteriaAlternateId= Criteria.where("alternateId.alternateId").is(alternateId);
            if (tableQuery.getCollection().equals("approved")) {
                AlternateIdDB alternateIdDB = mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriaAlternateId)), AlternateIdDB.class);
                newReturnObject.PerformReturnObject(alternateIdDB);
            } else if (tableQuery.getCollection().equals("unapproved")) {
                AlternateIdUnapprovedDB alternateIdUnapprovedDB = mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriaAlternateId)), AlternateIdUnapprovedDB.class);
                newReturnObject.PerformReturnObject(alternateIdUnapprovedDB);
            } else {
                newReturnObject.setReturncode(0);
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MAI010", language));
            }

        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MAI002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());

        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject alternateIdStatusChanger(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language, String setStatus) {
        try {
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            String alternateId=tableQuery.getPrimaryKey();
            AlternateIdDB alternateIdDBResult = findByAlternateId(alternateId);
            AlternateIdUnapprovedDB alternateIdUnapprovedDB = findUnapprovedByAlternateId(alternateId);
            String collection=tableQuery.getCollection();



            AuditHistory auditHistory = new AuditHistory(audit, "tm_alternate_id_unapproved");
            AlternateIdDB alternateIdDB=null;

            if(collection.equals("approved")){
                alternateIdDB=alternateIdDBResult!=null?alternateIdDBResult:null;
            }
            if(collection.equals("unapproved")){
                alternateIdDB=alternateIdUnapprovedDB!=null?new AlternateIdDB(alternateIdUnapprovedDB):null;
            }
            if(alternateIdDB!=null) { //record must exist to alter status.
                alternateIdDB.getAlternateId().setVersion(alternateIdDB.getAlternateId().getVersion() + 1);
                alternateIdDB.addAuditInfo(audit);
                auditHistory.addLastStatus(alternateIdDBResult);//last version record.
                alternateIdDB.getAlternateId().setStatus(setStatus);//changing the status.
                auditHistory.addContent(mongoTemplate.save(new AlternateIdUnapprovedDB(alternateIdDB)));


                mongoTemplate.insert(auditHistory);
                newReturnObject.PerformReturnObject("Success");
                newReturnObject.setReturncode(1);
            }else{
//                  error record does not exist to update.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MAI006", language));
            }


        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MAI002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public AlternateIdDB findByAlternateId(String alternateId) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("alternateId.alternateId").is(alternateId));
            return mongoTemplate.findOne(query, AlternateIdDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "findByAlternateId");
            return null;
        }
    }

    @Override
    public AlternateIdUnapprovedDB findUnapprovedByAlternateId(String alternateId) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("alternateId.alternateId").is(alternateId));
            return mongoTemplate.findOne(query, AlternateIdUnapprovedDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "findUnapprovedByAlternateId");
            return null;
        }
    }

    @Override
    public long countByAlternateId(String alternateId) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("alternateId.alternateId").is(alternateId));
            return mongoTemplate.count(query, AlternateIdDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByAlternateId");
            return 0;
        }
    }

    @Override
    public long countByUnapprovedAlternateId(String alternateId) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("alternateId.alternateId").is(alternateId));
            return mongoTemplate.count(query, AlternateIdUnapprovedDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByUnapprovedAlternateId");
            return 0;
        }
    }

    public Audit getLastAuditUnapproved(String alternateId, NewReturnObject newReturnObject, String language) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("alternateId.alternateId").is(alternateId));

            List<Audit> audit = mongoTemplate.findOne(query, AlternateIdUnapprovedDB.class).getAuditInfo();

            return audit.get(audit.size() - 1);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MAI002", language));
            return null;
        }
    }
}
