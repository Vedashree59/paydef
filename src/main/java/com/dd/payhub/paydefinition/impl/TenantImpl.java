package com.dd.payhub.paydefinition.impl;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.AuditHistory;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.model.Tenant;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
import com.dd.payhub.paydefinition.dal.TenantDAL;
<<<<<<< HEAD
import com.dd.payhub.paydefinition.entity.EventDefinitionDB;
import com.dd.payhub.paydefinition.entity.EventDefinitionUnapprovedDB;
=======
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
import com.dd.payhub.paydefinition.entity.TenantDB;
import com.dd.payhub.paydefinition.entity.TenantUnapprovedDB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Collation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TenantImpl implements TenantDAL {

    private static final Logger logger = LogManager.getLogger(TenantImpl.class);
    private final MongoTemplate mongoTemplate;
    private ErrorCodeDAL errorCodeDAL;

    @Autowired
    public TenantImpl(MongoTemplate mongoTemplate, ErrorCodeDAL errorCodeDAL) {
        this.mongoTemplate = mongoTemplate;
        this.errorCodeDAL = errorCodeDAL;
    }

    @Override
    public ReactTableRecord getAllTenantByPagination(ReactTableQuery tableQuery) {
        int page = 0;
        int pageSize = 5;
        int totalPage;
        long totalRecord = 0;
        ReactTableRecord tableRecord = new ReactTableRecord();
        String sortField=tableQuery.getSortField().isEmpty()?"sortDate":tableQuery.getSortField();

        Query query = new Query();//if
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.

        try {
            if (tableQuery.getPage() != 0) {
                page = (int) tableQuery.getPage();
            }
            if (tableQuery.getPageSize() != 0) {
                pageSize = (int) tableQuery.getPageSize();
            }
            //sorting order code.
            if(!tableQuery.getSortOrder().equals("true")){//false means order by asc.
                query.with(Sort.by(Sort.Direction.DESC,sortField));
            }
            else {
                query.with(Sort.by(Sort.Direction.ASC,sortField));
            }
            Criteria statusCriteria=Criteria.where("tenant.status").is(tableQuery.getStatus());
            query.addCriteria(Criteria.where("tenant.status").is(tableQuery.getStatus()));

            if (!tableQuery.getKeyword().isEmpty()) {
                Criteria tenantNameCriteria = Criteria.where("tenant.tenantId").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria tenantCodeCriteria = Criteria.where("tenant.tenantName").regex(String.valueOf(tableQuery.getKeyword()), "i");
                query.addCriteria(new Criteria().orOperator(tenantCodeCriteria, tenantNameCriteria).andOperator(statusCriteria));
            }
            if (tableQuery.getCollection().equals("approved")) {
                totalRecord = mongoTemplate.count(query, TenantDB.class);
                tableRecord.setTableRecord(mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), TenantDB.class));
            } else {
                totalRecord = mongoTemplate.find(query, TenantUnapprovedDB.class).size();

                tableRecord.setTableRecord((mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), TenantUnapprovedDB.class)));
            }
            totalPage = (int) Math.ceil(totalRecord / (float) pageSize);
            tableRecord.setTotalPage(totalPage);
            tableRecord.setTotalRecord(totalRecord);
            return tableRecord;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            tableRecord.setTotalPage( 0);
            tableRecord.setTotalRecord( 0);
            tableRecord.setTableRecord( new ArrayList<>());
            return tableRecord;
        }
    }

    @Override
    public NewReturnObject addTenant(Tenant tenant, String userId, NewReturnObject newReturnObject, String language) {
        try {

            TenantDB tenantDB=new TenantDB();
            tenantDB.setTenant(tenant);
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            tenantDB.addAuditInfo(audit);
            tenantDB.getTenant().setVersion(0);//setting the version.
            AuditHistory auditHistory = new AuditHistory(audit,"tm_tenant_unapproved");

            tenantDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
    //            inserting in the database.
            TenantUnapprovedDB tenantUnapprovedDB =mongoTemplate.insert(new TenantUnapprovedDB(tenantDB)) ;
            auditHistory.addContent(tenantUnapprovedDB);
            auditHistory.addLastStatus(null);

            mongoTemplate.insert(auditHistory);
            newReturnObject.PerformReturnObject(tenantUnapprovedDB);
            return newReturnObject;
        } catch (Exception ex) {
            logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCT002", language));//error code
            newReturnObject.setReturncode(0);
            return newReturnObject;
        }
    }

    @Override
    public NewReturnObject updateTenant(Tenant tenant, String userId, NewReturnObject newReturnObject, String language) {
        try {
            TenantDB tenantDB=new TenantDB();
            tenantDB.setTenant(tenant);

            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
//
            TenantUnapprovedDB tenantUnapprovedDB = findUnapprovedByTenantId(tenantDB.getTenant().getTenantId());
            TenantDB tenantDBResult = findByTenantId(tenantDB.getTenant().getTenantId());
            if(tenantDBResult !=null|| tenantUnapprovedDB !=null) {//record must exist to update

                AuditHistory auditHistory = new AuditHistory(audit,"tm_tenant_unapproved");


                if (tenantUnapprovedDB == null) {
                    tenantDB.setAuditInfo(tenantDBResult.getAuditInfo());
                    tenantDB.addAuditInfo(audit);
                    tenantDB.getTenant().setVersion(tenantDBResult.getTenant().getVersion()+1);//version gets increased during update
                    tenantDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.

                    auditHistory.addLastStatus(tenantDBResult);//last version record.

                } else {
                    tenantDB.setAuditInfo(tenantUnapprovedDB.getAuditInfo());
                    tenantDB.addAuditInfo(audit);
                    tenantDB.set_id(tenantUnapprovedDB.get_id());
                    tenantDB.getTenant().setVersion(tenantUnapprovedDB.getTenant().getVersion() + 1);//version gets increased during update
                    tenantDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
                    tenantDB.set_id(tenantUnapprovedDB.get_id());//setting the mongo ID.

                    auditHistory.addLastStatus(tenantUnapprovedDB);//old version record.

                }
                auditHistory.addContent(mongoTemplate.save(new TenantUnapprovedDB(tenantDB)));
                mongoTemplate.insert(auditHistory);

            }
            newReturnObject.PerformReturnObject(null);
            newReturnObject.setReturncode(1);
        } catch (Exception ex) {
            logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MTN002", language));//change
        }
        return newReturnObject;
    }
<<<<<<< HEAD
=======
    @Override
    public TenantDB findByTenantId(String tenantId) {
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        query.addCriteria(Criteria.where("tenant.tenantId").is(tenantId));
        return mongoTemplate.findOne(query, TenantDB.class);
    }

    @Override
    public TenantDB findApprovalRequiredTenantId(String tenantId) {
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        query.fields().include("tenant.fourEyeRequired");
        query.addCriteria(Criteria.where("tenant.tenantId").is(tenantId));
        return mongoTemplate.findOne(query, TenantDB.class);
    }

    public long countByTenantId(String tenantId) {
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        query.addCriteria(Criteria.where("tenant.tenantId").is(tenantId));
        return mongoTemplate.count(query, TenantDB.class);
    }

    @Override
    public TenantUnapprovedDB findUnapprovedByTenantId(String tenantId) {
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        query.addCriteria(Criteria.where("tenant.tenantId").is(tenantId));
        return mongoTemplate.findOne(query, TenantUnapprovedDB.class);
    }
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

    @Override
    public NewReturnObject getEditableTenant(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language) {
        try {
            //gives approved record only when there are no unapproved records.
            if(reactTableQuery.getCollection().equals("approved")){

                TenantUnapprovedDB unApprovedResult = findUnapprovedByTenantId(reactTableQuery.getPrimaryKey());
                if(unApprovedResult==null){
                    newReturnObject.PerformReturnObject(findByTenantId(reactTableQuery.getPrimaryKey()));
                }else{
                    newReturnObject.PerformReturnObject(new ArrayList<>());
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MTN007", language));
                }

            }else if(reactTableQuery.getCollection().equals("unapproved")){
                newReturnObject.PerformReturnObject(findUnapprovedByTenantId(reactTableQuery.getPrimaryKey()));
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MTN002", language));
        }
        return newReturnObject;
    }

    @Override
<<<<<<< HEAD
    public NewReturnObject rejectByTenantId(String tenantId, String userId, NewReturnObject newReturnObject, String language) {
=======
    public NewReturnObject deleteByTenantId(String tenantId, String userId, NewReturnObject newReturnObject, String language) {
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        try {
            TenantUnapprovedDB tenantUnapprovedDB = findUnapprovedByTenantId(tenantId);
            Audit audit = getLastAuditUnapproved(tenantId, newReturnObject, language);
            audit.setApproverId(userId);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            AuditHistory auditHistory = new AuditHistory(audit,"tm_tenant_unapproved");
            auditHistory.addContent(tenantUnapprovedDB);

            Query query = new Query();
            query.addCriteria(Criteria.where("tenant.tenantId").is(tenantId));
            mongoTemplate.remove(query, TenantUnapprovedDB.class);

            mongoTemplate.insert(auditHistory);

            newReturnObject.setReturncode(1);
        } catch (Exception ex) {
            logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MTN002", language));
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject approveTenant(String tenantId, String userId, NewReturnObject newReturnObject, String language) {
        try {
            newReturnObject=newReturnObject;
            TenantUnapprovedDB unApprovedResult = findUnapprovedByTenantId(tenantId);
            TenantDB tenantDB = findByTenantId(tenantId);


<<<<<<< HEAD
            Audit audit =
                    getLastAuditUnapproved(tenantId, newReturnObject, language);
=======
            Audit audit = audit = getLastAuditUnapproved(tenantId, newReturnObject, language);
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            audit.setApprovedDate(Instant.now().toEpochMilli());
            audit.setApproverId(userId);
            if(!audit.getCreatorId().equals(userId)){//creator and approver cannot be same.

            unApprovedResult.addAuditInfo(audit);//setting the audit.
            if(unApprovedResult!=null){//record exist to update.
                AuditHistory auditHistory = new AuditHistory(audit,"tm_tenant");

                if (tenantDB == null) {
                    // tm_tenant data is not present, create new record
                    auditHistory.addLastStatus(null);
                    auditHistory.addContent(mongoTemplate.insert(new TenantDB(unApprovedResult)));
                } else {
                    String mongoId = tenantDB.get_id();
                    auditHistory.addLastStatus(tenantDB);//old version
                    tenantDB = new TenantDB(unApprovedResult);
                    tenantDB.set_id(mongoId);

                    auditHistory.addContent( mongoTemplate.save(tenantDB));
                }
                newReturnObject.PerformReturnObject(null);

                mongoTemplate.insert(auditHistory);
<<<<<<< HEAD
                newReturnObject = rejectByTenantId(tenantId, userId, newReturnObject, language);//delete the un approved after approving.
=======
                newReturnObject = deleteByTenantId(tenantId, userId, newReturnObject, language);//delete the un approved after approving.
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            }else{//no record to approve.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MTN004", language));
            }
            }else{//approverID and creatorID are same.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MTN008", language));
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MTN002", language));
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject viewByTenantId(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language) {
        try {

            Criteria criteriatenantCode=Criteria.where("tenant.tenantId").is(tableQuery.getPrimaryKey());
//            Criteria criteriaStatus=Criteria.where("tenant.status").is("active");
            if(tableQuery.getCollection().equals("approved")){
                TenantDB tenantDB =mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriatenantCode)), TenantDB.class);
                newReturnObject.PerformReturnObject(tenantDB);
            }else if(tableQuery.getCollection().equals("unapproved")){
                TenantUnapprovedDB tenantUnapprovedDB =mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriatenantCode)), TenantUnapprovedDB.class);
                newReturnObject.PerformReturnObject(tenantUnapprovedDB);
            }
            else {
                newReturnObject.setReturncode(0);
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MTN005", language));
            }

        }
        catch (Exception ex) {
            logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MTN002", language));
        }
        return newReturnObject;
    }

    @Override
<<<<<<< HEAD
    public NewReturnObject tenantStatusChanger(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language, String setStatus) {
        try {
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            String eventID=tableQuery.getPrimaryKey();
            TenantDB eventDefinitionDBResult = findByTenantId(eventID);
            TenantUnapprovedDB eventDefinitionUnapprovedDB = findUnapprovedByTenantId(eventID);
            String collection=tableQuery.getCollection();



            AuditHistory auditHistory = new AuditHistory(audit, "tm_tenant_unapproved");
            TenantDB eventDefinitionDB=null;

            if(collection.equals("approved")){
                eventDefinitionDB=eventDefinitionDBResult!=null?eventDefinitionDBResult:null;
            }
            if(collection.equals("unapproved")){
                eventDefinitionDB=eventDefinitionUnapprovedDB!=null?new TenantDB(eventDefinitionUnapprovedDB):null;
            }
            if(eventDefinitionDB!=null) { //record must exist to alter status.
                eventDefinitionDB.getTenant().setVersion(eventDefinitionDB.getTenant().getVersion() + 1);
                eventDefinitionDB.addAuditInfo(audit);
                auditHistory.addLastStatus(eventDefinitionDBResult);//last version record.
                eventDefinitionDB.getTenant().setStatus(setStatus);//changing the status.
                auditHistory.addContent(mongoTemplate.save(new TenantUnapprovedDB(eventDefinitionDB)));


                mongoTemplate.insert(auditHistory);
                newReturnObject.PerformReturnObject("Success");
                newReturnObject.setReturncode(1);
            }else{
//                  error record does not exist to update.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MTN006", language));
            }


        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MTN002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }



    @Override
    public TenantDB findByTenantId(String tenantId) {
        try {
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        query.addCriteria(Criteria.where("tenant.tenantId").is(tenantId));
        return mongoTemplate.findOne(query, TenantDB.class);
        }catch (Exception ex){
            logger.error(ex.getMessage(), "findByTenantId");
            return null;
        }
    }

    @Override
    public TenantDB findApprovalRequiredTenantId(String tenantId) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.fields().include("tenant.fourEyeRequired");
            query.addCriteria(Criteria.where("tenant.tenantId").is(tenantId));
            return mongoTemplate.findOne(query, TenantDB.class);
        }catch (Exception ex){
            logger.error(ex.getMessage(), "findApprovalRequiredTenantId");
            return null;
        }
    }
    public long countByTenantId(String tenantId) {
        try {
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        query.addCriteria(Criteria.where("tenant.tenantId").is(tenantId));
        return mongoTemplate.count(query, TenantDB.class);
        }catch (Exception ex){
            logger.error(ex.getMessage(), "countByTenantId");
            return 0;
        }
    }
    @Override
    public long countByUnapprovedTenantId(String tenantId) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("tenant.tenantId").is(tenantId));
            return mongoTemplate.count(query, TenantDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByUnapprovedTenantId");
            return 0;
        }
    }

    @Override
    public TenantUnapprovedDB findUnapprovedByTenantId(String tenantId) {
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        query.addCriteria(Criteria.where("tenant.tenantId").is(tenantId));
        return mongoTemplate.findOne(query, TenantUnapprovedDB.class);
    }

    @Override
    public NewReturnObject listOfAllTenantIdsByRegx(String keyword, NewReturnObject newReturnObject, String language) {
        try {
            Query query=new Query();
            query.fields().include("tenant.tenantId").include("tenant.status").getFieldsObject();
            Criteria statusCriteria=Criteria.where("tenant.status").is("active");
            if(keyword.isEmpty()||keyword.equals(" ")){
                newReturnObject.PerformReturnObject(mongoTemplate.find(query.addCriteria(statusCriteria),TenantDB.class));
            }
            else{
                Criteria tenantIdCriteria=Criteria.where("tenant.tenantId").regex(keyword, "i");
                query.addCriteria(new Criteria().orOperator(tenantIdCriteria).andOperator(statusCriteria));
                newReturnObject.PerformReturnObject(mongoTemplate.find(query, TenantDB.class));
            }
=======
    public NewReturnObject listOfAllTenantIdsByRegx(String keyword, NewReturnObject newReturnObject, String language) {
        try {
            Criteria statusCriteria=Criteria.where("tenant.status").is("active");
            Criteria tenantIdCriteria=Criteria.where("tenant.tenantId").regex(keyword, "i");
            Query query=new Query();
            query.fields().include("tenant.tenantId").getFieldsObject();
            query.addCriteria(new Criteria().orOperator(tenantIdCriteria).andOperator(statusCriteria));
            newReturnObject.PerformReturnObject(mongoTemplate.find(query, TenantDB.class));

>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        }
        catch (Exception ex) {
            logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
            newReturnObject.PerformReturnObject(new ArrayList<>());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MTN002", language));
        }
        return newReturnObject;
    }

    public Audit getLastAuditUnapproved(String tenantId, NewReturnObject newReturnObject, String language) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("tenant.tenantId").is(tenantId));

            List<Audit> audit = mongoTemplate.findOne(query, TenantUnapprovedDB.class).getAuditInfo();

            return audit.get(audit.size() - 1);
        } catch (Exception ex) {
            logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MTN002", language));
            return null;
        }
    }

}
