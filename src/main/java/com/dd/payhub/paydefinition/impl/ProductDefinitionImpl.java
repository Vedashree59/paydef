package com.dd.payhub.paydefinition.impl;

import com.dd.payhub.paycommon.model.*;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
import com.dd.payhub.paydefinition.dal.ProductDefinitionDAL;
import com.dd.payhub.paydefinition.entity.*;
import com.dd.payhub.paydefinition.entity.ProductDefinitionDB;
import com.dd.payhub.paydefinition.entity.ProductDefinitionUnapprovedDB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Collation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ProductDefinitionImpl implements ProductDefinitionDAL {

    private static final Logger logger = LogManager.getLogger(ProductDefinitionImpl.class);
    private final MongoTemplate mongoTemplate;
    private ErrorCodeDAL errorCodeDAL;

    @Autowired

    public ProductDefinitionImpl(MongoTemplate mongoTemplate, ErrorCodeDAL errorCodeDAL) {
        this.mongoTemplate = mongoTemplate;
        this.errorCodeDAL = errorCodeDAL;
    }

    @Override
    public ReactTableRecord getAllProductDefinitionByPagination(ReactTableQuery tableQuery) {
        int page = 0;
        int pageSize = 5;
        int totalPage;
        long totalRecord = 0;
        ReactTableRecord tableRecord = new ReactTableRecord();
        String sortField = tableQuery.getSortField().isEmpty() ? "sortDate" : tableQuery.getSortField();

        Query query = new Query();//if
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.

        try {
            if (tableQuery.getPage() != 0) {
                page =  tableQuery.getPage();
            }
            if (tableQuery.getPageSize() != 0) {
                pageSize = tableQuery.getPageSize();
            }
            //sorting order code.
            if (!tableQuery.getSortOrder().equals("true")) {//false means order by asc.
                query.with(Sort.by(Sort.Direction.DESC, sortField));
            } else {
                query.with(Sort.by(Sort.Direction.ASC, sortField));
            }
            Criteria statusCriteria = Criteria.where("productDefinition.status").is(tableQuery.getStatus());
            query.addCriteria(Criteria.where("productDefinition.status").is(tableQuery.getStatus()));

            if (!tableQuery.getKeyword().isEmpty()) {
                Criteria productTypeCriteria = Criteria.where("productDefinition.productType").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria productIdCriteria = Criteria.where("productDefinition.productId").regex(String.valueOf(tableQuery.getKeyword()), "i");
                query.addCriteria(new Criteria().orOperator(productIdCriteria, productTypeCriteria).andOperator(statusCriteria));
            }
            if (tableQuery.getCollection().equals("approved")) {
                totalRecord = mongoTemplate.count(query, ProductDefinitionDB.class);

                tableRecord.setTableRecord(mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), ProductDefinitionDB.class));
            } else {
                totalRecord = mongoTemplate.find(query, ProductDefinitionUnapprovedDB.class).size();

                tableRecord.setTableRecord((mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), ProductDefinitionUnapprovedDB.class)));
            }
            totalPage = (int) Math.ceil(totalRecord / (float) pageSize);
            tableRecord.setTotalPage(totalPage);
            tableRecord.setTotalRecord(totalRecord);
            return tableRecord;
        } catch (Exception ex) {
            tableRecord.setTotalPage(0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord(new ArrayList<>());
            logger.error(ex.getMessage(),"getAllProductDefinitionByPagination");
            return tableRecord;
        }
    }

    @Override
    public NewReturnObject addProductDefinition(ProductDefinition productDefinition, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired) {
        try {
            ProductDefinitionDB productDefinitionDB = new ProductDefinitionDB();
            productDefinitionDB.setProductDefinition(productDefinition);
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            productDefinitionDB.addAuditInfo(audit);
            productDefinitionDB.getProductDefinition().setVersion(0);//setting the version.
            AuditHistory auditHistory;

            productDefinitionDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
            //            inserting in the database.
            if (approvalRequired) {
                //4eyeRequired = true  add to unapproved collection.
                auditHistory = new AuditHistory(audit, "tm_product_definition_unapproved");
                auditHistory.addContent(mongoTemplate.insert(new ProductDefinitionUnapprovedDB(productDefinitionDB)));
            } else {
                // 4eyeRequired = false adding to the main collection.
                auditHistory = new AuditHistory(audit, "tm_product_definition");
                auditHistory.addContent(mongoTemplate.insert(productDefinitionDB));
            }
            auditHistory.addLastStatus(null);
            mongoTemplate.insert(auditHistory);
            newReturnObject.PerformReturnObject("Success");
            return newReturnObject;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MPD002", language));
            newReturnObject.setReturncode(0);
            return newReturnObject;
        }
    }

    @Override
    public NewReturnObject getEditableProductDefinition(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language) {
        try {
            //gives approved record only when there are no unapproved records.
            if (reactTableQuery.getCollection().equals("approved")) {

                if (countByUnapprovedProductId(reactTableQuery.getPrimaryKey()) == 0) {
                    newReturnObject.PerformReturnObject(findByProductId(reactTableQuery.getPrimaryKey()));
                } else {
                    newReturnObject.PerformReturnObject(new ArrayList<>());
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MPD007", language));
                }

            } else if (reactTableQuery.getCollection().equals("unapproved")) {
                newReturnObject.PerformReturnObject(findUnapprovedByProductId(reactTableQuery.getPrimaryKey()));
            }
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MPD002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject updateProductDefinition(ProductDefinition productDefinition, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired) {
        try {
            ProductDefinitionDB productDefinitionDB = new ProductDefinitionDB();
            productDefinitionDB.setProductDefinition(productDefinition);
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            ProductDefinitionDB productDefinitionDBResult = findByProductId(productDefinitionDB.getProductDefinition().getProductId());
            ProductDefinitionUnapprovedDB productDefinitionUnapprovedDB = findUnapprovedByProductId(productDefinitionDB.getProductDefinition().getProductId());
            if (productDefinitionDBResult != null || productDefinitionUnapprovedDB != null) {//record must exist to update.

                if (approvalRequired) {//4eyeRequired = true  add to unapproved collection.

                    AuditHistory auditHistory = new AuditHistory(audit, "tm_product_definition_unapproved");

                    if (productDefinitionUnapprovedDB == null) {//fresh record in unapprove collection.
                        boolean approveStatus = productDefinitionDBResult.getProductDefinition().getStatus().equals(productDefinitionDB.getProductDefinition().getStatus());
                        if (approveStatus) { //status has not changed .
                            productDefinitionDB.setAuditInfo(productDefinitionDBResult.getAuditInfo());//adding the auditInfo
                            productDefinitionDB.addAuditInfo(audit);
                            productDefinitionDB.getProductDefinition().setVersion(productDefinitionDBResult.getProductDefinition().getVersion() + 1);
                            auditHistory.addLastStatus(productDefinitionDBResult);//last version record.
                        } else {
                            //cannot change the status during edit.
                            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MPD009", language));
                        }
                    } else {//updating the previous record in unapprove collection.
                        boolean unapproveStatus = productDefinitionUnapprovedDB.getProductDefinition().getStatus().equals(productDefinitionDB.getProductDefinition().getStatus());
                        if (unapproveStatus) { //status has not changed .
                            productDefinitionDB.setAuditInfo(productDefinitionUnapprovedDB.getAuditInfo());
                            productDefinitionDB.addAuditInfo(audit);
                            productDefinitionDB.getProductDefinition().setVersion(productDefinitionUnapprovedDB.getProductDefinition().getVersion() + 1);
                            productDefinitionDB.set_id(productDefinitionUnapprovedDB.get_id());//setting the mongo ID.
                            auditHistory.addLastStatus(productDefinitionUnapprovedDB);//old version record.
                        } else {
                            //cannot change the status during edit.
                            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MPD009", language));
                        }
                    }
                    auditHistory.addContent(mongoTemplate.save(new ProductDefinitionUnapprovedDB(productDefinitionDB)));
                    mongoTemplate.insert(auditHistory);
                } else { // 4eyeRequired = false adding to the main collection.

                    if (productDefinitionDBResult != null) {//record must exist to update.
                        boolean approveStatus = productDefinitionDBResult.getProductDefinition().getStatus().equals(productDefinitionDB.getProductDefinition().getStatus());
                        if (approveStatus) { //status has not changed .
                            AuditHistory auditHistory = new AuditHistory(audit, "tm_product_definition");

                            productDefinitionDB.setAuditInfo(productDefinitionDBResult.getAuditInfo());
                            productDefinitionDB.addAuditInfo(audit);
                            productDefinitionDB.getProductDefinition().setVersion(productDefinitionDBResult.getProductDefinition().getVersion() + 1);
                            productDefinitionDB.set_id(productDefinitionDBResult.get_id());//setting the mongo ID.

                            auditHistory.addLastStatus(productDefinitionDBResult);//last version record.
                            auditHistory.addContent(mongoTemplate.save(productDefinitionDB));
                            mongoTemplate.save(auditHistory);
                        }else {
                            //cannot change the status during edit.
                            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MPD009", language));
                        }
                    } else {
                        //error record does not exist to update.
                        newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MPD006", language));
                    }
                }
                newReturnObject.PerformReturnObject(null);
                newReturnObject.setReturncode(1);
            }else {
                //error record does not exist to update.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MPD006", language));
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MPD002", language));
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject approveProductDefinition(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            String productId=tableQuery.getPrimaryKey();
            ProductDefinitionUnapprovedDB unApprovedResult = findUnapprovedByProductId(productId);
            ProductDefinitionDB productDefinitionDB = findByProductId(productId);


            Audit audit = audit = getLastAuditUnapproved(productId, newReturnObject, language);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            audit.setApproverId(userId);
            if (!audit.getCreatorId().equals(userId)) {//creator and approver cannot be same.

                unApprovedResult.addAuditInfo(audit); //setting the audit.
                if (unApprovedResult != null) {//record exist to approve.
                    AuditHistory auditHistory = new AuditHistory(audit, "tm_product_definition");

                    if (productDefinitionDB == null) {
                        // data is not present in main collection, create new record
                        auditHistory.addLastStatus(null);
                        auditHistory.addContent(mongoTemplate.insert(new ProductDefinitionDB(unApprovedResult)));
                    } else {
                        String mongoId = productDefinitionDB.get_id();
                        auditHistory.addLastStatus(productDefinitionDB);//old version
                        productDefinitionDB = new ProductDefinitionDB(unApprovedResult);
                        productDefinitionDB.set_id(mongoId);
                        auditHistory.addContent(mongoTemplate.save(productDefinitionDB));
                    }
                    newReturnObject.PerformReturnObject(null);
                    mongoTemplate.insert(auditHistory);
                    newReturnObject = rejectByProductId(tableQuery, userId, newReturnObject, language);
                } else {//no record to approve.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MPD004", language));
                }
            } else {//approverID and creatorID are same.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MPD008", language));
            }

        } catch (Exception ex) {

            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MPD002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject rejectByProductId(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            String productId =tableQuery.getPrimaryKey();
            ProductDefinitionUnapprovedDB productDefinitionUnapprovedDB = findUnapprovedByProductId(productId);
            Audit audit = getLastAuditUnapproved(productId, newReturnObject, language);
            audit.setApproverId(userId);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            AuditHistory auditHistory = new AuditHistory(audit, "tm_product_definition_unapproved");
            auditHistory.addContent(productDefinitionUnapprovedDB);

            Query query = new Query();
            query.addCriteria(Criteria.where("productDefinition.productId").is(productId));
            mongoTemplate.remove(query, ProductDefinitionUnapprovedDB.class);//deleting the record from the unapproved collection.
            mongoTemplate.insert(auditHistory);//recording to the auditHistory.

            newReturnObject.setReturncode(1);
            newReturnObject.PerformReturnObject("Success");
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MPD002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject viewByProductId(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language) {
        try {
            String productId=tableQuery.getPrimaryKey();
            Criteria criteriaEventID= Criteria.where("productDefinition.productId").is(productId);
            if (tableQuery.getCollection().equals("approved")) {
                ProductDefinitionDB productDefinitionDB = mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriaEventID)), ProductDefinitionDB.class);
                newReturnObject.PerformReturnObject(productDefinitionDB);
            } else if (tableQuery.getCollection().equals("unapproved")) {
                ProductDefinitionUnapprovedDB productDefinitionUnapprovedDB = mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriaEventID)), ProductDefinitionUnapprovedDB.class);
                newReturnObject.PerformReturnObject(productDefinitionUnapprovedDB);
            } else {
                newReturnObject.setReturncode(0);
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MPD010", language));
            }

        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MPD002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());

        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject productDefinitionStatusChanger(ReactTableQuery tableQuery, String headerUserId, NewReturnObject newReturnObject, String language, String setStatus, Boolean approvalRequired) {
        try {
            Audit audit = new Audit(headerUserId, Instant.now().toEpochMilli(), "", 0.0);
            String productId = tableQuery.getPrimaryKey();
            ProductDefinitionDB productDefinitionDBResult = findByProductId(productId);
            ProductDefinitionUnapprovedDB productDefinitionUnapprovedDB = findUnapprovedByProductId(productId);
            if (approvalRequired) {//4EyeRequired=true .

                if (productDefinitionDBResult != null || productDefinitionUnapprovedDB != null) {//record must exist to alter status.

                    AuditHistory auditHistory = new AuditHistory(audit, "tm_product_definition_unapproved");
                    ProductDefinitionDB productDefinitionDB = productDefinitionDBResult != null ? productDefinitionDBResult : new ProductDefinitionDB(productDefinitionUnapprovedDB);//take the existing record.
                    productDefinitionDB.getProductDefinition().setVersion(productDefinitionDB.getProductDefinition().getVersion() + 1);
                    productDefinitionDB.addAuditInfo(audit);
                    auditHistory.addLastStatus(productDefinitionDBResult);//last version record.
                    String status = productDefinitionDB.getProductDefinition().getStatus();
                    productDefinitionDB.getProductDefinition().setStatus(setStatus);
                    auditHistory.addContent(mongoTemplate.save(new ProductDefinitionUnapprovedDB(productDefinitionDB)));
                    mongoTemplate.insert(auditHistory);
                    newReturnObject.PerformReturnObject("Success");
                    newReturnObject.setReturncode(1);
                } else {
                    //error record does not exist to update.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MPD006", language));

                }
            } else {
                //approval not required inserting to the main collection
                if (productDefinitionDBResult != null) {//record must exist to alter status.
                    AuditHistory auditHistory = new AuditHistory(audit, "tm_product_definition");
                    ProductDefinitionDB productDefinitionDB = productDefinitionDBResult;
                    productDefinitionDB.getProductDefinition().setVersion(productDefinitionDB.getProductDefinition().getVersion() + 1);
                    productDefinitionDB.addAuditInfo(audit);
                    auditHistory.addLastStatus(productDefinitionDBResult);//last version record.
                    productDefinitionDB.getProductDefinition().setStatus(setStatus);
                    auditHistory.addContent(mongoTemplate.save(productDefinitionDB));
                    mongoTemplate.insert(auditHistory);
                    newReturnObject.PerformReturnObject("Success");
                    newReturnObject.setReturncode(1);
                } else {
                    //error record does not exist to update.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MPD006", language));
                }
            }
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MPD002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public ProductDefinitionUnapprovedDB findUnapprovedByProductId(String productId) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("productDefinition.productId").is(productId));
            return mongoTemplate.findOne(query, ProductDefinitionUnapprovedDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "findByProductId");
            return null;
        }
    }

    @Override
    public ProductDefinitionDB findByProductId(String productId) {
        try{
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));
            query.addCriteria(Criteria.where("productDefinition.productId").is(productId));
            return mongoTemplate.findOne(query, ProductDefinitionDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "ProductDefinitionUnapprovedDB");
            return null;
        }
    }

    @Override
    public long countByProductId(String productId) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            query.addCriteria(Criteria.where("productDefinition.productId").is(productId));
            return mongoTemplate.count(query,ProductDefinitionDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByProductId");
            return 0;
        }
    }

    @Override
    public long countByUnapprovedProductId(String productId) {
        try {
            Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        query.addCriteria(Criteria.where("productDefinition.productId").is(productId));
            return mongoTemplate.count(query, ProductDefinitionUnapprovedDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByUnapprovedProductId");
            return 0;
        }
    }


    public Audit getLastAuditUnapproved(String productId, NewReturnObject newReturnObject, String language) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("productDefinition.productId").is(productId));

            List<Audit> audit = mongoTemplate.findOne(query, ProductDefinitionUnapprovedDB.class).getAuditInfo();

            return audit.get(audit.size() - 1);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MPD002", language));
            return null;
        }
    }
}
