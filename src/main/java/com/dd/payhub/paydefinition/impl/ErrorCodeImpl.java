package com.dd.payhub.paydefinition.impl;

import com.dd.payhub.paycommon.view.AppError;
import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
import com.dd.payhub.paydefinition.entity.ErrorCodeDB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ErrorCodeImpl implements ErrorCodeDAL {

    private static final Logger logger = LogManager.getLogger(ErrorCodeImpl.class);

    private final MongoTemplate mongoTemplate;

    @Autowired
    public ErrorCodeImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public AppError findByErrorCodeAndLanguage(String errorCode, String language) {
        Query query = new Query();
        query.addCriteria(Criteria.where("error.errorCode").is(errorCode).andOperator(Criteria.where("error.language").is(language)));
        ErrorCodeDB errorDetails = mongoTemplate.findOne(query, ErrorCodeDB.class);
        AppError appError = new AppError(errorCode, errorDetails.getError().getErrorDesc(), errorDetails.getError().getErrorType());
        return appError;
    }

    @Override
    public List<AppError> findByErrorCodeAndLanguage(String[] errorCode, String language) {
        Query query = new Query();
        query.addCriteria(Criteria.where("error.errorCode").in(errorCode).andOperator(Criteria.where("error.language").is(language)));
        List<ErrorCodeDB> errorDetails = mongoTemplate.find(query, ErrorCodeDB.class);
        List<AppError> appErrors = new ArrayList<AppError>();
        for (int i = 0; i < errorDetails.size(); i++) {
            appErrors.add(new AppError(errorDetails.get(i).getError().getErrorCode(), errorDetails.get(i).getError().getErrorDesc(), errorDetails.get(i).getError().getErrorType()));
        }
        return appErrors;
    }

}
