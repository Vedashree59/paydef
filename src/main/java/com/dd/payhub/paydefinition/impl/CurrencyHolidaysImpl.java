package com.dd.payhub.paydefinition.impl;

import com.dd.payhub.paycommon.model.*;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.dal.CurrencyHolidaysDAL;
import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
import com.dd.payhub.paydefinition.entity.*;
import com.dd.payhub.paydefinition.entity.CurrencyHolidaysUnapprovedDB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Collation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;


@Repository
public class CurrencyHolidaysImpl implements CurrencyHolidaysDAL {


    private static final Logger logger = LogManager.getLogger(CurrencyHolidaysImpl.class);
    private final MongoTemplate mongoTemplate;
    private ErrorCodeDAL errorCodeDAL;

    @Autowired

    public CurrencyHolidaysImpl(MongoTemplate mongoTemplate, ErrorCodeDAL errorCodeDAL) {
        this.mongoTemplate = mongoTemplate;
        this.errorCodeDAL = errorCodeDAL;
    }

    @Override
    public ReactTableRecord getAllCurrencyHolidaysByPagination(ReactTableQuery tableQuery) {
        int page = 0;
        int pageSize = 5;
        int totalPage;
        long totalRecord = 0;
        ReactTableRecord tableRecord = new ReactTableRecord();
        String sortField=tableQuery.getSortField().isEmpty()?"sortDate":tableQuery.getSortField();

        Query query = new Query();//if
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.

        try {
            if (tableQuery.getPage() != 0) {
                page = tableQuery.getPage();
            }
            if (tableQuery.getPageSize() != 0) {
                pageSize = tableQuery.getPageSize();
            }
            //sorting order code.
            if(!tableQuery.getSortOrder().equals("true")){//false means order by asc.
                query.with(Sort.by(Sort.Direction.DESC,sortField));
            }
            else {
                query.with(Sort.by(Sort.Direction.ASC,sortField));
            }
            Criteria statusCriteria = Criteria.where("currencyHolidays.status").is(tableQuery.getStatus());
            query.addCriteria(Criteria.where("currencyHolidays.status").is(tableQuery.getStatus()));

            if (!tableQuery.getKeyword().isEmpty()) {
                Criteria yearCriteria = Criteria.where("currencyHolidays.year").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria currencyCodeCriteria = Criteria.where("currencyHolidays.currencyCode").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria TenantIdCriteria = Criteria.where("currencyHolidays.tenantId").regex(String.valueOf(tableQuery.getKeyword()), "i");
                query.addCriteria(new Criteria().orOperator(TenantIdCriteria, currencyCodeCriteria, yearCriteria).andOperator(statusCriteria));
            }
            if (tableQuery.getCollection().equals("approved")) {
                totalRecord = mongoTemplate.count(query, CurrencyHolidaysDB.class);

                tableRecord.setTableRecord(mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), CurrencyHolidaysDB.class));
            } else {
                totalRecord = mongoTemplate.find(query, CurrencyHolidaysUnapprovedDB.class).size();

                tableRecord.setTableRecord((mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), CurrencyHolidaysUnapprovedDB.class)));
            }
            totalPage = (int) Math.ceil(totalRecord / (float) pageSize);
            tableRecord.setTotalPage(totalPage);
            tableRecord.setTotalRecord(totalRecord);
            return tableRecord;
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage());
            tableRecord.setTotalPage(0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord(new ArrayList<>());
            return tableRecord;
        }
    }

    @Override
    public NewReturnObject addCurrencyHolidays(CurrencyHolidays currencyHolidays, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired) {
        try {
            CurrencyHolidaysDB currencyHolidaysDB = new CurrencyHolidaysDB();
            currencyHolidaysDB.setCurrencyHolidays(currencyHolidays);
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            currencyHolidaysDB.addAuditInfo(audit);
            currencyHolidaysDB.getCurrencyHolidays().setVersion(0);//setting the version.
            AuditHistory auditHistory;

            currencyHolidaysDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
            //            inserting in the database.
            if (approvalRequired) {
                //4eyeRequired = true  add to unapproved collection.
                auditHistory = new AuditHistory(audit, "tm_currency_holidays_unapproved");
                auditHistory.addContent(mongoTemplate.insert(new CurrencyHolidaysUnapprovedDB(currencyHolidaysDB)));
            } else {
                // 4eyeRequired = false adding to the main collection.
                auditHistory = new AuditHistory(audit, "tm_currency_holidays");
                auditHistory.addContent(mongoTemplate.insert(currencyHolidaysDB));
            }
            auditHistory.addLastStatus(null);
            mongoTemplate.insert(auditHistory);
            newReturnObject.PerformReturnObject("Success");
            return newReturnObject;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYH02", language));
            newReturnObject.setReturncode(0);
            return newReturnObject;
        }
    }

    @Override
    public NewReturnObject getEditableCurrencyHolidays(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language) {
        try {
            String tenantId=reactTableQuery.getQueryFields().get("tenantId").toString();
            String currencyCode=reactTableQuery.getQueryFields().get("currencyCode").toString();
            String year=reactTableQuery.getQueryFields().get("year").toString();
            //gives approved record only when there are no unapproved records.

            //gives approved record only when there are no unapproved records.
            if (reactTableQuery.getCollection().equals("approved")) {
                if ((countByTenantIdCurrencyCodeYear(tenantId,currencyCode,year)) == 0) {
                    newReturnObject.PerformReturnObject(findByTenantIdCurrencyCodeYear(tenantId,currencyCode,year));
                } else {
                    newReturnObject.PerformReturnObject(new ArrayList<>());
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYH07", language));
                }
            } else if (reactTableQuery.getCollection().equals("unapproved")) {
                newReturnObject.PerformReturnObject(findUnapprovedByTenantIdCurrencyCodeYear(tenantId,currencyCode,year));
            }
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYH02", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject updateCurrencyHolidays(CurrencyHolidays currencyHolidays, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired) {
        try {
            CurrencyHolidaysDB currencyHolidaysDB=new CurrencyHolidaysDB();
            currencyHolidaysDB.setCurrencyHolidays(currencyHolidays);

            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
//
            CurrencyHolidaysDB currencyHolidaysDBResult = findByTenantIdCurrencyCodeYear(currencyHolidaysDB.getCurrencyHolidays().getTenantId(), currencyHolidaysDB.getCurrencyHolidays().getCurrencyCode(), currencyHolidaysDB.getCurrencyHolidays().getYear());
            CurrencyHolidaysUnapprovedDB currencyHolidaysUnapprovedDB = findUnapprovedByTenantIdCurrencyCodeYear(currencyHolidaysDB.getCurrencyHolidays().getTenantId(), currencyHolidaysDB.getCurrencyHolidays().getCurrencyCode(), currencyHolidaysDB.getCurrencyHolidays().getYear());
            if (currencyHolidaysDBResult != null || currencyHolidaysUnapprovedDB != null) {//record must exist to update.

                if (approvalRequired) {//4eyeRequired = true  add to unapproved collection.

                    AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_holidays_unapproved");

                    if (currencyHolidaysUnapprovedDB == null) {//fresh record in unapprove collection.
                        boolean approveStatus = currencyHolidaysDBResult.getCurrencyHolidays().getStatus().equals(currencyHolidaysDB.getCurrencyHolidays().getStatus());
                        if (approveStatus) { //status has not changed .
                            currencyHolidaysDB.setAuditInfo(currencyHolidaysDBResult.getAuditInfo());//adding the auditInfo
                            currencyHolidaysDB.addAuditInfo(audit);
                            currencyHolidaysDB.getCurrencyHolidays().setVersion(currencyHolidaysDBResult.getCurrencyHolidays().getVersion() + 1);
                            auditHistory.addLastStatus(currencyHolidaysDBResult);//last version record.
                        } else {
                            //cannot change the status during edit.
                            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYH09", language));
                        }
                    } else {//updating the previous record in unapprove collection.
                        boolean unapproveStatus = currencyHolidaysUnapprovedDB.getCurrencyHolidays().getStatus().equals(currencyHolidaysDB.getCurrencyHolidays().getStatus());
                        if (unapproveStatus) { //status has not changed .
                            currencyHolidaysDB.setAuditInfo(currencyHolidaysUnapprovedDB.getAuditInfo());
                            currencyHolidaysDB.addAuditInfo(audit);
                            currencyHolidaysDB.getCurrencyHolidays().setVersion(currencyHolidaysUnapprovedDB.getCurrencyHolidays().getVersion() + 1);
                            currencyHolidaysDB.set_id(currencyHolidaysUnapprovedDB.get_id());//setting the mongo ID.
                            auditHistory.addLastStatus(currencyHolidaysUnapprovedDB);//old version record.
                        } else {
                            //cannot change the status during edit.
                            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYH09", language));
                        }
                    }
                    auditHistory.addContent(mongoTemplate.save(new CurrencyHolidaysUnapprovedDB(currencyHolidaysDB)));
                    mongoTemplate.insert(auditHistory);
                } else { // 4eyeRequired = false adding to the main collection.

                    if (currencyHolidaysDBResult != null) {//record must exist to update.
                        boolean approveStatus = currencyHolidaysDBResult.getCurrencyHolidays().getStatus().equals(currencyHolidaysDB.getCurrencyHolidays().getStatus());
                        if (approveStatus) { //status has not changed .
                            AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_holidays");

                            currencyHolidaysDB.setAuditInfo(currencyHolidaysDBResult.getAuditInfo());
                            currencyHolidaysDB.addAuditInfo(audit);
                            currencyHolidaysDB.getCurrencyHolidays().setVersion(currencyHolidaysDBResult.getCurrencyHolidays().getVersion() + 1);
                            currencyHolidaysDB.set_id(currencyHolidaysDBResult.get_id());//setting the mongo ID.

                            auditHistory.addLastStatus(currencyHolidaysDBResult);//last version record.
                            auditHistory.addContent(mongoTemplate.save(currencyHolidaysDB));
                            mongoTemplate.save(auditHistory);
                        }else {
                            //cannot change the status during edit.
                            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYH09", language));
                        }
                    } else {
                        //error record does not exist to update.
                        newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYH06", language));
                    }
                }
                newReturnObject.PerformReturnObject(null);
                newReturnObject.setReturncode(1);
            }else {
                //error record does not exist to update.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYH06", language));
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYH02", language));
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject approveCurrencyHolidays(ReactTableQuery reactTableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            String tenantId=reactTableQuery.getQueryFields().get("tenantId").toString();
            String currencyCode=reactTableQuery.getQueryFields().get("currencyCode").toString();
            String year=reactTableQuery.getQueryFields().get("year").toString();
            CurrencyHolidaysUnapprovedDB unApprovedResult = findUnapprovedByTenantIdCurrencyCodeYear(tenantId, currencyCode, year);
            CurrencyHolidaysDB currencyHolidaysDB = findByTenantIdCurrencyCodeYear(tenantId, currencyCode, year);


            Audit audit = audit = getLastAuditUnapproved(tenantId, currencyCode, year, newReturnObject, language);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            audit.setApproverId(userId);
            if (!audit.getCreatorId().equals(userId)) {//creator and approver cannot be same.

                unApprovedResult.addAuditInfo(audit);
                if (unApprovedResult != null) {//record exist to update.
                    AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_holidays");

                    if (currencyHolidaysDB == null) {
                        //  data is not present, create new record
                        auditHistory.addLastStatus(null);
                        unApprovedResult.addAuditInfo(audit);
                        auditHistory.addContent(mongoTemplate.insert(new CurrencyHolidaysDB(unApprovedResult)));
                    } else {
                        String mongoId = currencyHolidaysDB.get_id();
                        auditHistory.addLastStatus(currencyHolidaysDB);//old version
                        currencyHolidaysDB = new CurrencyHolidaysDB(unApprovedResult);
                        currencyHolidaysDB.set_id(mongoId);//setting the mongoID
                        currencyHolidaysDB.addAuditInfo(audit);//setting the audit.
                        auditHistory.addContent(mongoTemplate.save(currencyHolidaysDB));
                    }
                    mongoTemplate.insert(auditHistory);
                    newReturnObject = rejectByTenantIdCurrencyCodeYear(reactTableQuery, userId, newReturnObject, language);
                } else {//no record to approve.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYH04", language));
                }
            } else {//approverID and creatorID are same.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYH08", language));
            }

        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYH02", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject rejectByTenantIdCurrencyCodeYear(ReactTableQuery reactTableQuery, String userid, NewReturnObject newReturnObject, String language) {
        try {
            String tenantId=reactTableQuery.getQueryFields().get("tenantId").toString();
            String currencyCode=reactTableQuery.getQueryFields().get("currencyCode").toString();
            String year=reactTableQuery.getQueryFields().get("year").toString();
            CurrencyHolidaysUnapprovedDB currencyHolidaysUnapprovedDB = findUnapprovedByTenantIdCurrencyCodeYear(tenantId, currencyCode, year);
            Audit audit = getLastAuditUnapproved(tenantId, currencyCode, year, newReturnObject, language);
            audit.setApproverId(userid);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_holidays_unapproved");
            auditHistory.addContent(currencyHolidaysUnapprovedDB);

            Query query = new Query();
            Criteria yearCriteria = Criteria.where("currencyHolidays.year").is(year);
            Criteria currencyHolidaysCodeCriteria = Criteria.where("currencyHolidays.currencyCode").is(currencyCode);
            Criteria tenantIdCriteria = Criteria.where("currencyHolidays.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, currencyHolidaysCodeCriteria, yearCriteria));
            mongoTemplate.remove(query, CurrencyHolidaysUnapprovedDB.class);
            mongoTemplate.insert(auditHistory);

            newReturnObject.setReturncode(1);
            newReturnObject.PerformReturnObject("Success");
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYH02", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject viewByTenantIdCurrencyCodeYear(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language) {
        try {
            String tenantId=tableQuery.getQueryFields().get("tenantId").toString();
            String currencyCode=tableQuery.getQueryFields().get("currencyCode").toString();
            String year=tableQuery.getQueryFields().get("year").toString();

            Criteria criteriaTenant = Criteria.where("currencyHolidays.tenantId").is(tenantId);
            Criteria criteriaCurrencyCode = Criteria.where("currencyHolidays.currencyCode").is(currencyCode);
            Criteria criteriaYear = Criteria.where("currencyHolidays.year").is(year);
            if (tableQuery.getCollection().equals("approved")) {
                CurrencyHolidaysDB currencyHolidaysDB = mongoTemplate.findOne(Query.query(new Criteria().andOperator(criteriaTenant,criteriaCurrencyCode,criteriaYear)), CurrencyHolidaysDB.class);
                newReturnObject.PerformReturnObject(currencyHolidaysDB);
            } else if (tableQuery.getCollection().equals("unapproved")) {
                CurrencyHolidaysUnapprovedDB currencyHolidaysUnapprovedDB = mongoTemplate.findOne(Query.query(new Criteria().orOperator(criteriaTenant,criteriaCurrencyCode,criteriaYear)), CurrencyHolidaysUnapprovedDB.class);
                newReturnObject.PerformReturnObject(currencyHolidaysUnapprovedDB);
            } else {
                newReturnObject.setReturncode(0);
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYH10", language));
            }

        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYH02", language));
        }
        return newReturnObject;
    }
    @Override
    public NewReturnObject currencyHolidaysStatusChanger(ReactTableQuery tableQuery, String headerUserId, NewReturnObject newReturnObject, String language, String setStatus, Boolean approvalRequired) {

        try {
            Audit audit = new Audit(headerUserId, Instant.now().toEpochMilli(), "", 0.0);

            String tenantId=tableQuery.getQueryFields().get("tenantId").toString();
            String currencyCode=tableQuery.getQueryFields().get("currencyCode").toString();
            String year=tableQuery.getQueryFields().get("year").toString();
            CurrencyHolidaysDB currencyHolidaysDBResult = findByTenantIdCurrencyCodeYear(tenantId,currencyCode,year);
            CurrencyHolidaysUnapprovedDB currencyHolidaysUnapprovedDB = findUnapprovedByTenantIdCurrencyCodeYear(tenantId,currencyCode,year);
            if (approvalRequired) {//4EyeRequired=true .

                if (currencyHolidaysDBResult != null || currencyHolidaysUnapprovedDB != null) {//record must exist to alter status.

                    AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_holidays_unapproved");
                    CurrencyHolidaysDB eventDefinitionDB = currencyHolidaysDBResult != null ? currencyHolidaysDBResult : new CurrencyHolidaysDB(currencyHolidaysUnapprovedDB);//take the existing record.
                    eventDefinitionDB.getCurrencyHolidays().setVersion(eventDefinitionDB.getCurrencyHolidays().getVersion() + 1);
                    eventDefinitionDB.addAuditInfo(audit);
                    auditHistory.addLastStatus(currencyHolidaysDBResult);//last version record.
                    String status = eventDefinitionDB.getCurrencyHolidays().getStatus();
                    eventDefinitionDB.getCurrencyHolidays().setStatus(setStatus);
                    auditHistory.addContent(mongoTemplate.save(new CurrencyHolidaysUnapprovedDB(eventDefinitionDB)));
                    mongoTemplate.insert(auditHistory);
                    newReturnObject.PerformReturnObject("Success");
                    newReturnObject.setReturncode(1);
                } else {
                    //error record does not exist to update.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYH06", language));

                }
            } else {
                //approval not required inserting to the main collection
                if (currencyHolidaysDBResult != null) {//record must exist to alter status.
                    AuditHistory auditHistory = new AuditHistory(audit, "tm_currency_holidays");
                    CurrencyHolidaysDB currencyHolidaysDB = currencyHolidaysDBResult;
                    currencyHolidaysDB.getCurrencyHolidays().setVersion(currencyHolidaysDB.getCurrencyHolidays().getVersion() + 1);
                    currencyHolidaysDB.addAuditInfo(audit);
                    auditHistory.addLastStatus(currencyHolidaysDBResult);//last version record.
                    currencyHolidaysDB.getCurrencyHolidays().setStatus(setStatus);
                    auditHistory.addContent(mongoTemplate.save(currencyHolidaysDB));
                    mongoTemplate.insert(auditHistory);
                    newReturnObject.PerformReturnObject("Success");
                    newReturnObject.setReturncode(1);
                } else {
                    //error record does not exist to update.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYH06", language));
                }
            }
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYH02", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public CurrencyHolidaysDB findByTenantIdCurrencyCodeYear(String tenantId, String currencyCode, String year) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            Criteria yearCriteria = Criteria.where("currencyHolidays.year").is(year);
            Criteria currencyHolidaysCodeCriteria = Criteria.where("currencyHolidays.currencyCode").is(currencyCode);
            Criteria tenantIdCriteria = Criteria.where("currencyHolidays.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, currencyHolidaysCodeCriteria, yearCriteria));
            return mongoTemplate.findOne(query, CurrencyHolidaysDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByCurrencyHolidaysCode");
            return null;
        }
    }

    @Override
    public CurrencyHolidaysUnapprovedDB findUnapprovedByTenantIdCurrencyCodeYear(String tenantId, String currencyCode, String year) {
        try{
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            Criteria yearCriteria = Criteria.where("currencyHolidays.year").is(year);
            Criteria currencyHolidaysCodeCriteria = Criteria.where("currencyHolidays.currencyCode").is(currencyCode);
            Criteria tenantIdCriteria = Criteria.where("currencyHolidays.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, currencyHolidaysCodeCriteria, yearCriteria));
            return mongoTemplate.findOne(query, CurrencyHolidaysUnapprovedDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByCurrencyHolidaysCode");
            return null;
        }
    }


    public long countByTenantIdCurrencyCodeYear(String tenantId, String currencyCode, String year) {
        try{
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            Criteria yearCriteria = Criteria.where("currencyHolidays.year").is(year);
            Criteria currencyHolidaysCodeCriteria = Criteria.where("currencyHolidays.currencyCode").is(currencyCode);
            Criteria tenantIdCriteria = Criteria.where("currencyHolidays.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, currencyHolidaysCodeCriteria, yearCriteria));
            return mongoTemplate.count(query, CurrencyHolidaysUnapprovedDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByCurrencyHolidaysCode");
            return 0;
        }
    }

    @Override
    public long countByUnapprovedTenantIdCurrencyCodeYear(String tenantId, String currencyCode, String year) {
        try{
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            Criteria yearCriteria = Criteria.where("currencyHolidays.year").is(year);
            Criteria currencyHolidaysCodeCriteria = Criteria.where("currencyHolidays.currencyCode").is(currencyCode);
            Criteria tenantIdCriteria = Criteria.where("currencyHolidays.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, currencyHolidaysCodeCriteria, yearCriteria));
            return mongoTemplate.count(query, CurrencyHolidaysUnapprovedDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "countByCurrencyHolidaysCode");
            return 0;
        }
    }


    public Audit getLastAuditUnapproved(String tenantId, String currencyCode, String year, NewReturnObject newReturnObject, String language) {
        try {
            Query query = new Query();
            Criteria yearCriteria = Criteria.where("currencyHolidays.year").is(year);
            Criteria currencyHolidaysCodeCriteria = Criteria.where("currencyHolidays.currencyCode").is(currencyCode);
            Criteria tenantIdCriteria = Criteria.where("currencyHolidays.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, currencyHolidaysCodeCriteria, yearCriteria));

            List<Audit> audit = mongoTemplate.findOne(query, CurrencyHolidaysUnapprovedDB.class).getAuditInfo();

            return audit.get(audit.size() - 1);
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCYH02", language));
            return null;
        }
    }
}
