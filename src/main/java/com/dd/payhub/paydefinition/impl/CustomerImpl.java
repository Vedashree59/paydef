package com.dd.payhub.paydefinition.impl;

import com.dd.payhub.paycommon.model.*;

import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.dal.CustomerDAL;
import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
import com.dd.payhub.paydefinition.entity.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Collation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerImpl implements CustomerDAL {

    private static final Logger logger = LogManager.getLogger(CustomerImpl.class);
    private final MongoTemplate mongoTemplate;
    private ErrorCodeDAL errorCodeDAL;

    @Autowired
    public CustomerImpl(MongoTemplate mongoTemplate, ErrorCodeDAL errorCodeDAL) {
        this.mongoTemplate = mongoTemplate;
        this.errorCodeDAL = errorCodeDAL;
    }

    @Override
    public ReactTableRecord getAllCustomerByPagination(ReactTableQuery tableQuery) {
        int page = 0;
        int pageSize = 5;
        int totalPage;
        long totalRecord = 0;
        ReactTableRecord tableRecord = new ReactTableRecord();
        String sortField = tableQuery.getSortField().isEmpty() ? "sortDate" : tableQuery.getSortField();

        Query query = new Query();//if
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.

        try {
            if (tableQuery.getPage() != 0) {
                page = tableQuery.getPage();
            }
            if (tableQuery.getPageSize() != 0) {
                pageSize = tableQuery.getPageSize();
            }
            //sorting order code.
            if (!tableQuery.getSortOrder().equals("true")) {//false means order by asc.
                query.with(Sort.by(Sort.Direction.DESC, sortField));
            } else {
                query.with(Sort.by(Sort.Direction.ASC, sortField));
            }
            Criteria statusCriteria = Criteria.where("customer.status").is(tableQuery.getStatus());
            query.addCriteria(Criteria.where("customer.status").is(tableQuery.getStatus()));

            if (!tableQuery.getKeyword().isEmpty()) {
                Criteria customerIdCriteria = Criteria.where("customer.customerId").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria customerShortNameCriteria = Criteria.where("customer.shortName").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria customerFullNameCriteria = Criteria.where("customer.fullName").regex(String.valueOf(tableQuery.getKeyword()), "i");
                query.addCriteria(new Criteria().orOperator(customerIdCriteria, customerShortNameCriteria, customerFullNameCriteria).andOperator(statusCriteria));
            }
            if (tableQuery.getCollection().equals("approved")) {
                totalRecord = mongoTemplate.count(query, CustomerDB.class);

                tableRecord.setTableRecord(mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), CustomerDB.class));
            } else {
                totalRecord = mongoTemplate.find(query, CustomerUnapprovedDB.class).size();

                tableRecord.setTableRecord((mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), CustomerUnapprovedDB.class)));
            }
            totalPage = (int) Math.ceil(totalRecord / (float) pageSize);
            tableRecord.setTotalPage(totalPage);
            tableRecord.setTotalRecord(totalRecord);
            return tableRecord;
        } catch (Exception ex) {
            tableRecord.setTotalPage(0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord(new ArrayList<>());
            logger.error(ex.getMessage());
            return tableRecord;
        }
    }

    @Override
    public NewReturnObject addCustomer(Customer customer, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired) {
        try {
            CustomerDB customerDB = new CustomerDB();
            customerDB.setCustomer(customer);
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            customerDB.addAuditInfo(audit);
            customerDB.getCustomer().setVersion(0);//setting the version.
            AuditHistory auditHistory;

            customerDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.
            //            inserting in the database.
            if (approvalRequired) {
                //4eyeRequired = true  add to unapproved collection.
                auditHistory = new AuditHistory(audit, "tm_customer_unapproved");
                auditHistory.addContent(mongoTemplate.insert(new CustomerUnapprovedDB(customerDB)));
            } else {
                // 4eyeRequired = false adding to the main collection.
                auditHistory = new AuditHistory(audit, "tm_customer");
                auditHistory.addContent(mongoTemplate.insert(customerDB));
            }
            auditHistory.addLastStatus(null);
            mongoTemplate.insert(auditHistory);
            newReturnObject.PerformReturnObject("Success");
            return newReturnObject;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU002", language));
            newReturnObject.setReturncode(0);
            return newReturnObject;
        }
    }

    @Override
    public NewReturnObject getEditableCustomer(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language) {
        try {
            //superKey
            String tenantId = reactTableQuery.getQueryFields().get("tenantId").toString();
            String customerId= reactTableQuery.getQueryFields().get("customerId").toString();
            //gives approved record only when there are no unapproved records.
            if (reactTableQuery.getCollection().equals("approved")) {
                if (countUnapprovedByTenantIdCustomerId(tenantId,customerId) == 0) {
                    newReturnObject.PerformReturnObject(findByTenantIdCustomerId(tenantId,customerId));
                } else {
                    newReturnObject.PerformReturnObject(new ArrayList<>());
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU007", language));
                }
            } else if (reactTableQuery.getCollection().equals("unapproved")) {
                newReturnObject.PerformReturnObject(findUnapprovedByTenantIdCustomerId(tenantId,customerId));
            }
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject updateCustomer(Customer customer, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired) {
        try {
            //superKey
            String tenantId =customer.getTenantId();
            String customerId= customer.getCustomerId() ;
            CustomerDB customerDB = new CustomerDB();
            customerDB.setCustomer(customer);
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            CustomerDB customerDBResult = findByTenantIdCustomerId(tenantId,customerId);
            CustomerUnapprovedDB customerUnapprovedDB = findUnapprovedByTenantIdCustomerId(tenantId,customerId);
            if (customerDBResult != null || customerUnapprovedDB != null) {//record must exist to update.

                    if (approvalRequired) {//4eyeRequired = true  add to unapproved collection.

                        AuditHistory auditHistory = new AuditHistory(audit, "tm_customer_unapproved");

                        if (customerUnapprovedDB == null) {//fresh record in unapprove collection.
                            boolean approveStatus = customerDBResult.getCustomer().getStatus().equals(customerDB.getCustomer().getStatus());
                            if (approveStatus) { //status has not changed .
                                customerDB.setAuditInfo(customerDBResult.getAuditInfo());//adding the auditInfo
                                customerDB.addAuditInfo(audit);
                                customerDB.getCustomer().setVersion(customerDBResult.getCustomer().getVersion() + 1);
                                auditHistory.addLastStatus(customerDBResult);//last version record.
                            } else {
                                //cannot change the status during edit.
                                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU009", language));
                            }
                        } else {//updating the previous record in unapprove collection.
                            boolean unapproveStatus = customerUnapprovedDB.getCustomer().getStatus().equals(customerDB.getCustomer().getStatus());
                            if (unapproveStatus) { //status has not changed .
                                customerDB.setAuditInfo(customerUnapprovedDB.getAuditInfo());
                                customerDB.addAuditInfo(audit);
                                customerDB.getCustomer().setVersion(customerUnapprovedDB.getCustomer().getVersion() + 1);
                                customerDB.set_id(customerUnapprovedDB.get_id());//setting the mongo ID.
                                auditHistory.addLastStatus(customerUnapprovedDB);//old version record.
                            } else {
                                //cannot change the status during edit.
                                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU009", language));
                            }
                        }
                        auditHistory.addContent(mongoTemplate.save(new CustomerUnapprovedDB(customerDB)));
                        mongoTemplate.insert(auditHistory);
                    } else { // 4eyeRequired = false adding to the main collection.

                        if (customerDBResult != null) {//record must exist to update.
                            boolean approveStatus = customerDBResult.getCustomer().getStatus().equals(customerDB.getCustomer().getStatus());
                            if (approveStatus) { //status has not changed .
                                AuditHistory auditHistory = new AuditHistory(audit, "tm_customer");

                                customerDB.setAuditInfo(customerDBResult.getAuditInfo());
                                customerDB.addAuditInfo(audit);
                                customerDB.getCustomer().setVersion(customerDBResult.getCustomer().getVersion() + 1);
                                customerDB.set_id(customerDBResult.get_id());//setting the mongo ID.

                                auditHistory.addLastStatus(customerDBResult);//last version record.
                                auditHistory.addContent(mongoTemplate.save(customerDB));
                                mongoTemplate.save(auditHistory);
                            }else {
                                //cannot change the status during edit.
                                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU009", language));
                            }
                        } else {
                            //error record does not exist to update.
                            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU006", language));
                        }
                    }
                    newReturnObject.PerformReturnObject(null);
                    newReturnObject.setReturncode(1);
            }else {
                //error record does not exist to update.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU006", language));
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU002", language));
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject approveCustomer(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            //superKey
            String tenantId = tableQuery.getQueryFields().get("tenantId").toString();
            String customerId= tableQuery.getQueryFields().get("customerId").toString();
            CustomerUnapprovedDB unApprovedResult = findUnapprovedByTenantIdCustomerId(tenantId,customerId);
            CustomerDB customerDB = findByTenantIdCustomerId(tenantId,customerId);

            Audit audit  = getLastAuditUnapproved(tableQuery, newReturnObject, language);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            audit.setApproverId(userId);
            if (!audit.getCreatorId().equals(userId)) {//creator and approver cannot be same.

                if (unApprovedResult != null) {//record exist to update.
                    AuditHistory auditHistory = new AuditHistory(audit, "tm_customer");

                    if (customerDB == null) {
                        //  data is not present, create new record
                        auditHistory.addLastStatus(null);
                        unApprovedResult.addAuditInfo(audit);
                        auditHistory.addContent(mongoTemplate.insert(new CustomerDB(unApprovedResult)));
                    } else {
                        String mongoId = customerDB.get_id();
                        auditHistory.addLastStatus(customerDB);//old version
                        customerDB = new CustomerDB(unApprovedResult);
                        customerDB.set_id(mongoId);//setting the mongoID
                        customerDB.addAuditInfo(audit);//setting the audit.
                        auditHistory.addContent(mongoTemplate.save(customerDB));
                    }
                    mongoTemplate.insert(auditHistory);
                    newReturnObject = rejectByTenantIdCustomerId(tableQuery, userId, newReturnObject, language);
                } else {//no record to approve.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU004", language));
                }
            } else {//approverID and creatorID are same.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU008", language));
            }

        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject rejectByTenantIdCustomerId(ReactTableQuery tableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            //superKey
            String tenantId = tableQuery.getQueryFields().get("tenantId").toString();
            String customerId= tableQuery.getQueryFields().get("customerId").toString();

            CustomerUnapprovedDB customerUnapprovedDB = findUnapprovedByTenantIdCustomerId(tenantId,customerId);
//            Audit audit = getLastAuditUnapproved(tableQuery, newReturnObject, language);
//            audit.setApproverId(userId);
//            audit.setApprovedDate(Instant.now().toEpochMilli());
//            AuditHistory auditHistory = new AuditHistory(audit, "tm_customer_unapproved");
//            auditHistory.addContent(customerUnapprovedDB);
            Query query = new Query();
            Criteria tenantCustomerCriteria=Criteria.where("customer.tenantId").is(tenantId).andOperator(Criteria.where("customer.customerId").is(customerId));
            query.addCriteria(tenantCustomerCriteria);
            mongoTemplate.remove(query, CustomerUnapprovedDB.class);//deleting the record from the unapproved collection.
//            mongoTemplate.insert(auditHistory);//recording to the auditHistory.

            newReturnObject.setReturncode(1);
            newReturnObject.PerformReturnObject("Success");
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());

        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject viewByTenantIdCustomerId(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language) {
        try {
            //superKey
            String tenantId = tableQuery.getQueryFields().get("tenantId").toString();
            String customerId= tableQuery.getQueryFields().get("customerId").toString();
            Criteria tenantCustomerCriteria=Criteria.where("customer.tenantId").is(tenantId).andOperator(Criteria.where("customer.customerId").is(customerId));
            if (tableQuery.getCollection().equals("approved")) {
                CustomerDB customerDB = mongoTemplate.findOne(Query.query(new Criteria().orOperator(tenantCustomerCriteria)), CustomerDB.class);
                newReturnObject.PerformReturnObject(customerDB);
            } else if (tableQuery.getCollection().equals("unapproved")) {
                CustomerUnapprovedDB customerUnapprovedDB = mongoTemplate.findOne(Query.query(new Criteria().orOperator(tenantCustomerCriteria)), CustomerUnapprovedDB.class);
                newReturnObject.PerformReturnObject(customerUnapprovedDB);
            } else {
                newReturnObject.setReturncode(0);
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU010", language));
            }

        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public NewReturnObject customerStatusChanger(ReactTableQuery tableQuery, String headerUserId, NewReturnObject newReturnObject, String language, String setStatus, Boolean approvalRequired) {
        try {
            Audit audit = new Audit(headerUserId, Instant.now().toEpochMilli(), "", 0.0);
            //superKey
            String tenantId = tableQuery.getQueryFields().get("tenantId").toString();
            String customerId= tableQuery.getQueryFields().get("customerId").toString();

            CustomerDB customerDBResult = findByTenantIdCustomerId(tenantId,customerId);
            CustomerUnapprovedDB customerUnapprovedDB = findUnapprovedByTenantIdCustomerId(tenantId,customerId);
            if (approvalRequired) {//4EyeRequired=true .

                if (customerDBResult != null || customerUnapprovedDB != null) {//record must exist to alter status.

                    AuditHistory auditHistory = new AuditHistory(audit, "tm_customer_unapproved");
                    CustomerDB eventDefinitionDB = customerDBResult != null ? customerDBResult : new CustomerDB(customerUnapprovedDB);//take the existing record.
                    eventDefinitionDB.getCustomer().setVersion(eventDefinitionDB.getCustomer().getVersion() + 1);
                    eventDefinitionDB.addAuditInfo(audit);
                    auditHistory.addLastStatus(customerDBResult);//last version record.
                    String status = eventDefinitionDB.getCustomer().getStatus();
                    eventDefinitionDB.getCustomer().setStatus(setStatus);
                    auditHistory.addContent(mongoTemplate.save(new CustomerUnapprovedDB(eventDefinitionDB)));
                    mongoTemplate.insert(auditHistory);
                    newReturnObject.PerformReturnObject("Success");
                    newReturnObject.setReturncode(1);
                } else {
                    //error record does not exist to update.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU006", language));

                }
            } else {
                //approval not required inserting to the main collection
                if (customerDBResult != null) {//record must exist to alter status.
                    AuditHistory auditHistory = new AuditHistory(audit, "tm_customer");
                    CustomerDB eventDefinitionDB = customerDBResult;
                    eventDefinitionDB.getCustomer().setVersion(eventDefinitionDB.getCustomer().getVersion() + 1);
                    eventDefinitionDB.addAuditInfo(audit);
                    auditHistory.addLastStatus(customerDBResult);//last version record.
                    eventDefinitionDB.getCustomer().setStatus(setStatus);
                    auditHistory.addContent(mongoTemplate.save(eventDefinitionDB));
                    mongoTemplate.insert(auditHistory);
                    newReturnObject.PerformReturnObject("Success");
                    newReturnObject.setReturncode(1);
                } else {
                    //error record does not exist to update.
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU006", language));
                }
            }
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
        }
        return newReturnObject;
    }

    @Override
    public CustomerDB findByTenantIdCustomerId(String tenantId,String customerId) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            Criteria tenantCustomerCriteria=Criteria.where("customer.tenantId").is(tenantId).andOperator(Criteria.where("customer.customerId").is(customerId));
            query.addCriteria(tenantCustomerCriteria);
            return mongoTemplate.findOne(query, CustomerDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "findByTenantIdCustomerId");
            return null;
        }
    }

    @Override
    public CustomerUnapprovedDB findUnapprovedByTenantIdCustomerId(String tenantId,String customerId) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            Criteria tenantCustomerCriteria=Criteria.where("customer.tenantId").is(tenantId).andOperator(Criteria.where("customer.customerId").is(customerId));
            query.addCriteria(tenantCustomerCriteria);
            return mongoTemplate.findOne(query, CustomerUnapprovedDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "findUnapprovedByTenantIdCustomerId");
            return null;
        }
    }

    @Override
    public long countUnapprovedByTenantIdCustomerId(String tenantId,String customerId) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            Criteria tenantCustomerCriteria=Criteria.where("customer.tenantId").is(tenantId).andOperator(Criteria.where("customer.customerId").is(customerId));
            query.addCriteria(tenantCustomerCriteria);
            return mongoTemplate.count(query, CustomerUnapprovedDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "findByTenantIdCustomerId");
            return 0;
        }
    }

    @Override
    public void listOfAllCustomerIdsByRegx(String keyword,String tenantId, NewReturnObject newReturnObject, String language) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("customer.tenantId").is(tenantId));
            query.fields().include("customer.tenantId").include("customer.customerId");
            query.with(Sort.by(Sort.Direction.ASC,"customer.customerId"));
            Criteria statusCriteria=Criteria.where("customer.status").is("active");
            if(keyword.isEmpty()||keyword.equals(" ")){
                newReturnObject.PerformReturnObject(mongoTemplate.find(query.addCriteria(statusCriteria),CustomerDB.class));
            }
            else{
                Criteria customerIdCriteria=Criteria.where("customer.customerId").regex(keyword, "i");
                query.addCriteria(new Criteria().orOperator(customerIdCriteria).andOperator(statusCriteria));
                newReturnObject.PerformReturnObject(mongoTemplate.find(query, CustomerDB.class));
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "listOfAllCustomerIdsByRegx");
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU002", language));
        }
    }

    @Override
    public long countByTenantIdCustomerId(String tenantId,String customerId) {
        try {
            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            Criteria tenantCustomerCriteria=Criteria.where("customer.tenantId").is(tenantId).andOperator(Criteria.where("customer.customerId").is(customerId));
            query.addCriteria(tenantCustomerCriteria);
            return mongoTemplate.count(query, CustomerDB.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), "findByTenantIdCustomerId");
            return 0;
        }
    }

    private Audit getLastAuditUnapproved(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language) {
        try {
            //superKey
            String tenantId = tableQuery.getQueryFields().get("tenantId").toString();
            String customerId= tableQuery.getQueryFields().get("customerId").toString();

            Query query = new Query();
            query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
            Criteria tenantCustomerCriteria=Criteria.where("customer.tenantId").is(tenantId).andOperator(Criteria.where("customer.customerId").is(customerId));
            query.addCriteria(tenantCustomerCriteria);

            List<Audit> audit = mongoTemplate.findOne(query, CustomerUnapprovedDB.class).getAuditInfo();

            return audit.get(audit.size() - 1);//returning the last audit record ie the recent audit trail.

        } catch (Exception ex) {
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCU002", language));
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
            return null;
        }

    }

}


