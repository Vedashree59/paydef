package com.dd.payhub.paydefinition.impl;

import com.dd.payhub.paycommon.model.*;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.dal.CurrencyCountryHolidaysDAL;
import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
import com.dd.payhub.paydefinition.entity.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Collation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CurrencyCountryHolidaysImpl implements CurrencyCountryHolidaysDAL {

    private static final Logger logger = LogManager.getLogger(CurrencyCountryHolidaysImpl.class);
    private final MongoTemplate mongoTemplate;
    private ErrorCodeDAL errorCodeDAL;

    @Autowired
    public CurrencyCountryHolidaysImpl(MongoTemplate mongoTemplate, ErrorCodeDAL errorCodeDAL) {
        this.mongoTemplate = mongoTemplate;
        this.errorCodeDAL = errorCodeDAL;
    }

    @Override
    public NewReturnObject addCurrencyCountryHolidays(CurrencyCountryHolidays currencyCountryHolidays, String userId, NewReturnObject newReturnObject, String language, Boolean approvalRequired) {

        try {
            CurrencyCountryHolidaysDB currencyCountryHolidaysDB=new CurrencyCountryHolidaysDB();
            currencyCountryHolidaysDB.setCurrencyCountryHolidays(currencyCountryHolidays);
            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
            currencyCountryHolidaysDB.addAuditInfo(audit);
            currencyCountryHolidaysDB.getCurrencyCountryHolidays().setVersion(0);
            AuditHistory auditHistory = new AuditHistory(audit,"tm_currency_country_holidays_unapproved");
            currencyCountryHolidaysDB.setSortDate(audit.getCreationDate());//setting the recent change date for sorting order.

            if(approvalRequired){//4eyeRequired add to unapproved collection.
                CurrencyCountryHolidaysUnapprovedDB currencyCountryHolidaysUnapprovedDB =mongoTemplate.insert(new CurrencyCountryHolidaysUnapprovedDB(currencyCountryHolidaysDB)) ;
                auditHistory.addContent(currencyCountryHolidaysUnapprovedDB);
            }else{//no 4eyeRequired adding to the main collection.
                auditHistory.addContent(mongoTemplate.insert(currencyCountryHolidaysDB));
            }
            auditHistory.addLastStatus(null);
            mongoTemplate.insert(auditHistory);
            newReturnObject.PerformReturnObject("Success");
            return newReturnObject;
        } catch (Exception ex) {
            logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
<<<<<<< HEAD
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY02", language));
=======
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY002", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            newReturnObject.setReturncode(0);
            return newReturnObject;
        }
    }
    @Override
    public ReactTableRecord getAllCurrencyCountryHolidaysByPagination(ReactTableQuery tableQuery) {
        int page = 0;
        int pageSize = 5;
        int totalPage;
        long totalRecord = 0;
        ReactTableRecord tableRecord = new ReactTableRecord();
        String sortField=tableQuery.getSortField().isEmpty()?"sortDate":tableQuery.getSortField();

        Query query = new Query();//if
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.

        try {
            if (tableQuery.getPage() != 0) {
                page = (int) tableQuery.getPage();
            }
            if (tableQuery.getPageSize() != 0) {
                pageSize = (int) tableQuery.getPageSize();
            }

            if(!tableQuery.getSortOrder().equals("true")){//false means order by asc.
                query.with(Sort.by(Sort.Direction.DESC,sortField));
            }
            else {
                query.with(Sort.by(Sort.Direction.ASC,sortField));
            }
//            Query query = new Query();//if
            Criteria statusCriteria=Criteria.where("currencyCountryHolidays.status").is(tableQuery.getStatus());
            query.addCriteria(Criteria.where("currencyCountryHolidays.status").is(tableQuery.getStatus()));

            if (!tableQuery.getKeyword().isEmpty()) {
                Criteria yearCriteria = Criteria.where("currencyCountryHolidays.year").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria currencyCodeCriteria = Criteria.where("currencyCountryHolidays.currencyCode").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria countryCodeCriteria = Criteria.where("currencyCountryHolidays.countryCode").regex(String.valueOf(tableQuery.getKeyword()), "i");
                Criteria tenantIdCriteria = Criteria.where("currencyCountryHolidays.tenantId").regex(String.valueOf(tableQuery.getKeyword()), "i");
                query.addCriteria(new Criteria().orOperator(tenantIdCriteria, currencyCodeCriteria,countryCodeCriteria, yearCriteria).andOperator(statusCriteria));
            }
            if (tableQuery.getCollection().equals("approved")) {
                totalRecord = mongoTemplate.count(query, CurrencyCountryHolidaysDB.class);

                tableRecord.setTableRecord(mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), CurrencyCountryHolidaysDB.class));
            } else {
                totalRecord = mongoTemplate.find(query, CurrencyCountryHolidaysUnapprovedDB.class).size();

                tableRecord.setTableRecord((mongoTemplate.find(query.limit(pageSize).skip(page * pageSize), CurrencyCountryHolidaysUnapprovedDB.class)));
            }
            totalPage = (int) Math.ceil(totalRecord / (float) pageSize);
            tableRecord.setTotalPage(totalPage);
            tableRecord.setTotalRecord(totalRecord);
            return tableRecord;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            tableRecord.setTotalPage( 0);
            tableRecord.setTotalRecord( 0);
            tableRecord.setTableRecord( new ArrayList<>());
            return tableRecord;
        }
    }

    public long countByTenantIdCurrencyCodeYear(String tenantId,String countryCode,String currencyCode, String year) {
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        Criteria yearCriteria = Criteria.where("currencyCountryHolidays.year").is(year);
        Criteria countryCodeCriteria = Criteria.where("currencyCountryHolidays.countryCode").is(countryCode);
        Criteria currencyCodeCriteria = Criteria.where("currencyCountryHolidays.currencyCode").is(currencyCode);
        Criteria tenantIdCriteria = Criteria.where("currencyCountryHolidays.tenantId").is(tenantId);
        query.addCriteria(new Criteria().andOperator(tenantIdCriteria, countryCodeCriteria,currencyCodeCriteria, yearCriteria));
        return mongoTemplate.count(query, TenantDB.class);
    }

    @Override
    public CurrencyCountryHolidaysUnapprovedDB findUnapprovedByTenantIdCurrencyCountryCodeYear(String tenantId, String countryCode,String currencyCode, String year) {
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        Criteria yearCriteria = Criteria.where("currencyCountryHolidays.year").is(year);
        Criteria countryCodeCriteria = Criteria.where("currencyCountryHolidays.countryCode").is(countryCode);
        Criteria currencyCodeCriteria = Criteria.where("currencyCountryHolidays.currencyCode").is(currencyCode);
        Criteria tenantIdCriteria = Criteria.where("currencyCountryHolidays.tenantId").is(tenantId);
        query.addCriteria(new Criteria().andOperator(tenantIdCriteria, countryCodeCriteria,currencyCodeCriteria, yearCriteria));
        return mongoTemplate.findOne(query, CurrencyCountryHolidaysUnapprovedDB.class);
    }

    @Override
    public CurrencyCountryHolidaysDB findByTenantIdCurrencyCountryCodeYear(String tenantId, String countryCode, String currencyCode, String year) {
        Query query = new Query();
        query.collation(Collation.of("en").strength(Collation.ComparisonLevel.secondary()));//case insensitive search.
        Criteria yearCriteria = Criteria.where("currencyCountryHolidays.year").is(year);
        Criteria countryCodeCriteria = Criteria.where("currencyCountryHolidays.countryCode").is(countryCode);
        Criteria currencyCodeCriteria = Criteria.where("currencyCountryHolidays.currencyCode").is(currencyCode);
        Criteria tenantIdCriteria = Criteria.where("currencyCountryHolidays.tenantId").is(tenantId);
        query.addCriteria(new Criteria().andOperator(tenantIdCriteria, countryCodeCriteria,currencyCodeCriteria, yearCriteria));
        return mongoTemplate.findOne(query, CurrencyCountryHolidaysDB.class);
    }

    @Override
    public NewReturnObject updateCurrencyCountryHolidays(CurrencyCountryHolidays currencyCountryHolidays, String userId, NewReturnObject newReturnObject, String language,Boolean approvalRequired) {
        try {
            CurrencyCountryHolidaysDB currencyCountryHolidaysDB=new CurrencyCountryHolidaysDB();
            currencyCountryHolidaysDB.setCurrencyCountryHolidays(currencyCountryHolidays);

            Audit audit = new Audit(userId, Instant.now().toEpochMilli(), "", 0.0);
//
            CurrencyCountryHolidaysUnapprovedDB currencyCountryHolidaysUnapprovedDB = findUnapprovedByTenantIdCurrencyCountryCodeYear(currencyCountryHolidaysDB.getCurrencyCountryHolidays().getTenantId(),currencyCountryHolidaysDB.getCurrencyCountryHolidays().getCountryCode(),currencyCountryHolidaysDB.getCurrencyCountryHolidays().getCurrencyCode(),currencyCountryHolidaysDB.getCurrencyCountryHolidays().getYear());
            CurrencyCountryHolidaysDB currencyCountryHolidaysDBResult = findByTenantIdCurrencyCountryCodeYear(currencyCountryHolidaysDB.getCurrencyCountryHolidays().getTenantId(),currencyCountryHolidaysDB.getCurrencyCountryHolidays().getCountryCode(),currencyCountryHolidaysDB.getCurrencyCountryHolidays().getCurrencyCode(),currencyCountryHolidaysDB.getCurrencyCountryHolidays().getYear());
            if(currencyCountryHolidaysDBResult !=null|| currencyCountryHolidaysUnapprovedDB !=null) {//record must exist to update

                AuditHistory auditHistory = new AuditHistory(audit,"tm_currency_country_holidays_unapproved");


                if (currencyCountryHolidaysUnapprovedDB == null) {
                    currencyCountryHolidaysDB.setAuditInfo(currencyCountryHolidaysDBResult.getAuditInfo());
                    currencyCountryHolidaysDB.addAuditInfo(audit);
                    currencyCountryHolidaysDB.getCurrencyCountryHolidays().setVersion(currencyCountryHolidaysDBResult.getCurrencyCountryHolidays().getVersion()+1);//version gets increased during update
                    currencyCountryHolidaysDB.setSortDate(audit.getCreationDate());//recent change date


                    auditHistory.addLastStatus(currencyCountryHolidaysDBResult);//last version record.



                } else {
                    currencyCountryHolidaysDB.setAuditInfo(currencyCountryHolidaysUnapprovedDB.getAuditInfo());
                    currencyCountryHolidaysDB.addAuditInfo(audit);
                    currencyCountryHolidaysDB.set_id(currencyCountryHolidaysUnapprovedDB.get_id());
                    currencyCountryHolidaysDB.getCurrencyCountryHolidays().setVersion(currencyCountryHolidaysUnapprovedDB.getCurrencyCountryHolidays().getVersion() + 1);//version gets increased during update
                    currencyCountryHolidaysDB.setSortDate(audit.getCreationDate());//recent change date
                    currencyCountryHolidaysDB.set_id(currencyCountryHolidaysUnapprovedDB.get_id());//setting the mongo ID.

                    auditHistory.addLastStatus(currencyCountryHolidaysUnapprovedDB);//old version record.

                }
                auditHistory.addContent(mongoTemplate.insert(new CurrencyCountryHolidaysUnapprovedDB(currencyCountryHolidaysDB)));
                mongoTemplate.insert(auditHistory);

            }
            newReturnObject.PerformReturnObject(null);
            newReturnObject.setReturncode(1);
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
<<<<<<< HEAD
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY02", language));//change
=======
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY002", language));//change
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        }
        return newReturnObject;
    }


    @Override
    public NewReturnObject getEditableCurrencyCountryHolidays(ReactTableQuery reactTableQuery, NewReturnObject newReturnObject, String language) {
        try{

            String tenantId=reactTableQuery.getQueryFields().get("tenantId").toString();
            String countryCode = reactTableQuery.getQueryFields().get("countryCode").toString();
            String currencyCode=reactTableQuery.getQueryFields().get("currencyCode").toString();
            String year=reactTableQuery.getQueryFields().get("year").toString();
            //gives approved record only when there are no unapproved records.
            if(reactTableQuery.getCollection().equals("approved")){
                CurrencyCountryHolidaysUnapprovedDB unApprovedResult = findUnapprovedByTenantIdCurrencyCountryCodeYear(tenantId,countryCode,currencyCode,year);
                if(unApprovedResult==null){
                    newReturnObject.PerformReturnObject(findByTenantIdCurrencyCountryCodeYear(tenantId,countryCode,currencyCode,year));
                }else{
                    newReturnObject.PerformReturnObject(new ArrayList<>());
<<<<<<< HEAD
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY07", language));
=======
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY007", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                }

            }else if(reactTableQuery.getCollection().equals("unapproved")){
                newReturnObject.PerformReturnObject(findUnapprovedByTenantIdCurrencyCountryCodeYear(tenantId,countryCode,currencyCode,year));
            }

        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
<<<<<<< HEAD
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY02", language));
=======
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY002", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        }
            return newReturnObject;
    }

    @Override
    public NewReturnObject approveCurrencyCountryHolidays(ReactTableQuery reactTableQuery, String userId, NewReturnObject newReturnObject, String language) {
        try {
            String tenantId=reactTableQuery.getQueryFields().get("tenantId").toString();
            String countryCode = reactTableQuery.getQueryFields().get("countryCode").toString();
            String currencyCode=reactTableQuery.getQueryFields().get("currencyCode").toString();
            String year=reactTableQuery.getQueryFields().get("year").toString();
//            newReturnObject=newReturnObject;
            CurrencyCountryHolidaysUnapprovedDB unApprovedResult = findUnapprovedByTenantIdCurrencyCountryCodeYear(tenantId,countryCode,currencyCode,year);
            CurrencyCountryHolidaysDB currencyCountryHolidaysDB = findByTenantIdCurrencyCountryCodeYear(tenantId,countryCode,currencyCode,year);


            Audit audit = getLastAuditUnapproved(tenantId,countryCode,currencyCode,year, newReturnObject, language);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            audit.setApproverId(userId);

            unApprovedResult.addAuditInfo(audit);
            if(!audit.getCreatorId().equals(userId)){//creator and approver cannot be same.

                unApprovedResult.addAuditInfo(audit);//setting the audit.
                if(unApprovedResult!=null){//record exist to update.
                    AuditHistory auditHistory = new AuditHistory(audit,"tm_currency_country_holidays");

                    if (currencyCountryHolidaysDB == null) {
                        // tm_tenant data is not present, create new record
                        auditHistory.addLastStatus(null);
                        auditHistory.addContent(mongoTemplate.insert(new CurrencyCountryHolidaysDB(unApprovedResult)));
                    } else {
                        String mongoId = currencyCountryHolidaysDB.get_id();
                        auditHistory.addLastStatus(currencyCountryHolidaysDB);//old version
                        currencyCountryHolidaysDB = new CurrencyCountryHolidaysDB(unApprovedResult);
                        currencyCountryHolidaysDB.set_id(mongoId);

                        auditHistory.addContent( mongoTemplate.save(currencyCountryHolidaysDB));
                    }
                    newReturnObject.PerformReturnObject(null);

                    mongoTemplate.insert(auditHistory);
                    newReturnObject = deleteByTenantIdCurrencyCountryCodeYear(reactTableQuery, userId, newReturnObject, language);//delete the un approved after approving.
                }else{//no record to approve.
<<<<<<< HEAD
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY04", language));
                }
            }else{//approverID and creatorID are same.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY08", language));
=======
                    newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY004", language));
                }
            }else{//approverID and creatorID are same.
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY008", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            }

        } catch (Exception ex) {
            logger.error(ex);
            System.out.println(ex.getStackTrace());
            logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
<<<<<<< HEAD
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY02", language));
=======
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY002", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        }
        return newReturnObject;
    }
    @Override
    public NewReturnObject deleteByTenantIdCurrencyCountryCodeYear(ReactTableQuery reactTableQuery, String userid, NewReturnObject newReturnObject, String language) {
        try {
            String tenantId=reactTableQuery.getQueryFields().get("tenantId").toString();
            String countryCode = reactTableQuery.getQueryFields().get("countryCode").toString();
            String currencyCode=reactTableQuery.getQueryFields().get("currencyCode").toString();
            String year = reactTableQuery.getQueryFields().get("year").toString();
            CurrencyCountryHolidaysUnapprovedDB currencyCountryHolidaysUnapprovedDB = findUnapprovedByTenantIdCurrencyCountryCodeYear(tenantId,countryCode,currencyCode,year);
            Audit audit = getLastAuditUnapproved(tenantId,countryCode,currencyCode,year, newReturnObject, language);
            audit.setApproverId(userid);
            audit.setApprovedDate(Instant.now().toEpochMilli());
            AuditHistory auditHistory = new AuditHistory(audit,"tm_currency_country_holidays_unapproved");
            auditHistory.addContent(currencyCountryHolidaysUnapprovedDB);

            Query query = new Query();
            Criteria yearCriteria = Criteria.where("currencyCountryHolidays.year").is(year);
            Criteria countryCodeCriteria = Criteria.where("currencyCountryHolidays.countryCode").is(countryCode);
            Criteria currencyCodeCriteria = Criteria.where("currencyCountryHolidays.currencyCode").is(currencyCode);
            Criteria tenantIdCriteria = Criteria.where("currencyCountryHolidays.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, countryCodeCriteria,currencyCodeCriteria, yearCriteria));
            mongoTemplate.remove(query, CurrencyCountryHolidaysUnapprovedDB.class);

            mongoTemplate.insert(auditHistory);

            newReturnObject.setReturncode(1);
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
<<<<<<< HEAD
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY02", language));
=======
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY002", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        }
        return newReturnObject;
    }


    @Override
    public NewReturnObject viewByTableQuery(ReactTableQuery tableQuery, NewReturnObject newReturnObject, String language) {
        try {
            String tenantId=tableQuery.getQueryFields().get("tenantId").toString();
            String countryCode = tableQuery.getQueryFields().get("countryCode").toString();
            String currencyCode=tableQuery.getQueryFields().get("currencyCode").toString();
            String year=tableQuery.getQueryFields().get("year").toString();
//            Criteria criteriaStatus=Criteria.where("currencyHolidays.status").is("active");
            if(tableQuery.getCollection().equals("approved")){
                CurrencyCountryHolidaysDB currencyCountryHolidaysDB =findByTenantIdCurrencyCountryCodeYear(tenantId,countryCode,currencyCode,year);//mongoTemplate.findOne(Query.query(new Criteria().andOperator(criteriaMongoId)), CurrencyCountryHolidaysDB.class);
                newReturnObject.PerformReturnObject(currencyCountryHolidaysDB);
            }else if(tableQuery.getCollection().equals("unapproved")){
                CurrencyCountryHolidaysUnapprovedDB currencyCountryHolidaysUnapprovedDB =findUnapprovedByTenantIdCurrencyCountryCodeYear(tenantId,countryCode,currencyCode,year);
                newReturnObject.PerformReturnObject(currencyCountryHolidaysUnapprovedDB);
            }
            else {
                newReturnObject.setReturncode(0);
<<<<<<< HEAD
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY05", language));
=======
                newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY005", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            }

        }
        catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(),newReturnObject.getAppErrorString());
            newReturnObject.setReturncode(0);
<<<<<<< HEAD
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY02", language));
=======
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY002", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        }
        return newReturnObject;
    }

    public Audit getLastAuditUnapproved(String tenantId, String countryCode, String currencyCode,String year, NewReturnObject newReturnObject, String language) {
        try {
            Query query = new Query();
            Criteria yearCriteria = Criteria.where("currencyCountryHolidays.year").is(year);
            Criteria currencyCodeCriteria = Criteria.where("currencyCountryHolidays.currencyCode").is(currencyCode);
            Criteria countryCodeCriteria = Criteria.where("currencyCountryHolidays.countryCode").is(countryCode);
            Criteria tenantIdCriteria = Criteria.where("currencyCountryHolidays.tenantId").is(tenantId);
            query.addCriteria(new Criteria().andOperator(tenantIdCriteria, currencyCodeCriteria, countryCodeCriteria,yearCriteria));

            List<Audit> audit = mongoTemplate.findOne(query, CurrencyCountryHolidaysUnapprovedDB.class).getAuditInfo();

            return audit.get(audit.size() - 1);
        } catch (Exception ex) {
            logger.error(ex);
            logger.error(ex.getMessage(), newReturnObject.getAppErrorString());
<<<<<<< HEAD
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY02", language));
=======
            newReturnObject.addError(errorCodeDAL.findByErrorCodeAndLanguage("MCCY002", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            return null;
        }
    }



}
