package com.dd.payhub.paydefinition.controller;


import com.dd.payhub.paycommon.model.User;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paydefinition.dal.UserDAL;
import com.dd.payhub.paydefinition.service.PHUtilityService;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/user")
public class UserController {
    public static final Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
    public static final String passwordEncryptionKey = "9y$B&E)H@McQfTjW";
    private static final Logger logger = LogManager.getLogger(UserController.class);
    private UserDAL userDAL;
    private PHUtilityService utilityService;
    private NewReturnObject newReturnObject;

    @Autowired
    public UserController(UserDAL userDAL, PHUtilityService utilityService) {
        this.userDAL = userDAL;
        this.utilityService = utilityService;
        this.newReturnObject = new NewReturnObject();
    }

    @PostMapping("/verifyUser")
    public String verifyUser(@RequestBody User user) {

        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        newReturnObject = new NewReturnObject(0, "\"\"");
        logger.info("/verifyUser/POST API request is calling and verifying the bank id and email id from user collection .");

        try {
            userDAL.verifyUser(user,this.newReturnObject,language);
        } catch (Exception ex) {
            logger.error("Exception is generated while doing getAllUserDetails method operation ", ex);
            newReturnObject.addError(utilityService.singleError( "MUS012", language));
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/login")
    public String loginUser(@RequestBody User user, HttpServletRequest request) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        newReturnObject = new NewReturnObject(0, "\"\"");
        logger.info("/login/POST API request is calling and verifying the login details from user collection .");

        try {
            Key aesKey = new SecretKeySpec(passwordEncryptionKey.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
            byte[] encrypted = cipher.doFinal(user.getPassword().getBytes());
            System.out.println(new String(encrypted, "UTF8"));
            user.setPassword(new String(encrypted, "UTF8"));
            userDAL.checkUserLoginCredentials(user,request,this.newReturnObject,language);
        } catch (Exception ex) {
            System.out.println(ex);
            logger.error("Exception is generated while doing getAllUserDetails method operation ", ex);
            newReturnObject.addError(utilityService.singleError("MUS012", language));
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/useridsbytenantid")
    public String listOfUserIdsByTenantId(@RequestAttribute(value = "userid") String headerUserId,@RequestBody String tenantID){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try{
            newReturnObject= userDAL.listOfUserIdsByTenantId(tenantID,newReturnObject,language);
        }catch (Exception ex) {
            logger.error("Exception in CustomerController.listOfUserIds", this.newReturnObject.getAppErrorString(), ex);
            newReturnObject.PerformReturnObject(new ArrayList<>());
            newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code

        }
        return newReturnObject.setReturnJSON();
    }

}
