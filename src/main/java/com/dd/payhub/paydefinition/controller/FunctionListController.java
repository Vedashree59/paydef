package com.dd.payhub.paydefinition.controller;


import com.dd.payhub.paycommon.model.FunctionDefinition;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.config.FnOptConfig;
import com.dd.payhub.paydefinition.dal.FunctionListDAL;
import com.dd.payhub.paydefinition.entity.FunctionListDB;
import com.dd.payhub.paydefinition.entity.FunctionListUnapprovedDB;
<<<<<<< HEAD
=======
import com.dd.payhub.paydefinition.entity.FunctionListDB;
import com.dd.payhub.paydefinition.entity.FunctionListUnapprovedDB;
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
import com.dd.payhub.paydefinition.service.PHUtilityService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", allowedHeaders = "Content-Type, Accept, reconnection, User-Token")
@RestController
@RequestMapping("/functionlist")
public class FunctionListController {
    private static final Logger logger = LogManager.getLogger(FunctionListController.class);
    private FunctionListDAL functionListDAL;
    private PHUtilityService utilityService;
    private NewReturnObject newReturnObject;

    @Autowired
    public FunctionListController(FunctionListDAL functionListDAL, PHUtilityService utilityService) {
        this.functionListDAL = functionListDAL;
        this.utilityService = utilityService;
        this.newReturnObject = new NewReturnObject();
    }

    @PostMapping("/all")
<<<<<<< HEAD
    public String getAllFunctionList(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {//, , @RequestHeader(value = "bankid") String headerBankId
=======
    public String getAllFunctionList(@RequestBody ReactTableQuery tableQuery , @RequestAttribute(value = "userid") String headerUserId) {//, , @RequestHeader(value = "bankid") String headerBankId
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        ReactTableRecord tableRecord = new ReactTableRecord();
        try {
<<<<<<< HEAD
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MFN001", language, FnOptConfig.fIdFunctionListController   , FnOptConfig.opFView)) {//error code.
=======
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdfunctionListController, FnOptConfig.opFView)) {//error code.
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                tableRecord = functionListDAL.getAllFunctionListByPagination(tableQuery);
                newReturnObject.PerformReturnObject(tableRecord);
            }
            return newReturnObject.setReturnJSON();
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
<<<<<<< HEAD
            this.newReturnObject.addError(utilityService.singleError("MFN002", language));//error code
            logger.error(ex.getMessage(), "Exception in FunctionListController.getAllFunctionList", this.newReturnObject.getAppErrorString());
=======
            this.newReturnObject.addError(utilityService.singleError("MCT002", language));//error code
            logger.error(ex.getMessage(),"Exception in FunctionListController.getAllFunctionList", this.newReturnObject.getAppErrorString());
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

            return newReturnObject.setReturnJSON();
        }
    }

    @PostMapping("/add")
    public String newFunctionList(@RequestBody FunctionDefinition functionDefinition, @RequestAttribute(value = "userid") String headerUserId) {
        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            FunctionListDB functionListDBResult = null;
            FunctionListUnapprovedDB functionListUnapprovedDBResult = null;

<<<<<<< HEAD
            if (validateFunctionList(functionDefinition, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MFN001", language, FnOptConfig.fIdFunctionListController , FnOptConfig.opFNew)) {//error code.
=======
            if (validateFunctionList(functionDefinition, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdfunctionListController, FnOptConfig.opFNew)) {//error code.
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                //valid data to insert new record.
                functionListDBResult = functionListDAL.findByFunctionId(functionDefinition.getFunctionId());
                functionListUnapprovedDBResult = functionListDAL.findUnapprovedByFunctionId(functionDefinition.getFunctionId());
                if (functionListDBResult == null && functionListUnapprovedDBResult == null) {//record does not exist so insertion can be done.
                    newReturnObject = functionListDAL.addFunctionList(functionDefinition, headerUserId, newReturnObject, language);//
                } else {//Error record exist duplication not allowed.
<<<<<<< HEAD
                    this.newReturnObject.addError(utilityService.singleError("MFN003", language));
=======
                    this.newReturnObject.addError(utilityService.singleError("MCT003", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                    this.newReturnObject.setReturncode(0);
                }
            }

        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
<<<<<<< HEAD
            this.newReturnObject.addError(utilityService.singleError("MFN002", language));//error code
            logger.error(ex.getMessage(), "Exception in FunctionListController.addFunctionList", this.newReturnObject.getAppErrorString());
=======
            this.newReturnObject.addError(utilityService.singleError("MCT002", language));//error code
            logger.error(ex.getMessage(),"Exception in FunctionListController.addFunctionList", this.newReturnObject.getAppErrorString());
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc


        }
        return this.newReturnObject.setReturnJSON();

    }

    @PostMapping("/update")
    public String updateFunctionList(@RequestBody FunctionDefinition functionDefinition, @RequestAttribute(value = "userid") String headerUserId) {

        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
<<<<<<< HEAD
            if (validateFunctionList(functionDefinition, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MFN001", language, FnOptConfig.fIdFunctionListController , FnOptConfig.opFNew)) {//error code.
=======
            if (validateFunctionList(functionDefinition, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdfunctionListController, FnOptConfig.opFNew)) {//error code.
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                newReturnObject = functionListDAL.updateFunctionList(functionDefinition, headerUserId, this.newReturnObject, language);
            }

        } catch (Exception ex) {
<<<<<<< HEAD
            this.newReturnObject.addError(utilityService.singleError("MFN002", language));//error code
            logger.error(ex.getMessage(), "Exception in FunctionListController.updateFunctionList", this.newReturnObject.getAppErrorString());
=======
            this.newReturnObject.addError(utilityService.singleError("MCT002", language));//error code
            logger.error(ex.getMessage(),"Exception in FunctionListController.updateFunctionList", this.newReturnObject.getAppErrorString());
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

        }
        return this.newReturnObject.setReturnJSON();

    }


    @PostMapping("/approve")
    public String approveFunctionList(@RequestAttribute(value = "userid") String headerUserId, @RequestBody String functionListID) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
<<<<<<< HEAD
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MFN001", language, FnOptConfig.fIdFunctionListController   , FnOptConfig.opFAuth)) {//error code.
=======
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdfunctionListController, FnOptConfig.opFAuth)) {//error code.
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

                newReturnObject = functionListDAL.approveFunctionList(functionListID, headerUserId, this.newReturnObject, language);
            }

        } catch (Exception ex) {
<<<<<<< HEAD
            this.newReturnObject.addError(utilityService.singleError("MFN002", language));//error code
            logger.error(ex.getMessage(), "Exception in FunctionListController.approveFunctionList", this.newReturnObject.getAppErrorString());
=======
            this.newReturnObject.addError(utilityService.singleError("MCT002", language));//error code
            logger.error(ex.getMessage(),"Exception in FunctionListController.approveFunctionList", this.newReturnObject.getAppErrorString());
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

        }
        return this.newReturnObject.setReturnJSON();
    }

    @DeleteMapping("/reject")//api to delete the record from the tm_functionList_unapproved.
    public String rejectFunctionList(@RequestBody String functionListId, @RequestAttribute(value = "userid") String headerUserId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

<<<<<<< HEAD
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MFN001", language, FnOptConfig.fIdFunctionListController   , FnOptConfig.opFDelete)) {//error code.
=======
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdfunctionListController, FnOptConfig.opFDelete)) {//error code.
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

                newReturnObject = functionListDAL.deleteByFunctionId(functionListId, headerUserId, newReturnObject, language);
            }
        } catch (Exception ex) {
<<<<<<< HEAD
            logger.error(ex.getMessage(), "Exception in FunctionListController.rejectFunctionList", this.newReturnObject.getAppErrorString());

            this.newReturnObject.addError(utilityService.singleError("MFN002", language));//error code
=======
            logger.error(ex.getMessage(),"Exception in FunctionListController.rejectFunctionList", this.newReturnObject.getAppErrorString());

            this.newReturnObject.addError(utilityService.singleError("MCT002", language));//error code
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

        }
        return newReturnObject.setReturnJSON();

    }
<<<<<<< HEAD

    @PostMapping("/view")
    public String viewFunctionList(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MFN001", language, FnOptConfig.fIdFunctionListController   , FnOptConfig.opFView)) {//error code.
                newReturnObject = functionListDAL.viewByFunctionId(tableQuery, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MFN002", language));//error code
            logger.error(ex.getMessage(), "Exception in FunctionListController.view", this.newReturnObject.getAppErrorString(), ex);
=======
    @PostMapping("/view")
    public String viewFunctionList(@RequestBody ReactTableQuery tableQuery,@RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdfunctionListController, FnOptConfig.opFView)) {//error code.
                newReturnObject = functionListDAL.viewByFunctionId(tableQuery, newReturnObject, language);
            }
        }catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCT002", language));//error code
            logger.error(ex.getMessage(),"Exception in FunctionListController.view",this.newReturnObject.getAppErrorString(),ex);
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        }
        return newReturnObject.setReturnJSON();


    }


    @PostMapping("/geteditable")
<<<<<<< HEAD
    public String getEditableFunctionList(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery tableQuery) {
=======
    public String getEditableFunctionList(@RequestAttribute(value = "userid") String headerUserId, @RequestBody String functionId) {
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        try {
            FunctionListUnapprovedDB countryUnapprovedDB = null;
            FunctionListDB countryDB = null;
<<<<<<< HEAD
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MFN001", language, FnOptConfig.fIdFunctionListController   , FnOptConfig.opFView)) {
                //user has the permission to view
                newReturnObject = functionListDAL.getEditableFunctionList(tableQuery, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MFN002", language));//server error code .
            logger.error(ex.getMessage(), "Exception in FunctionListController.getEditableFunctionList", this.newReturnObject.getAppErrorString());
=======
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdfunctionListController, FnOptConfig.opFView)) {
                //user has the permission to view
                newReturnObject = functionListDAL.getEditableFunctionList(functionId, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCT002", language));//error code
            logger.error(ex.getMessage(),"Exception in FunctionListController.getEditableFunctionList", this.newReturnObject.getAppErrorString());
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

        }
        return newReturnObject.setReturnJSON();
    }
<<<<<<< HEAD

    @PostMapping("/list")
    public String getListofFunction(@RequestAttribute(value = "userid") String headerUserId, @RequestBody String keyword) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            functionListDAL.getListOfFunction(keyword, this.newReturnObject, language);
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MFN002", language));//server error code .
            logger.error(ex.getMessage(), "Exception in FunctionListController.getListofFunction", this.newReturnObject.getAppErrorString());
        }
        return newReturnObject.setReturnJSON();
    }

    private boolean validateFunctionList(FunctionDefinition functionDefinition, String language) {

        //mandatory fields.
        try {
            if ((!functionDefinition.getFunctionId().isEmpty()) && (!functionDefinition.getFunctionName().isEmpty())) {

            } else {
                this.newReturnObject.addError(utilityService.singleError("MFN004", language));//error code for mandatory fields.
            }
            return this.newReturnObject.getAppErrors().size() > 0 ? false : true;
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MFN002", language));//error code for server.
            return false;
        }

=======
    private boolean validateFunctionList(FunctionDefinition functionDefinition, String language) {
        boolean valid = false;
        //mandatory fields.
        try {
            if ((!functionDefinition.getFunctionId().isEmpty()) && (!functionDefinition.getFunctionName().isEmpty())  ) {
                valid = true;
            } else {
                this.newReturnObject.addError(utilityService.singleError("MCT004", language));//error code
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCT002", language));//error code
        }
        return valid;
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
    }
}
