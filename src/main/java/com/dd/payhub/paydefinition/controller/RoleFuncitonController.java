package com.dd.payhub.paydefinition.controller;

import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.model.RoleFunction;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.config.FnOptConfig;
import com.dd.payhub.paydefinition.dal.FunctionListDAL;
import com.dd.payhub.paydefinition.dal.RoleFunctionDAL;
import com.dd.payhub.paydefinition.dal.TenantDAL;
import com.dd.payhub.paydefinition.service.PHUtilityService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", allowedHeaders = "Content-Type, Accept, reconnection, User-Token")
@RestController
@RequestMapping("/rolefunction")
public class RoleFuncitonController {
    private static final Logger logger = LogManager.getLogger(RoleFuncitonController.class);
    private RoleFunctionDAL roleFunctionDAL;
    private PHUtilityService utilityService;
    private TenantDAL tenantDAL;
    private FunctionListDAL functionListDAL;
    private NewReturnObject newReturnObject;
    @Autowired
    public RoleFuncitonController(RoleFunctionDAL roleFunctionDAL, PHUtilityService utilityService, TenantDAL tenantDAL, FunctionListDAL functionListDAL) {
        this.roleFunctionDAL = roleFunctionDAL;
        this.utilityService = utilityService;
        this.tenantDAL = tenantDAL;
        this.functionListDAL = functionListDAL;
        this.newReturnObject = new NewReturnObject();
    }




    @PostMapping("/all")
    public String getAllRoleFunction( @RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {//, , @RequestHeader(value = "bankid") String headerBankId
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        ReactTableRecord tableRecord = new ReactTableRecord();
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MRF001", language, FnOptConfig.fIdRoleFunctionController, FnOptConfig.opFView)) {//error code.
                tableRecord = roleFunctionDAL.getAllRoleFunctionByPagination(tableQuery);
                newReturnObject.PerformReturnObject(tableRecord);
            }
            return newReturnObject.setReturnJSON();
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MRF002", language));//error code
            logger.error(ex.getMessage(), "Exception in RoleFunctionController.getAllRoleFunction", this.newReturnObject.getAppErrorString());

            return newReturnObject.setReturnJSON();
        }
    }

    @PostMapping("/add")
    public String addRoleFunction(@RequestBody RoleFunction roleFunction, @RequestAttribute(value = "userid") String headerUserId) {
        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (validateRoleFunction(roleFunction, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MRF001", language, FnOptConfig.fIdRoleFunctionController, FnOptConfig.opFNew)) {//error code.
                //valid data to insert new record.
                long approvedResult = roleFunctionDAL.countByRoleId(roleFunction.getRoleId());
                long unapprovedResult = roleFunctionDAL.countUnapprovedByRoleId(roleFunction.getRoleId());
                if (approvedResult == 0 && unapprovedResult == 0) {//record does not exist so insertion can be done.
                    roleFunctionDAL.addRoleFunction(roleFunction, headerUserId, newReturnObject, language);//
                } else {//Error record exist duplication not allowed.
                    this.newReturnObject.addError(utilityService.singleError("MCT003", language));
                    this.newReturnObject.setReturncode(0);
                }
            }

        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MRF002", language));//error code
            logger.error(ex.getMessage(), "Exception in RoleFunctionController.addRoleFunction", this.newReturnObject.getAppErrorString());
        }
        return this.newReturnObject.setReturnJSON();

    }

    @PostMapping("/geteditable")
    public String getEditableRoleFunction(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery tableQuery) {
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MRF001", language, FnOptConfig.fIdRoleFunctionController, FnOptConfig.opFEdit)) {
                //user has the permission to Edit
                roleFunctionDAL.getEditableRoleFunction(tableQuery, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MRF002", language));//error code
            logger.error(ex.getMessage(), "Exception in RoleFunctionController.getEditableRoleFunction", this.newReturnObject.getAppErrorString());

        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/update")
    public String updateFunctionList(@RequestBody RoleFunction roleFunction, @RequestAttribute(value = "userid") String headerUserId) {

        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            if (validateRoleFunction(roleFunction, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MRF001", language, FnOptConfig.fIdRoleFunctionController, FnOptConfig.opFEdit)) {//error code.
                roleFunctionDAL.updateRoleFunction(roleFunction, headerUserId, this.newReturnObject, language);
            }

        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MFN002", language));//error code
            logger.error(ex.getMessage(), "Exception in FunctionListController.updateFunctionList", this.newReturnObject.getAppErrorString());

        }
        return this.newReturnObject.setReturnJSON();

    }

    @DeleteMapping("/delete")
    public String deleteRoleFunction(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery tableQuery) {
        try {

        } catch (Exception ex) {

        }
        return  "";
    }

    @PostMapping("/view")
    public String viewRoleFunction(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery tableQuery){
        try{

        }catch (Exception ex){

        }
        return "";
    }

    private boolean validateRoleFunction(RoleFunction roleFunction, String language) {
        boolean valid = true;
        //mandatory fields.
        try {
            if (roleFunction.getRoleId().isEmpty()) {
                this.newReturnObject.addError(utilityService.singleError("MRF011", language));
                valid = false;
            }
            if (roleFunction.getRoleName().isEmpty()) {
                this.newReturnObject.addError(utilityService.singleError("MRF012", language));
                valid = false;
            }
            if (roleFunction.getTenantId().isEmpty()) {
                this.newReturnObject.addError(utilityService.singleError("MRF013", language));
                valid = false;
            } else {
                if (tenantDAL.countByTenantId(roleFunction.getTenantId()) == 0) {
                    this.newReturnObject.addError(utilityService.singleError("MRF018", language));
                }
            }

            if (!valid) {
                this.newReturnObject.addError(utilityService.singleError("MRF004", language));//error code
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MRF002", language));//error code
        }
        return valid;
    }
}
