package com.dd.payhub.paydefinition.controller;


import com.dd.payhub.paycommon.model.Currency;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.config.FnOptConfig;
import com.dd.payhub.paydefinition.dal.CurrencyDAL;
import com.dd.payhub.paydefinition.service.PHUtilityService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/currency")
public class CurrencyController {
    private static final Logger logger = LogManager.getLogger(CurrencyController.class);
    private CurrencyDAL currencyDAL;
    private PHUtilityService utilityService;
    private NewReturnObject newReturnObject;

    @Autowired
    public CurrencyController(CurrencyDAL currencyDAL, PHUtilityService utilityService) {
        this.currencyDAL = currencyDAL;
        this.utilityService = utilityService;
        newReturnObject = new NewReturnObject();
    }

    @PostMapping("/all")
    public String getAllCurrency(@RequestBody ReactTableQuery tableQuery , @RequestAttribute(value = "userid") String headerUserId) {//, , @RequestAttribute(value = "bankid") String headerCustomerId
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        ReactTableRecord tableRecord = new ReactTableRecord();

        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCY001", language, FnOptConfig.fIdCurrencyController, FnOptConfig.opFView)) {
                tableRecord = currencyDAL.getAllCurrencyByPagination(tableQuery);
                newReturnObject.PerformReturnObject(tableRecord);
            }
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValueAsString(tableRecord);
            return newReturnObject.setReturnJSON();
        } catch (Exception ex) {
            tableRecord.setTotalPage( 0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord( new ArrayList<>());
            newReturnObject.PerformReturnObject(tableRecord);
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCY002", language));//error code
            logger.error("Exception in CurrencyController.getAllCurrency", this.newReturnObject.getAppErrorString(), ex);
            return newReturnObject.setReturnJSON();
        }
    }

    @PostMapping("/add")
    public String newCurrency(@RequestBody Currency currency, @RequestAttribute(value = "userid") String headerUserId) {
        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (validateCurrency(currency, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCY001", language, FnOptConfig.fIdCurrencyController, FnOptConfig.opFNew)) {//error code.
                //valid data to insert new record.
                long approvedCount = currencyDAL.countByCurrencyCode(currency.getCurrencyCode());
                long unapprovedCount = currencyDAL.countByUnapprovedCurrencyCode(currency.getCurrencyCode());
                if (  approvedCount  == 0 && unapprovedCount == 0) {//record does not exist so insertion can be done.
                    newReturnObject = currencyDAL.addCurrency(currency, headerUserId, newReturnObject, language);//
                } else {//Error record exist duplication not allowed.
                    this.newReturnObject.addError(utilityService.singleError("MCY003", language));
                    this.newReturnObject.setReturncode(0);
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCY002", language));//error code
            logger.error("Exception in CurrencyController.newCurrency", this.newReturnObject.getAppErrorString(), ex);

        }
        return this.newReturnObject.setReturnJSON();
    }

    @PostMapping("/geteditable")
    public String getEditableCurrency(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery tableQuery) {

        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCY001", language, FnOptConfig.fIdCurrencyController, FnOptConfig.opFView)) {
                //user has the permission to view
                newReturnObject = currencyDAL.getEditableCurrency(tableQuery, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCY002", language));//error code
            logger.error("Exception in CurrencyController.getEditableCurrency", this.newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/update")
    public String updateCurrency(@RequestBody Currency currency, @RequestAttribute(value = "userid") String headerUserId) {

        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            if (validateCurrency(currency, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCY001", language, FnOptConfig.fIdCurrencyController, FnOptConfig.opFEdit)) {//error code.
                newReturnObject = currencyDAL.updateCurrency(currency, headerUserId, this.newReturnObject, language);
            }

        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCY002", language));//error code
            logger.error("Exception in CurrencyController.updateCurrency", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @PostMapping("/view")
    public String viewCurrency(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCY001", language, FnOptConfig.fIdCurrencyController, FnOptConfig.opFView)) {//error code.
                newReturnObject = currencyDAL.viewByCurrencyCode(tableQuery, newReturnObject, language);
            }
        }catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCY002", language));//error code
            logger.error("Exception in CurrencyController.viewCurrency",this.newReturnObject.getAppErrorString(),ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/approve")
    public String approveCurrency(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery tableQuery) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCY001", language, FnOptConfig.fIdCurrencyController, FnOptConfig.opFAuth)) {//error code.

                newReturnObject = currencyDAL.approveCurrency(tableQuery, headerUserId, this.newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCY002", language));//error code
            logger.error("Exception in CurrencyController.approveCurrency", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @DeleteMapping("/reject")//api to delete the record from the tm_event_definition
    // _unapproved.
    public String rejectCurrency(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCY001", language, FnOptConfig.fIdCurrencyController, FnOptConfig.opFDelete)) {//error code.

                newReturnObject = currencyDAL.rejectByCurrencyCode(tableQuery, headerUserId, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCY002", language));//error code
            logger.error("Exception in CurrencyController.rejectCurrency", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();

    }

    @DeleteMapping("/delete")
    public String inActivateCurrency(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCY001", language, FnOptConfig.fIdCurrencyController, FnOptConfig.opFDelete)) {//error code.
                String status = currencyDAL.findByCurrencyCode(tableQuery.getPrimaryKey()).getCurrency().getStatus();
                if (status.equals("inactive")) {
                    this.newReturnObject.addError(utilityService.singleError("MCY012", language));//error code
                } else {
                    newReturnObject = currencyDAL.currencyStatusChanger(tableQuery, headerUserId, newReturnObject, language, "inactive");
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCY002", language));
            logger.error("Exception in CurrencyController.inActivate", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();

    }

    @PostMapping("/activate")
    public String reActivateCurrency(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            String collection=tableQuery.getCollection();
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCY001", language, FnOptConfig.fIdCurrencyController, FnOptConfig.opFReactivate)) {
                String status =collection.equals("approved")?currencyDAL.findByCurrencyCode(tableQuery.getPrimaryKey()).getCurrency().getStatus():currencyDAL.findUnapprovedByCurrencyCode(tableQuery.getPrimaryKey()).getCurrency().getStatus();
                if (status.equals("active")) {
                    this.newReturnObject.addError(utilityService.singleError("MCY011", language));//error code
                } else {
                    newReturnObject = currencyDAL.currencyStatusChanger(tableQuery, headerUserId, newReturnObject, language, "active");
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCY002", language));//error code
            logger.error("Exception in CurrencyController.reActivate", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();

    }


    @PostMapping("/currencynamesregx")
    public String listOFCountries(@RequestAttribute(value = "userid") String headerUserId,@RequestBody String keyword){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try{
            newReturnObject= currencyDAL.listOfAllCurrencyCodeAndNamesByRegx(keyword,newReturnObject,language);
        }catch (Exception ex) {
            logger.error("Exception in TenantController.listOFCountries", this.newReturnObject.getAppErrorString(), ex);
            newReturnObject.PerformReturnObject(new ArrayList<>());
            newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCY002", language));//error code

        }
        return newReturnObject.setReturnJSON();
    }



    private boolean validateCurrency(Currency currency, String language) {

        //mandatory fields
        try {
            if ((!currency.getCurrencyCode().isEmpty()) && (!currency.getCurrencyName().isEmpty()) && (!currency.getCurrencyShortName().isEmpty()) && (!String.valueOf(currency.getDecimals()).isEmpty()) && (!currency.getRoundingRule().isEmpty())  && (!String.valueOf(currency.getRoundToUnit()).isEmpty()))
            {

                if(!(currency.getCurrencyCode().length()==3)) //currencyCode should be Of 3 character .
                {
                    this.newReturnObject.addError(utilityService.singleError("MCY015", language));//error code
                }

                if(!(((currency.getRoundingRule().equals("NEAREST"))||(currency.getRoundingRule().equals("UP"))||(currency.getRoundingRule().equals("DOWN"))||(currency.getRoundingRule().equals("TRUNCATE"))))) //Should be any of this 2.
                {
                    this.newReturnObject.addError(utilityService.singleError("MCY016", language));//error code
                }

                if(!((currency.getRoundToUnit()==0)||(currency.getRoundToUnit()==1)||(currency.getRoundToUnit()==5)||(currency.getRoundToUnit()==10))){
                    this.newReturnObject.addError(utilityService.singleError("MCY013", language));//error code
                }
                if(!((currency.getDecimals()>=0)&&(currency.getDecimals()<=4)))
                {
                    this.newReturnObject.addError(utilityService.singleError("MCY014", language));//error code

                }
            } else {
                this.newReturnObject.addError(utilityService.singleError("MCY004", language));//error code
            }
            return this.newReturnObject.getAppErrors().size()>0?false:true;


        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCY002", language));//error code
            return false;
        }

    }
}
