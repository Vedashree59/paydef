<<<<<<< HEAD
//package com.dd.payhub.paydefinition.controller;
//
//import com.dd.payhub.paycommon.model.*;
//import com.dd.payhub.paycommon.view.NewReturnObject;
//import com.dd.payhub.paycommon.view.ReactTableRecord;
//import com.dd.payhub.paydefinition.config.FnOptConfig;
//import com.dd.payhub.paydefinition.dal.CountryDAL;
//import com.dd.payhub.paydefinition.dal.CurrencyCountryHolidaysDAL;
//import com.dd.payhub.paydefinition.dal.CurrencyDAL;
//import com.dd.payhub.paydefinition.dal.TenantDAL;
//import com.dd.payhub.paydefinition.entity.*;
//import com.dd.payhub.paydefinition.service.PHUtilityService;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.i18n.LocaleContextHolder;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
//@CrossOrigin(origins = "*", allowedHeaders = "*")
//@RestController
//@RequestMapping("/currencycountryholidays")
//public class CurrencyCountryHolidaysController {
//    private static final Logger logger = LogManager.getLogger(CurrencyCountryHolidaysController.class);
//    private CurrencyCountryHolidaysDAL currencyCountryHolidaysDAL;
//    private TenantDAL tenantDAL;
//    private CurrencyDAL currencyDAL;
//    private CountryDAL countryDAL;
//    private PHUtilityService utilityService;
//    private NewReturnObject newReturnObject;
//
//    @Autowired
//    public CurrencyCountryHolidaysController(TenantDAL tenantDAL, CountryDAL countryDAL,CurrencyDAL currencyDAL, CurrencyCountryHolidaysDAL currencyCountryHolidaysDAL, PHUtilityService utilityService) {
//        this.currencyCountryHolidaysDAL = currencyCountryHolidaysDAL;
//        this.utilityService = utilityService;
//        this.countryDAL = countryDAL;
//        this.currencyDAL = currencyDAL;
//        this.tenantDAL = tenantDAL;
//        newReturnObject = new NewReturnObject();
//    }
//
//
//    @PostMapping("/all")
//    public String getAllCurrencyCountryHolidays(@RequestBody ReactTableQuery tableQuery ,@RequestAttribute(value = "tenantId") String bankId, @RequestAttribute(value = "userid") String headerUserId) {//, , @RequestAttribute(value = "bankid") String headerTenantIdCurrencyCodeYear
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        ReactTableRecord tableRecord = new ReactTableRecord();
//
//        try {
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCurrencyCountryHolidaysController, FnOptConfig.opFView,bankId)) {
//                tableRecord = currencyCountryHolidaysDAL.getAllCurrencyCountryHolidaysByPagination(tableQuery);
//                newReturnObject.PerformReturnObject(tableRecord);
//            }
//            return newReturnObject.setReturnJSON();
//        } catch (Exception ex) {
//            tableRecord.setTotalPage( 0);
//            tableRecord.setTotalRecord(0);
//            tableRecord.setTableRecord( new ArrayList<>());
//            newReturnObject.PerformReturnObject(tableRecord);
//            this.newReturnObject.setReturncode(0);
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyCountryHolidaysController.getAllCurrencyCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
//            return newReturnObject.setReturnJSON();
//        }
//    }
//
//    @PostMapping("/add")
//    public String newCurrencyCountryHolidays(@RequestBody CurrencyCountryHolidays currencyCountryHolidays, @RequestAttribute(value = "tenantId") String bankId, @RequestAttribute(value = "userid") String headerUserId) {
//        //validation.
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//
//            if (validateCurrencyCountryHolidays(currencyCountryHolidays, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCurrencyCountryHolidaysController, FnOptConfig.opFNew,bankId)) {//error code.
//                //valid data to insert new record.
//                long approvedCount = currencyCountryHolidaysDAL.countByTenantIdCurrencyCodeYear(currencyCountryHolidays.getTenantIdCurrencyCodeYear());
//                long unapprovedCount = currencyCountryHolidaysDAL.countByUnapprovedTenantIdCurrencyCodeYear(currencyCountryHolidays.getTenantIdCurrencyCodeYear());
//                if (approvedCount == 0 && unapprovedCount == 0) {//record does not exist so insertion can be done.
//                    Boolean approvalRequired=tenantDAL.findApprovalRequiredTenantId(currencyCountryHolidays.getTenantId()).getTenant().getFourEyeRequired();
//                    newReturnObject = currencyCountryHolidaysDAL.addCurrencyCountryHolidays(currencyCountryHolidays, headerUserId, newReturnObject, language,approvalRequired);//
//                } else {//Error record exist duplication not allowed.
//                    this.newReturnObject.addError(utilityService.singleError("MCU003", language));
//                    this.newReturnObject.setReturncode(0);
//                }
//            }
//
//        } catch (Exception ex) {
//            this.newReturnObject.setReturncode(0);
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyCountryHolidaysController.newCurrencyCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
//        }
//        return this.newReturnObject.setReturnJSON();
//    }
//
//
//    @PostMapping("/geteditable")
//    public String getEditableCurrencyCountryHolidays(@RequestAttribute(value = "userid") String headerUserId,@RequestAttribute(value = "tenantId") String bankId, @RequestBody ReactTableQuery tableQuery) {
//
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        try {
//
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdCurrencyCountryHolidaysController, FnOptConfig.opFView,bankId)) {
//                //user has the permission to view
//                newReturnObject = currencyCountryHolidaysDAL.getEditableCurrencyCountryHolidays(tableQuery, newReturnObject, language);
//            }
//        } catch (Exception ex) {
//            this.newReturnObject.setReturncode(0);
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyCountryHolidaysController.getEditableCurrencyCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
//        }
//        return newReturnObject.setReturnJSON();
//    }
//    @PostMapping("/update")
//    public String updateCurrencyCountryHolidays(@RequestBody CurrencyCountryHolidays currencyCountryHolidays,@RequestAttribute(value = "tenantId") String bankId, @RequestAttribute(value = "userid") String headerUserId) {
//
//        //validation.
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//            //change for token in future.
//            if (validateCurrencyCountryHolidays(currencyCountryHolidays, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCurrencyCountryHolidaysController, FnOptConfig.opFEdit,bankId)) {//error code.
//                Boolean approvalRequired=tenantDAL.findApprovalRequiredTenantId(currencyCountryHolidays.getTenantId()).getTenant().getFourEyeRequired();
//                newReturnObject = currencyCountryHolidaysDAL.updateCurrencyCountryHolidays(currencyCountryHolidays, headerUserId, this.newReturnObject, language,approvalRequired);
//            }
//
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyCountryHolidaysController.updateCurrencyCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
//        }
//        return this.newReturnObject.setReturnJSON();
//
//    }
//
//    @PostMapping("/view")
//    public String viewCurrencyCountryHolidays(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "tenantId") String bankId,@RequestAttribute(value = "userid") String headerUserId) {
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCurrencyCountryHolidaysController, FnOptConfig.opFView,bankId)) {//error code.
//
//                newReturnObject = currencyCountryHolidaysDAL.viewByTenantIdCurrencyCodeYear(tableQuery, newReturnObject, language);
//            }
//        }catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyCountryHolidaysController.viewCurrencyCountryHolidays",this.newReturnObject.getAppErrorString(),ex);
//        }
//        return newReturnObject.setReturnJSON();
//    }
//
//
//    @PostMapping("/approve")
//    public String approveCurrencyCountryHolidays(@RequestAttribute(value = "userid") String headerUserId,@RequestAttribute(value = "tenantId") String bankId, @RequestBody ReactTableQuery tableQuery) {
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCurrencyCountryHolidaysController, FnOptConfig.opFAuth,bankId)) {//error code.
//
//                newReturnObject = currencyCountryHolidaysDAL.approveCurrencyCountryHolidays(tableQuery, headerUserId, this.newReturnObject, language);
//            }
//
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyCountryHolidaysController.approveCurrencyCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
//        }
//        return this.newReturnObject.setReturnJSON();
//    }
//
//    @DeleteMapping("/reject")//api to delete the record from the tm_event_definition
//    // _unapproved.
//    public String rejectCurrencyCountryHolidays(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFDelete)) {//error code.
//                newReturnObject = currencyCountryHolidaysDAL.rejectByTenantIdCurrencyCodeYear(tableQuery, headerUserId, newReturnObject, language);
//            }
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyCountryHolidaysController.rejectCurrencyCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
//
//        }
//        return newReturnObject.setReturnJSON();
//    }
//    @DeleteMapping("/delete")
//    public String inActivateCurrencyCountryHolidays(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFDelete)) {//error code.
//                boolean approvalRequired = approvalRequirementOfTenant(tableQuery.getPrimaryKey(),language);
//                String status = currencyCountryHolidaysDAL.findByTenantIdCurrencyCodeYear(tableQuery.getPrimaryKey()).getCurrencyCountryHolidays().getStatus();
//                if (status.equals("inactive")) {
//                    this.newReturnObject.addError(utilityService.singleError("MCU012", language));//error code
//                } else {
//                    newReturnObject = currencyCountryHolidaysDAL.currencyCountryHolidaysStatusChanger(tableQuery, headerUserId, newReturnObject, language,"inactive",approvalRequired);
//                }
//            }
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyCountryHolidaysController.inActivateCurrencyCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
//
//        }
//        return newReturnObject.setReturnJSON();
//    }
//    @PostMapping("/activate")
//    public String reActivateCurrencyCountryHolidays(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFReactivate)) {//error code.
//                boolean approvalRequired = approvalRequirementOfTenant(tableQuery.getPrimaryKey(),language);
//                String status =tableQuery.getCollection().equals("approved")?currencyCountryHolidaysDAL.findByTenantIdCurrencyCodeYear(tableQuery.getPrimaryKey()).getCurrencyCountryHolidays().getStatus():currencyCountryHolidaysDAL.findUnapprovedByTenantIdCurrencyCodeYear(tableQuery.getPrimaryKey()).getCurrencyCountryHolidays().getStatus();
//
//                if (status.equals("active")) {
//                    this.newReturnObject.addError(utilityService.singleError("MCU011", language));//error code
//                } else {
//                    newReturnObject = currencyCountryHolidaysDAL.currencyCountryHolidaysStatusChanger(tableQuery, headerUserId, newReturnObject, language,"active",approvalRequired);
//                }
//            }
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyCountryHolidaysController.reActivateCurrencyCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
//
//        }
//        return newReturnObject.setReturnJSON();
//
//    }
//
//    private boolean validateCurrencyCountryHolidays(CurrencyCountryHolidays currencyCountryHolidays, String language) {
//
//        //mandatory fields.
//        try {
//            if ((!currencyCountryHolidays.getTenantId().isEmpty()) && (!currencyCountryHolidays.getCountryCode().isEmpty()) && (!currencyCountryHolidays.getCurrencyCode().isEmpty())&& ((currencyCountryHolidays.getYear().length()) == 4) && (currencyCountryHolidays.getHolidayList() != null)) {
//                if (tenantIdCurrencyCountryCodeCheck(currencyCountryHolidays, language)) {
//                    if (!(currencyCountryHolidays.getYear().length() == 4)) {
//                        this.newReturnObject.addError(utilityService.singleError("MCUH11", language));//error code
//                    }
//                    currencyCountryHolidays = longMediumDateCalcuator(currencyCountryHolidays);
//                }
//            } else {
//                this.newReturnObject.addError(utilityService.singleError("MCY004", language));//error code
//
//            }
//            return this.newReturnObject.getAppErrors().size() > 0 ? false : true;
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCY002", language));//error code
//            return false;
//        }
//
//    }
//
//    private CurrencyCountryHolidays longMediumDateCalcuator(CurrencyCountryHolidays currencyCountryHolidays){
//        List<Holiday> holidays=currencyCountryHolidays.getHolidayList();
//        for(int i = 0; i< holidays.size();i++){
////            if((currencyCountryHolidays.getYear()).equals(holidays.get(i).getHolidayDate().getYear())){
//            Holiday holiday = holidays.get(i);
//            PayHubDate payHubDate = new PayHubDate(holiday.getHolidayDate().getYear(),holiday.getHolidayDate().getMonth(),holiday.getHolidayDate().getDay());
//            holidays.get(i).setHolidayDate(payHubDate);
////            }
////            else{
////                this.newReturnObject.addError(utilityService.singleError( "MCCY003",language));
////            }
//
//        }
//        return currencyCountryHolidays;
//    }
//
//    private boolean tenantIdCurrencyCountryCodeCheck(CurrencyCountryHolidays currencyCountryHolidays, String language) {
//
//        try {
//
//            TenantDB approvedTenantResult = tenantDAL.findByTenantId(currencyCountryHolidays.getTenantId());
//            CurrencyDB approvedCurrencyCodeResult = currencyDAL.findByCurrencyCode(currencyCountryHolidays.getCurrencyCode());
//            CountryDB approvedCountryCodeResult = countryDAL.findByCountryCode(currencyCountryHolidays.getCountryCode());
//            if (approvedTenantResult == null && approvedCountryCodeResult == null && approvedCurrencyCodeResult ==null) {
//                this.newReturnObject.addError(utilityService.singleError("MCCY009", language));
//            }
//            return this.newReturnObject.getAppErrors().size() > 0 ? false : true;
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCCY002", language));//error code
//            logger.error("Exception in CountryHolidaysController.updateCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
//            return false;
//        }
//
//    }
//}
=======
package com.dd.payhub.paydefinition.controller;

import com.dd.payhub.paycommon.model.*;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.config.FnOptConfig;
import com.dd.payhub.paydefinition.dal.CountryDAL;
import com.dd.payhub.paydefinition.dal.CurrencyCountryHolidaysDAL;
import com.dd.payhub.paydefinition.dal.CurrencyDAL;
import com.dd.payhub.paydefinition.dal.TenantDAL;
import com.dd.payhub.paydefinition.entity.*;
import com.dd.payhub.paydefinition.service.PHUtilityService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/currencycountryholidays")
public class CurrencyCountryHolidaysController {
    private static final Logger logger = LogManager.getLogger(CurrencyCountryHolidaysController.class);
    private CurrencyCountryHolidaysDAL currencyCountryHolidaysDAL;
    private TenantDAL tenantDAL;
    private CurrencyDAL currencyDAL;
    private CountryDAL countryDAL;
    private PHUtilityService utilityService;
    private NewReturnObject newReturnObject;

    @Autowired
    public CurrencyCountryHolidaysController(TenantDAL tenantDAL, CountryDAL countryDAL,CurrencyDAL currencyDAL, CurrencyCountryHolidaysDAL currencyCountryHolidaysDAL, PHUtilityService utilityService) {
        this.currencyCountryHolidaysDAL = currencyCountryHolidaysDAL;
        this.utilityService = utilityService;
        this.countryDAL = countryDAL;
        this.currencyDAL = currencyDAL;
        this.tenantDAL = tenantDAL;
        newReturnObject = new NewReturnObject();
    }

    @PostMapping("/all")
    public String getAllCurrencyCountryHolidays(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        ReactTableRecord tableRecord = new ReactTableRecord();
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCCY001", language, FnOptConfig.fIdCurrencyCountryHolidaysController, FnOptConfig.opFView)) {//error code.
                tableRecord = currencyCountryHolidaysDAL.getAllCurrencyCountryHolidaysByPagination(tableQuery);
                newReturnObject.PerformReturnObject(tableRecord);
            }
            return newReturnObject.setReturnJSON();
        } catch (Exception ex) {
            logger.error(ex);
            tableRecord.setTotalPage( 0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord( new ArrayList<>());
            newReturnObject.PerformReturnObject(tableRecord);
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCCY002", language));//error code
            logger.error("Exception in CurrencyCountryHolidaysController.getAllCurrencyCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
            return newReturnObject.setReturnJSON();
        }
    }

    @PostMapping("/add")
    public String newCurrencyCountryHolidays(@RequestBody CurrencyCountryHolidays currencyCountryHolidays, @RequestAttribute(value = "userid") String headerUserId) {
        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            if (validateCurrencyCountryHolidays(currencyCountryHolidays, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCCY001", language, FnOptConfig.fIdCurrencyCountryHolidaysController, FnOptConfig.opFNew)) {//error code.
                //valid data to insert new record.
                CurrencyCountryHolidaysDB currencyCountryHolidaysDB = currencyCountryHolidaysDAL.findByTenantIdCurrencyCountryCodeYear(currencyCountryHolidays.getTenantId(),currencyCountryHolidays.getCountryCode(),currencyCountryHolidays.getCurrencyCode(),currencyCountryHolidays.getYear());
                CurrencyCountryHolidaysUnapprovedDB currencyCountryHolidaysUnapprovedDB = currencyCountryHolidaysDAL.findUnapprovedByTenantIdCurrencyCountryCodeYear(currencyCountryHolidays.getTenantId(),currencyCountryHolidays.getCountryCode(),currencyCountryHolidays.getCurrencyCode(),currencyCountryHolidays.getYear());
                if (currencyCountryHolidaysDB == null && currencyCountryHolidaysUnapprovedDB == null) {//record does not exist so insertion can be done.
                    Boolean approvalRequired=tenantDAL.findApprovalRequiredTenantId(currencyCountryHolidays.getTenantId()).getTenant().getFourEyeRequired();
                    newReturnObject = currencyCountryHolidaysDAL.addCurrencyCountryHolidays(currencyCountryHolidays, headerUserId, newReturnObject, language,approvalRequired);//
                } else {//Error record exist duplication not allowed.
                    this.newReturnObject.addError(utilityService.singleError("MCCY003", language));
                    this.newReturnObject.setReturncode(0);
                }
            }
        } catch (Exception ex) {
            logger.error(ex);
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCCY002", language));//error code
            logger.error("Exception in CurrencyCountryHolidaysController.newCurrencyCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @PostMapping("/update")
    public String updateCurrencyCountryHolidays(@RequestBody CurrencyCountryHolidays currencyCountryHolidays, @RequestAttribute(value = "userid") String headerUserId) {
        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            if (validateCurrencyCountryHolidays(currencyCountryHolidays, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCCY001", language, FnOptConfig.fIdCurrencyCountryHolidaysController, FnOptConfig.opFNew)) {//error code.
                Boolean approvalRequired=tenantDAL.findApprovalRequiredTenantId(currencyCountryHolidays.getTenantId()).getTenant().getFourEyeRequired();
                newReturnObject = currencyCountryHolidaysDAL.updateCurrencyCountryHolidays(currencyCountryHolidays, headerUserId, this.newReturnObject, language,approvalRequired);
            }
        } catch (Exception ex) {
            logger.error(ex);
            this.newReturnObject.addError(utilityService.singleError("MCCY002", language));//error code
            logger.error("Exception in CurrencyCountryHolidaysController.updateCurrencyCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }


    @PostMapping("/geteditable")
    public String getEditableCurrencyCountryHolidays(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery reactTableQuery) {

        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCCY001", language, FnOptConfig.fIdCurrencyCountryHolidaysController, FnOptConfig.opFView)) {
                //user has the permission to view
                newReturnObject = currencyCountryHolidaysDAL.getEditableCurrencyCountryHolidays(reactTableQuery, newReturnObject, language);
            }
        } catch (Exception ex) {
            logger.error(ex);
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCCY002", language));//error code
            logger.error("Exception in CurrencyCountryHolidaysController.getEditableCurrencyCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/approve")
    public String approveCurrencyCountryHolidays(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery reactTableQuery) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCCY001", language, FnOptConfig.fIdCurrencyCountryHolidaysController, FnOptConfig.opFAuth)) {//error code.

                newReturnObject = currencyCountryHolidaysDAL.approveCurrencyCountryHolidays(reactTableQuery, headerUserId, this.newReturnObject, language);
            }

        } catch (Exception ex) {
            logger.error(ex);
            this.newReturnObject.addError(utilityService.singleError("MCCY002", language));//error code
            logger.error("Exception in CurrencyCountryHolidaysController.approveHolidays", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @DeleteMapping("/reject")//api to delete the record from the tm_country_holidays_unapproved.
    public String rejectCurrencyCountryHolidays(@RequestBody ReactTableQuery reactTableQuery, @RequestAttribute(value = "userid") String headerUserId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCCY001", language, FnOptConfig.fIdCurrencyCountryHolidaysController, FnOptConfig.opFDelete)) {//error code.

                newReturnObject = currencyCountryHolidaysDAL.deleteByTenantIdCurrencyCountryCodeYear(reactTableQuery, headerUserId, newReturnObject, language);
            }
        } catch (Exception ex) {
            logger.error(ex);
            logger.error("Exception in CurrencyCountryHolidaysController.rejectCurrencyCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
            this.newReturnObject.addError(utilityService.singleError("MCCY002", language));//error code

        }
        return newReturnObject.setReturnJSON();

    }

    @PostMapping("/view")
    public String view(@RequestBody ReactTableQuery tableQuery) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            newReturnObject= currencyCountryHolidaysDAL.viewByTableQuery(tableQuery,newReturnObject,language);
        }catch (Exception ex) {
            logger.error(ex);
            this.newReturnObject.addError(utilityService.singleError("MCCY002", language));//error code
            logger.error("Exception in CurrencyCountryHolidaysController.view",this.newReturnObject.getAppErrorString(),ex);
        }
        return newReturnObject.setReturnJSON();


    }


    private boolean validateCurrencyCountryHolidays(CurrencyCountryHolidays currencyCountryHolidays, String language) {

        //mandatory fields.
        try {
            if ((!currencyCountryHolidays.getTenantId().isEmpty()) && (!currencyCountryHolidays.getCountryCode().isEmpty()) && (!currencyCountryHolidays.getCurrencyCode().isEmpty())&& ((currencyCountryHolidays.getYear().length()) == 4) && (currencyCountryHolidays.getHolidayList() != null)) {
                if (tenantIdCurrencyCountryCodeCheck(currencyCountryHolidays, language)) {
                    if (!(currencyCountryHolidays.getYear().length() == 4)) {
                        this.newReturnObject.addError(utilityService.singleError("MCUH11", language));//error code
                    }
                    currencyCountryHolidays = longMediumDateCalcuator(currencyCountryHolidays);
                }
            } else {
                this.newReturnObject.addError(utilityService.singleError("MCY004", language));//error code

            }
            return this.newReturnObject.getAppErrors().size() > 0 ? false : true;
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCY002", language));//error code
            return false;
        }

    }

    private CurrencyCountryHolidays longMediumDateCalcuator(CurrencyCountryHolidays currencyCountryHolidays){
        List<Holiday> holidays=currencyCountryHolidays.getHolidayList();
        for(int i = 0; i< holidays.size();i++){
//            if((currencyCountryHolidays.getYear()).equals(holidays.get(i).getHolidayDate().getYear())){
            Holiday holiday = holidays.get(i);
            PayHubDate payHubDate = new PayHubDate(holiday.getHolidayDate().getYear(),holiday.getHolidayDate().getMonth(),holiday.getHolidayDate().getDay());
            holidays.get(i).setHolidayDate(payHubDate);
//            }
//            else{
//                this.newReturnObject.addError(utilityService.singleError( "MCCY003",language));
//            }

        }
        return currencyCountryHolidays;
    }

    private boolean tenantIdCurrencyCountryCodeCheck(CurrencyCountryHolidays currencyCountryHolidays, String language) {

        try {

            TenantDB approvedTenantResult = tenantDAL.findByTenantId(currencyCountryHolidays.getTenantId());
            CurrencyDB approvedCurrencyCodeResult = currencyDAL.findByCurrencyCode(currencyCountryHolidays.getCurrencyCode());
            CountryDB approvedCountryCodeResult = countryDAL.findByCountryCode(currencyCountryHolidays.getCountryCode());
            if (approvedTenantResult == null && approvedCountryCodeResult == null && approvedCurrencyCodeResult ==null) {
                this.newReturnObject.addError(utilityService.singleError("MCCY009", language));
            }
            return this.newReturnObject.getAppErrors().size() > 0 ? false : true;
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCCY002", language));//error code
            logger.error("Exception in CountryHolidaysController.updateCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
            return false;
        }

    }
}
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
