package com.dd.payhub.paydefinition.controller;


import com.dd.payhub.paycommon.model.ProductDefinition;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.config.FnOptConfig;
import com.dd.payhub.paydefinition.dal.ProductDefinitionDAL;
import com.dd.payhub.paydefinition.dal.TenantDAL;
import com.dd.payhub.paydefinition.dal.UserDAL;
import com.dd.payhub.paydefinition.entity.ProductDefinitionDB;
import com.dd.payhub.paydefinition.entity.TenantDB;
import com.dd.payhub.paydefinition.service.PHUtilityService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/productdefinition")
public class ProductDefinitionController {

    private static final Logger logger = LogManager.getLogger(ProductDefinitionController.class);
    private ProductDefinitionDAL productDefinitionDAL;
    private TenantDAL tenantDAL;
    private PHUtilityService utilityService;
    private NewReturnObject newReturnObject;
    @Autowired
    public ProductDefinitionController(UserDAL userDAL, ProductDefinitionDAL productDefinitionDAL, TenantDAL tenantDAL, PHUtilityService utilityService) {
        this.productDefinitionDAL = productDefinitionDAL;
        this.tenantDAL = tenantDAL;
        this.utilityService = utilityService;
        newReturnObject=new NewReturnObject();
    }

    @PostMapping("/all")
    public String getAllProductDefinition(@RequestBody ReactTableQuery tableQuery ,@RequestAttribute(value = "tenantId") String tenantId, @RequestAttribute(value = "userid") String headerUserId) {//, , @RequestAttribute(value = "bankid") String headerProductDefinitionId
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        newReturnObject = new NewReturnObject(0, "\"\"");
        ReactTableRecord tableRecord = new ReactTableRecord();

        try {
            if (utilityService.creatorHasPermission(headerUserId, newReturnObject, "MPD001", language, FnOptConfig.fIdProductDefinitionController, FnOptConfig.opFView,tenantId)) {
                tableRecord = productDefinitionDAL.getAllProductDefinitionByPagination(tableQuery);
                newReturnObject.PerformReturnObject(tableRecord);
            }
            return newReturnObject.setReturnJSON();
        } catch (Exception ex) {
            tableRecord.setTotalPage( 0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord( new ArrayList<>());
            newReturnObject.PerformReturnObject(tableRecord);
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MPD002", language));//error code
            logger.error("Exception in ProductDefinitionController.getAllProductDefinition", this.newReturnObject.getAppErrorString(), ex);
            return newReturnObject.setReturnJSON();
        }
    }

    @PostMapping("/add")
    public String newProductDefinition(@RequestBody ProductDefinition productDefinition ,@RequestAttribute(value = "tenantId") String tenantId, @RequestAttribute(value = "userid") String headerUserId) {
        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (validateProductDefinition(productDefinition, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MPD001", language, FnOptConfig.fIdProductDefinitionController, FnOptConfig.opFNew, tenantId)) {//error code.
                //valid data to insert new record.
                long approvedCount = productDefinitionDAL.countByProductId(productDefinition.getProductId());
                long unapprovedCount = productDefinitionDAL.countByUnapprovedProductId(productDefinition.getProductId());
                if (approvedCount == 0 && unapprovedCount == 0) {//record does not exist so insertion can be done.
                    Boolean approvalRequired = tenantDAL.findApprovalRequiredTenantId(productDefinition.getTenantId()).getTenant().getFourEyeRequired();
                    newReturnObject = productDefinitionDAL.addProductDefinition(productDefinition, headerUserId, newReturnObject, language, approvalRequired);//
                }else {//Error record exist duplication not allowed.
                    this.newReturnObject.addError(utilityService.singleError("MPD003", language));
                    this.newReturnObject.setReturncode(0);
                }
            }

        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MPD002", language));//error code
            logger.error("Exception in ProductDefinitionController.newProductDefinition", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

     @PostMapping("/geteditable")
    public String getEditableProductDefinition(@RequestAttribute(value = "userid") String headerUserId,@RequestAttribute(value = "tenantId") String tenantId, @RequestBody ReactTableQuery tableQuery) {

         String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
         this.newReturnObject = new NewReturnObject(0, "\"\"");
         try {

             if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MPD001", language, FnOptConfig.fIdProductDefinitionController, FnOptConfig.opFView,tenantId)) {
                 //user has the permission to view
                 newReturnObject = productDefinitionDAL.getEditableProductDefinition(tableQuery, newReturnObject, language);
             }
         } catch (Exception ex) {
             this.newReturnObject.setReturncode(0);
             this.newReturnObject.addError(utilityService.singleError("MPD002", language));//error code
             logger.error("Exception in ProductDefinitionController.getEditableProductDefinition", this.newReturnObject.getAppErrorString(), ex);
         }
         return newReturnObject.setReturnJSON();
     }

    @PostMapping("/update")
    public String updateProductDefinition(@RequestBody ProductDefinition productDefinition, @RequestAttribute(value = "userid") String headerUserId,@RequestAttribute(value = "tenantId") String tenantId) {

        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            if (validateProductDefinition(productDefinition, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MPD001", language, FnOptConfig.fIdProductDefinitionController, FnOptConfig.opFEdit,tenantId)) {//error code.
                Boolean approvalRequired=tenantDAL.findApprovalRequiredTenantId(productDefinition.getTenantId()).getTenant().getFourEyeRequired();
                newReturnObject = productDefinitionDAL.updateProductDefinition(productDefinition, headerUserId, this.newReturnObject, language,approvalRequired);
            }

        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MPD002", language));//error code
            logger.error("Exception in ProductDefinitionController.updateProductDefinition", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @PostMapping("/view")
    public String viewProductDefinition(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "tenantId") String tenantId,@RequestAttribute(value = "userid") String headerUserId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MPD001", language, FnOptConfig.fIdProductDefinitionController, FnOptConfig.opFView,tenantId)) {//error code.

                newReturnObject = productDefinitionDAL.viewByProductId(tableQuery, newReturnObject, language);
            }
        }catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MPD002", language));//error code
            logger.error("Exception in ProductDefinitionController.viewProductDefinition",this.newReturnObject.getAppErrorString(),ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/approve")
    public String approveProductDefinition(@RequestAttribute(value = "userid") String headerUserId, @RequestAttribute(value = "tenantId") String tenantId, @RequestBody ReactTableQuery tableQuery) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MPD001", language, FnOptConfig.fIdProductDefinitionController, FnOptConfig.opFAuth,tenantId)) {//error code.

                newReturnObject = productDefinitionDAL.approveProductDefinition(tableQuery, headerUserId, this.newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MPD002", language));//error code
            logger.error("Exception in ProductDefinitionController.approveProductDefinition", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @DeleteMapping("/reject")//api to delete the record from the tm_event_definition
    // _unapproved.
    public String rejectProductDefinition(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MPD001", language, FnOptConfig.fIdProductDefinitionController, FnOptConfig.opFDelete)) {//error code.

                newReturnObject = productDefinitionDAL.rejectByProductId(tableQuery, headerUserId, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MPD002", language));//error code
            logger.error("Exception in ProductDefinitionController.rejectProductDefinition", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();

    }

    @DeleteMapping("/delete")
    public String inActivateProductDefinition(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MPD001", language, FnOptConfig.fIdProductDefinitionController, FnOptConfig.opFDelete)) {//error code.
                boolean approvalRequired = approvalRequirementOfTenant(tableQuery.getPrimaryKey(),language);
                String status = productDefinitionDAL.findByProductId(tableQuery.getPrimaryKey()).getProductDefinition().getStatus();
                if (status.equals("inactive")) {
                    this.newReturnObject.addError(utilityService.singleError("MPD012", language));//error code same status
                } else {
                    newReturnObject = productDefinitionDAL.productDefinitionStatusChanger(tableQuery, headerUserId, newReturnObject, language,"inactive",approvalRequired);
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MPD002", language));//error code
            logger.error("Exception in ProductDefinitionController.inActivateProductDefinition", this.newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();

    }
<<<<<<< HEAD
    @PostMapping("/activate")
    public String activateProductDefinition(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            String collection=tableQuery.getCollection();
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MPD001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFReactivate)) {
                boolean approvalRequired = approvalRequirementOfTenant(tableQuery.getPrimaryKey(),language);
                String status = productDefinitionDAL.findByProductId(tableQuery.getPrimaryKey()).getProductDefinition().getStatus();
                if (status.equals("active")) {
                    this.newReturnObject.addError(utilityService.singleError("MPD011", language));//error code
                } else {
                    newReturnObject = productDefinitionDAL.productDefinitionStatusChanger(tableQuery, headerUserId, newReturnObject, language,"active",approvalRequired);
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MPD002", language));//error code
            logger.error("Exception in EventDefinitionController.reActivate", this.newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();

    }
=======

>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
    private boolean validateProductDefinition(ProductDefinition productDefinition, String language) {
        //mandatory fields.
        try {
            if ((!productDefinition.getTenantId().isEmpty()) && (!productDefinition.getProductId().isEmpty()) && (!productDefinition.getAllowedEventList().isEmpty())&&(productDefinition!=null)) {
                TenantDB tenantDB  = tenantDAL.findByTenantId(productDefinition.getTenantId());
                if (tenantDB == null) {
                    this.newReturnObject.addError(utilityService.singleError("MPD0014", language));
                }
                if (!((productDefinition.getProductType().equals("FXSPOT")) || (productDefinition.getProductType().equals("FXFWD")) || (productDefinition.getProductType().equals("FX")))) {
                    this.newReturnObject.addError(utilityService.singleError("MPD015", language));//error code
                }
            }
            return this.newReturnObject.getAppErrors().size()>0?false:true;
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MPD002", language));//error code
            return false;
        }
    }

    private boolean approvalRequirementOfTenant(String primaryKey, String language) {
        try {
            ProductDefinitionDB productDefinitionDB = productDefinitionDAL.findByProductId(primaryKey);
            String tenantID = null;
            TenantDB tenantDB = null;
            if (productDefinitionDB == null) {
                this.newReturnObject.addError(utilityService.singleError("MPD006", language));
            } else {
                tenantDB = tenantDAL.findApprovalRequiredTenantId(productDefinitionDB.getProductDefinition().getTenantId());
            }
            if (tenantDB == null) {
                this.newReturnObject.addError(utilityService.singleError("MPD006", language));
            } else {
                return tenantDB.getTenant().getFourEyeRequired();
            }
            return this.newReturnObject.getAppErrors().size() > 0 ? true : tenantDB.getTenant().getFourEyeRequired();
        }catch  (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MPD002", language));
            logger.error(ex.getMessage());
            return true;
        }
    }
}
