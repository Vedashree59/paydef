package com.dd.payhub.paydefinition.controller;


import com.dd.payhub.paycommon.model.*;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.config.FnOptConfig;
import com.dd.payhub.paydefinition.dal.CurrencyDAL;
import com.dd.payhub.paydefinition.dal.CurrencyHolidaysDAL;
import com.dd.payhub.paydefinition.dal.TenantDAL;
import com.dd.payhub.paydefinition.dal.UserDAL;
import com.dd.payhub.paydefinition.entity.*;
import com.dd.payhub.paydefinition.service.PHUtilityService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/currencyholidays")
public class CurrencyHolidaysController {

    private static final Logger logger = LogManager.getLogger(CurrencyController.class);
    private CurrencyHolidaysDAL currencyHolidaysDAL;
    private PHUtilityService utilityService;
    private TenantDAL tenantDAL;
    private UserDAL userDAL;
    private CurrencyDAL currencyDAL;
    private NewReturnObject newReturnObject;

    @Autowired
    public CurrencyHolidaysController(CurrencyHolidaysDAL currencyHolidaysDAL,TenantDAL tenantDAL,UserDAL userDAL,CurrencyDAL currencyDAL, PHUtilityService utilityService) {
        this.currencyHolidaysDAL = currencyHolidaysDAL;
        this.utilityService = utilityService;
        this.userDAL = userDAL;
        this.tenantDAL = tenantDAL;
        this.currencyDAL = currencyDAL;
        newReturnObject=new NewReturnObject();
    }

    @PostMapping("/all")

<<<<<<< HEAD
    public String getAllCurrencyHolidays(@RequestBody ReactTableQuery tableQuery ,@RequestHeader(value = "tenantId") String tenantId, @RequestHeader(value = "userid") String headerUserId) {
=======
    public String getAllCurrencyHolidays(@RequestBody ReactTableQuery tableQuery ,@RequestAttribute(value = "tenantId") String tenantId, @RequestAttribute(value = "userid") String headerUserId) {
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        newReturnObject = new NewReturnObject(0, "\"\"");
        ReactTableRecord tableRecord = new ReactTableRecord();
        try {
            if (utilityService.creatorHasPermission(headerUserId, newReturnObject, "MCYH01", language, FnOptConfig.fIdCurrencyHolidaysController, FnOptConfig.opFView,tenantId)) {//error code.
                tableRecord = currencyHolidaysDAL.getAllCurrencyHolidaysByPagination(tableQuery);
                newReturnObject.PerformReturnObject(tableRecord);
            }
            return newReturnObject.setReturnJSON();
        } catch (Exception ex) {
            logger.error(ex);
            tableRecord.setTotalPage( 0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord( new ArrayList<>());
            newReturnObject.PerformReturnObject(tableRecord);
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCYH02", language));//error code
            logger.error("Exception in CurrencyHolidaysController.getAllCurrencyHolidays", this.newReturnObject.getAppErrorString(), ex);
            return newReturnObject.setReturnJSON();
        }
    }

    @PostMapping("/add")
<<<<<<< HEAD
    public String newCurrencyHolidays(@RequestBody CurrencyHolidays currencyHolidays, @RequestHeader(value = "userid") String headerUserId, @RequestHeader(value = "tenantId") String tenantId) {
=======
    public String newCurrencyHolidays(@RequestBody CurrencyHolidays currencyHolidays, @RequestAttribute(value = "userid") String headerUserId, @RequestAttribute(value = "tenantId") String tenantId) {
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (validateCurrencyHolidays(currencyHolidays, language,headerUserId) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYH01", language, FnOptConfig.fIdCurrencyHolidaysController, FnOptConfig.opFNew,tenantId)) {//error code.
                //valid data to insert new record.
                long approvedCount = currencyHolidaysDAL.countByTenantIdCurrencyCodeYear(currencyHolidays.getTenantId(),currencyHolidays.getCurrencyCode(),currencyHolidays.getYear());
                long unapprovedCount = currencyHolidaysDAL.countByUnapprovedTenantIdCurrencyCodeYear(currencyHolidays.getTenantId(),currencyHolidays.getCurrencyCode(),currencyHolidays.getYear());
                if (approvedCount == 0 && unapprovedCount == 0) {//record does not exist so insertion can be done.
                    Boolean approvalRequired=tenantDAL.findApprovalRequiredTenantId(currencyHolidays.getTenantId()).getTenant().getFourEyeRequired();
                    newReturnObject = currencyHolidaysDAL.addCurrencyHolidays(currencyHolidays, headerUserId, newReturnObject, language,approvalRequired);//
                } else {//Error record exist duplication not allowed.
                    this.newReturnObject.addError(utilityService.singleError("MCYH03", language));
                    this.newReturnObject.setReturncode(0);
                }
            }

        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCYH02", language));//error code
            logger.error("Exception in CurrencyHolidaysController.newCurrencyHolidays", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }


    @PostMapping("/geteditable")

<<<<<<< HEAD
    public String getEditableCurrencyHolidays(@RequestHeader(value = "userid") String headerUserId, @RequestHeader(value = "tenantId") String tenantId, @RequestBody ReactTableQuery tableQuery) {
=======
    public String getEditableCurrencyHolidays(@RequestAttribute(value = "userid") String headerUserId, @RequestAttribute(value = "tenantId") String tenantId, @RequestBody ReactTableQuery tableQuery) {
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYH01", language, FnOptConfig.fIdCurrencyHolidaysController, FnOptConfig.opFView,tenantId)) {
                //user has the permission to view
                newReturnObject = currencyHolidaysDAL.getEditableCurrencyHolidays(tableQuery, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCYH02", language));//error code
            logger.error("Exception in CurrencyHolidaysController.getEditableCurrencyHolidays", this.newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }


    @PostMapping("/update")
<<<<<<< HEAD
    public String updateCurrencyHolidays(@RequestBody CurrencyHolidays currencyHolidays, @RequestHeader(value = "userid") String headerUserId,@RequestHeader(value = "tenantId") String tenantId) {
=======
    public String updateCurrencyHolidays(@RequestBody CurrencyHolidays currencyHolidays, @RequestAttribute(value = "userid") String headerUserId,@RequestAttribute(value = "tenantId") String tenantId) {
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            if (validateCurrencyHolidays(currencyHolidays, language,headerUserId) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYH01", language, FnOptConfig.fIdCurrencyHolidaysController, FnOptConfig.opFEdit,tenantId)) {//error code.
                Boolean approvalRequired=tenantDAL.findApprovalRequiredTenantId(currencyHolidays.getTenantId()).getTenant().getFourEyeRequired();
                newReturnObject = currencyHolidaysDAL.updateCurrencyHolidays(currencyHolidays, headerUserId, this.newReturnObject, language,approvalRequired);
            }

        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCYH02", language));//error code
            logger.error("Exception in CurrencyHolidaysController.updateCurrencyHolidays", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @PostMapping("/approve")
<<<<<<< HEAD
    public String approveCurrencyHolidays(@RequestHeader(value = "userid") String headerUserId,@RequestHeader(value = "tenantId") String tenantId, @RequestBody ReactTableQuery tableQuery) {
=======
    public String approveCurrencyHolidays(@RequestAttribute(value = "userid") String headerUserId,@RequestAttribute(value = "tenantId") String tenantId, @RequestBody ReactTableQuery tableQuery) {
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYH01", language, FnOptConfig.fIdCurrencyHolidaysController, FnOptConfig.opFAuth,tenantId)) {//error code.

                newReturnObject = currencyHolidaysDAL.approveCurrencyHolidays(tableQuery, headerUserId, this.newReturnObject, language);
            }

        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCYH02", language));//error code
            logger.error("Exception in CurrencyHolidaysController.approveCurrencyHolidays", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @DeleteMapping("/reject")
    // _unapproved.
<<<<<<< HEAD
    public String rejectCurrencyHolidays(@RequestBody ReactTableQuery tableQuery, @RequestHeader(value = "userid") String headerUserId) {
=======
    public String rejectCurrencyHolidays(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYH01", language, FnOptConfig.fIdCurrencyHolidaysController, FnOptConfig.opFDelete)) {//error code.
                newReturnObject = currencyHolidaysDAL.rejectByTenantIdCurrencyCodeYear(tableQuery, headerUserId, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCYH02", language));//error code
            logger.error("Exception in CurrencyHolidaysController.rejectCurrencyHolidays", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();
    }
    @DeleteMapping("/delete")
<<<<<<< HEAD
    public String inActivateCurrencyHolidays(@RequestBody ReactTableQuery tableQuery, @RequestHeader(value = "userid") String headerUserId){
=======
    public String inActivateCurrencyHolidays(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            String tenantId=tableQuery.getQueryFields().get("tenantId").toString();
            String currencyCode=tableQuery.getQueryFields().get("currencyCode").toString();
            String year=tableQuery.getQueryFields().get("year").toString();
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYH01", language, FnOptConfig.fIdCurrencyHolidaysController, FnOptConfig.opFDelete)) {//error code.
                boolean approvalRequired = approvalRequirementOfTenant(tableQuery,language);
                String status = currencyHolidaysDAL.findByTenantIdCurrencyCodeYear(tenantId,currencyCode,year).getCurrencyHolidays().getStatus();
                if (status.equals("inactive")) {
                    this.newReturnObject.addError(utilityService.singleError("MCYH12", language));//error code
                } else {
                    newReturnObject = currencyHolidaysDAL.currencyHolidaysStatusChanger(tableQuery, headerUserId, newReturnObject, language,"inactive",approvalRequired);
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCYH02", language));//error code
            logger.error("Exception in CurrencyHolidaysController.inActivateCurrencyHolidays", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/view")
<<<<<<< HEAD
    public String viewCurrencyHolidays(@RequestBody ReactTableQuery tableQuery, @RequestHeader(value = "tenantId") String tenantId,@RequestHeader(value = "userid") String headerUserId) {
=======
    public String viewCurrencyHolidays(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "tenantId") String tenantId,@RequestAttribute(value = "userid") String headerUserId) {
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYH01", language, FnOptConfig.fIdCurrencyHolidaysController, FnOptConfig.opFView,tenantId)) {//error code.

                newReturnObject = currencyHolidaysDAL.viewByTenantIdCurrencyCodeYear(tableQuery, newReturnObject, language);
            }
        }catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCYH02", language));//error code
            logger.error("Exception in CurrencyHolidaysController.viewCurrencyHolidays",this.newReturnObject.getAppErrorString(),ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/activate")
<<<<<<< HEAD
    public String reActivateCurrencyHolidays(@RequestBody ReactTableQuery tableQuery, @RequestHeader(value = "userid") String headerUserId){
=======
    public String reActivateCurrencyHolidays(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            String tenantId=tableQuery.getQueryFields().get("tenantId").toString();
            String currencyCode=tableQuery.getQueryFields().get("currencyCode").toString();
            String year=tableQuery.getQueryFields().get("year").toString();
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYH01", language, FnOptConfig.fIdCurrencyHolidaysController, FnOptConfig.opFReactivate)) {//error code.
                boolean approvalRequired = approvalRequirementOfTenant(tableQuery,language);
                String status =tableQuery.getCollection().equals("approved")?currencyHolidaysDAL.findByTenantIdCurrencyCodeYear(tenantId,currencyCode,year).getCurrencyHolidays().getStatus():currencyHolidaysDAL.findUnapprovedByTenantIdCurrencyCodeYear(tenantId,currencyCode,year).getCurrencyHolidays().getStatus();

                if (status.equals("active")) {
                    this.newReturnObject.addError(utilityService.singleError("MCYH11", language));//error code
                } else {
                    newReturnObject = currencyHolidaysDAL.currencyHolidaysStatusChanger(tableQuery, headerUserId, newReturnObject, language,"active",approvalRequired);
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCYH02", language));//error code
            logger.error("Exception in CurrencyHolidaysController.reActivateCurrencyHolidays", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();

    }

    private boolean approvalRequirementOfTenant(ReactTableQuery tableQuery, String language) {
        try {
            String tenantId=tableQuery.getQueryFields().get("tenantId").toString();
            String currencyCode=tableQuery.getQueryFields().get("currencyCode").toString();
            String year=tableQuery.getQueryFields().get("year").toString();
           CurrencyHolidaysDB currencyHolidaysDB = currencyHolidaysDAL.findByTenantIdCurrencyCodeYear(tenantId,currencyCode,year);
            TenantDB tenantDB = null;
            if (currencyHolidaysDB == null) {
                this.newReturnObject.addError(utilityService.singleError("MCYH06", language));
            } else {
                tenantDB = tenantDAL.findApprovalRequiredTenantId(currencyHolidaysDB.getCurrencyHolidays().getTenantId());
            }
            if (tenantDB == null) {
                this.newReturnObject.addError(utilityService.singleError("MCYH06", language));
            } else {
                return tenantDB.getTenant().getFourEyeRequired();
            }
            return this.newReturnObject.getAppErrors().size() > 0 ? true : tenantDB.getTenant().getFourEyeRequired();
        }catch  (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCYH02", language));
            logger.error(ex.getMessage());
            return true;
        }
    }

    private boolean validateCurrencyHolidays(CurrencyHolidays currencyHolidays, String language, String userID) {

        //mandatory fields.
        try {
            if ((!currencyHolidays.getTenantId().isEmpty()) && (!currencyHolidays.getCurrencyCode().isEmpty()) && (!currencyHolidays.getYear().isEmpty()) && (currencyHolidays.getHolidayList() != null)) {
                if (tenantIdCurrencyCodeCheck(currencyHolidays, language,userID)) {

                    if (!(currencyHolidays.getYear().length() == 4)) {
                        this.newReturnObject.addError(utilityService.singleError("MCYH15", language));//error code
                    }
                    if(!userDAL.findTenantIDByOfficialEmailId(userID).getUser().getTenantId().equals(currencyHolidays.getTenantId())){
                        this.newReturnObject.addError(utilityService.singleError("MCYH14", language));
                    }
                    currencyHolidays = longMediumDateCalcuator(currencyHolidays);
                }

            } else {
                this.newReturnObject.addError(utilityService.singleError("MCYH04", language));//error code

            }
            return this.newReturnObject.getAppErrors().size() > 0 ? false : true;
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCYH02", language));//error code
            return false;
        }

    }

    private CurrencyHolidays longMediumDateCalcuator(CurrencyHolidays currencyHolidays) {
        List<Holiday> holidays = currencyHolidays.getHolidayList();
        // if(currencyHolidays.getYear().startsWith())
        for (int i = 0; i < holidays.size(); i++) {
//            if((currencyHolidays.getYear()).equals(holidays.get(i).getHolidayDate().getYear())){
            Holiday holiday = holidays.get(i);
            PayHubDate payHubDate = new PayHubDate(holiday.getHolidayDate().getYear(), holiday.getHolidayDate().getMonth(), holiday.getHolidayDate().getDay());
            holidays.get(i).setHolidayDate(payHubDate);
            //   }
//            else{
//                this.newReturnObject.addError(utilityService.singleError( "MCYH03",language));
//            }

        }
        return currencyHolidays;
    }

    private boolean tenantIdCurrencyCodeCheck(CurrencyHolidays currencyHolidays, String language,String userID) {

        try {
            if(userDAL.findTenantIDByOfficialEmailId(userID)==null){
                this.newReturnObject.addError(utilityService.singleError("MCYH14", language));
            }
            if (tenantDAL.countByTenantId(currencyHolidays.getTenantId()) == 0) {
                this.newReturnObject.addError(utilityService.singleError("MCYH14", language));
            }
            if ( currencyDAL.countByCurrencyCode(currencyHolidays.getCurrencyCode()) == 0) {
                this.newReturnObject.addError(utilityService.singleError("MCYH13", language));
            }
            return this.newReturnObject.getAppErrors().size() > 0 ? false : true;
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCYH02", language));//error code
            logger.error("Exception in CurrencyHolidaysController.updateCurrencyHolidays", this.newReturnObject.getAppErrorString(), ex);
            return false;
        }
    }

}
