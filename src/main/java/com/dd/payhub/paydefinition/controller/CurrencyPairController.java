<<<<<<< HEAD
//package com.dd.payhub.paydefinition.controller;
//
//import com.dd.payhub.paycommon.model.CurrencyPair;
//import com.dd.payhub.paycommon.model.CurrencyPair;
//import com.dd.payhub.paycommon.model.ReactTableQuery;
//import com.dd.payhub.paycommon.view.NewReturnObject;
//import com.dd.payhub.paycommon.view.ReactTableRecord;
//import com.dd.payhub.paydefinition.config.FnOptConfig;
//import com.dd.payhub.paydefinition.dal.CountryDAL;
//import com.dd.payhub.paydefinition.dal.CurrencyDAL;
//import com.dd.payhub.paydefinition.dal.CurrencyPairDAL;
//import com.dd.payhub.paydefinition.dal.TenantDAL;
//import com.dd.payhub.paydefinition.entity.*;
//import com.dd.payhub.paydefinition.service.PHUtilityService;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.i18n.LocaleContextHolder;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.ArrayList;
//
//@CrossOrigin(origins = "*", allowedHeaders = "*")
//@RestController
//@RequestMapping("/currencypair")
//public class CurrencyPairController {
//    private static final Logger logger = LogManager.getLogger(CurrencyPairController.class);
//    private CurrencyPairDAL currencyPairDAL;
//    private TenantDAL tenantDAL;
//    private CurrencyDAL currencyDAL;
//
//    private PHUtilityService utilityService;
//    private NewReturnObject newReturnObject;
//
//    @Autowired
//    public CurrencyPairController(TenantDAL tenantDAL,CurrencyDAL currencyDAL ,CurrencyPairDAL currencyPairDAL, PHUtilityService utilityService) {
//        this.currencyPairDAL = currencyPairDAL;
//        this.utilityService = utilityService;
//
//        this.tenantDAL = tenantDAL;
//        this.currencyDAL=currencyDAL;
//        newReturnObject = new NewReturnObject();
//    }
//
//
//    @PostMapping("/all")
//    public String getAllCurrencyPair(@RequestBody ReactTableQuery tableQuery ,@RequestAttribute(value = "tenantId") String bankId, @RequestAttribute(value = "userid") String headerUserId) {//, , @RequestAttribute(value = "bankid") String headerTenantIdCurrencyCodeYear
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        ReactTableRecord tableRecord = new ReactTableRecord();
//
//        try {
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCurrencyPairController, FnOptConfig.opFView,bankId)) {
//                tableRecord = currencyPairDAL.getAllCurrencyPairByPagination(tableQuery);
//                newReturnObject.PerformReturnObject(tableRecord);
//            }
//            return newReturnObject.setReturnJSON();
//        } catch (Exception ex) {
//            tableRecord.setTotalPage( 0);
//            tableRecord.setTotalRecord(0);
//            tableRecord.setTableRecord( new ArrayList<>());
//            newReturnObject.PerformReturnObject(tableRecord);
//            this.newReturnObject.setReturncode(0);
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyPairController.getAllCurrencyPair", this.newReturnObject.getAppErrorString(), ex);
//            return newReturnObject.setReturnJSON();
//        }
//    }
//
//    @PostMapping("/add")
//    public String newCurrencyPair(@RequestBody CurrencyPair currencyPair, @RequestAttribute(value = "tenantId") String bankId, @RequestAttribute(value = "userid") String headerUserId) {
//        //validation.
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//
//            if (validateCurrencyPair(currencyPair, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCurrencyPairController, FnOptConfig.opFNew,bankId)) {//error code.
//                //valid data to insert new record.
//                long approvedCount = currencyPairDAL.countByTenantIdCurrencyCodeYear(currencyPair.getTenantIdCurrencyCodeYear());
//                long unapprovedCount = currencyPairDAL.countByUnapprovedTenantIdCurrencyCodeYear(currencyPair.getTenantIdCurrencyCodeYear());
//                if (approvedCount == 0 && unapprovedCount == 0) {//record does not exist so insertion can be done.
//                    Boolean approvalRequired=tenantDAL.findApprovalRequiredTenantId(currencyPair.getTenantId()).getTenant().getFourEyeRequired();
//                    newReturnObject = currencyPairDAL.addCurrencyPair(currencyPair, headerUserId, newReturnObject, language,approvalRequired);//
//                } else {//Error record exist duplication not allowed.
//                    this.newReturnObject.addError(utilityService.singleError("MCU003", language));
//                    this.newReturnObject.setReturncode(0);
//                }
//            }
//
//        } catch (Exception ex) {
//            this.newReturnObject.setReturncode(0);
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyPairController.newCurrencyPair", this.newReturnObject.getAppErrorString(), ex);
//        }
//        return this.newReturnObject.setReturnJSON();
//    }
//
//
//    @PostMapping("/geteditable")
//    public String getEditableCurrencyPair(@RequestAttribute(value = "userid") String headerUserId,@RequestAttribute(value = "tenantId") String bankId, @RequestBody ReactTableQuery tableQuery) {
//
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        try {
//
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdCurrencyPairController, FnOptConfig.opFView,bankId)) {
//                //user has the permission to view
//                newReturnObject = currencyPairDAL.getEditableCurrencyPair(tableQuery, newReturnObject, language);
//            }
//        } catch (Exception ex) {
//            this.newReturnObject.setReturncode(0);
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyPairController.getEditableCurrencyPair", this.newReturnObject.getAppErrorString(), ex);
//        }
//        return newReturnObject.setReturnJSON();
//    }
//    @PostMapping("/update")
//    public String updateCurrencyPair(@RequestBody CurrencyPair currencyPair,@RequestAttribute(value = "tenantId") String bankId, @RequestAttribute(value = "userid") String headerUserId) {
//
//        //validation.
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//            //change for token in future.
//            if (validateCurrencyPair(currencyPair, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCurrencyPairController, FnOptConfig.opFEdit,bankId)) {//error code.
//                Boolean approvalRequired=tenantDAL.findApprovalRequiredTenantId(currencyPair.getTenantId()).getTenant().getFourEyeRequired();
//                newReturnObject = currencyPairDAL.updateCurrencyPair(currencyPair, headerUserId, this.newReturnObject, language,approvalRequired);
//            }
//
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyPairController.updateCurrencyPair", this.newReturnObject.getAppErrorString(), ex);
//        }
//        return this.newReturnObject.setReturnJSON();
//
//    }
//
//    @PostMapping("/view")
//    public String viewCurrencyPair(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "tenantId") String bankId,@RequestAttribute(value = "userid") String headerUserId) {
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCurrencyPairController, FnOptConfig.opFView,bankId)) {//error code.
//
//                newReturnObject = currencyPairDAL.viewByTenantIdCurrencyCodeYear(tableQuery, newReturnObject, language);
//            }
//        }catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyPairController.viewCurrencyPair",this.newReturnObject.getAppErrorString(),ex);
//        }
//        return newReturnObject.setReturnJSON();
//    }
//
//
//    @PostMapping("/approve")
//    public String approveCurrencyPair(@RequestAttribute(value = "userid") String headerUserId,@RequestAttribute(value = "tenantId") String bankId, @RequestBody ReactTableQuery tableQuery) {
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCurrencyPairController, FnOptConfig.opFAuth,bankId)) {//error code.
//
//                newReturnObject = currencyPairDAL.approveCurrencyPair(tableQuery, headerUserId, this.newReturnObject, language);
//            }
//
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyPairController.approveCurrencyPair", this.newReturnObject.getAppErrorString(), ex);
//        }
//        return this.newReturnObject.setReturnJSON();
//    }
//
//    @DeleteMapping("/reject")//api to delete the record from the tm_event_definition
//    // _unapproved.
//    public String rejectCurrencyPair(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFDelete)) {//error code.
//                newReturnObject = currencyPairDAL.rejectByTenantIdCurrencyCodeYear(tableQuery, headerUserId, newReturnObject, language);
//            }
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyPairController.rejectCurrencyPair", this.newReturnObject.getAppErrorString(), ex);
//
//        }
//        return newReturnObject.setReturnJSON();
//    }
//    @DeleteMapping("/delete")
//    public String inActivateCurrencyPair(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFDelete)) {//error code.
//                boolean approvalRequired = approvalRequirementOfTenant(tableQuery.getPrimaryKey(),language);
//                String status = currencyPairDAL.findByTenantIdCurrencyCodeYear(tableQuery.getPrimaryKey()).getCurrencyPair().getStatus();
//                if (status.equals("inactive")) {
//                    this.newReturnObject.addError(utilityService.singleError("MCU012", language));//error code
//                } else {
//                    newReturnObject = currencyPairDAL.currencyPairStatusChanger(tableQuery, headerUserId, newReturnObject, language,"inactive",approvalRequired);
//                }
//            }
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyPairController.inActivateCurrencyPair", this.newReturnObject.getAppErrorString(), ex);
//
//        }
//        return newReturnObject.setReturnJSON();
//    }
//    @PostMapping("/activate")
//    public String reActivateCurrencyPair(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFReactivate)) {//error code.
//                boolean approvalRequired = approvalRequirementOfTenant(tableQuery.getPrimaryKey(),language);
//                String status =tableQuery.getCollection().equals("approved")?currencyPairDAL.findByTenantIdCurrencyCodeYear(tableQuery.getPrimaryKey()).getCurrencyPair().getStatus():currencyPairDAL.findUnapprovedByTenantIdCurrencyCodeYear(tableQuery.getPrimaryKey()).getCurrencyPair().getStatus();
//
//                if (status.equals("active")) {
//                    this.newReturnObject.addError(utilityService.singleError("MCU011", language));//error code
//                } else {
//                    newReturnObject = currencyPairDAL.currencyPairStatusChanger(tableQuery, headerUserId, newReturnObject, language,"active",approvalRequired);
//                }
//            }
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyPairController.reActivateCurrencyPair", this.newReturnObject.getAppErrorString(), ex);
//
//        }
//        return newReturnObject.setReturnJSON();
//
//    }
//
//    private boolean validateCurrencyPair(CurrencyPair currencyPair, String language) {
//
//        //mandatory fields.
//        try {
//            if ((!currencyPair.getTenantId().isEmpty()) && (!currencyPair.getCCYCode1().isEmpty()) && ((!currencyPair.getCCYCode2().isEmpty()) &&
//                    (!String.valueOf(currencyPair.getSpotRatePricingDecimal()).isEmpty())) && (!String.valueOf(currencyPair.getPipsDivisor()).isEmpty())&&(!currencyPair.getQuotedCCYId().isEmpty()) &&
//                    (!String.valueOf(currencyPair.getRoundToUnit()).isEmpty()) && (!currencyPair.getMultiplyOrdivide().isEmpty()) && (!currencyPair.getRateCalculationCCYId().isEmpty())
//            && (!currencyPair.getRoundingRule().isEmpty()) && (!String.valueOf(currencyPair.getSpotDays()).isEmpty()) && (!currencyPair.getCalendarId().isEmpty()) &&
//                    ( (!(currencyPair.getUseCCYCalendar()==true))|| (!currencyPair.getUseCCYCalendar()==false) )) {
//                if (!(tenantIdCountryCodeCurrencyCodeCheck(currencyPair, language))) {
//                    this.newReturnObject.addError(utilityService.singleError("MCYR04", language));//error code
//                }
//                if(!((currencyPair.getRoundingRule().equals("NEAREST"))||(currencyPair.getRoundingRule().equals("UP"))||(currencyPair.getRoundingRule().equals("DOWN"))||(currencyPair.getRoundingRule().equals("TRUNCATE")))) //Should be any of this 4.
//                {
//                    this.newReturnObject.addError(utilityService.singleError("MCY010", language));//error code
//                }
//                if (!(((currencyPair.getPipsDivisor()) == 1) || ((currencyPair.getPipsDivisor()) == 100)|| ((currencyPair.getPipsDivisor()) == 1000)|| ((currencyPair.getPipsDivisor()) == 10000)  ))
//                {
//                    this.newReturnObject.addError(utilityService.singleError("MCYR11", language));
//                }
//                if (!( (currencyPair.getRoundToUnit()==0)||(currencyPair.getRoundToUnit()==1)||(currencyPair.getRoundToUnit()==5)||(currencyPair.getRoundToUnit()==10)) )
//                {
//                    this.newReturnObject.addError(utilityService.singleError("MCYR13", language));
//                }
//            }
//            return this.newReturnObject.getAppErrors().size()>0?false:true;
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
//            return false;
//        }
//    }
//
//
//
//    private boolean tenantIdCountryCodeCurrencyCodeCheck(CurrencyPair currencyPair, String language) {
//
//        try {
//
//            TenantDB approvedTenantResult = tenantDAL.findByTenantId(currencyPair.getTenantId());
//            CurrencyDB approvedSettelement = currencyDAL.findByCurrencyCode(currencyPair.getCCYCode1());
//            CurrencyDB approvedCurrencyCodeResult = currencyDAL.findByCurrencyCode(currencyPair.getCCYCode2());
//            CurrencyDB approvedCountryCodeResult = currencyDAL.findByCurrencyCode(currencyPair.getQuotedCCYId());
//            if (approvedTenantResult == null) {
//                this.newReturnObject.addError(utilityService.singleError("MCYR15", language));
//            }
//            if (approvedCurrencyCodeResult==null){
//                this.newReturnObject.addError(utilityService.singleError("MCYR16", language));
//            }
//            if (approvedSettelement==null){
//                this.newReturnObject.addError(utilityService.singleError("MCYR17", language));
//            }
//            if (approvedCountryCodeResult == null) {
//                this.newReturnObject.addError(utilityService.singleError("MCYR18", language));
//            }
//            return this.newReturnObject.getAppErrors().size()>0?false:true;
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
//            logger.error("Exception in CurrencyPairController.updateCurrencyPair", this.newReturnObject.getAppErrorString(), ex);
//            return false;
//        }
//
//    }
//
//}
=======
package com.dd.payhub.paydefinition.controller;

import com.dd.payhub.paycommon.model.CurrencyPair;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.config.FnOptConfig;
import com.dd.payhub.paydefinition.dal.CountryDAL;
import com.dd.payhub.paydefinition.dal.CurrencyDAL;
import com.dd.payhub.paydefinition.dal.CurrencyPairDAL;
import com.dd.payhub.paydefinition.dal.TenantDAL;
import com.dd.payhub.paydefinition.entity.*;
import com.dd.payhub.paydefinition.service.PHUtilityService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/currencypair")
public class CurrencyPairController {
    private static final Logger logger = LogManager.getLogger(CurrencyPairController.class);
    private CurrencyPairDAL currencyPairDAL;
    private TenantDAL tenantDAL;
    private CurrencyDAL currencyDAL;

    private PHUtilityService utilityService;
    private NewReturnObject newReturnObject;

    @Autowired
    public CurrencyPairController(TenantDAL tenantDAL,CurrencyDAL currencyDAL ,CurrencyPairDAL currencyPairDAL, PHUtilityService utilityService) {
        this.currencyPairDAL = currencyPairDAL;
        this.utilityService = utilityService;

        this.tenantDAL = tenantDAL;
        this.currencyDAL=currencyDAL;
        newReturnObject = new NewReturnObject();
    }


    @PostMapping("/all")
    public String getAllCurrencyPair(@RequestBody ReactTableQuery tableQuery, @RequestHeader(value = "userid") String headerUserId) {
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        ReactTableRecord tableRecord = new ReactTableRecord();
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYR01", language, FnOptConfig.fIdCurrencyPairController, FnOptConfig.opFView)) {//error code.
                tableRecord = currencyPairDAL.getAllCurrencyPairByPagination(tableQuery);
                newReturnObject.PerformReturnObject(tableRecord);
            }
            return newReturnObject.setReturnJSON();
        } catch (Exception ex) {
            tableRecord.setTotalPage(0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord(new ArrayList<>());
            newReturnObject.PerformReturnObject(tableRecord);
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
            logger.error("Exception in CurrencyPairController.getAllCurrencyPair", this.newReturnObject.getAppErrorString(), ex);
            return newReturnObject.setReturnJSON();
        }
    }

    @PostMapping("/add")
    public String newCurrencyPair(@RequestBody CurrencyPair currencyPair, @RequestHeader(value = "userid") String headerUserId) {
        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            if (validateCurrencyPair(currencyPair, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYR01", language, FnOptConfig.fIdCurrencyPairController, FnOptConfig.opFNew)) {//error code.
                //valid data to insert new record.
                CurrencyPairDB currencyPairDB = currencyPairDAL.findByTenantIdCountryCodeCurrencyCode(currencyPair.getTenantId(), currencyPair.getCCYCode1(), currencyPair.getCCYCode2());
                CurrencyPairUnapprovedDB currencyPairUnapprovedDB = currencyPairDAL.findUnapprovedByTenantIdCountryCodeCurrencyCode(currencyPair.getTenantId(), currencyPair.getCCYCode1(), currencyPair.getCCYCode2());
                if (currencyPairDB == null && currencyPairUnapprovedDB == null) {//record does not exist so insertion can be done.
                    Boolean approvalRequired = tenantDAL.findApprovalRequiredTenantId(currencyPair.getTenantId()).getTenant().getFourEyeRequired();
                    newReturnObject = currencyPairDAL.addCurrencyPair(currencyPair, headerUserId, newReturnObject, language, approvalRequired);//
                } else {//Error record exist duplication not allowed.
                    this.newReturnObject.addError(utilityService.singleError("MCYR03", language));
                    this.newReturnObject.setReturncode(0);
                }
            } else {
                this.newReturnObject.addError(utilityService.singleError("MCYR09", language));
                this.newReturnObject.setReturncode(0);
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
            logger.error("Exception in CurrencyPairController.newCurrencyPair", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @PostMapping("/update")
    public String updateCurrencyPair(@RequestBody CurrencyPair currencyPair, @RequestHeader(value = "userid") String headerUserId) {
        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            if (validateCurrencyPair(currencyPair, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYR01", language, FnOptConfig.fIdCurrencyPairController, FnOptConfig.opFNew)) {//error code.
                Boolean approvalRequired = tenantDAL.findApprovalRequiredTenantId(currencyPair.getTenantId()).getTenant().getFourEyeRequired();
                newReturnObject = currencyPairDAL.updateCurrencyPair(currencyPair, headerUserId, this.newReturnObject, language, approvalRequired);
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
            logger.error("Exception in CurrencyPairController.updateCurrencyPair", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }


    @PostMapping("/geteditable")
    public String getEditableCurrencyPair(@RequestHeader(value = "userid") String headerUserId, @RequestBody ReactTableQuery reactTableQuery) {

        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        try {

            CurrencyPairUnapprovedDB currencyPairUnapprovedDB = null;
            CurrencyPairDB currencyPairDB = null;
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYR01", language, FnOptConfig.fIdCurrencyPairController, FnOptConfig.opFView)) {
                //user has the permission to view
                newReturnObject = currencyPairDAL.getEditableCurrencyPair(reactTableQuery, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
            logger.error("Exception in CurrencyPairController.getEditableCurrencyPair", this.newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/approve")
    public String approveCurrencyPair(@RequestHeader(value = "userid") String headerUserId, @RequestBody ReactTableQuery reactTableQuery) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYR01", language, FnOptConfig.fIdCurrencyPairController, FnOptConfig.opFAuth)) {//error code.

                newReturnObject = currencyPairDAL.approveCurrencyPair(reactTableQuery, headerUserId, this.newReturnObject, language);
            }

        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
            logger.error("Exception in CurrencyPairController.approveCurrencyPair", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @DeleteMapping("/reject")//api to delete the record from the tm_currency_rules_unapproved.
    public String rejectCurrencyPair(@RequestBody ReactTableQuery reactTableQuery, @RequestHeader(value = "userid") String headerUserId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYR01", language, FnOptConfig.fIdCurrencyPairController, FnOptConfig.opFDelete)) {//error code.

                newReturnObject = currencyPairDAL.deleteByTenantIdCountryCodeCurrencyCode(reactTableQuery, headerUserId, newReturnObject, language);
            }
        } catch (Exception ex) {
            logger.error("Exception in CurrencyPairController.rejectCurrencyPair", this.newReturnObject.getAppErrorString(), ex);
            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code

        }
        return newReturnObject.setReturnJSON();

    }

    @PostMapping("/view")
    public String view(@RequestBody ReactTableQuery tableQuery) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            newReturnObject = currencyPairDAL.viewByTenantIdCountryCodeCurrencyCode(tableQuery, newReturnObject, language);
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
            logger.error("Exception in CurrencyPairController.view", this.newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();


    }

    private boolean validateCurrencyPair(CurrencyPair currencyPair, String language) {

        //mandatory fields.
        try {
            if ((!currencyPair.getTenantId().isEmpty()) && (!currencyPair.getCCYCode1().isEmpty()) && ((!currencyPair.getCCYCode2().isEmpty()) &&
                    (!String.valueOf(currencyPair.getSpotRatePricingDecimal()).isEmpty())) && (!String.valueOf(currencyPair.getPipsDivisor()).isEmpty())&&(!currencyPair.getQuotedCCYId().isEmpty()) &&
                    (!String.valueOf(currencyPair.getRoundToUnit()).isEmpty()) && (!currencyPair.getMultiplyOrdivide().isEmpty()) && (!currencyPair.getRateCalculationCCYId().isEmpty())
            && (!currencyPair.getRoundingRule().isEmpty()) && (!String.valueOf(currencyPair.getSpotDays()).isEmpty()) && (!currencyPair.getCalendarId().isEmpty()) &&
                    ( (!(currencyPair.getUseCCYCalendar()==true))|| (!currencyPair.getUseCCYCalendar()==false) )) {
                if (!(tenantIdCountryCodeCurrencyCodeCheck(currencyPair, language))) {
                    this.newReturnObject.addError(utilityService.singleError("MCYR04", language));//error code
                }
                if(!((currencyPair.getRoundingRule().equals("NEAREST"))||(currencyPair.getRoundingRule().equals("UP"))||(currencyPair.getRoundingRule().equals("DOWN"))||(currencyPair.getRoundingRule().equals("TRUNCATE")))) //Should be any of this 4.
                {
                    this.newReturnObject.addError(utilityService.singleError("MCY010", language));//error code
                }
                if (!(((currencyPair.getPipsDivisor()) == 1) || ((currencyPair.getPipsDivisor()) == 100)|| ((currencyPair.getPipsDivisor()) == 1000)|| ((currencyPair.getPipsDivisor()) == 10000)  ))
                {
                    this.newReturnObject.addError(utilityService.singleError("MCYR11", language));
                }
                if (!( (currencyPair.getRoundToUnit()==0)||(currencyPair.getRoundToUnit()==1)||(currencyPair.getRoundToUnit()==5)||(currencyPair.getRoundToUnit()==10)) )
                {
                    this.newReturnObject.addError(utilityService.singleError("MCYR13", language));
                }
            }
            return this.newReturnObject.getAppErrors().size()>0?false:true;
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
            return false;
        }
    }



    private boolean tenantIdCountryCodeCurrencyCodeCheck(CurrencyPair currencyPair, String language) {

        try {

            TenantDB approvedTenantResult = tenantDAL.findByTenantId(currencyPair.getTenantId());
            CurrencyDB approvedSettelement = currencyDAL.findByCurrencyCode(currencyPair.getCCYCode1());
            CurrencyDB approvedCurrencyCodeResult = currencyDAL.findByCurrencyCode(currencyPair.getCCYCode2());
            CurrencyDB approvedCountryCodeResult = currencyDAL.findByCurrencyCode(currencyPair.getQuotedCCYId());
            if (approvedTenantResult == null) {
                this.newReturnObject.addError(utilityService.singleError("MCYR15", language));
            }
            if (approvedCurrencyCodeResult==null){
                this.newReturnObject.addError(utilityService.singleError("MCYR16", language));
            }
            if (approvedSettelement==null){
                this.newReturnObject.addError(utilityService.singleError("MCYR17", language));
            }
            if (approvedCountryCodeResult == null) {
                this.newReturnObject.addError(utilityService.singleError("MCYR18", language));
            }
            return this.newReturnObject.getAppErrors().size()>0?false:true;
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
            logger.error("Exception in CurrencyPairController.updateCurrencyPair", this.newReturnObject.getAppErrorString(), ex);
            return false;
        }

    }

}
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
