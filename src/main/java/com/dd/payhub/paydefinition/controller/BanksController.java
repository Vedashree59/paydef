package com.dd.payhub.paydefinition.controller;

import com.dd.payhub.paycommon.model.Banks;
import com.dd.payhub.paycommon.model.Banks;
import com.dd.payhub.paycommon.model.ReactTableQuery;

import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.config.FnOptConfig;
import com.dd.payhub.paydefinition.dal.BanksDAL;
import com.dd.payhub.paydefinition.entity.BanksDB;
import com.dd.payhub.paydefinition.entity.BanksUnapprovedDB;
import com.dd.payhub.paydefinition.service.PHUtilityService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/banks")
public class BanksController {

    private static final Logger logger = LogManager.getLogger(BanksController.class);
    private BanksDAL banksDAL;
    private PHUtilityService utilityService;
    private NewReturnObject newReturnObject;
    @Autowired
    public BanksController(BanksDAL banksDAL, PHUtilityService utilityService) {
        this.banksDAL = banksDAL;
        this.utilityService = utilityService;
        newReturnObject=new NewReturnObject();
    }


    @PostMapping("/all")
    public String getAllBanks(@RequestBody ReactTableQuery tableQuery , @RequestAttribute(value = "userid") String headerUserId) {//, , @RequestAttribute(value = "bankid") String headerCustomerId
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        ReactTableRecord tableRecord = new ReactTableRecord();

        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MBS001", language, FnOptConfig.fIdBanksController, FnOptConfig.opFView)) {
                tableRecord = banksDAL.getAllBanksByPagination(tableQuery);
                newReturnObject.PerformReturnObject(tableRecord);
            }
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValueAsString(tableRecord);
            return newReturnObject.setReturnJSON();
        } catch (Exception ex) {
            tableRecord.setTotalPage( 0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord( new ArrayList<>());
            newReturnObject.PerformReturnObject(tableRecord);
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MBS002", language));//error code
            logger.error("Exception in BanksController.getAllBanks", this.newReturnObject.getAppErrorString(), ex);
            return newReturnObject.setReturnJSON();
        }
    }

    @PostMapping("/add")
    public String newBanks(@RequestBody Banks banks, @RequestAttribute(value = "userid") String headerUserId) {
        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (validateBank(banks, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MBS001", language, FnOptConfig.fIdBanksController, FnOptConfig.opFNew)) {//error code.
                //valid data to insert new record.
                long approvedCount = banksDAL.countByBankListId(banks.getBankListId());
                long unapprovedCount = banksDAL.countByUnapprovedBankListId(banks.getBankListId());
                if (  approvedCount  == 0 && unapprovedCount == 0) {//record does not exist so insertion can be done.
                    newReturnObject = banksDAL.addBank(banks, headerUserId, newReturnObject, language);//
                } else {//Error record exist duplication not allowed.
                    this.newReturnObject.addError(utilityService.singleError("MBS003", language));
                    this.newReturnObject.setReturncode(0);
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MBS002", language));//error code
            logger.error("Exception in BanksController.newBanks", this.newReturnObject.getAppErrorString(), ex);

        }
        return this.newReturnObject.setReturnJSON();
    }

    @PostMapping("/geteditable")
    public String getEditableBanks(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery tableQuery) {

        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MBS001", language, FnOptConfig.fIdBanksController, FnOptConfig.opFView)) {
                //user has the permission to view
                newReturnObject = banksDAL.getEditableBank(tableQuery, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MBS002", language));//error code
            logger.error("Exception in BanksController.getEditableBanks", this.newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/update")
    public String updateBanks(@RequestBody Banks banks, @RequestAttribute(value = "userid") String headerUserId) {

        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            if (validateBank(banks, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MBS001", language, FnOptConfig.fIdBanksController, FnOptConfig.opFEdit)) {//error code.
                newReturnObject = banksDAL.updateBank(banks, headerUserId, this.newReturnObject, language);
            }

        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MBS002", language));//error code
            logger.error("Exception in BanksController.updateBanks", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @PostMapping("/view")
    public String viewBanks(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MBS001", language, FnOptConfig.fIdBanksController, FnOptConfig.opFView)) {//error code.
                newReturnObject = banksDAL.viewByBankListId(tableQuery, newReturnObject, language);
            }
        }catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MBS002", language));//error code
            logger.error("Exception in BanksController.viewBanks",this.newReturnObject.getAppErrorString(),ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/approve")
    public String approveBanks(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery tableQuery) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MBS001", language, FnOptConfig.fIdBanksController, FnOptConfig.opFAuth)) {//error code.

                newReturnObject = banksDAL.approveBank(tableQuery, headerUserId, this.newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MBS002", language));//error code
            logger.error("Exception in BanksController.approveBanks", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @DeleteMapping("/reject")//api to delete the record from the tm_event_definition
    // _unapproved.
    public String rejectBanks(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MBS001", language, FnOptConfig.fIdBanksController, FnOptConfig.opFDelete)) {//error code.

                newReturnObject = banksDAL.rejectByBankListId(tableQuery, headerUserId, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MBS002", language));//error code
            logger.error("Exception in BanksController.rejectBanks", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();

    }

    @DeleteMapping("/delete")
    public String inActivateBanks(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MBS001", language, FnOptConfig.fIdBanksController, FnOptConfig.opFDelete)) {//error code.
                String status = banksDAL.findByBankListId(tableQuery.getPrimaryKey()).getBank().getStatus();
                if (status.equals("inactive")) {
                    this.newReturnObject.addError(utilityService.singleError("MBS012", language));//error code
                } else {
                    newReturnObject = banksDAL.banksStatusChanger(tableQuery, headerUserId, newReturnObject, language, "inactive");
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MBS002", language));
            logger.error("Exception in BanksController.inActivate", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();

    }

    @PostMapping("/activate")
    public String reActivateBanks(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            String collection=tableQuery.getCollection();
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MBS001", language, FnOptConfig.fIdBanksController, FnOptConfig.opFReactivate)) {
                String status =collection.equals("approved")?banksDAL.findByBankListId(tableQuery.getPrimaryKey()).getBank().getStatus():banksDAL.findUnapprovedByBankListId(tableQuery.getPrimaryKey()).getBank().getStatus();
                if (status.equals("active")) {
                    this.newReturnObject.addError(utilityService.singleError("MBS011", language));//error code
                } else {
                    newReturnObject = banksDAL.banksStatusChanger(tableQuery, headerUserId, newReturnObject, language, "active");
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MBS002", language));//error code
            logger.error("Exception in BanksController.reActivate", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();

    }
<<<<<<< HEAD
    @PostMapping("/banklistids")
    public String bankListIDS(@RequestBody ReactTableQuery tableQuery,@RequestAttribute(value = "userid") String headerUserId){
       newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try{
            newReturnObject=banksDAL.listOfIds(tableQuery.getKeyword(),newReturnObject,language);
        }catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MBS002", language));//error code
            logger.error("Exception in BanksController.bankListIDS", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();
    }
=======
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc


    private boolean validateBank(Banks bank, String language) {

        //mandatory fields.
        try {
<<<<<<< HEAD
            if ((!bank.getBankListId().isEmpty()) && (!bank.getBankName().isEmpty()) && (!bank.getBankShortName().isEmpty()) && (!bank.getStatus().isEmpty()) && (!bank.getRoutingCodeType().isEmpty()) && (!bank.getRoutingCodeId().isEmpty())  && (bank.getCommunication()!= null)) {

                if(!(bank.getBankListId().length()==12))
                {
                    this.newReturnObject.addError(utilityService.singleError("MBS013", language));//error code
                }

            } else {
                this.newReturnObject.addError(utilityService.singleError("MBS004", language));//error code
=======
            if ((!bank.getBankListId().isEmpty()) && (!bank.getBankName().isEmpty()) && (!bank.getBankShortName().isEmpty()) && (!bank.getStatus().isEmpty()) && (!bank.getRoutingCodeType().isEmpty()) && (!bank.getRoutingCodeId().isEmpty())  && (bank.getCommunication()!= null) && (bank.getAddress()!=null)) {

                if(!(bank.getBankListId().length()==12))
                {
                    this.newReturnObject.addError(utilityService.singleError("MBS014", language));//error code
                }

            } else {
                this.newReturnObject.addError(utilityService.singleError("MBS013", language));//error code
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

            }
            return this.newReturnObject.getAppErrors().size()>0?false:true;
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MBS002", language));//error code
            return false;
        }


    }
}
