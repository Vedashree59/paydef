<<<<<<< HEAD
//package com.dd.payhub.paydefinition.controller;
//
//
//import com.dd.payhub.paycommon.model.CurrencyRules;
//import com.dd.payhub.paycommon.model.CurrencyRules;
//import com.dd.payhub.paycommon.model.CurrencyRules;
//import com.dd.payhub.paycommon.model.ReactTableQuery;
//import com.dd.payhub.paycommon.view.NewReturnObject;
//import com.dd.payhub.paycommon.view.ReactTableRecord;
//import com.dd.payhub.paydefinition.config.FnOptConfig;
//import com.dd.payhub.paydefinition.dal.CountryDAL;
//import com.dd.payhub.paydefinition.dal.CurrencyDAL;
//import com.dd.payhub.paydefinition.dal.CurrencyRulesDAL;
//import com.dd.payhub.paydefinition.dal.TenantDAL;
//import com.dd.payhub.paydefinition.entity.*;
//import com.dd.payhub.paydefinition.service.PHUtilityService;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.i18n.LocaleContextHolder;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.ArrayList;
//
//@CrossOrigin(origins = "*", allowedHeaders = "*")
//@RestController
//@RequestMapping("/currencyrules")
//public class CurrencyRulesController {
//    private static final Logger logger = LogManager.getLogger(CurrencyRulesController.class);
//    private CurrencyRulesDAL currencyRulesDAL;
//    private TenantDAL tenantDAL;
//    private CurrencyDAL currencyDAL;
//    private CountryDAL countryDAL;
//    private PHUtilityService utilityService;
//    private NewReturnObject newReturnObject;
//
//    @Autowired
//    public CurrencyRulesController(TenantDAL tenantDAL,CountryDAL countryDAL,CurrencyDAL currencyDAL ,CurrencyRulesDAL currencyRulesDAL, PHUtilityService utilityService) {
//        this.currencyRulesDAL = currencyRulesDAL;
//        this.utilityService = utilityService;
//        this.countryDAL = countryDAL;
//        this.tenantDAL = tenantDAL;
//        this.currencyDAL=currencyDAL;
//        newReturnObject = new NewReturnObject();
//    }
//
//
//    @PostMapping("/all")
//    public String getAllCurrencyRules(@RequestBody ReactTableQuery tableQuery ,@RequestAttribute(value = "tenantId") String bankId, @RequestAttribute(value = "userid") String headerUserId) {//, , @RequestAttribute(value = "bankid") String headerTenantIdCurrencyCodeYear
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        ReactTableRecord tableRecord = new ReactTableRecord();
//
//        try {
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCurrencyRulesController, FnOptConfig.opFView,bankId)) {
//                tableRecord = currencyRulesDAL.getAllCurrencyRulesByPagination(tableQuery);
//                newReturnObject.PerformReturnObject(tableRecord);
//            }
//            return newReturnObject.setReturnJSON();
//        } catch (Exception ex) {
//            tableRecord.setTotalPage( 0);
//            tableRecord.setTotalRecord(0);
//            tableRecord.setTableRecord( new ArrayList<>());
//            newReturnObject.PerformReturnObject(tableRecord);
//            this.newReturnObject.setReturncode(0);
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyRulesController.getAllCurrencyRules", this.newReturnObject.getAppErrorString(), ex);
//            return newReturnObject.setReturnJSON();
//        }
//    }
//
//    @PostMapping("/add")
//    public String newCurrencyRules(@RequestBody CurrencyRules currencyRules, @RequestAttribute(value = "tenantId") String bankId, @RequestAttribute(value = "userid") String headerUserId) {
//        //validation.
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//
//            if (validateCurrencyRules(currencyRules, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCurrencyRulesController, FnOptConfig.opFNew,bankId)) {//error code.
//                //valid data to insert new record.
//                long approvedCount = currencyRulesDAL.countByTenantIdCurrencyCodeYear(currencyRules.getTenantIdCurrencyCodeYear());
//                long unapprovedCount = currencyRulesDAL.countByUnapprovedTenantIdCurrencyCodeYear(currencyRules.getTenantIdCurrencyCodeYear());
//                if (approvedCount == 0 && unapprovedCount == 0) {//record does not exist so insertion can be done.
//                    Boolean approvalRequired=tenantDAL.findApprovalRequiredTenantId(currencyRules.getTenantId()).getTenant().getFourEyeRequired();
//                    newReturnObject = currencyRulesDAL.addCurrencyRules(currencyRules, headerUserId, newReturnObject, language,approvalRequired);//
//                } else {//Error record exist duplication not allowed.
//                    this.newReturnObject.addError(utilityService.singleError("MCU003", language));
//                    this.newReturnObject.setReturncode(0);
//                }
//            }
//
//        } catch (Exception ex) {
//            this.newReturnObject.setReturncode(0);
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyRulesController.newCurrencyRules", this.newReturnObject.getAppErrorString(), ex);
//        }
//        return this.newReturnObject.setReturnJSON();
//    }
//
//
//    @PostMapping("/geteditable")
//    public String getEditableCurrencyRules(@RequestAttribute(value = "userid") String headerUserId,@RequestAttribute(value = "tenantId") String bankId, @RequestBody ReactTableQuery tableQuery) {
//
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        try {
//
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdCurrencyRulesController, FnOptConfig.opFView,bankId)) {
//                //user has the permission to view
//                newReturnObject = currencyRulesDAL.getEditableCurrencyRules(tableQuery, newReturnObject, language);
//            }
//        } catch (Exception ex) {
//            this.newReturnObject.setReturncode(0);
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyRulesController.getEditableCurrencyRules", this.newReturnObject.getAppErrorString(), ex);
//        }
//        return newReturnObject.setReturnJSON();
//    }
//    @PostMapping("/update")
//    public String updateCurrencyRules(@RequestBody CurrencyRules currencyRules,@RequestAttribute(value = "tenantId") String bankId, @RequestAttribute(value = "userid") String headerUserId) {
//
//        //validation.
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//            //change for token in future.
//            if (validateCurrencyRules(currencyRules, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCurrencyRulesController, FnOptConfig.opFEdit,bankId)) {//error code.
//                Boolean approvalRequired=tenantDAL.findApprovalRequiredTenantId(currencyRules.getTenantId()).getTenant().getFourEyeRequired();
//                newReturnObject = currencyRulesDAL.updateCurrencyRules(currencyRules, headerUserId, this.newReturnObject, language,approvalRequired);
//            }
//
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyRulesController.updateCurrencyRules", this.newReturnObject.getAppErrorString(), ex);
//        }
//        return this.newReturnObject.setReturnJSON();
//
//    }
//
//    @PostMapping("/view")
//    public String viewCurrencyRules(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "tenantId") String bankId,@RequestAttribute(value = "userid") String headerUserId) {
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCurrencyRulesController, FnOptConfig.opFView,bankId)) {//error code.
//
//                newReturnObject = currencyRulesDAL.viewByTenantIdCurrencyCodeYear(tableQuery, newReturnObject, language);
//            }
//        }catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyRulesController.viewCurrencyRules",this.newReturnObject.getAppErrorString(),ex);
//        }
//        return newReturnObject.setReturnJSON();
//    }
//
//
//    @PostMapping("/approve")
//    public String approveCurrencyRules(@RequestAttribute(value = "userid") String headerUserId,@RequestAttribute(value = "tenantId") String bankId, @RequestBody ReactTableQuery tableQuery) {
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCurrencyRulesController, FnOptConfig.opFAuth,bankId)) {//error code.
//
//                newReturnObject = currencyRulesDAL.approveCurrencyRules(tableQuery, headerUserId, this.newReturnObject, language);
//            }
//
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyRulesController.approveCurrencyRules", this.newReturnObject.getAppErrorString(), ex);
//        }
//        return this.newReturnObject.setReturnJSON();
//    }
//
//    @DeleteMapping("/reject")//api to delete the record from the tm_event_definition
//    // _unapproved.
//    public String rejectCurrencyRules(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFDelete)) {//error code.
//                newReturnObject = currencyRulesDAL.rejectByTenantIdCurrencyCodeYear(tableQuery, headerUserId, newReturnObject, language);
//            }
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyRulesController.rejectCurrencyRules", this.newReturnObject.getAppErrorString(), ex);
//
//        }
//        return newReturnObject.setReturnJSON();
//    }
//    @DeleteMapping("/delete")
//    public String inActivateCurrencyRules(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFDelete)) {//error code.
//                boolean approvalRequired = approvalRequirementOfTenant(tableQuery.getPrimaryKey(),language);
//                String status = currencyRulesDAL.findByTenantIdCurrencyCodeYear(tableQuery.getPrimaryKey()).getCurrencyRules().getStatus();
//                if (status.equals("inactive")) {
//                    this.newReturnObject.addError(utilityService.singleError("MCU012", language));//error code
//                } else {
//                    newReturnObject = currencyRulesDAL.currencyRulesStatusChanger(tableQuery, headerUserId, newReturnObject, language,"inactive",approvalRequired);
//                }
//            }
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyRulesController.inActivateCurrencyRules", this.newReturnObject.getAppErrorString(), ex);
//
//        }
//        return newReturnObject.setReturnJSON();
//    }
//    @PostMapping("/activate")
//    public String reActivateCurrencyRules(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
//        this.newReturnObject = new NewReturnObject(0, "\"\"");
//        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
//        try {
//            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFReactivate)) {//error code.
//                boolean approvalRequired = approvalRequirementOfTenant(tableQuery.getPrimaryKey(),language);
//                String status =tableQuery.getCollection().equals("approved")?currencyRulesDAL.findByTenantIdCurrencyCodeYear(tableQuery.getPrimaryKey()).getCurrencyRules().getStatus():currencyRulesDAL.findUnapprovedByTenantIdCurrencyCodeYear(tableQuery.getPrimaryKey()).getCurrencyRules().getStatus();
//
//                if (status.equals("active")) {
//                    this.newReturnObject.addError(utilityService.singleError("MCU011", language));//error code
//                } else {
//                    newReturnObject = currencyRulesDAL.currencyRulesStatusChanger(tableQuery, headerUserId, newReturnObject, language,"active",approvalRequired);
//                }
//            }
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
//            logger.error("Exception in CurrencyRulesController.reActivateCurrencyRules", this.newReturnObject.getAppErrorString(), ex);
//
//        }
//        return newReturnObject.setReturnJSON();
//
//    }
//
//
//    private boolean validateCurrencyRules(CurrencyRules currencyRules, String language) {
//        String[] accrualTypeCheck={"NONE","A365","A360","A365(Act)","30/360","30E/360","30E+/360","Act/Act(Bond)","Act/Act(ISDA)"};
//        String[] valueDateAdjustment={"EntityDateEndTime","EntityTradeDate","NonStandard"};
//
//        //mandatory fields.
//        try {
//            if ((!currencyRules.getTenantId().isEmpty()) && (!currencyRules.getCountryCode().isEmpty()) && ((!currencyRules.getCurrencyCode().isEmpty()) &&
//                    (!String.valueOf(currencyRules.getSpotDays()).isEmpty())) && (!String.valueOf(currencyRules.getAllowSettlement()).isEmpty())&&(!currencyRules.getAccrualType().isEmpty()) &&
//                    (!String.valueOf(currencyRules.getSettlementOffsetDays()).isEmpty()) && (!currencyRules.getSettlementCurrencyCode().isEmpty())) {
//                if (!(tenantIdCountryCodeCurrencyCodeCheck(currencyRules, language))) {
//                    this.newReturnObject.addError(utilityService.singleError("MCYR04", language));//error code
//                }
//                if (!(((currencyRules.getSpotDays()) >= 0) && ((currencyRules.getSpotDays()) <= 9)))
//                {
//                    this.newReturnObject.addError(utilityService.singleError("MCYR10", language));
//            }
//                if (!(((currencyRules.getSettlementOffsetDays()) >= 0) && ((currencyRules.getSettlementOffsetDays()) <= 9)))
//                {
//                    this.newReturnObject.addError(utilityService.singleError("MCYR11", language));
//                }
//                if(!(AccrualTypeCheck(accrualTypeCheck,currencyRules.getAccrualType())))
//                {
//                    this.newReturnObject.addError(utilityService.singleError("MCYR12", language));
//                }
//                if (!(((currencyRules.getAllowSettlement())==true)||(currencyRules.getAllowSettlement()==false)))
//                {
//                    this.newReturnObject.addError(utilityService.singleError("MCYR13", language));
//                }
//                if(!(AccrualTypeCheck(valueDateAdjustment,currencyRules.getValueDateAdjustment()))) {
//                    this.newReturnObject.addError(utilityService.singleError("MCYR14", language));
//                }
//
//            }
//           return this.newReturnObject.getAppErrors().size()>0?false:true;
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
//            return false;
//        }
//    }
//
//    private boolean AccrualTypeCheck (String[] accrualTypeCheck , String targetValue)
//    {
//        for(String s : accrualTypeCheck)
//        {
//            if(s.equals(targetValue))
//            {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    private boolean tenantIdCountryCodeCurrencyCodeCheck(CurrencyRules currencyRules, String language) {
//
//        try {
//
//            TenantDB approvedTenantResult = tenantDAL.findByTenantId(currencyRules.getTenantId());
//            long approvedSettelement = currencyDAL.countByCurrencyCode(currencyRules.getSettlementCurrencyCode());
//            long approvedCurrencyCodeResult = currencyDAL.countByCurrencyCode(currencyRules.getCurrencyCode());
//            long approvedCountryCodeResult = countryDAL.countByCountryCode(currencyRules.getCountryCode());
//            if (approvedTenantResult == null) {
//                this.newReturnObject.addError(utilityService.singleError("MCYR15", language));
//            }
//            if (approvedCurrencyCodeResult==0){
//                this.newReturnObject.addError(utilityService.singleError("MCYR16", language));
//            }
//            if (approvedSettelement==0){
//                this.newReturnObject.addError(utilityService.singleError("MCYR17", language));
//            }
//            if (approvedCountryCodeResult == 0) {
//                    this.newReturnObject.addError(utilityService.singleError("MCYR18", language));
//            }
//            return this.newReturnObject.getAppErrors().size()>0?false:true;
//        } catch (Exception ex) {
//            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
//            logger.error("Exception in CurrencyRulesController.updateCurrencyRules", this.newReturnObject.getAppErrorString(), ex);
//            return false;
//        }
//
//    }
//}
=======
package com.dd.payhub.paydefinition.controller;


import com.dd.payhub.paycommon.model.CurrencyRules;
import com.dd.payhub.paycommon.model.CurrencyRules;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.config.FnOptConfig;
import com.dd.payhub.paydefinition.dal.CountryDAL;
import com.dd.payhub.paydefinition.dal.CurrencyDAL;
import com.dd.payhub.paydefinition.dal.CurrencyRulesDAL;
import com.dd.payhub.paydefinition.dal.TenantDAL;
import com.dd.payhub.paydefinition.entity.*;
import com.dd.payhub.paydefinition.service.PHUtilityService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/currencyrules")
public class CurrencyRulesController {
    private static final Logger logger = LogManager.getLogger(CurrencyRulesController.class);
    private CurrencyRulesDAL currencyRulesDAL;
    private TenantDAL tenantDAL;
    private CurrencyDAL currencyDAL;
    private CountryDAL countryDAL;
    private PHUtilityService utilityService;
    private NewReturnObject newReturnObject;

    @Autowired
    public CurrencyRulesController(TenantDAL tenantDAL,CountryDAL countryDAL,CurrencyDAL currencyDAL ,CurrencyRulesDAL currencyRulesDAL, PHUtilityService utilityService) {
        this.currencyRulesDAL = currencyRulesDAL;
        this.utilityService = utilityService;
        this.countryDAL = countryDAL;
        this.tenantDAL = tenantDAL;
        this.currencyDAL=currencyDAL;
        newReturnObject = new NewReturnObject();
    }

    @PostMapping("/all")
    public String getAllCurrencyRules(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        ReactTableRecord tableRecord = new ReactTableRecord();
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYR01", language, FnOptConfig.fIdCurrencyRulesController, FnOptConfig.opFView)) {//error code.
                tableRecord = currencyRulesDAL.getAllCurrencyRulesByPagination(tableQuery);
                newReturnObject.PerformReturnObject(tableRecord);
            }
            return newReturnObject.setReturnJSON();
        } catch (Exception ex) {
            tableRecord.setTotalPage(0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord(new ArrayList<>());
            newReturnObject.PerformReturnObject(tableRecord);
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
            logger.error("Exception in CurrencyRulesController.getAllCurrencyRules", this.newReturnObject.getAppErrorString(), ex);
            return newReturnObject.setReturnJSON();
        }
    }

    @PostMapping("/add")
    public String newCurrencyRules(@RequestBody CurrencyRules currencyRules, @RequestAttribute(value = "userid") String headerUserId) {
        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            if (validateCurrencyRules(currencyRules, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYR01", language, FnOptConfig.fIdCurrencyRulesController, FnOptConfig.opFNew)) {//error code.
                //valid data to insert new record.
                CurrencyRulesDB currencyRulesDB = currencyRulesDAL.findByTenantIdCountryCodeCurrencyCode(currencyRules.getTenantId(), currencyRules.getCountryCode(), currencyRules.getCurrencyCode());
                CurrencyRulesUnapprovedDB currencyRulesUnapprovedDB = currencyRulesDAL.findUnapprovedByTenantIdCountryCodeCurrencyCode(currencyRules.getTenantId(), currencyRules.getCountryCode(), currencyRules.getCurrencyCode());
                if (currencyRulesDB == null && currencyRulesUnapprovedDB == null) {//record does not exist so insertion can be done.
                    Boolean approvalRequired = tenantDAL.findApprovalRequiredTenantId(currencyRules.getTenantId()).getTenant().getFourEyeRequired();
                    newReturnObject = currencyRulesDAL.addCurrencyRules(currencyRules, headerUserId, newReturnObject, language, approvalRequired);//
                } else {//Error record exist duplication not allowed.
                    this.newReturnObject.addError(utilityService.singleError("MCYR03", language));
                    this.newReturnObject.setReturncode(0);
                }
            } else {
                this.newReturnObject.addError(utilityService.singleError("MCYR09", language));
                this.newReturnObject.setReturncode(0);
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
            logger.error("Exception in CurrencyRulesController.newCurrencyRules", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @PostMapping("/update")
    public String updateCurrencyRules(@RequestBody CurrencyRules currencyRules, @RequestAttribute(value = "userid") String headerUserId) {
        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            if (validateCurrencyRules(currencyRules, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYR01", language, FnOptConfig.fIdCurrencyRulesController, FnOptConfig.opFNew)) {//error code.
                Boolean approvalRequired = tenantDAL.findApprovalRequiredTenantId(currencyRules.getTenantId()).getTenant().getFourEyeRequired();
                newReturnObject = currencyRulesDAL.updateCurrencyRules(currencyRules, headerUserId, this.newReturnObject, language, approvalRequired);
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
            logger.error("Exception in CurrencyRulesController.updateCurrencyRules", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }


    @PostMapping("/geteditable")
    public String getEditableCurrencyRules(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery reactTableQuery) {

        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        try {

            CurrencyRulesUnapprovedDB currencyRulesUnapprovedDB = null;
            CurrencyRulesDB currencyRulesDB = null;
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYR01", language, FnOptConfig.fIdCurrencyRulesController, FnOptConfig.opFView)) {
                //user has the permission to view
                newReturnObject = currencyRulesDAL.getEditableCurrencyRules(reactTableQuery, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
            logger.error("Exception in CurrencyRulesController.getEditableCurrencyRules", this.newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/approve")
    public String approveCurrencyRules(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery reactTableQuery) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYR01", language, FnOptConfig.fIdCurrencyRulesController, FnOptConfig.opFAuth)) {//error code.

                newReturnObject = currencyRulesDAL.approveCurrencyRules(reactTableQuery, headerUserId, this.newReturnObject, language);
            }

        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
            logger.error("Exception in CurrencyRulesController.approveCurrencyRules", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @DeleteMapping("/reject")//api to delete the record from the tm_currency_rules_unapproved.
    public String rejectCurrencyRules(@RequestBody ReactTableQuery reactTableQuery, @RequestAttribute(value = "userid") String headerUserId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCYR01", language, FnOptConfig.fIdCurrencyRulesController, FnOptConfig.opFDelete)) {//error code.

                newReturnObject = currencyRulesDAL.deleteByTenantIdCountryCodeCurrencyCode(reactTableQuery, headerUserId, newReturnObject, language);
            }
        } catch (Exception ex) {
            logger.error("Exception in CurrencyRulesController.rejectCurrencyRules", this.newReturnObject.getAppErrorString(), ex);
            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code

        }
        return newReturnObject.setReturnJSON();

    }

    @PostMapping("/view")
    public String view(@RequestBody ReactTableQuery tableQuery) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            newReturnObject = currencyRulesDAL.viewByTenantIdCountryCodeCurrencyCode(tableQuery, newReturnObject, language);
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
            logger.error("Exception in CurrencyRulesController.view", this.newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();


    }

    private boolean validateCurrencyRules(CurrencyRules currencyRules, String language) {
        String[] accrualTypeCheck={"NONE","A365","A360","A365(Act)","30/360","30E/360","30E+/360","Act/Act(Bond)","Act/Act(ISDA)"};
        String[] valueDateAdjustment={"EntityDateEndTime","EntityTradeDate","NonStandard"};

        //mandatory fields.
        try {
            if ((!currencyRules.getTenantId().isEmpty()) && (!currencyRules.getCountryCode().isEmpty()) && ((!currencyRules.getCurrencyCode().isEmpty()) &&
                    (!String.valueOf(currencyRules.getSpotDays()).isEmpty())) && (!String.valueOf(currencyRules.getAllowSettlement()).isEmpty())&&(!currencyRules.getAccrualType().isEmpty()) &&
                    (!String.valueOf(currencyRules.getSettlementOffsetDays()).isEmpty()) && (!currencyRules.getSettlementCurrencyCode().isEmpty())) {
                if (!(tenantIdCountryCodeCurrencyCodeCheck(currencyRules, language))) {
                    this.newReturnObject.addError(utilityService.singleError("MCYR04", language));//error code
                }
                if (!(((currencyRules.getSpotDays()) >= 0) && ((currencyRules.getSpotDays()) <= 9)))
                {
                    this.newReturnObject.addError(utilityService.singleError("MCYR10", language));
            }
                if (!(((currencyRules.getSettlementOffsetDays()) >= 0) && ((currencyRules.getSettlementOffsetDays()) <= 9)))
                {
                    this.newReturnObject.addError(utilityService.singleError("MCYR11", language));
                }
                if(!(AccrualTypeCheck(accrualTypeCheck,currencyRules.getAccrualType())))
                {
                    this.newReturnObject.addError(utilityService.singleError("MCYR12", language));
                }
                if (!((currencyRules.getAllowSettlement())==true)||(currencyRules.getAllowSettlement()==false))
                {
                    this.newReturnObject.addError(utilityService.singleError("MCYR13", language));
                }
                if(!(AccrualTypeCheck(valueDateAdjustment,currencyRules.getValueDateAdjustment()))) {
                    this.newReturnObject.addError(utilityService.singleError("MCYR14", language));
                }

            }
           return this.newReturnObject.getAppErrors().size()>0?false:true;
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
            return false;
        }
    }

    private boolean AccrualTypeCheck (String[] accrualTypeCheck , String targetValue)
    {
        for(String s : accrualTypeCheck)
        {
            if(s.equals(targetValue))
            {
                return true;
            }
        }
        return false;
    }

    private boolean tenantIdCountryCodeCurrencyCodeCheck(CurrencyRules currencyRules, String language) {

        try {

            TenantDB approvedTenantResult = tenantDAL.findByTenantId(currencyRules.getTenantId());
            CurrencyDB approvedSettelement = currencyDAL.findByCurrencyCode(currencyRules.getSettlementCurrencyCode());
            CurrencyDB approvedCurrencyCodeResult = currencyDAL.findByCurrencyCode(currencyRules.getCurrencyCode());
            CountryDB approvedCountryCodeResult = countryDAL.findByCountryCode(currencyRules.getCountryCode());
            if (approvedTenantResult == null) {
                this.newReturnObject.addError(utilityService.singleError("MCYR15", language));
            }
            if (approvedCurrencyCodeResult==null){
                this.newReturnObject.addError(utilityService.singleError("MCYR16", language));
            }
            if (approvedSettelement==null){
                this.newReturnObject.addError(utilityService.singleError("MCYR17", language));
            }
            if (approvedCountryCodeResult == null) {
                    this.newReturnObject.addError(utilityService.singleError("MCYR18", language));
            }
            return this.newReturnObject.getAppErrors().size()>0?false:true;
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCYR02", language));//error code
            logger.error("Exception in CurrencyRulesController.updateCurrencyRules", this.newReturnObject.getAppErrorString(), ex);
            return false;
        }

    }
}
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
