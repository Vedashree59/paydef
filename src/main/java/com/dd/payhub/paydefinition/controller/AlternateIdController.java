package com.dd.payhub.paydefinition.controller;

import com.dd.payhub.paycommon.model.AlternateId;
import com.dd.payhub.paycommon.model.AlternateId;
import com.dd.payhub.paycommon.model.AlternateId;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.config.FnOptConfig;
import com.dd.payhub.paydefinition.dal.AlternateIdDAL;
import com.dd.payhub.paydefinition.service.PHUtilityService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/alternateid")

public class AlternateIdController {
    private static final Logger logger = LogManager.getLogger(AlternateIdController.class);
    private AlternateIdDAL alternateIdDAL;
    private PHUtilityService utilityService;

    private NewReturnObject newReturnObject;

    @Autowired
    public AlternateIdController(AlternateIdDAL alternateIdDAL, PHUtilityService utilityService) {
        this.alternateIdDAL = alternateIdDAL;
        this.utilityService = utilityService;
        this.newReturnObject = new NewReturnObject();
    }

    @PostMapping("/all")
    public String getAllAlternateId(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {//, , @RequestAttribute(value = "bankid") String headerCustomerId
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        ReactTableRecord tableRecord = new ReactTableRecord();

        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MAI001", language, FnOptConfig.fIdAlternateIdController, FnOptConfig.opFView)) {
                tableRecord = alternateIdDAL.getAllAlternateIdByPagination(tableQuery);
                newReturnObject.PerformReturnObject(tableRecord);
            }
            return newReturnObject.setReturnJSON();
        } catch (Exception ex) {
            tableRecord.setTotalPage(0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord(new ArrayList<>());
            newReturnObject.PerformReturnObject(tableRecord);
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MAI002", language));//error code
            logger.error("Exception in AlternateIdController.getAllAlternateId", this.newReturnObject.getAppErrorString(), ex);
            return newReturnObject.setReturnJSON();
        }
    }

    @PostMapping("/add")
    public String newAlternateId(@RequestBody AlternateId alternateId, @RequestAttribute(value = "userid") String headerUserId) {
        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (validateAlternateId(alternateId, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MTN001", language, FnOptConfig.fIdAlternateIdController, FnOptConfig.opFNew)) {//error code.
                //valid data to insert new record.
                long approvedCount = alternateIdDAL.countByAlternateId(alternateId.getAlternateId());
                long unapprovedCount = alternateIdDAL.countByUnapprovedAlternateId(alternateId.getAlternateId());
                if (  approvedCount  == 0 && unapprovedCount == 0) {//record does not exist so insertion can be done.
                    newReturnObject = alternateIdDAL.addAlternateId(alternateId, headerUserId, newReturnObject, language);//
                } else {//Error record exist duplication not allowed.
                    this.newReturnObject.addError(utilityService.singleError("MAI003", language));
                    this.newReturnObject.setReturncode(0);
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MAI002", language));//error code
            logger.error("Exception in AlternateIdController.newAlternateId", this.newReturnObject.getAppErrorString(), ex);

        }
        return this.newReturnObject.setReturnJSON();
    }

    @PostMapping("/geteditable")
    public String getEditableAlternateId(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery tableQuery) {

        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MAI001", language, FnOptConfig.fIdAlternateIdController, FnOptConfig.opFView)) {
                //user has the permission to view
                newReturnObject = alternateIdDAL.getEditableAlternateId(tableQuery, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MAI002", language));//error code
            logger.error("Exception in AlternateIdController.getEditableAlternateId", this.newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/update")
    public String updateAlternateId(@RequestBody AlternateId alternateId, @RequestAttribute(value = "userid") String headerUserId) {

        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            if (validateAlternateId(alternateId, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MAI001", language, FnOptConfig.fIdAlternateIdController, FnOptConfig.opFEdit)) {//error code.
                newReturnObject = alternateIdDAL.updateAlternateId(alternateId, headerUserId, this.newReturnObject, language);
            }

        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MAI002", language));//error code
            logger.error("Exception in AlternateIdController.updateAlternateId", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @PostMapping("/view")
    public String viewAlternateId(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MAI001", language, FnOptConfig.fIdAlternateIdController, FnOptConfig.opFView)) {//error code.
                newReturnObject = alternateIdDAL.viewByAlternateId(tableQuery, newReturnObject, language);
            }
        }catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MAI002", language));//error code
            logger.error("Exception in AlternateIdController.viewAlternateId",this.newReturnObject.getAppErrorString(),ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/approve")
    public String approveAlternateId(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery tableQuery) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MAI001", language, FnOptConfig.fIdAlternateIdController, FnOptConfig.opFAuth)) {//error code.

                newReturnObject = alternateIdDAL.approveAlternateId(tableQuery, headerUserId, this.newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MAI002", language));//error code
            logger.error("Exception in AlternateIdController.approveAlternateId", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @DeleteMapping("/reject")//api to delete the record from the tm_alternate_id
    // _unapproved.
    public String rejectAlternateId(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MAI001", language, FnOptConfig.fIdAlternateIdController, FnOptConfig.opFDelete)) {//error code.

                newReturnObject = alternateIdDAL.rejectByAlternateId(tableQuery, headerUserId, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MAI002", language));//error code
            logger.error("Exception in AlternateIdController.rejectAlternateId", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();

    }

    @DeleteMapping("/delete")
    public String inActivateAlternateId(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MAI001", language, FnOptConfig.fIdAlternateIdController, FnOptConfig.opFDelete)) {//error code.
                String status = alternateIdDAL.findByAlternateId(tableQuery.getPrimaryKey()).getAlternateId().getStatus();
                if (status.equals("inactive")) {
                    this.newReturnObject.addError(utilityService.singleError("MAI012", language));//error code
                } else {
                    newReturnObject = alternateIdDAL.alternateIdStatusChanger(tableQuery, headerUserId, newReturnObject, language, "inactive");
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MAI002", language));
            logger.error("Exception in AlternateIdController.inActivate", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/activate")
    public String reActivateAlternateId(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            String collection=tableQuery.getCollection();
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MAI001", language, FnOptConfig.fIdAlternateIdController, FnOptConfig.opFReactivate)) {
                String status =collection.equals("approved")?alternateIdDAL.findByAlternateId(tableQuery.getPrimaryKey()).getAlternateId().getStatus():alternateIdDAL.findUnapprovedByAlternateId(tableQuery.getPrimaryKey()).getAlternateId().getStatus();
                if (status.equals("active")) {
                    this.newReturnObject.addError(utilityService.singleError("MAI011", language));//error code
                } else {
                    newReturnObject = alternateIdDAL.alternateIdStatusChanger(tableQuery, headerUserId, newReturnObject, language, "active");
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MAI002", language));//error code
            logger.error("Exception in AlternateIdController.reActivate", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();

    }

    private boolean validateAlternateId(AlternateId alternateId, String language) {

        if(alternateId.getAlternateId().isEmpty()||alternateId.getStatus().isEmpty()){
            //mandatory fields are empty.
            this.newReturnObject.addError(utilityService.singleError("MAI004", language));
        }
        return this.newReturnObject.getAppErrors().size()>0?false:true;
    }

}
