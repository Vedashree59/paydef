package com.dd.payhub.paydefinition.controller;

import com.dd.payhub.paycommon.model.*;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.config.FnOptConfig;
import com.dd.payhub.paydefinition.dal.CountryDAL;
import com.dd.payhub.paydefinition.dal.CountryHolidaysDAL;
import com.dd.payhub.paydefinition.dal.TenantDAL;
import com.dd.payhub.paydefinition.dal.UserDAL;
import com.dd.payhub.paydefinition.entity.*;
import com.dd.payhub.paydefinition.service.PHUtilityService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/countryholidays")
public class CountryHolidaysController {
    private static final Logger logger = LogManager.getLogger(CountryHolidaysController.class);
    private CountryHolidaysDAL countryHolidaysDAL;
    private TenantDAL tenantDAL;
    private CountryDAL countryDAL;
    private UserDAL userDAL;
    private PHUtilityService utilityService;
    private NewReturnObject newReturnObject;

    @Autowired
    public CountryHolidaysController(TenantDAL tenantDAL, CountryDAL countryDAL, CountryHolidaysDAL countryHolidaysDAL, PHUtilityService utilityService,UserDAL userDAL) {
        this.countryHolidaysDAL = countryHolidaysDAL;
        this.utilityService = utilityService;
        this.countryDAL = countryDAL;
        this.tenantDAL = tenantDAL;
        this.userDAL=userDAL;
        newReturnObject = new NewReturnObject();
    }

    @PostMapping("/all")
    public String getAllCountryHolidays(@RequestBody ReactTableQuery tableQuery ,@RequestAttribute(value = "tenantId") String bankId, @RequestAttribute(value = "userid") String headerUserId) {//, , @RequestAttribute(value = "bankid") String headerTenantIdCountryCodeYear
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        ReactTableRecord tableRecord = new ReactTableRecord();

        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCUH01", language, FnOptConfig.fIdCountryHolidaysController, FnOptConfig.opFView,bankId)) {
                tableRecord = countryHolidaysDAL.getAllCountryHolidaysByPagination(tableQuery);
                newReturnObject.PerformReturnObject(tableRecord);
            }
            return newReturnObject.setReturnJSON();
        } catch (Exception ex) {
            tableRecord.setTotalPage( 0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord( new ArrayList<>());
            newReturnObject.PerformReturnObject(tableRecord);
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCUH02", language));//error code
            logger.error("Exception in CountryHolidaysController.getAllCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
            return newReturnObject.setReturnJSON();
        }
    }

    @PostMapping("/add")
    public String newCountryHolidays(@RequestBody CountryHolidays countryHolidays,@RequestAttribute(value = "tenantId") String bankId, @RequestAttribute(value = "userid") String headerUserId) {
        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (validateCountryHolidays(countryHolidays, language,headerUserId) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCUH01", language, FnOptConfig.fIdCountryHolidaysController, FnOptConfig.opFNew,bankId)) {//error code.
                //valid data to insert new record.
                long approvedCount = countryHolidaysDAL.countByTenantIdCountryCodeYear(countryHolidays.getTenantId(),countryHolidays.getCountryCode(),countryHolidays.getYear());
                long unapprovedCount = countryHolidaysDAL.countByUnapprovedTenantIdCountryCodeYear(countryHolidays.getTenantId(),countryHolidays.getCountryCode(),countryHolidays.getYear());
                if (approvedCount == 0 && unapprovedCount == 0) {//record does not exist so insertion can be done.
                    Boolean approvalRequired=tenantDAL.findApprovalRequiredTenantId(countryHolidays.getTenantId()).getTenant().getFourEyeRequired();
                    newReturnObject = countryHolidaysDAL.addCountryHolidays(countryHolidays, headerUserId, newReturnObject, language,approvalRequired);//
                } else {//Error record exist duplication not allowed.
                    this.newReturnObject.addError(utilityService.singleError("MCUH03", language));
                    this.newReturnObject.setReturncode(0);
                }
            }

        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCUH02", language));//error code
            logger.error("Exception in CountryHolidaysController.newCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }


    @PostMapping("/geteditable")
    public String getEditableCountryHolidays(@RequestAttribute(value = "userid") String headerUserId,@RequestAttribute(value = "tenantId") String bankId, @RequestBody ReactTableQuery tableQuery) {

        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCUH01", language, FnOptConfig.fIdCountryHolidaysController, FnOptConfig.opFView,bankId)) {
                //user has the permission to view
                newReturnObject = countryHolidaysDAL.getEditableCountryHolidays(tableQuery, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCUH02", language));//error code
            logger.error("Exception in CountryHolidaysController.getEditableCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }
    @PostMapping("/update")
    public String updateCountryHolidays(@RequestBody CountryHolidays countryHolidays,@RequestAttribute(value = "tenantId") String bankId, @RequestAttribute(value = "userid") String headerUserId) {

        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            if (validateCountryHolidays(countryHolidays, language,headerUserId) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCUH01", language, FnOptConfig.fIdCountryHolidaysController, FnOptConfig.opFEdit,bankId)) {//error code.
                Boolean approvalRequired=tenantDAL.findApprovalRequiredTenantId(countryHolidays.getTenantId()).getTenant().getFourEyeRequired();
                newReturnObject = countryHolidaysDAL.updateCountryHolidays(countryHolidays, headerUserId, this.newReturnObject, language,approvalRequired);
            }

        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCUH02", language));//error code
            logger.error("Exception in CountryHolidaysController.updateCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();

    }

    @PostMapping("/view")
    public String viewCountryHolidays(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "tenantId") String bankId,@RequestAttribute(value = "userid") String headerUserId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCUH01", language, FnOptConfig.fIdCountryHolidaysController, FnOptConfig.opFView,bankId)) {//error code.

                newReturnObject = countryHolidaysDAL.viewByTenantIdCountryCodeYear(tableQuery, newReturnObject, language);
            }
        }catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCUH02", language));//error code
            logger.error("Exception in CountryHolidaysController.viewCountryHolidays",this.newReturnObject.getAppErrorString(),ex);
        }
        return newReturnObject.setReturnJSON();
    }


    @PostMapping("/approve")
    public String approveCountryHolidays(@RequestAttribute(value = "userid") String headerUserId,@RequestAttribute(value = "tenantId") String bankId, @RequestBody ReactTableQuery tableQuery) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCUH01", language, FnOptConfig.fIdCountryHolidaysController, FnOptConfig.opFAuth,bankId)) {//error code.

                newReturnObject = countryHolidaysDAL.approveCountryHolidays(tableQuery, headerUserId, this.newReturnObject, language);
            }

        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCUH02", language));//error code
            logger.error("Exception in CountryHolidaysController.approveCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @DeleteMapping("/reject")
    // _unapproved.
    public String rejectCountryHolidays(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCUH01", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFDelete)) {//error code.
                newReturnObject = countryHolidaysDAL.rejectByTenantIdCountryCodeYear(tableQuery, headerUserId, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCUH02", language));//error code
            logger.error("Exception in CountryHolidaysController.rejectCountryHolidays", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();
    }
    @DeleteMapping("/delete")
    public String inActivateCountryHolidays(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            String tenantId=tableQuery.getQueryFields().get("tenantId").toString();
            String countryCode=tableQuery.getQueryFields().get("countryCode").toString();
            String year=tableQuery.getQueryFields().get("year").toString();
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCUH01", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFDelete)) {//error code.
                boolean approvalRequired = approvalRequirementOfTenant(tableQuery,language);
                String status = countryHolidaysDAL.findByTenantIdCountryCodeYear(tenantId,countryCode,year).getCountryHolidays().getStatus();
                if (status.equals("inactive")) {
                    this.newReturnObject.addError(utilityService.singleError("MCUH12", language));//error code
                } else {
                    newReturnObject = countryHolidaysDAL.countryHolidaysStatusChanger(tableQuery, headerUserId, newReturnObject, language,"inactive",approvalRequired);
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCUH02", language));//error code
            logger.error("Exception in CountryHolidaysController.inActivateCountryHolidays", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();
    }
    @PostMapping("/activate")
    public String reActivateCountryHolidays(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            String tenantId=tableQuery.getQueryFields().get("tenantId").toString();
            String countryCode=tableQuery.getQueryFields().get("countryCode").toString();
            String year=tableQuery.getQueryFields().get("year").toString();
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCUH01", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFReactivate)) {//error code.
                boolean approvalRequired = approvalRequirementOfTenant(tableQuery,language);
                String status =tableQuery.getCollection().equals("approved")?countryHolidaysDAL.findByTenantIdCountryCodeYear(tenantId,countryCode,year).getCountryHolidays().getStatus():countryHolidaysDAL.findUnapprovedByTenantIdCountryCodeYear(tenantId,countryCode,year).getCountryHolidays().getStatus();

                if (status.equals("active")) {
                    this.newReturnObject.addError(utilityService.singleError("MCUH11", language));//error code
                } else {
                    newReturnObject = countryHolidaysDAL.countryHolidaysStatusChanger(tableQuery, headerUserId, newReturnObject, language,"active",approvalRequired);
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCUH02", language));//error code
            logger.error("Exception in CountryHolidaysController.reActivateCountryHolidays", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();

    }

    private boolean approvalRequirementOfTenant(ReactTableQuery tableQuery, String language) {
        try {
            String tenantId=tableQuery.getQueryFields().get("tenantId").toString();
            String countryCode=tableQuery.getQueryFields().get("countryCode").toString();
            String year=tableQuery.getQueryFields().get("year").toString();
            CountryHolidaysDB countryHolidaysDB = countryHolidaysDAL.findByTenantIdCountryCodeYear(tenantId,countryCode,year);
            TenantDB tenantDB = null;
            if (countryHolidaysDB == null) {
                this.newReturnObject.addError(utilityService.singleError("MCUH06", language));
            } else {
                tenantDB = tenantDAL.findApprovalRequiredTenantId(countryHolidaysDB.getCountryHolidays().getTenantId());
            }
            if (tenantDB == null) {
                this.newReturnObject.addError(utilityService.singleError("MCUH06", language));
            } else {
                return tenantDB.getTenant().getFourEyeRequired();
            }
            return this.newReturnObject.getAppErrors().size() > 0 ? true : tenantDB.getTenant().getFourEyeRequired();
        }catch  (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCUH02", language));
            logger.error(ex.getMessage());
            return true;
        }
    }

    private boolean validateCountryHolidays(CountryHolidays countryHolidays, String language,String userID) {

        //mandatory fields.
        try {
            if ((!countryHolidays.getTenantId().isEmpty()) && (!countryHolidays.getCountryCode().isEmpty()) && (!countryHolidays.getYear().isEmpty()) && (countryHolidays.getHolidayList() != null)) {
                if (tenantIdCountryCodeCheck(countryHolidays, language,userID)) {

                    if (!(countryHolidays.getYear().length() == 4)) {
                        this.newReturnObject.addError(utilityService.singleError("MCUH15", language));//error code
                    }
<<<<<<< HEAD
                    if(!userDAL.findTenantIDByOfficialEmailId(userID).getUser().getTenantId().equals(countryHolidays.getTenantId())){
=======
                    if(!userDAL.findTenantIDByOfficialEmailId(userID).getUser().getOfficialEmailId().equals(countryHolidays.getTenantId())){
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                        this.newReturnObject.addError(utilityService.singleError("MCUH14", language));
                    }
                    countryHolidays = longMediumDateCalcuator(countryHolidays);
                }

            } else {
                this.newReturnObject.addError(utilityService.singleError("MCUH04", language));//error code

            }
            return this.newReturnObject.getAppErrors().size() > 0 ? false : true;
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCUH02", language));//error code
            return false;
        }

    }

    private CountryHolidays longMediumDateCalcuator(CountryHolidays countryHolidays) {
        List<Holiday> holidays = countryHolidays.getHolidayList();
        // if(countryHolidays.getYear().startsWith())
        for (int i = 0; i < holidays.size(); i++) {
//            if((countryHolidays.getYear()).equals(holidays.get(i).getHolidayDate().getYear())){
            Holiday holiday = holidays.get(i);
            PayHubDate payHubDate = new PayHubDate(holiday.getHolidayDate().getYear(), holiday.getHolidayDate().getMonth(), holiday.getHolidayDate().getDay());
            holidays.get(i).setHolidayDate(payHubDate);
            //   }
//            else{
//                this.newReturnObject.addError(utilityService.singleError( "MCUH03",language));
//            }

        }
        return countryHolidays;
    }

    private boolean tenantIdCountryCodeCheck(CountryHolidays countryHolidays, String language,String userID) {

        try {
            if(userDAL.findTenantIDByOfficialEmailId(userID)==null){
                this.newReturnObject.addError(utilityService.singleError("MCUH14", language));
            }
            if (tenantDAL.countByTenantId(countryHolidays.getTenantId()) == 0) {
                this.newReturnObject.addError(utilityService.singleError("MCUH14", language));
            }
            if ( countryDAL.countByCountryCode(countryHolidays.getCountryCode()) == 0) {
                this.newReturnObject.addError(utilityService.singleError("MCUH13", language));
            }
            return this.newReturnObject.getAppErrors().size() > 0 ? false : true;
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCUH02", language));//error code
            logger.error("Exception in CountryHolidaysController.updateCountryHolidays", this.newReturnObject.getAppErrorString(), ex);
            return false;
        }
    }
}





