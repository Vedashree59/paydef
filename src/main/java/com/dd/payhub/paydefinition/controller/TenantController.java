package com.dd.payhub.paydefinition.controller;

import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.model.Tenant;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.config.FnOptConfig;
import com.dd.payhub.paydefinition.dal.BanksDAL;
import com.dd.payhub.paydefinition.dal.CountryDAL;
import com.dd.payhub.paydefinition.dal.TenantDAL;

import com.dd.payhub.paydefinition.entity.*;
import com.dd.payhub.paydefinition.service.PHUtilityService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin(origins = "*", allowedHeaders = "Content-Type, Accept, reconnection, User-Token")
@RestController
@RequestMapping("/tenant")
public class TenantController {
    private static final Logger logger = LogManager.getLogger(TenantController.class);
    private TenantDAL tenantDAL;
    private BanksDAL banksDAL;
    private CountryDAL countryDAL;
    private PHUtilityService utilityService;
    private NewReturnObject newReturnObject;

    @Autowired

    public TenantController(CountryDAL countryDAL, TenantDAL tenantDAL, PHUtilityService utilityService, BanksDAL banksDAL) {
        this.tenantDAL = tenantDAL;
        this.utilityService = utilityService;
<<<<<<< HEAD
        this.banksDAL = banksDAL;
        this.countryDAL = countryDAL;
        newReturnObject = new NewReturnObject();
    }

    @PostMapping("/all")

    public String getAllTenant(@RequestBody ReactTableQuery tableQuery , @RequestAttribute(value = "userid") String headerUserId) {//, , @RequestHeader(value = "bankid") String headerBankId
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        newReturnObject = new NewReturnObject(0, "\"\"");
        ReactTableRecord tableRecord = new ReactTableRecord();
        try {
            if (utilityService.creatorHasPermission(headerUserId, newReturnObject, "MTN001", language, FnOptConfig.fIdTenantController, FnOptConfig.opFView)) {//error code.
=======
        this.banksDAL=banksDAL;
        this.countryDAL = countryDAL;
        newReturnObject=new NewReturnObject();

    }



    @PostMapping("/all")

    public String getAllTenant(@RequestBody ReactTableQuery tableQuery , @RequestAttribute(value = "userid") String headerUserId) {//, , @RequestAttribute(value = "bankid") String headerBankId
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        ReactTableRecord tableRecord = new ReactTableRecord();
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MTN001", language, FnOptConfig.fIdTenantController, FnOptConfig.opFView)) {//error code.
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                tableRecord = tenantDAL.getAllTenantByPagination(tableQuery);
                newReturnObject.PerformReturnObject(tableRecord);
            }
            return newReturnObject.setReturnJSON();
        } catch (Exception ex) {
<<<<<<< HEAD
            tableRecord.setTotalPage( 0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord( new ArrayList<>());
            newReturnObject.PerformReturnObject(tableRecord);
            newReturnObject.setReturncode(0);
            newReturnObject.addError(utilityService.singleError("MTN002", language));//error code
            logger.error("Exception in TenantController.getAllTenant", newReturnObject.getAppErrorString(), ex);
=======
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MTN002", language));//error code
            logger.error("Exception in TenantController.getAllTenant", this.newReturnObject.getAppErrorString(), ex);
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            return newReturnObject.setReturnJSON();
        }
    }


    @PostMapping("/add")
    public String newTenant(@RequestBody Tenant tenant, @RequestAttribute(value = "userid") String headerUserId) {
        //validation.
<<<<<<< HEAD
        newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (validateTenant(tenant, language) && utilityService.creatorHasPermission(headerUserId, newReturnObject, "MTN001", language, FnOptConfig.fIdTenantController, FnOptConfig.opFNew)) {//error code.//valid data to insert new record.

                if ( tenantDAL.countByTenantId(tenant.getTenantId()) == 0 && tenantDAL.countByUnapprovedTenantId(tenant.getTenantId()) == 0) {//record does not exist so insertion can be done.
                    newReturnObject = tenantDAL.addTenant(tenant, headerUserId, newReturnObject, language);//
                } else {//Error record exist duplication not allowed.
                    newReturnObject.addError(utilityService.singleError("MTN003", language));
                    newReturnObject.setReturncode(0);
                }
            }
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(utilityService.singleError("MTN002", language));//error code
            logger.error("Exception in TenantController.newTenant", newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
=======
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            TenantDB tenantDB = null;
            TenantUnapprovedDB tenantUnapprovedDB = null;

            if (validateTenant(tenant, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MTN001", language, FnOptConfig.fIdTenantController, FnOptConfig.opFNew)) {//error code.
                //valid data to insert new record.
                tenantDB = tenantDAL.findByTenantId(tenant.getTenantId());
                tenantUnapprovedDB = tenantDAL.findUnapprovedByTenantId(tenant.getTenantId());
                if (tenantDB == null && tenantUnapprovedDB == null) {//record does not exist so insertion can be done.
                    newReturnObject = tenantDAL.addTenant(tenant, headerUserId, newReturnObject, language);//
                } else {//Error record exist duplication not allowed.
                    this.newReturnObject.addError(utilityService.singleError("MTN003", language));
                    this.newReturnObject.setReturncode(0);
                }
            }

        } catch (Exception ex)  {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MTN002", language));//error code
            logger.error("Exception in TenantController.newTenant", this.newReturnObject.getAppErrorString(), ex);

        }
        return this.newReturnObject.setReturnJSON();

>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
    }


    @PostMapping("/update")
    public String updateTenant(@RequestBody Tenant tenant, @RequestAttribute(value = "userid") String headerUserId) {

        //validation.
<<<<<<< HEAD
        newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            if (validateTenant(tenant, language) && utilityService.creatorHasPermission(headerUserId, newReturnObject, "MTN001", language, FnOptConfig.fIdTenantController, FnOptConfig.opFEdit)) {//error code.
                newReturnObject = tenantDAL.updateTenant(tenant, headerUserId, newReturnObject, language);
            }

        } catch (Exception ex) {
            newReturnObject.addError(utilityService.singleError("MTN002", language));//error code
            logger.error("Exception in TenantController.updateTenant", newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
=======
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            if (validateTenant(tenant, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MTN001", language, FnOptConfig.fIdTenantController, FnOptConfig.opFEdit)) {//error code.
                newReturnObject = tenantDAL.updateTenant(tenant, headerUserId, this.newReturnObject, language);
            }

        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MTN002", language));//error code
            logger.error("Exception in TenantController.updateTenant", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

    }

    @PostMapping("/geteditable")
    public String getEditableTenant(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery reactTableQuery) {

        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
<<<<<<< HEAD
        newReturnObject = new NewReturnObject(0, "\"\"");
        try {

            if (utilityService.creatorHasPermission(headerUserId, newReturnObject, "MTN001", language, FnOptConfig.fIdTenantController, FnOptConfig.opFView)) {
=======
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MTN001", language, FnOptConfig.fIdTenantController, FnOptConfig.opFView)) {
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                //user has the permission to view
                newReturnObject = tenantDAL.getEditableTenant(reactTableQuery, newReturnObject, language);
            }
        } catch (Exception ex) {
<<<<<<< HEAD
            newReturnObject.setReturncode(0);
            newReturnObject.addError(utilityService.singleError("MTN002", language));//error code
            logger.error("Exception in TenantController.getEditableTenant", newReturnObject.getAppErrorString(), ex);
=======
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MTN002", language));//error code
            logger.error("Exception in TenantController.getEditableTenant", this.newReturnObject.getAppErrorString(), ex);
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        }
        return newReturnObject.setReturnJSON();
    }
    @PostMapping("/approve")
<<<<<<< HEAD
    public String approveTenant(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery reactTableQuery) {
        newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, newReturnObject, "MTN001", language, FnOptConfig.fIdTenantController, FnOptConfig.opFAuth)) {//error code.

                newReturnObject = tenantDAL.approveTenant(reactTableQuery.getPrimaryKey(), headerUserId, newReturnObject, language);
            }

        } catch (Exception ex) {
            newReturnObject.addError(utilityService.singleError("MTN002", language));//error code
            logger.error("Exception in BankController.approveTenant", newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }
    @DeleteMapping("/reject")//api to delete the record from the tm_tenant_unapproved.
    public String rejectTenant(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {
        newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (utilityService.creatorHasPermission(headerUserId, newReturnObject, "MTN001", language, FnOptConfig.fIdTenantController, FnOptConfig.opFDelete)) {//error code.

                newReturnObject = tenantDAL.rejectByTenantId(tableQuery.getPrimaryKey(), headerUserId, newReturnObject, language);
            }
        } catch (Exception ex) {
            logger.error("Exception in TenantController.rejectTenant", newReturnObject.getAppErrorString(), ex);
            newReturnObject.addError(utilityService.singleError("MTN002", language));//error code
=======
    public String approveTenant(@RequestAttribute(value = "userid") String headerUserId, @RequestBody String tenantId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MTN001", language, FnOptConfig.fIdTenantController, FnOptConfig.opFAuth)) {//error code.

                newReturnObject = tenantDAL.approveTenant(tenantId, headerUserId, this.newReturnObject, language);
            }

        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MTN002", language));//error code
            logger.error("Exception in BankController.approveTenant", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }
    @DeleteMapping("/reject")//api to delete the record from the tm_tenant_unapproved.
    public String rejectTenant(@RequestBody String tenantId, @RequestAttribute(value = "userid") String headerUserId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MTN001", language, FnOptConfig.fIdTenantController, FnOptConfig.opFDelete)) {//error code.

                newReturnObject = tenantDAL.deleteByTenantId(tenantId, headerUserId, newReturnObject, language);
            }
        } catch (Exception ex) {
            logger.error("Exception in TenantController.rejectTenant", this.newReturnObject.getAppErrorString(), ex);
            this.newReturnObject.addError(utilityService.singleError("MTN002", language));//error code
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

        }
        return newReturnObject.setReturnJSON();

    }

<<<<<<< HEAD


    @PostMapping("/view")
    public String viewTenant(@RequestBody ReactTableQuery tableQuery){
        newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            newReturnObject=tenantDAL.viewByTenantId(tableQuery,newReturnObject,language);
        }catch (Exception ex) {
            newReturnObject.addError(utilityService.singleError("MTN002", language));//error code
            logger.error("Exception in CountryController.view",newReturnObject.getAppErrorString(),ex);
        }
        return newReturnObject.setReturnJSON();

    }

    @DeleteMapping("/delete")
    public String inActivateEventDefinition(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MED001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFDelete)) {//error code.
                String status = tenantDAL.findByTenantId(tableQuery.getPrimaryKey()).getTenant().getStatus();
                if (status.equals("inactive")) {
                    this.newReturnObject.addError(utilityService.singleError("MED012", language));//error code
                } else {
                    newReturnObject = tenantDAL.tenantStatusChanger(tableQuery, headerUserId, newReturnObject, language, "inactive");
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MED002", language));
            logger.error("Exception in EventDefinitionController.inActivate", this.newReturnObject.getAppErrorString(), ex);
=======
        @PostMapping("/banklistids")
    public String listOFBanksIds(@RequestAttribute(value = "userid") String headerUserId,@RequestBody String bankListId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try{
            newReturnObject=banksDAL.listOfIds(bankListId,newReturnObject,language);
        }catch (Exception ex) {
            logger.error("Exception in TenantController.listOFBanksIds", this.newReturnObject.getAppErrorString(), ex);
            newReturnObject.PerformReturnObject(new ArrayList<>());
            newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MTN002", language));//error code
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

        }
        return newReturnObject.setReturnJSON();
    }

<<<<<<< HEAD
    @PostMapping("/activate")
    public String reActivateEventDefinition(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MED001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFDelete)) {//error code.
                String status = tenantDAL.findByTenantId(tableQuery.getPrimaryKey()).getTenant().getStatus();
                if (status.equals("active")) {
                    this.newReturnObject.addError(utilityService.singleError("MED012", language));//error code
                } else {
                    newReturnObject = tenantDAL.tenantStatusChanger(tableQuery, headerUserId, newReturnObject, language, "active");
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MED002", language));
            logger.error("Exception in EventDefinitionController.inActivate", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();

    }


    @PostMapping("/tenantidsregx")
    public String listOfTenantIdsRegx(@RequestAttribute(value = "userid") String headerUserId,@RequestBody String keyword){ {
        newReturnObject = new NewReturnObject(0, "\"\"");
=======

    @PostMapping("/view")
    public String viewTenant(@RequestBody ReactTableQuery tableQuery){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            newReturnObject=tenantDAL.viewByTenantId(tableQuery,newReturnObject,language);
        }catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MTN002", language));//error code
            logger.error("Exception in CountryController.view",this.newReturnObject.getAppErrorString(),ex);
        }
        return newReturnObject.setReturnJSON();


    }

    @PostMapping("/tenantidsregx")
    public String listOfTenantIdsRegx(@RequestAttribute(value = "userid") String headerUserId,@RequestBody String keyword){ {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try{
            newReturnObject= tenantDAL.listOfAllTenantIdsByRegx(keyword,newReturnObject,language);
        }catch (Exception ex) {
<<<<<<< HEAD
            logger.error("Exception in CustomerController.listOfTenantIdsRegx", newReturnObject.getAppErrorString(), ex);
            newReturnObject.PerformReturnObject(new ArrayList<>());
            newReturnObject.setReturncode(0);
            newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
=======
            logger.error("Exception in CustomerController.listOfTenantIdsRegx", this.newReturnObject.getAppErrorString(), ex);
            newReturnObject.PerformReturnObject(new ArrayList<>());
            newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

        }
        return newReturnObject.setReturnJSON();
    }
    }
    private boolean validateTenant(Tenant tenant, String language) {
<<<<<<< HEAD

        try{
            if((!tenant.getTenantId().isEmpty())&&((!tenant.getTenantName().isEmpty())&&(!tenant.getHomeCountry().isEmpty())&&(!tenant.getBankListId().isEmpty())&&(!tenant.getStatus().isEmpty())&&(tenant.getFourEyeRequired()!=null))) {
                //all mandatory fields are given and now verifying the bankListId
                BanksDB banksDB = banksDAL.findByBankListId(tenant.getBankListId());
                if (banksDB == null) {
                    //error bankListId invalid
                    newReturnObject.addError(utilityService.singleError("MTN013", language));//error code
                }

                //country code validation


                if (countryDAL.findByCountryCode(tenant.getAddress().getCountryCode())== null) {
                    newReturnObject.addError(utilityService.singleError("MTN014", language));//error code
                }


                if(countryDAL.findByCountryName(tenant.getHomeCountry())==null){
                    newReturnObject.addError(utilityService.singleError("MTN015", language));//error code
                }
            }
            else {
                //error mandatory fields are not valid.
                newReturnObject.addError(utilityService.singleError("MTN004", language));//error code
            }
            return newReturnObject.getAppErrors().size()>0?false:true;

        }catch (Exception ex){
            newReturnObject.addError(utilityService.singleError("MTN002", language));//error code
            return false;
        }

=======
        boolean returnType=false;
        try{
            if((!tenant.getTenantId().isEmpty())&&((!tenant.getTenantName().isEmpty())&&(!tenant.getHomeCountry().isEmpty())&&(!tenant.getBankListId().isEmpty())&&(!tenant.getStatus().isEmpty())&&(tenant.getFourEyeRequired()!=null)))
            {
                //all mandatory fields are given and now verifying the bankListId
                BanksDB banksDB=banksDAL.findByBankListId(tenant.getBankListId());
                if(banksDB!=null){
                    returnType= true;
                }else{
                    //error bankListId invalid
                    this.newReturnObject.addError(utilityService.singleError("MTN007", language));//error code
                    return false;
                }
                CountryDB countryDB=countryDAL.findByCountryCode(tenant.getAddress().getCountryCode());
                if(countryDB!=null){
                    returnType= true;
                }else{
                //error bankListId invalid
                this.newReturnObject.addError(utilityService.singleError("MTN010", language));//error code
                return false;
            }
                countryDB=countryDAL.findByCountryName(tenant.getHomeCountry());
                if(countryDB!=null){
                    returnType= true;
                }else{
                    //error bankListId invalid
                    this.newReturnObject.addError(utilityService.singleError("MTN011", language));//error code
                    return false;
                }

            }
            else {
                //error mandatory fields are not valid.
                this.newReturnObject.addError(utilityService.singleError("MTN004", language));//error code
                return false;
            }

        }catch (Exception ex){
            this.newReturnObject.addError(utilityService.singleError("MTN002", language));//error code
            return false;
        }
        return returnType;
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
    }
}