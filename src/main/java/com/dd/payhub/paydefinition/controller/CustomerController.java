package com.dd.payhub.paydefinition.controller;

import com.dd.payhub.paycommon.model.Customer;
import com.dd.payhub.paycommon.model.PayHubDate;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.config.FnOptConfig;
import com.dd.payhub.paydefinition.dal.CustomerDAL;
import com.dd.payhub.paydefinition.dal.TenantDAL;
import com.dd.payhub.paydefinition.dal.UserDAL;
import com.dd.payhub.paydefinition.entity.CustomerDB;
import com.dd.payhub.paydefinition.entity.TenantDB;
import com.dd.payhub.paydefinition.service.PHUtilityService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/customer")
public class CustomerController {
    private static final Logger logger = LogManager.getLogger(CustomerController.class);
    private CustomerDAL customerDAL;
    private PHUtilityService utilityService;
    private TenantDAL tenantDAL;
    private NewReturnObject newReturnObject;

    @Autowired
    public CustomerController(UserDAL userDAL,CustomerDAL customerDAL, PHUtilityService utilityService,TenantDAL tenantDAL) {
        this.customerDAL = customerDAL;
        this.utilityService = utilityService;
        this.tenantDAL=tenantDAL;

        newReturnObject=new NewReturnObject();
    }

    @PostMapping("/all")
    public String getAllCustomer(@RequestBody ReactTableQuery tableQuery ,@RequestAttribute(value = "tenantId") String tenantId, @RequestAttribute(value = "userid") String headerUserId) {//, , @RequestAttribute(value = "bankid") String headerCustomerId
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        ReactTableRecord tableRecord = new ReactTableRecord();

        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCustomerController, FnOptConfig.opFView,tenantId)) {
                tableRecord = customerDAL.getAllCustomerByPagination(tableQuery);
                newReturnObject.PerformReturnObject(tableRecord);
            }
            return newReturnObject.setReturnJSON();
        } catch (Exception ex) {
            tableRecord.setTotalPage( 0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord( new ArrayList<>());
            newReturnObject.PerformReturnObject(tableRecord);
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
            logger.error("Exception in CustomerController.getAllCustomer", this.newReturnObject.getAppErrorString(), ex);
            return newReturnObject.setReturnJSON();
        }
    }

    @PostMapping("/add")
    public String newCustomer(@RequestBody Customer customer,@RequestAttribute(value = "tenantId") String tenantId, @RequestAttribute(value = "userid") String headerUserId) {
        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (validateCustomer(customer,tenantId,  language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCustomerController, FnOptConfig.opFNew, tenantId)) {//error code.
                //valid data to insert new record.
                long approvedCount = customerDAL.countByTenantIdCustomerId(customer.getTenantId(),customer.getCustomerId());
                long unapprovedCount = customerDAL.countUnapprovedByTenantIdCustomerId(customer.getTenantId(),customer.getCustomerId());
                if (approvedCount == 0 && unapprovedCount == 0) {//record does not exist so insertion can be done.
                    Boolean approvalRequired = tenantDAL.findApprovalRequiredTenantId(customer.getTenantId()).getTenant().getFourEyeRequired();
                    newReturnObject = customerDAL.addCustomer(customer, headerUserId, newReturnObject, language, approvalRequired);//
                }else {//Error record exist duplication not allowed.
                    this.newReturnObject.addError(utilityService.singleError("MCU003", language));
                    this.newReturnObject.setReturncode(0);
                }
            }

        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
            logger.error("Exception in CustomerController.newCustomer", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @PostMapping("/geteditable")
    public String getEditableCustomer(@RequestAttribute(value = "userid") String headerUserId,@RequestAttribute(value = "tenantId") String tenantId, @RequestBody ReactTableQuery tableQuery) {

        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdCustomerController, FnOptConfig.opFView,tenantId)) {
                //user has the permission to view
                newReturnObject = customerDAL.getEditableCustomer(tableQuery, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
            logger.error("Exception in CustomerController.getEditableCustomer", this.newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/update")
    public String updateCustomer(@RequestBody Customer customer,@RequestAttribute(value = "tenantId") String tenantId, @RequestAttribute(value = "userid") String headerUserId) {

        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            if (validateCustomer(customer,tenantId, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCustomerController, FnOptConfig.opFEdit,tenantId)) {//error code.
                Boolean approvalRequired=tenantDAL.findApprovalRequiredTenantId(customer.getTenantId()).getTenant().getFourEyeRequired();
                newReturnObject = customerDAL.updateCustomer(customer, headerUserId, this.newReturnObject, language,approvalRequired);
            }

        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
            logger.error("Exception in CustomerController.updateCustomer", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();

    }

    @PostMapping("/view")
    public String viewCustomer(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "tenantId") String tenantId,@RequestAttribute(value = "userid") String headerUserId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCustomerController, FnOptConfig.opFView,tenantId)) {//error code.

                newReturnObject = customerDAL.viewByTenantIdCustomerId(tableQuery, newReturnObject, language);
            }
        }catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
            logger.error("Exception in CustomerController.viewCustomer",this.newReturnObject.getAppErrorString(),ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/approve")
    public String approveCustomer(@RequestAttribute(value = "userid") String headerUserId,@RequestAttribute(value = "tenantId") String tenantId, @RequestBody ReactTableQuery tableQuery) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCustomerController, FnOptConfig.opFAuth,tenantId)) {//error code.

                newReturnObject = customerDAL.approveCustomer(tableQuery, headerUserId, this.newReturnObject, language);
            }

        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
            logger.error("Exception in CustomerController.approveCustomer", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @DeleteMapping("/reject")//api to delete the record from the tm_event_definition
    public String rejectCustomer(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCU001", language, FnOptConfig.fIdCustomerController, FnOptConfig.opFDelete)) {//error code.
                newReturnObject = customerDAL.rejectByTenantIdCustomerId(tableQuery, headerUserId, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
            logger.error("Exception in CustomerController.rejectCustomer", this.newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @DeleteMapping("/delete")
    public String inActivateCustomer(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        NewReturnObject newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, newReturnObject, "MCU001", language, FnOptConfig.fIdCustomerController, FnOptConfig.opFDelete)) {//error code.
                //superKey
                String tenantId = tableQuery.getQueryFields().get("tenantId").toString();
                String customerId= tableQuery.getQueryFields().get("customerId").toString();

                boolean approvalRequired = approvalRequirementOfTenant(tenantId,customerId,language);

                String status = customerDAL.findByTenantIdCustomerId(tenantId,customerId).getCustomer().getStatus();
                if (status.equals("inactive")) {
                    newReturnObject.addError(utilityService.singleError("MCU012", language));//error code same status
                } else {
                    newReturnObject = customerDAL.customerStatusChanger(tableQuery, headerUserId, newReturnObject, language,"inactive",approvalRequired);
                }
            }
        } catch (Exception ex) {
            newReturnObject.addError(utilityService.singleError("MCU002", language));//error code
            logger.error("Exception in CustomerController.inActivateCustomer", newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/activate")
    public String reActivateCustomer(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        NewReturnObject newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, newReturnObject, "MCU001", language, FnOptConfig.fIdCustomerController, FnOptConfig.opFReactivate)) {
//                String status =tableQuery.getCollection().equals("approved")?customerDAL.findByCustomerId(tableQuery.getPrimaryKey()).getCustomer().getStatus():customerDAL.findUnapprovedByCustomerId(tableQuery.getPrimaryKey()).getCustomer().getStatus();
                //superKey
                String tenantId = tableQuery.getQueryFields().get("tenantId").toString();
                String customerId= tableQuery.getQueryFields().get("customerId").toString();
                boolean approvalRequired = approvalRequirementOfTenant(tenantId,customerId,language);

                String status = customerDAL.findByTenantIdCustomerId(tenantId,customerId).getCustomer().getStatus();
                if (status.equals("active")) {
                    newReturnObject.addError(utilityService.singleError("MCU011", language));//error code same status
                } else {
                    newReturnObject = customerDAL.customerStatusChanger(tableQuery, headerUserId, newReturnObject, language,"active",approvalRequired);
                }
            }
        } catch (Exception ex) {
            newReturnObject.addError(utilityService.singleError("MCU002", language));
            logger.error("Exception in CustomerController.reActivateCustomer", newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/customeridsregx")
    public String listOfAllCustomerIdsByRegx(@RequestBody String keyword,@RequestAttribute(value = "tenantId") String tenantId,@RequestAttribute(value = "userid") String headerUserId){
       NewReturnObject newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, newReturnObject, "MCU001", language, FnOptConfig.fIdCustomerController, FnOptConfig.opFView)) {
                customerDAL.listOfAllCustomerIdsByRegx(keyword,tenantId,newReturnObject,language);
            }
        } catch (Exception ex) {
            newReturnObject.addError(utilityService.singleError("MCU002", language));
            logger.error("Exception in CustomerController.reActivateCustomer", newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }

    private boolean approvalRequirementOfTenant(String tenantId,String customerId, String language) {
        try {
            CustomerDB customerDB = customerDAL.findByTenantIdCustomerId(tenantId,customerId);
            TenantDB tenantDB = null;
            if (customerDB == null) {
                newReturnObject.addError(utilityService.singleError("MCU006", language));
            } else {
                tenantDB = tenantDAL.findApprovalRequiredTenantId(customerDB.getCustomer().getTenantId());
            }
            if (tenantDB == null) {
                newReturnObject.addError(utilityService.singleError("MCU006", language));
            } else {
                return tenantDB.getTenant().getFourEyeRequired();
            }
            return newReturnObject.getAppErrors().size() > 0 ? true : tenantDB.getTenant().getFourEyeRequired();
        }catch  (Exception ex) {
            newReturnObject.addError(utilityService.singleError("MCU002", language));
            logger.error(ex.getMessage());
            return true;
        }
    }

    private boolean validateCustomer(Customer customer, String userTenantID, String language) {
        if((!customer.getTenantId().isEmpty())&&(!customer.getCustomerId().isEmpty())){
            if(tenantDAL.countByTenantId(customer.getTenantId())==0) {//!customer.getTenantId().equalsIgnoreCase(tenantDAL.findByTenantId(customer.getTenantId()).getTenant().getTenantId())
                //error invalid tenantID.
                newReturnObject.addError(utilityService.singleError("MCU013",language));
            }
            if(!customer.getTenantId().equals(userTenantID)){
//                error tenant id must match
                newReturnObject.addError(utilityService.singleError("MCU013",language));
            }
            customer.setOpeningDate(new PayHubDate(customer.getOpeningDate().getYear(), customer.getOpeningDate().getMonth(),customer.getOpeningDate().getDay()));
        }else{
            newReturnObject.addError(utilityService.singleError("MCU004",language));
        }
        return newReturnObject.getAppErrors().size()>0?false:true;
    }
}
