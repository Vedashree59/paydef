package com.dd.payhub.paydefinition.controller;

import com.dd.payhub.paycommon.model.Country;
import com.dd.payhub.paycommon.model.Country;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.config.FnOptConfig;
import com.dd.payhub.paydefinition.dal.CountryDAL;
import com.dd.payhub.paydefinition.entity.CountryDB;
import com.dd.payhub.paydefinition.entity.CountryUnapprovedDB;
import com.dd.payhub.paydefinition.service.PHUtilityService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin(origins = "*", allowedHeaders = "Content-Type, Accept, reconnection, User-Token")
@RestController
@RequestMapping("/country")
public class CountryController {

        private static final Logger logger = LogManager.getLogger(CountryController.class);
        private CountryDAL countryDAL;
        private PHUtilityService utilityService;
        private NewReturnObject newReturnObject;
    @Autowired
    public CountryController(CountryDAL countryDAL, PHUtilityService utilityService) {
        this.countryDAL = countryDAL;
        this.utilityService = utilityService;
        newReturnObject=new NewReturnObject();
    }

    @PostMapping("/all")
    public String getAllCountry(@RequestBody ReactTableQuery tableQuery , @RequestAttribute(value = "userid") String headerUserId) {//, , @RequestAttribute(value = "bankid") String headerCustomerId
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        ReactTableRecord tableRecord = new ReactTableRecord();

        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdCountryController, FnOptConfig.opFView)) {
                tableRecord = countryDAL.getAllCountryByPagination(tableQuery);
                newReturnObject.PerformReturnObject(tableRecord);
            }
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValueAsString(tableRecord);
            return newReturnObject.setReturnJSON();
        } catch (Exception ex) {
            tableRecord.setTotalPage( 0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord( new ArrayList<>());
            newReturnObject.PerformReturnObject(tableRecord);
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCT002", language));//error code
            logger.error("Exception in CountryController.getAllCountry", this.newReturnObject.getAppErrorString(), ex);
            return newReturnObject.setReturnJSON();
        }
    }

    @PostMapping("/add")
    public String newCountry(@RequestBody Country country, @RequestAttribute(value = "userid") String headerUserId) {
        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (validateCountry(country, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdCountryController, FnOptConfig.opFNew)) {//error code.
                //valid data to insert new record.
                long approvedCount = countryDAL.countByCountryCode(country.getCountryCode());
                long unapprovedCount = countryDAL.countByUnapprovedCountryCode(country.getCountryCode());
                if (  approvedCount  == 0 && unapprovedCount == 0) {//record does not exist so insertion can be done.
                    newReturnObject = countryDAL.addCountry(country, headerUserId, newReturnObject, language);//
                } else {//Error record exist duplication not allowed.
                    this.newReturnObject.addError(utilityService.singleError("MCT003", language));
                    this.newReturnObject.setReturncode(0);
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCT002", language));//error code
            logger.error("Exception in CountryController.newCountry", this.newReturnObject.getAppErrorString(), ex);

        }
        return this.newReturnObject.setReturnJSON();
    }

    @PostMapping("/geteditable")
    public String getEditableCountry(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery tableQuery) {

        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdCountryController, FnOptConfig.opFView)) {
                //user has the permission to view
                newReturnObject = countryDAL.getEditableCountry(tableQuery, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MCT002", language));//error code
            logger.error("Exception in CountryController.getEditableCountry", this.newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/update")
    public String updateCountry(@RequestBody Country country, @RequestAttribute(value = "userid") String headerUserId) {

        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            if (validateCountry(country, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdCountryController, FnOptConfig.opFEdit)) {//error code.
                newReturnObject = countryDAL.updateCountry(country, headerUserId, this.newReturnObject, language);
            }

        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCT002", language));//error code
            logger.error("Exception in CountryController.updateCountry", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @PostMapping("/view")
    public String viewCountry(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdCountryController, FnOptConfig.opFView)) {//error code.
                newReturnObject = countryDAL.viewByCountryCode(tableQuery, newReturnObject, language);
            }
        }catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCT002", language));//error code
            logger.error("Exception in CountryController.viewCountry",this.newReturnObject.getAppErrorString(),ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/approve")
    public String approveCountry(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery tableQuery) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdCountryController, FnOptConfig.opFAuth)) {//error code.

                newReturnObject = countryDAL.approveCountry(tableQuery, headerUserId, this.newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCT002", language));//error code
            logger.error("Exception in CountryController.approveCountry", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @DeleteMapping("/reject")//api to delete the record from the tm_event_definition
    // _unapproved.
    public String rejectCountry(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdCountryController, FnOptConfig.opFDelete)) {//error code.

                newReturnObject = countryDAL.rejectByCountryCode(tableQuery, headerUserId, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCT002", language));//error code
            logger.error("Exception in CountryController.rejectCountry", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();

    }

    @DeleteMapping("/delete")
    public String inActivateCountry(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdCountryController, FnOptConfig.opFDelete)) {//error code.
                String status = countryDAL.findByCountryCode(tableQuery.getPrimaryKey()).getCountry().getStatus();
                if (status.equals("inactive")) {
                    this.newReturnObject.addError(utilityService.singleError("MCT012", language));//error code
                } else {
                    newReturnObject = countryDAL.countryStatusChanger(tableQuery, headerUserId, newReturnObject, language, "inactive");
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCT002", language));
            logger.error("Exception in CountryController.inActivate", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();

    }

    @PostMapping("/activate")
    public String reActivateCountry(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            String collection=tableQuery.getCollection();
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdCountryController, FnOptConfig.opFReactivate)) {
                String status =collection.equals("approved")?countryDAL.findByCountryCode(tableQuery.getPrimaryKey()).getCountry().getStatus():countryDAL.findUnapprovedByCountryCode(tableQuery.getPrimaryKey()).getCountry().getStatus();
                if (status.equals("active")) {
                    this.newReturnObject.addError(utilityService.singleError("MCT011", language));//error code
                } else {
                    newReturnObject = countryDAL.countryStatusChanger(tableQuery, headerUserId, newReturnObject, language, "active");
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCT002", language));//error code
            logger.error("Exception in CountryController.reActivate", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();

    }

    @PostMapping("/countrynamesregx")
    public String listOFCountries(@RequestAttribute(value = "userid") String headerUserId,@RequestBody String keyword){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try{
            newReturnObject= countryDAL.listOfAllCountriesCodeAndNamesByRegx(keyword,newReturnObject,language);
        }catch (Exception ex) {
            logger.error("Exception in TenantController.listOFCountries", this.newReturnObject.getAppErrorString(), ex);
            newReturnObject.PerformReturnObject(new ArrayList<>());
            newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MTN002", language));//error code

        }
        return newReturnObject.setReturnJSON();
    }


        private boolean validateCountry(Country country, String language) {

        //mandatory fields.
        try {
            if ((!country.getCountryCode().isEmpty()) && (!country.getCountryName().isEmpty()) && (!country.getCountryShortName().isEmpty()) ) {
               if(!(country.getCountryShortName().length()<=20))
               {
                   this.newReturnObject.addError(utilityService.singleError("MCT013", language));//error code
               }
               if (!(country.getCountryCode().length()==2))
               {
                   this.newReturnObject.addError(utilityService.singleError("MCT014", language));//error code
               }

            } else {
                this.newReturnObject.addError(utilityService.singleError("MCT004", language));//error code

            }
            return this.newReturnObject.getAppErrors().size()>0?false:true;
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MCT002", language));//error code
            return false;
        }



    }

    }

