package com.dd.payhub.paydefinition.controller;

import com.dd.payhub.paycommon.model.Customer;
import com.dd.payhub.paycommon.model.FeeDefinition;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.config.FnOptConfig;
import com.dd.payhub.paydefinition.dal.CurrencyDAL;
import com.dd.payhub.paydefinition.dal.FeeDefinitionDAL;
import com.dd.payhub.paydefinition.dal.TenantDAL;
import com.dd.payhub.paydefinition.entity.CustomerDB;
import com.dd.payhub.paydefinition.entity.FeeDefinitionDB;
import com.dd.payhub.paydefinition.entity.TenantDB;
import com.dd.payhub.paydefinition.service.PHUtilityService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.ListIterator;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/feedefinition")
public class FeeDefinitionController {
    private static final Logger logger = LogManager.getLogger(FeeDefinitionController.class);
    private FeeDefinitionDAL feeDefinitionDAL;
    private PHUtilityService utilityService;
    private TenantDAL tenantDAL;
    private NewReturnObject newReturnObject;
    private CurrencyDAL currencyDAL;

    @Autowired
    public FeeDefinitionController(FeeDefinitionDAL pFeeDefinitionDAL, PHUtilityService pUtilityService,TenantDAL pTenantDAL,CurrencyDAL pCurrencyDAL) {
        this.feeDefinitionDAL = pFeeDefinitionDAL;
        this.utilityService = pUtilityService;
        this.tenantDAL=pTenantDAL;
        this.currencyDAL=pCurrencyDAL;
        newReturnObject=new NewReturnObject();
    }


    @PostMapping("/all")
    public String getAllFeeDefinition(@RequestBody ReactTableQuery tableQuery , @RequestAttribute(value = "userid") String headerUserId, @RequestAttribute(value = "tenantId") String tenantId) {//, , @RequestAttribute(value = "bankid") String headerCustomerId
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        newReturnObject = new NewReturnObject(0, "\"\"");
        ReactTableRecord tableRecord = new ReactTableRecord();

        try {
            if (utilityService.creatorHasPermission(headerUserId, newReturnObject, "MFD001", language, FnOptConfig.fIdFeeDefinitionController, FnOptConfig.opFView)) {
                tableRecord = feeDefinitionDAL.getAllFeeDefinitionByPagination(tableQuery);
                newReturnObject.PerformReturnObject(tableRecord);
            }
            return newReturnObject.setReturnJSON();
        } catch (Exception ex) {
            tableRecord.setTotalPage( 0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord( new ArrayList<>());
            newReturnObject.PerformReturnObject(tableRecord);
            newReturnObject.setReturncode(0);
            newReturnObject.addError(utilityService.singleError("MFD002", language));//error code
            logger.error("Exception in FeeDefinitionController.getAllFeeDefinition", newReturnObject.getAppErrorString(), ex.getMessage());
            return newReturnObject.setReturnJSON();
        }
    }
    @PostMapping("/add")
    public String newFeeDefinition(@RequestBody FeeDefinition feeDefinition, @RequestAttribute(value = "tenantId") String bankId, @RequestAttribute(value = "userid") String headerUserId) {
        //validation.
        newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (validateFeeDefinition(feeDefinition, language) && utilityService.creatorHasPermission(headerUserId, newReturnObject, "MFD001", language, FnOptConfig.fIdCustomerController, FnOptConfig.opFNew, bankId)) {//error code.
                //valid data to insert new record.
                long approvedCount = feeDefinitionDAL.countByFeeIdTenantId(feeDefinition.getFeeId(),feeDefinition.getTenantId());
                long unapprovedCount = feeDefinitionDAL.countByUnapprovedFeeIdTenantId(feeDefinition.getFeeId(),feeDefinition.getTenantId());
                if (approvedCount == 0 && unapprovedCount == 0) {//record does not exist so insertion can be done.
                    Boolean approvalRequired = tenantDAL.findApprovalRequiredTenantId(feeDefinition.getTenantId()).getTenant().getFourEyeRequired();
                    newReturnObject = feeDefinitionDAL.addFeeDefinition(feeDefinition, headerUserId, newReturnObject, language, approvalRequired);//
                }else {//Error record exist duplication not allowed.
                   newReturnObject.addError(utilityService.singleError("MFD003", language));
                   newReturnObject.setReturncode(0);
                }
            }
        } catch (Exception ex) {
            newReturnObject.setReturncode(0);
            newReturnObject.addError(utilityService.singleError("MFD002", language));//error code
            logger.error("Exception in FeeDefinitionController.newFeeDefinition", newReturnObject.getAppErrorString(), ex.getMessage());
        }
        return newReturnObject.setReturnJSON();
    }


    @PostMapping("/geteditable")
    public String getEditableFeeDefinition(@RequestAttribute(value = "userid") String headerUserId,@RequestAttribute(value = "tenantId") String bankId, @RequestBody ReactTableQuery tableQuery) {

        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MCT001", language, FnOptConfig.fIdFeeDefinitionController, FnOptConfig.opFView,bankId)) {
                //user has the permission to view
                newReturnObject = feeDefinitionDAL.getEditableFeeDefinition(tableQuery, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MFD002", language));//error code
            logger.error("Exception in FeeDefinitionController.getEditableFeeDefinition", this.newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/update")
    public String updateFeeDefinition(@RequestBody FeeDefinition feeDefinition, @RequestAttribute(value = "tenantId") String bankId, @RequestAttribute(value = "userid") String headerUserId) {

        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (validateFeeDefinition(feeDefinition, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MFD001", language, FnOptConfig.fIdFeeDefinitionController, FnOptConfig.opFEdit,bankId)) {//error code.
                Boolean approvalRequired=tenantDAL.findApprovalRequiredTenantId(feeDefinition.getTenantId()).getTenant().getFourEyeRequired();
                newReturnObject = feeDefinitionDAL.updateFeeDefinition(feeDefinition, headerUserId, this.newReturnObject, language,approvalRequired);
            }

        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MFD002", language));//error code
            logger.error("Exception in FeeDefinitionController.updateFeeDefinition", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();

    }

    @PostMapping("/view")
    public String viewFeeDefinition(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "tenantId") String bankId,@RequestAttribute(value = "userid") String headerUserId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MFD001", language, FnOptConfig.fIdFeeDefinitionController, FnOptConfig.opFView,bankId)) {//error code.

                newReturnObject = feeDefinitionDAL.viewByFeeIdTenantId(tableQuery, newReturnObject, language);
            }
        }catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MFD002", language));//error code
            logger.error("Exception in FeeDefinitionController.viewFeeDefinition",this.newReturnObject.getAppErrorString(),ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/approve")
    public String approveFeeDefinition(@RequestAttribute(value = "userid") String headerUserId,@RequestAttribute(value = "tenantId") String bankId, @RequestBody ReactTableQuery tableQuery) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MFD001", language, FnOptConfig.fIdFeeDefinitionController, FnOptConfig.opFAuth,bankId)) {//error code.

                newReturnObject = feeDefinitionDAL.approveFeeDefinition(tableQuery, headerUserId, this.newReturnObject, language);
            }

        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MFD002", language));//error code
            logger.error("Exception in FeeDefinitionController.approveFeeDefinition", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @DeleteMapping("/reject")//api to delete the record from the tm_event_definition
    public String rejectFeeDefinition(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MFD001", language, FnOptConfig.fIdFeeDefinitionController, FnOptConfig.opFDelete)) {//error code.
                newReturnObject = feeDefinitionDAL.rejectByFeeIdTenantId(tableQuery, headerUserId, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MFD002", language));//error code
            logger.error("Exception in FeeDefinitionController.rejectFeeDefinition", this.newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }


    @DeleteMapping("/delete")
    public String inActivateCustomer(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        NewReturnObject newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, newReturnObject, "MFD001", language, FnOptConfig.fIdCustomerController, FnOptConfig.opFDelete)) {//error code.
                String feeId = tableQuery.getQueryFields().get("feeId").toString();
                String tenantId=tableQuery.getQueryFields().get("tenantId").toString();
                boolean approvalRequired = approvalRequirementOfTenant(tableQuery,language);
                String status = feeDefinitionDAL.findByFeeIdTenantId(feeId,tenantId).getFeeDefinition().getStatus();
                if (status.equals("inactive")) {
                    newReturnObject.addError(utilityService.singleError("MFD012", language));//error code same status
                } else {
                    newReturnObject = feeDefinitionDAL.feeDefinitionStatusChanger(tableQuery, headerUserId, newReturnObject, language,"inactive",approvalRequired);
                }
            }
        } catch (Exception ex) {
            newReturnObject.addError(utilityService.singleError("MFD002", language));//error code
            logger.error("Exception in CustomerController.inActivateCustomer", newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/activate")
    public String reActivateCustomer(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        NewReturnObject newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, newReturnObject, "MFD001", language, FnOptConfig.fIdCustomerController, FnOptConfig.opFReactivate)) {
                String feeId = tableQuery.getQueryFields().get("feeId").toString();
                String tenantId=tableQuery.getQueryFields().get("tenantId").toString();
                boolean approvalRequired = approvalRequirementOfTenant(tableQuery,language);
//                String status =tableQuery.getCollection().equals("approved")?feeDefinitionDAL.findByCustomerId(tableQuery.getPrimaryKey()).getCustomer().getStatus():feeDefinitionDAL.findUnapprovedByCustomerId(tableQuery.getPrimaryKey()).getCustomer().getStatus();
                String status = feeDefinitionDAL.findByFeeIdTenantId(feeId,tenantId).getFeeDefinition().getStatus();
                if (status.equals("active")) {
                    newReturnObject.addError(utilityService.singleError("MFD011", language));//error code same status
                } else {
                    newReturnObject = feeDefinitionDAL.feeDefinitionStatusChanger(tableQuery, headerUserId, newReturnObject, language,"active",approvalRequired);
                }
            }
        } catch (Exception ex) {
            newReturnObject.addError(utilityService.singleError("MFD002", language));
            logger.error("Exception in CustomerController.reActivateCustomer", newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }

    private boolean approvalRequirementOfTenant(ReactTableQuery tableQuery, String language) {
        try {
            String feeId = tableQuery.getQueryFields().get("feeId").toString();
            String tenantId=tableQuery.getQueryFields().get("tenantId").toString();
            FeeDefinitionDB feeDefinitionDB = feeDefinitionDAL.findByFeeIdTenantId(feeId,tenantId);
            TenantDB tenantDB = null;
            if (feeDefinitionDB == null) {
                newReturnObject.addError(utilityService.singleError("MFD006", language));
            } else {
                tenantDB = tenantDAL.findApprovalRequiredTenantId(feeDefinitionDB.getFeeDefinition().getTenantId());
            }
            if (tenantDB == null) {
                newReturnObject.addError(utilityService.singleError("MFD006", language));
            } else {
                return tenantDB.getTenant().getFourEyeRequired();
            }
            return newReturnObject.getAppErrors().size() > 0 ? true : tenantDB.getTenant().getFourEyeRequired();
        }catch  (Exception ex) {
            newReturnObject.addError(utilityService.singleError("MFD002", language));
            logger.error(ex.getMessage());
            return true;
        }
    }


    private boolean validateFeeDefinition(FeeDefinition feeDefinition, String language) {
        if((!feeDefinition.getFeeId().isEmpty())&&(!feeDefinition.getTenantId().isEmpty())&&(!feeDefinition.getFeeType().isEmpty())&&(feeDefinition.getMaxFee()!=null)&&(feeDefinition.getMaxFee()!=null)){
            if(feeDefinition.getFeeId().length()!=12){
                newReturnObject.addError(utilityService.singleError("MFD013",language));
            }
            if(tenantDAL.countByTenantId(feeDefinition.getTenantId())<=0){
                newReturnObject.addError(utilityService.singleError("MFD014",language));
            }
            if (!(feeDefinition.getFeeType().equals("F")||feeDefinition.equals("S")||feeDefinition.equals("T"))){
                newReturnObject.addError(utilityService.singleError("MFD015",language));
            }
            if(feeDefinition.getSlabs().size()>0){
                for(int i=0;i<feeDefinition.getSlabs().size();i++){
                    if(feeDefinition.getSlabs().get(i).getTierHigherAmount().getCurrencyCode().equals(feeDefinition.getSlabs().get(i).getTierLowerAmount().getCurrencyCode())){
                        if(currencyDAL.countByCurrencyCode(feeDefinition.getSlabs().get(i).getTierHigherAmount().getCurrencyCode())<=0){
                            newReturnObject.addError(utilityService.singleError("MFD016",language));
                        }
                    }else {
                        newReturnObject.addError(utilityService.singleError("MFD017",language));
                    }
                }
            }
            if(feeDefinition.getMinFee().getCurrencyCode().equals(feeDefinition.getMaxFee().getCurrencyCode())){//both min and max currency code must match.
                if(currencyDAL.countByCurrencyCode(feeDefinition.getMinFee().getCurrencyCode())<=0){//currency code must match the currency collection.
                    newReturnObject.addError(utilityService.singleError("MFD016",language));
                }
                if(feeDefinition.getMinFee().getAmountValue()<0||feeDefinition.getMaxFee().getAmountValue()<0){//amount cant be negitive.
                    newReturnObject.addError(utilityService.singleError("MFD018",language));
                }
            }else {
                newReturnObject.addError(utilityService.singleError("MFD017",language));
            }
        }else{//mandatory fields are empty.
            newReturnObject.addError(utilityService.singleError("MFD004",language));
        }
        return newReturnObject.getAppErrors().size()>0?false:true;

    }

}
