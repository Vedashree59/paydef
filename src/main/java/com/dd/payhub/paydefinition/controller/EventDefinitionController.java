package com.dd.payhub.paydefinition.controller;

import com.dd.payhub.paycommon.model.EventDefinition;
import com.dd.payhub.paycommon.model.ReactTableQuery;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paycommon.view.ReactTableRecord;
import com.dd.payhub.paydefinition.config.FnOptConfig;
import com.dd.payhub.paydefinition.dal.EventDefinitionDAL;
import com.dd.payhub.paydefinition.service.PHUtilityService;
<<<<<<< HEAD
=======
import com.fasterxml.jackson.databind.ObjectMapper;
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/eventdefinition")

public class EventDefinitionController {
    private static final Logger logger = LogManager.getLogger(EventDefinitionController.class);
    private EventDefinitionDAL eventDefinitionDAL;
    private PHUtilityService utilityService;

    private NewReturnObject newReturnObject;

    @Autowired
    public EventDefinitionController(EventDefinitionDAL eventDefinitionDAL, PHUtilityService utilityService) {
        this.eventDefinitionDAL = eventDefinitionDAL;
        this.utilityService = utilityService;
        this.newReturnObject = new NewReturnObject();
    }

    @PostMapping("/all")
    public String getAllEventDefinition(@RequestBody ReactTableQuery tableQuery , @RequestAttribute(value = "userid") String headerUserId) {//, , @RequestAttribute(value = "bankid") String headerCustomerId
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        ReactTableRecord tableRecord = new ReactTableRecord();

        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MTN001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFView)) {
                tableRecord = eventDefinitionDAL.getAllEventDefinitionByPagination(tableQuery);
                newReturnObject.PerformReturnObject(tableRecord);
            }
<<<<<<< HEAD
=======
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValueAsString(tableRecord);
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            return newReturnObject.setReturnJSON();
        } catch (Exception ex) {
            tableRecord.setTotalPage( 0);
            tableRecord.setTotalRecord(0);
            tableRecord.setTableRecord( new ArrayList<>());
            newReturnObject.PerformReturnObject(tableRecord);
            this.newReturnObject.setReturncode(0);
<<<<<<< HEAD
            this.newReturnObject.addError(utilityService.singleError("MED002", language));//error code
=======
            this.newReturnObject.addError(utilityService.singleError("MTN002", language));//error code
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            logger.error("Exception in EventDefinitionController.getAllEventDefinition", this.newReturnObject.getAppErrorString(), ex);
            return newReturnObject.setReturnJSON();
        }
    }

    @PostMapping("/add")
    public String newEventDefinition(@RequestBody EventDefinition eventDefinition, @RequestAttribute(value = "userid") String headerUserId) {
        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (validateEventDefinition(eventDefinition, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MTN001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFNew)) {//error code.
                //valid data to insert new record.
                long approvedCount = eventDefinitionDAL.countByEventId(eventDefinition.getEventId());
                long unapprovedCount = eventDefinitionDAL.countByUnapprovedEventId(eventDefinition.getEventId());
                if (  approvedCount  == 0 && unapprovedCount == 0) {//record does not exist so insertion can be done.
                    newReturnObject = eventDefinitionDAL.addEventDefinition(eventDefinition, headerUserId, newReturnObject, language);//
                } else {//Error record exist duplication not allowed.
<<<<<<< HEAD
                    this.newReturnObject.addError(utilityService.singleError("MED003", language));
=======
                    this.newReturnObject.addError(utilityService.singleError("MTN003", language));
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                    this.newReturnObject.setReturncode(0);
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
<<<<<<< HEAD
            this.newReturnObject.addError(utilityService.singleError("MED002", language));//error code
=======
            this.newReturnObject.addError(utilityService.singleError("MTN002", language));//error code
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            logger.error("Exception in EventDefinitionController.newEventDefinition", this.newReturnObject.getAppErrorString(), ex);

        }
        return this.newReturnObject.setReturnJSON();
    }

    @PostMapping("/geteditable")
    public String getEditableEventDefinition(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery tableQuery) {

        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MED001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFView)) {
                //user has the permission to view
                newReturnObject = eventDefinitionDAL.getEditableEventDefinition(tableQuery, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.setReturncode(0);
            this.newReturnObject.addError(utilityService.singleError("MED002", language));//error code
            logger.error("Exception in EventDefinitionController.getEditableEventDefinition", this.newReturnObject.getAppErrorString(), ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/update")
    public String updateEventDefinition(@RequestBody EventDefinition eventDefinition, @RequestAttribute(value = "userid") String headerUserId) {

        //validation.
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            //change for token in future.
            if (validateEventDefinition(eventDefinition, language) && utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MED001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFEdit)) {//error code.
                newReturnObject = eventDefinitionDAL.updateEventDefinition(eventDefinition, headerUserId, this.newReturnObject, language);
            }

        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MED002", language));//error code
            logger.error("Exception in EventDefinitionController.updateEventDefinition", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @PostMapping("/view")
    public String viewEventDefinition(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MED001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFView)) {//error code.
                newReturnObject = eventDefinitionDAL.viewByEventId(tableQuery, newReturnObject, language);
            }
        }catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MED002", language));//error code
            logger.error("Exception in EventDefinitionController.viewEventDefinition",this.newReturnObject.getAppErrorString(),ex);
        }
        return newReturnObject.setReturnJSON();
    }

    @PostMapping("/approve")
    public String approveEventDefinition(@RequestAttribute(value = "userid") String headerUserId, @RequestBody ReactTableQuery tableQuery) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MED001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFAuth)) {//error code.

                newReturnObject = eventDefinitionDAL.approveEventDefinition(tableQuery, headerUserId, this.newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MED002", language));//error code
            logger.error("Exception in EventDefinitionController.approveEventDefinition", this.newReturnObject.getAppErrorString(), ex);
        }
        return this.newReturnObject.setReturnJSON();
    }

    @DeleteMapping("/reject")//api to delete the record from the tm_event_definition
    // _unapproved.
    public String rejectEventDefinition(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId) {
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MED001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFDelete)) {//error code.

                newReturnObject = eventDefinitionDAL.rejectByEventId(tableQuery, headerUserId, newReturnObject, language);
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MED002", language));//error code
            logger.error("Exception in EventDefinitionController.rejectEventDefinition", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();

    }

    @DeleteMapping("/delete")
        public String inActivateEventDefinition(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {

            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MED001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFDelete)) {//error code.
                    String status = eventDefinitionDAL.findByEventId(tableQuery.getPrimaryKey()).getEventDefinition().getStatus();
<<<<<<< HEAD
                if (status.equals("inactive")) {
=======
                if (status == "inactive") {
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                    this.newReturnObject.addError(utilityService.singleError("MED012", language));//error code
                } else {
                    newReturnObject = eventDefinitionDAL.eventDefinitionStatusChanger(tableQuery, headerUserId, newReturnObject, language, "inactive");
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MED002", language));
            logger.error("Exception in EventDefinitionController.inActivate", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();
<<<<<<< HEAD
=======

>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
    }

    @PostMapping("/activate")
    public String reActivateEventDefinition(@RequestBody ReactTableQuery tableQuery, @RequestAttribute(value = "userid") String headerUserId){
        this.newReturnObject = new NewReturnObject(0, "\"\"");
        String language = utilityService.getLanguage(LocaleContextHolder.getLocale());
        try {
            String collection=tableQuery.getCollection();
            if (utilityService.creatorHasPermission(headerUserId, this.newReturnObject, "MED001", language, FnOptConfig.fIdEventDefinitionController, FnOptConfig.opFReactivate)) {
                String status =collection.equals("approved")?eventDefinitionDAL.findByEventId(tableQuery.getPrimaryKey()).getEventDefinition().getStatus():eventDefinitionDAL.findUnapprovedByEventId(tableQuery.getPrimaryKey()).getEventDefinition().getStatus();
                if (status.equals("active")) {
<<<<<<< HEAD
                    this.newReturnObject.addError(utilityService.singleError("MED011", language));//error code
=======
                    this.newReturnObject.addError(utilityService.singleError("MED012", language));//error code
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
                } else {
                    newReturnObject = eventDefinitionDAL.eventDefinitionStatusChanger(tableQuery, headerUserId, newReturnObject, language, "active");
                }
            }
        } catch (Exception ex) {
            this.newReturnObject.addError(utilityService.singleError("MED002", language));//error code
            logger.error("Exception in EventDefinitionController.reActivate", this.newReturnObject.getAppErrorString(), ex);

        }
        return newReturnObject.setReturnJSON();

    }

<<<<<<< HEAD
    private boolean validateEventDefinition(EventDefinition eventDefinition, String language) {
=======
    private boolean validateEventDefinition( EventDefinition eventDefinition, String language) {
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

        if(eventDefinition.getEventId().isEmpty()||eventDefinition.getStatus().isEmpty()||eventDefinition.getEventDescription().isEmpty()||eventDefinition.getEventTrigger().isEmpty()){
            //mandatory fields are empty.
            this.newReturnObject.addError(utilityService.singleError("MED004", language));
        }
        else{
            //validation of fields.
            if(!(eventDefinition.getEventTrigger().equals("U")||eventDefinition.getEventTrigger().equals("M")||eventDefinition.getEventTrigger().equals("S"))){
<<<<<<< HEAD
                this.newReturnObject.addError(utilityService.singleError("MED013", language));//error code
=======
                this.newReturnObject.addError(utilityService.singleError("MED011", language));//error code
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
            }
        }
        return this.newReturnObject.getAppErrors().size()>0?false:true;
    }
}
