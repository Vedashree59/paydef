package com.dd.payhub.paydefinition.config;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.security.Key;

public class FnOptConfig {
    //token verification
    public static final Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
    //operationIDs
    public static final String opFNew = "pNew";
    public static final String opFEdit = "pEdit";
    public static final String opFDelete = "pDelete";
    public static final String opFAuth = "pAuth";
    public static final String opFCancel = "pCancle";
    public static final String opFReverse = "pReverse";
    public static final String opFHold = "pHold";
    public static final String opFView = "pView";
    public static final String opFAdd = "pAdd";
    public static final String opFReactivate = "pReactivate";

    //functionIDs
<<<<<<< HEAD
    public static final String fIdTenantController = "MBANKID";
    public static final String fIdBanksController = "MBNKLST";
    public static final String fIdCountryCurrencyController = "MCONCCY";
    public static final String fIdCustomerController = "MCUSTID";
    public static final String fIdFunctionListController = "MFNLIST";
    public static final String fIdRoleFunctionController = "MROLEFN";
    public static final String fIdUserController = "MUSRCTR";
    public static final String fIdCountryController = "MCONCNT";
    public static final String fIdCurrencyController = "MCURNCY";
    public static final String fIdCountryHolidaysController = "MEVNTDF";
    public static final String fIdCurrencyHolidaysController = "MBNKLST";
    public static final String fIdCurrencyCountryHolidaysController = "MCUCCYH";
    public static final String fIdEventDefinitionController = "MEVNTDF";
    public static final String fIdProductDefinitionController="MEVNTDF";
    public static final String fIdCurrencyPairController="MEVNTDF";
    public static final String fIdFeeDefinitionController="MEVNTDF";
    public static final String fIdAlternateIdController = "MEVNTDF";
    //payin function IDS
    public static final String fIdInBoundController = "TINBCTR";
    public static final String fIdOutBoundController = "TOUTCTR";
    public static final String fIdCoreController = "TCORCTR";
=======
    public static final String fIdTenantController ="MBANKID";
    public static final String fIdBanksController ="MBNKLST";
    public static final String fIdCountryCurrencyController="MCONCCY";
    public static final String fIdCustomerController="MCUSTID";
    public static final String fIdfunctionListController="MFNLIST";
    public static final String fIdRoleFunctionController="MROLEFN";
    public static final String fIdUserController = "MUSRCTR";
    public static final String fIdCountryController = "MCNTRY";
    public static final String fIdCurrencyController="MCRNCY";
    public static final String fIdCountryHolidaysController="MCRNCY";
    public static final String fIdCurrencyHolidaysController="MEVNTDF";
    public static final String fIdCurrencyRulesController="MBNKLST";
    public static final String fIdCurrencyPairController="MBNKLST";
    public static final String fIdCurrencyCountryHolidaysController="MBNKLST";
    public static final String fIdEventDefinitionController="MBNKLST";
    public static final String fIdProductDefinitionController="MEVNTDF";
    public static final String opFReactivate="MBNKLST";
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

}
