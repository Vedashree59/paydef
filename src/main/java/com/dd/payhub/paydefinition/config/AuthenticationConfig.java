//package com.dd.payhub.paydefinition.config;
//
//import com.dd.payhub.paydefinition.service.AuthorizationInterceptorService;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.CorsRegistry;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//@Configuration
//@EnableWebMvc
//public class AuthenticationConfig implements WebMvcConfigurer {
//
//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/**")
//                .allowedMethods("GET, OPTIONS, HEAD, PUT, POST, DELETE").exposedHeaders("reconnection","User-Token");
//    }
//
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(new AuthorizationInterceptorService()).addPathPatterns("/**").excludePathPatterns("/user/login", "/user/verifyUser","/swagger-ui.html");//
//    }
//}
