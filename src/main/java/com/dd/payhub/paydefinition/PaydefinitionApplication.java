package com.dd.payhub.paydefinition;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class PaydefinitionApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaydefinitionApplication.class, args);
	}

}
