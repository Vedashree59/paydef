package com.dd.payhub.paydefinition.entity;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.CurrencyCountryHolidays;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;


@Document(collection = "tm_currency_country_holidays")
public class CurrencyCountryHolidaysDB {
    @Id
    private String _id;
    private CurrencyCountryHolidays currencyCountryHolidays;
<<<<<<< HEAD
    private List<Audit> auditInfo;
=======
    private List<Audit> auditInfo =new ArrayList<>();
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
    private double sortDate;

    //private String version;
    //private String status;


    public CurrencyCountryHolidaysDB() {
<<<<<<< HEAD
        this.sortDate=0;
        this.auditInfo =new ArrayList<>();
=======
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
    }

    public CurrencyCountryHolidaysDB(CurrencyCountryHolidaysUnapprovedDB unApprovedResult) {
        this._id = unApprovedResult.get_id();
        this.currencyCountryHolidays=unApprovedResult.getCurrencyCountryHolidays();
        this.auditInfo = unApprovedResult.getAuditInfo();
        this.sortDate = unApprovedResult.getSortDate();
        // this.HolidayList = holidayList;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public CurrencyCountryHolidays getCurrencyCountryHolidays() {
        return currencyCountryHolidays;
    }

    public void setCurrencyCountryHolidays(CurrencyCountryHolidays currencyCountryHolidays) {
        this.currencyCountryHolidays = currencyCountryHolidays;
    }

    public List<Audit> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<Audit> auditInfo) {
        this.auditInfo = auditInfo;
    }

    public double getSortDate() {
        return sortDate;
    }

    public void setSortDate(double sortDate) {
        this.sortDate = sortDate;
    }

    public void addAuditInfo(Audit audit) {
        this.auditInfo.add(audit);
    }
}


