package com.dd.payhub.paydefinition.entity;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.Country;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "tm_country")
public class CountryDB {
    @Id
    private String _id;
    private Country country;
<<<<<<< HEAD
    private List<Audit> auditInfo ;
=======
    private List<Audit> auditInfo =new ArrayList<>();
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
    private double sortDate;

    public CountryDB(){
        this.sortDate=0;
<<<<<<< HEAD
        this.auditInfo =new ArrayList<>();
=======
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
    }

    public CountryDB(CountryUnapprovedDB unApprovedResult) {
        this._id=unApprovedResult.get_id();
        this.country=unApprovedResult.getCountry();
        this.auditInfo=unApprovedResult.getAuditInfo();
        this.sortDate=unApprovedResult.getSortDate();
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public List<Audit> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<Audit> auditInfo) {
        this.auditInfo = auditInfo;
    }

    public void addAuditInfo(Audit audit) {
        this.auditInfo.add(audit);
    }

    public double getSortDate() {
        return sortDate;
    }

    public void setSortDate(double sortDate) {
        this.sortDate = sortDate;
    }
}
