package com.dd.payhub.paydefinition.entity;


import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.Country;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "tm_country_unapproved")
public class CountryUnapprovedDB {
    @Id
    private String _id;
    private Country country;
<<<<<<< HEAD
    private List<Audit> auditInfo ;
    private double sortDate;

    public CountryUnapprovedDB() {
        this.sortDate=0;
        this.auditInfo =new ArrayList<>();
=======
    private List<Audit> auditInfo = new ArrayList<>();
    private double sortDate;

    public CountryUnapprovedDB() {
        this.sortDate = 0;
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
    }

    public CountryUnapprovedDB(CountryDB countryDB) {
        this._id = countryDB.get_id();
        this.country = countryDB.getCountry();
        this.auditInfo = countryDB.getAuditInfo();
        this.sortDate = countryDB.getSortDate();
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country countryDBUnapproved) {
        this.country = countryDBUnapproved;
    }

    public List<Audit> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<Audit> auditInfo) {
        this.auditInfo = auditInfo;
    }

    public void addAuditInfo(Audit audit) {
        this.auditInfo.add(audit);
    }

    public double getSortDate() {
        return sortDate;
    }

    public void setSortDate(double sortDate) {
        this.sortDate = sortDate;
    }
}
