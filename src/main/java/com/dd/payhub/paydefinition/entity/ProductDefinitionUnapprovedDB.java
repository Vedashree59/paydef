package com.dd.payhub.paydefinition.entity;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.ProductDefinition;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "tm_product_definition_unapproved")
public class ProductDefinitionUnapprovedDB {

    @Id
    private String _id;
    private ProductDefinition productDefinition;
    private List<Audit> auditInfo=new ArrayList<>();
    private double sortDate;

    public ProductDefinitionUnapprovedDB() {
        this.sortDate=0;
        this.auditInfo=new ArrayList<>();
    }

    public ProductDefinitionUnapprovedDB(ProductDefinitionDB productDefinitionDB){
        this._id = productDefinitionDB.get_id();
        this.productDefinition = productDefinitionDB.getProductDefinition();
        this.auditInfo = productDefinitionDB.getAuditInfo();
        this.sortDate = productDefinitionDB.getSortDate();
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public ProductDefinition getProductDefinition() {
        return productDefinition;
    }

    public void setProductDefinition(ProductDefinition productDefinition) {
        this.productDefinition = productDefinition;
    }

    public List<Audit> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<Audit> auditInfo) {
        this.auditInfo = auditInfo;
    }

    public double getSortDate() {
        return sortDate;
    }

    public void setSortDate(double sortDate) {
        this.sortDate = sortDate;
    }

    public void addAuditInfo(Audit audit) {
        this.auditInfo.add(audit);
    }
}
