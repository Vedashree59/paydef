package com.dd.payhub.paydefinition.entity;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.CurrencyHolidays;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document("tm_currency_holidays")
public class CurrencyHolidaysDB {

    @Id
    private String _id;
    private CurrencyHolidays currencyHolidays;
    private List<Audit> auditInfo =new ArrayList<>();
    private double sortDate;


    public CurrencyHolidaysDB() {
        this.sortDate=0;
        this.auditInfo=new ArrayList<>();
    }

    public CurrencyHolidaysDB(CurrencyHolidaysUnapprovedDB unApprovedResult) {
        this._id=unApprovedResult.get_id();
        this.currencyHolidays= unApprovedResult.getCurrencyHolidays();
        this.auditInfo=unApprovedResult.getAuditInfo();
        this.sortDate = unApprovedResult.getSortDate();
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public CurrencyHolidays getCurrencyHolidays() {
        return currencyHolidays;
    }

    public void setCurrencyHolidays(CurrencyHolidays currencyHolidays) {
        this.currencyHolidays = currencyHolidays;
    }

    public List<Audit> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<Audit> auditInfo) {
        this.auditInfo = auditInfo;
    }

    public void addAuditInfo(Audit audit){
        this.auditInfo.add(audit);
    }

    public double getSortDate() {
        return sortDate;
    }

    public void setSortDate(double sortDate) {
        this.sortDate = sortDate;
    }
}