package com.dd.payhub.paydefinition.entity;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.CurrencyRules;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "tm_currency_rules_unapproved")
public class CurrencyRulesUnapprovedDB {
    @Id
    private String _id;
    private CurrencyRules currencyRules;
    private List<Audit> auditInfo =new ArrayList<>();
    private double sortDate;


    public CurrencyRulesUnapprovedDB() {
    }
    public CurrencyRulesUnapprovedDB(CurrencyRulesDB currencyRules) {
        this._id =currencyRules.get_id();
        this.currencyRules = currencyRules.getCurrencyRules();
        this.auditInfo = currencyRules.getAuditInfo();
        this.sortDate = currencyRules.getSortDate();
    }


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public CurrencyRules getCurrencyRules() {
        return currencyRules;
    }




    public void setCurrencyRules(CurrencyRules currencyRules) {
        this.currencyRules = currencyRules;
    }

    public List<Audit> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<Audit> auditInfo) {
        this.auditInfo = auditInfo;
    }

    public double getSortDate() {
        return sortDate;
    }

    public void setSortDate(double sortDate) {
        this.sortDate = sortDate;
    }
    public void addAuditInfo(Audit audit) {
        this.auditInfo.add(audit);
    }
}
