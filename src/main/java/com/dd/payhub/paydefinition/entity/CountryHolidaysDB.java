package com.dd.payhub.paydefinition.entity;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.CountryHolidays;
import com.dd.payhub.paycommon.model.Holiday;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "tm_country_holidays")
public class CountryHolidaysDB {
    @Id
    private String _id;
    private CountryHolidays countryHolidays;
<<<<<<< HEAD
    private List<Audit> auditInfo;
    private double sortDate;

    public CountryHolidaysDB() {
        this.sortDate=0;
        this.auditInfo =new ArrayList<>();
=======
    private List<Audit> auditInfo =new ArrayList<>();
    private double sortDate;



    public CountryHolidaysDB() {
        this.sortDate=0;
        this.auditInfo=new ArrayList<>();
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
    }

    public CountryHolidaysDB(CountryHolidaysUnapprovedDB unApprovedResult) {
        this._id = unApprovedResult.get_id();
        this.countryHolidays=unApprovedResult.getCountryHolidays();
        this.auditInfo = unApprovedResult.getAuditInfo();
        this.sortDate=unApprovedResult.getSortDate();
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public CountryHolidays getCountryHolidays() {
        return countryHolidays;
    }

    public void setCountryHolidays(CountryHolidays countryHolidays) {
        this.countryHolidays = countryHolidays;
    }

    public List<Audit> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<Audit> auditInfo) {
        this.auditInfo = auditInfo;
    }
    public void addAuditInfo(Audit audit) {
        this.auditInfo.add(audit);
    }

    public double getSortDate() {
        return sortDate;
    }

    public void setSortDate(double sortDate) {
        this.sortDate = sortDate;
    }
}

