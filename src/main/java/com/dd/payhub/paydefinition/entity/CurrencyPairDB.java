package com.dd.payhub.paydefinition.entity;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.CurrencyPair;
import com.dd.payhub.paycommon.model.CurrencyPair;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "tm_currency_Pair")
public class CurrencyPairDB {
    @Id
    private String _id;

    private CurrencyPair currencyPair;
    private List<Audit> auditInfo =new ArrayList<>();
    private double sortDate;

    public CurrencyPairDB() {
        this.sortDate=0;
        this.auditInfo=new ArrayList<>();;
    }

    public CurrencyPairDB(CurrencyPairUnapprovedDB unApprovedResult) {
        this._id = unApprovedResult.get_id();
        this.currencyPair=unApprovedResult.getCurrencyPair();
        this.auditInfo = unApprovedResult.getAuditInfo();
        this.sortDate = unApprovedResult.getSortDate();

    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
    }

    public List<Audit> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<Audit> auditInfo) {
        this.auditInfo = auditInfo;
    }

    public double getSortDate() {
        return sortDate;
    }

    public void setSortDate(double sortDate) {
        this.sortDate = sortDate;
    }

    public void addAuditInfo(Audit audit) {
        this.auditInfo.add(audit);
    }

}
