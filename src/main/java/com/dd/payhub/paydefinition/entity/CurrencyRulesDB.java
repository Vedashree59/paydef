package com.dd.payhub.paydefinition.entity;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.CurrencyRules;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "tm_currency_rules")
public class CurrencyRulesDB {
    @Id
    private String _id;
    private CurrencyRules currencyRules;
    private List<Audit> auditInfo =new ArrayList<>();
    private double sortDate;

    public CurrencyRulesDB() {
    }

    public CurrencyRulesDB(CurrencyRulesUnapprovedDB currencyRluesUnapprovedDB) {
        this._id =currencyRluesUnapprovedDB.get_id();
        this.currencyRules = currencyRluesUnapprovedDB.getCurrencyRules();
        this.auditInfo = currencyRluesUnapprovedDB.getAuditInfo();
        this.sortDate = currencyRluesUnapprovedDB.getSortDate();
    }
    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public CurrencyRules getCurrencyRules() {
        return currencyRules;
    }

    public void setCurrencyRules(CurrencyRules currencyRules) {
        this.currencyRules = currencyRules;
    }

    public List<Audit> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<Audit> auditInfo) {
        this.auditInfo = auditInfo;
    }

    public double getSortDate() {
        return sortDate;
    }

    public void addAuditInfo(Audit audit) {
        this.auditInfo.add(audit);
    }

    public void setSortDate(double sortDate) {
        this.sortDate = sortDate;
    }

}
