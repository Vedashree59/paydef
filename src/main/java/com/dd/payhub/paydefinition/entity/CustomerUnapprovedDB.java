package com.dd.payhub.paydefinition.entity;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.Customer;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "tm_customer_unapproved")
public class CustomerUnapprovedDB {
    private String _id;
    private Customer customer;
    private List<Audit> auditInfo;
    private double sortDate;

    public CustomerUnapprovedDB() {
        this.sortDate=0;
        this.auditInfo =new ArrayList<>();
    }

    public CustomerUnapprovedDB(CustomerDB customerDB){
        _id=customerDB.get_id();
        customer=customerDB.getCustomer();
        auditInfo =customerDB.getAuditInfo();
        sortDate=customerDB.getSortDate();
}
    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Audit> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<Audit> auditInfo) {
        this.auditInfo = auditInfo;
    }

    public void addAuditInfo(Audit audit) {
        this.auditInfo.add(audit);
    }

    public double getSortDate() {
        return sortDate;
    }

    public void setSortDate(double sortDate) {
        this.sortDate = sortDate;
    }
}
