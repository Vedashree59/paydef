package com.dd.payhub.paydefinition.entity;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.Banks;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "tm_banks")
public class BanksDB {
    @Id
    private String _id;
    private Banks bank;
    private List<Audit> auditInfo =new ArrayList<>();
    private double sortDate;

    public double getSortDate() {
        return sortDate;
    }

    public void setSortDate(double sortDate) {
        this.sortDate = sortDate;
    }

    public BanksDB(){}

    public BanksDB(BanksUnapprovedDB unApprovedResult) {
        this._id=unApprovedResult.get_id();
        this.bank =unApprovedResult.getBank();
        this.auditInfo=unApprovedResult.getAuditInfo();
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Banks getBank() {
        return bank;
    }

    public void setBank(Banks bank) {
        this.bank = bank;
    }

    public List<Audit> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<Audit> auditInfo) {
        this.auditInfo = auditInfo;
    }

    public void addAuditInfo(Audit audit){
        this.auditInfo.add(audit);
    }
}
