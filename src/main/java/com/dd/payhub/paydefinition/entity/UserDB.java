package com.dd.payhub.paydefinition.entity;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.User;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.w3c.dom.UserDataHandler;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "tm_user")
public class UserDB {
    @Id
    private String _id;
    private User user;
    private List<Audit> auditInfo;

    public UserDB() {
        auditInfo=new ArrayList<>();
    }


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {this.user = user;}

    public List<Audit> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<Audit> auditInfo) {
        this.auditInfo = auditInfo;
    }
}
