package com.dd.payhub.paydefinition.entity;

import com.dd.payhub.paycommon.model.AlternateId;
import com.dd.payhub.paycommon.model.Audit;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "tm_alternate_id_unapproved")
public class AlternateIdUnapprovedDB {

    @Id
    private String _id;
    private AlternateId alternateId;
    private List<Audit> auditInfo;
    private double sortDate;

    public AlternateIdUnapprovedDB() {
        this.sortDate = 0;
        this.auditInfo=new ArrayList<>();
    }

    public AlternateIdUnapprovedDB(AlternateIdDB alternateIdDB) {
        this._id = alternateIdDB.get_id();
        this.alternateId = alternateIdDB.getAlternateId();
        this.auditInfo = alternateIdDB.getAuditInfo();
        this.sortDate = alternateIdDB.getSortDate();
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public AlternateId getAlternateId() {
        return alternateId;
    }

    public void setAlternateId(AlternateId alternateId) {
        this.alternateId = alternateId;
    }

    public List<Audit> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<Audit> auditInfo) {
        this.auditInfo = auditInfo;
    }

    public double getSortDate() {
        return sortDate;
    }

    public void setSortDate(double sortDate) {
        this.sortDate = sortDate;
    }

    public void addAuditInfo(Audit audit) {
        this.auditInfo.add(audit);
    }
}
