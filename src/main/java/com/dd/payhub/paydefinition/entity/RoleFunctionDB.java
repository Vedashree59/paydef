package com.dd.payhub.paydefinition.entity;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.RoleFunction;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "tm_role_function")
public class RoleFunctionDB {
    @Id
    private String _id;
    private RoleFunction role;
    private List<Audit> auditInfo =new ArrayList<>();
    private double sortDate;
    public RoleFunctionDB(){
        this.sortDate = 0;
        this.auditInfo=new ArrayList<>();
    }

    public RoleFunctionDB(RoleFunctionUnapprovedDB unApprovedResult) {
        this._id=unApprovedResult.get_id();
        this.role=unApprovedResult.getRole();
        this.auditInfo=unApprovedResult.getAuditInfo();
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public RoleFunction getRole() {
        return role;
    }

    public void setRole(RoleFunction role) {
        this.role = role;
    }

    public List<Audit> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<Audit> auditInfo) {
        this.auditInfo = auditInfo;
    }
    public void addAuditInfo(Audit audit) {
        this.auditInfo.add(audit);
    }

    public double getSortDate() {
        return sortDate;
    }

    public void setSortDate(double sortDate) {
        this.sortDate = sortDate;
    }
}
