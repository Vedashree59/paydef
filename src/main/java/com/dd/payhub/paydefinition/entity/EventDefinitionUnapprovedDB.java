package com.dd.payhub.paydefinition.entity;

<<<<<<< HEAD
import com.dd.payhub.paycommon.model.EventDefinition;
import com.dd.payhub.paycommon.model.Audit;
=======
import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.EventDefinition;
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
@Document(collection = "tm_event_definition_unapproved")
public class EventDefinitionUnapprovedDB {
    @Id
    private String _id;
    private EventDefinition eventDefinition;
    private List<Audit> auditInfo;
    private double sortDate;

    public EventDefinitionUnapprovedDB() {
        this.sortDate = 0;
        this.auditInfo=new ArrayList<>();
    }

    public EventDefinitionUnapprovedDB(EventDefinitionDB eventDefinitionDB) {
        this._id = eventDefinitionDB.get_id();
        this.eventDefinition = eventDefinitionDB.getEventDefinition();
        this.auditInfo = eventDefinitionDB.getAuditInfo();
        this.sortDate = eventDefinitionDB.getSortDate();
    }


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public EventDefinition getEventDefinition() {
        return eventDefinition;
    }

    public void setEventDefinition(EventDefinition eventDefinition) {
        this.eventDefinition = eventDefinition;
    }

    public List<Audit> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<Audit> auditInfo) {
        this.auditInfo = auditInfo;
    }

    public double getSortDate() {
        return sortDate;
    }

    public void setSortDate(double sortDate) {
        this.sortDate = sortDate;
    }

    public void addAuditInfo(Audit audit) {this.auditInfo.add(audit);
    }
}
