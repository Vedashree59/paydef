package com.dd.payhub.paydefinition.entity;

import com.dd.payhub.paycommon.model.ErrorCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "tm_error_code")
public class ErrorCodeDB {
    @Id
    private  String _id;
   private ErrorCode error;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public ErrorCode getError() {
        return error;
    }

    public void setError(ErrorCode error) {
        this.error = error;
    }
}
