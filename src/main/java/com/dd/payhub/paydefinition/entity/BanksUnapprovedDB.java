package com.dd.payhub.paydefinition.entity;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.Banks;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "tm_banks_unapproved")
public class BanksUnapprovedDB {

    @Id
    private String _id;
    private Banks bank;
    private List<Audit> auditInfo =new ArrayList<>();
    private double sortDate;

    public double getSortDate() {
        return sortDate;
    }

    public void setSortDate(double sortDate) {
        this.sortDate = sortDate;
    }

    public BanksUnapprovedDB() {
    }

    public BanksUnapprovedDB(BanksDB banksDB){
        this._id = banksDB.get_id();
        this.bank = banksDB.getBank();
        this.auditInfo = banksDB.getAuditInfo();
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Banks getBank() {
        return bank;
    }

    public void setBank(Banks bank) {
        this.bank = bank;
    }

    public List<Audit> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<Audit> auditInfo) {
        this.auditInfo = auditInfo;
    }

    public void addAuditInfo(Audit audit) {
        this.auditInfo.add(audit);
    }
}

