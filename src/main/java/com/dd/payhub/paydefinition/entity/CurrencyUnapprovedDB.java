package com.dd.payhub.paydefinition.entity;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.Currency;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document (collection = "tm_currency_unapproved")
public class CurrencyUnapprovedDB {

    @Id
    private String _id;
    private Currency currency;
<<<<<<< HEAD
    private List<Audit> auditInfo;
    private double sortDate;

    public CurrencyUnapprovedDB() {
        this.sortDate=0;
        this.auditInfo =new ArrayList<>();
    }
=======
    private List<Audit> auditInfo =new ArrayList<>();
    private double sortDate;

    public CurrencyUnapprovedDB() { }
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc

    public CurrencyUnapprovedDB(CurrencyDB currencyDB) {
        this._id = currencyDB.get_id();
        this.currency = currencyDB.getCurrency();
        this.auditInfo =currencyDB.getAuditInfo();
        this.sortDate=currencyDB.getSortDate();

    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public List<Audit> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<Audit> auditInfo) {
        this.auditInfo = auditInfo;
    }

    public void addAuditInfo(Audit audit) {
        this.auditInfo.add(audit);
    }
    public double getSortDate() {
        return sortDate;
    }

    public void setSortDate(double sortDate) {
        this.sortDate = sortDate;
    }
}
