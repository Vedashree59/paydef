package com.dd.payhub.paydefinition.entity;

import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.Tenant;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
@Document(collection = "tm_tenant")
public class TenantDB {
    private String _id;
    private Tenant tenant;
    private List<Audit> auditInfo=new ArrayList<>();
    private double sortDate;

    public TenantDB() {
<<<<<<< HEAD
        this.sortDate = 0;
        this.auditInfo=new ArrayList<>();
=======
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
    }

    public TenantDB(TenantUnapprovedDB tenantUnapprovedDB) {
        _id=tenantUnapprovedDB.get_id();
        tenant=tenantUnapprovedDB.getTenant();
        auditInfo=tenantUnapprovedDB.getAuditInfo();
        this.sortDate=tenantUnapprovedDB.getSortDate();
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    public List<Audit> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<Audit> auditInfo) {
        this.auditInfo = auditInfo;
    }

    public void addAuditInfo(Audit audit) {
        this.auditInfo.add(audit);
    }

    public double getSortDate() {
        return sortDate;
    }

    public void setSortDate(double sortDate) {
        this.sortDate = sortDate;
    }
}
