package com.dd.payhub.paydefinition.entity;

import com.dd.payhub.paycommon.model.Account;
import com.dd.payhub.paycommon.model.Audit;
import com.dd.payhub.paycommon.model.FeeDefinition;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "tm_fee_definition")
public class FeeDefinitionDB {
    @Id
    private String _id;
    private FeeDefinition feeDefinition;
    private List<Audit> auditInfo;
    private double sortDate;


    public FeeDefinitionDB() {
        auditInfo=new ArrayList<Audit>();
        sortDate=0;
    }
    public FeeDefinitionDB(FeeDefinitionUnapprovedDB feeDefinitionUnapproved) {
        _id = feeDefinitionUnapproved.get_id();
        feeDefinition = feeDefinitionUnapproved.getFeeDefinition();
        auditInfo = feeDefinitionUnapproved.getAuditInfo();
        sortDate = feeDefinitionUnapproved.getSortDate();
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public FeeDefinition getFeeDefinition() {
        return feeDefinition;
    }

    public void setFeeDefinition(FeeDefinition feeDefinition) {
        this.feeDefinition = feeDefinition;
    }

    public List<Audit> getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(List<Audit> auditInfo) {
        this.auditInfo = auditInfo;
    }

    public double getSortDate() {
        return sortDate;
    }

    public void setSortDate(double sortDate) {
        this.sortDate = sortDate;
    }

    public void addAuditInfo(Audit audit) {
        this.auditInfo.add(audit);
    }
}
