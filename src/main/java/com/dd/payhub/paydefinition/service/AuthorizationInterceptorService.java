<<<<<<< HEAD
//package com.dd.payhub.paydefinition.service;
//
//import com.dd.payhub.paycommon.view.AppError;
//import com.dd.payhub.paycommon.view.NewReturnObject;
//import com.dd.payhub.paydefinition.controller.UserController;
//import io.jsonwebtoken.Claims;
//import io.jsonwebtoken.Jws;
//import io.jsonwebtoken.Jwts;
//import org.springframework.web.servlet.HandlerInterceptor;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.Calendar;
//import java.util.Date;
//
//public class AuthorizationInterceptorService implements HandlerInterceptor {
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws IOException {
//        try {
//            String userToken = request.getHeader("User-Token");
//            if (userToken==null || userToken.isEmpty()) {
//                NewReturnObject newReturnObject = new NewReturnObject(2, "\"\"");
//                AppError appError = new AppError("SS00", "UserDB authentication failed", "E");
//                newReturnObject.addError(appError);
//                newReturnObject.setReturncode(2);
//                response.addHeader("Access-Control-Allow-Origin", "*");
//                response.addHeader("Access-Control-Allow-Headers", "*");
//                response.addHeader("Access-Control-Allow-Methods","GET, OPTIONS, HEAD, PUT, POST, DELETE");
//                response.getWriter().write(newReturnObject.setReturnJSON());
//                return false;
//            } else {
//                Jws<Claims> jws = Jwts.parser().setSigningKey(UserController.key).parseClaimsJws(userToken);
//                if (jws.getBody().getIssuer().equals(request.getHeader("origin"))) { // in live should be remove not(!) from the if statement
//                    request.setAttribute("userid", jws.getBody().get("userId"));
//                    request.setAttribute("tenantId", jws.getBody().get("tenantId"));
//                    long currentTime = new Date().getTime();
//                    if (jws.getBody().getExpiration().getTime() - currentTime < 1000 * 60 * 10 && jws.getBody().getExpiration().getTime() - currentTime >= 0) {
//                        response.addHeader("User-Token", Jwts.builder().setIssuer(request.getHeader("origin")).setExpiration(new Date(Calendar.getInstance().getTimeInMillis() + (1000 * 60 * 10))).setSubject("authentification").claim("userId", jws.getBody().get("userId")).claim("tenantId", jws.getBody().get("tenantId")).signWith(UserController.key).compact());
//                        response.addHeader("reconnection", "true");
//                    } else {
//                        response.addHeader("User-Token", userToken);
//                        response.addHeader("reconnection", "false");
//                    }
//                    return true;
//                }
//                NewReturnObject newReturnObject = new NewReturnObject(2, "\"\"");
//                AppError appError = new AppError("SS00", "UserDB token wrong", "E");
//                newReturnObject.addError(appError);
//                newReturnObject.setReturncode(2);
//                response.addHeader("Access-Control-Allow-Origin", "*");
//                response.addHeader("Access-Control-Allow-Headers", "*");
//                response.addHeader("Access-Control-Allow-Methods", "GET, OPTIONS, HEAD, PUT, POST, DELETE");
//                response.getWriter().write(newReturnObject.setReturnJSON());
//                return false;
//            }
//        } catch (Exception ex) {
//                 NewReturnObject newReturnObject = new NewReturnObject(2, "\"\"");
//            AppError appError = new AppError("SS00", "UserDB token wrong", "E");
//            newReturnObject.addError(appError);
//            newReturnObject.setReturncode(2);
////            response.addHeader("Access-Control-Allow-Origin", "*");
//            response.addHeader("Access-Control-Allow-Headers", "*");
//            response.addHeader("Access-Control-Allow-Methods","GET, OPTIONS, HEAD, PUT, POST, DELETE");
//            response.getWriter().write(newReturnObject.setReturnJSON());
//            return false;
//        }
//
//
//    }
//}
=======
package com.dd.payhub.paydefinition.service;

import com.dd.payhub.paycommon.view.AppError;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paydefinition.controller.UserController;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

public class AuthorizationInterceptorService implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws IOException {
        try {
            String userToken = request.getHeader("User-Token");
            if (userToken==null || userToken.isEmpty()) {
                NewReturnObject newReturnObject = new NewReturnObject(2, "\"\"");
                AppError appError = new AppError("SS00", "UserDB authentication failed", "E");
                newReturnObject.addError(appError);
                newReturnObject.setReturncode(2);
                response.addHeader("Access-Control-Allow-Origin", "*");
                response.addHeader("Access-Control-Allow-Headers", "*");
                response.addHeader("Access-Control-Allow-Methods","GET, OPTIONS, HEAD, PUT, POST, DELETE");
                response.getWriter().write(newReturnObject.setReturnJSON());
                return false;
            } else {
                Jws<Claims> jws = Jwts.parser().setSigningKey(UserController.key).parseClaimsJws(userToken);
                if (jws.getBody().getIssuer().equals(request.getHeader("origin"))) { // in live should be remove not(!) from the if statement
                    request.setAttribute("userid", jws.getBody().get("userId"));
                    request.setAttribute("tenantId", jws.getBody().get("tenantId"));
                    long currentTime = new Date().getTime();
                    if (jws.getBody().getExpiration().getTime() - currentTime < 1000 * 60 * 10 && jws.getBody().getExpiration().getTime() - currentTime >= 0) {
                        response.addHeader("User-Token", Jwts.builder().setIssuer(request.getHeader("origin")).setExpiration(new Date(Calendar.getInstance().getTimeInMillis() + (1000 * 60 * 10))).setSubject("authentification").claim("userId", jws.getBody().get("userId")).claim("tenantId", jws.getBody().get("tenantId")).signWith(UserController.key).compact());
                        response.addHeader("reconnection", "true");
                    } else {
                        response.addHeader("User-Token", userToken);
                        response.addHeader("reconnection", "false");
                    }
                    return true;
                }
                NewReturnObject newReturnObject = new NewReturnObject(2, "\"\"");
                AppError appError = new AppError("SS00", "UserDB token wrong", "E");
                newReturnObject.addError(appError);
                newReturnObject.setReturncode(2);
                response.addHeader("Access-Control-Allow-Origin", "*");
                response.addHeader("Access-Control-Allow-Headers", "*");
                response.addHeader("Access-Control-Allow-Methods", "GET, OPTIONS, HEAD, PUT, POST, DELETE");
                response.getWriter().write(newReturnObject.setReturnJSON());
                return false;
            }
        } catch (Exception ex) {
                 NewReturnObject newReturnObject = new NewReturnObject(2, "\"\"");
            AppError appError = new AppError("SS00", "UserDB token wrong", "E");
            newReturnObject.addError(appError);
            newReturnObject.setReturncode(2);
//            response.addHeader("Access-Control-Allow-Origin", "*");
            response.addHeader("Access-Control-Allow-Headers", "*");
            response.addHeader("Access-Control-Allow-Methods","GET, OPTIONS, HEAD, PUT, POST, DELETE");
            response.getWriter().write(newReturnObject.setReturnJSON());
            return false;
        }


    }
}
>>>>>>> a490d210cdd395d7cd4ec7f6cbfd88828214bdcc
