package com.dd.payhub.paydefinition.service;


import com.dd.payhub.paycommon.view.AppError;
import com.dd.payhub.paycommon.view.NewReturnObject;
import com.dd.payhub.paydefinition.dal.ErrorCodeDAL;
import com.dd.payhub.paydefinition.dal.UserDAL;
import com.dd.payhub.paydefinition.config.FnOptConfig;
import com.dd.payhub.paydefinition.entity.UserDB;
import com.dd.payhub.paydefinition.model.UserRole;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Locale;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.unwind;
@Repository
public class PHUtilityService {

        private static final Logger logger = LogManager.getLogger(PHUtilityService.class);

        private final MongoTemplate mongoTemplate;
        private ErrorCodeDAL errorCodeDAL;
        private UserDAL userDAL;

    @Autowired
    public PHUtilityService(MongoTemplate mongoTemplate, UserDAL userDAL, ErrorCodeDAL errorCodeDAL) {
        this.mongoTemplate = mongoTemplate;
        this.userDAL = userDAL;
        this.errorCodeDAL = errorCodeDAL;
    }

        //error code with multiple error
        public List<AppError> multipleErrors(String[] errorCode, String language) {
        return errorCodeDAL.findByErrorCodeAndLanguage(errorCode, language);
    }

        //error code with single error
        public AppError singleError(String errorCode, String language) {
        return errorCodeDAL.findByErrorCodeAndLanguage(errorCode, language);
    }

        //check the language from locale object for each request
        public String getLanguage(Locale locale) {
        String language = "en";
        if (locale.getLanguage().length() > 0) {
            language = locale.getLanguage();
        }
        return language;
    }

        // header user / creator validation(exists / not)
        public Boolean creatoridExist(String header, NewReturnObject newReturnObject, String errorCode, String language) {
        Boolean returnVal = true;
        UserDB userDB = userDAL.findByOfficialEmailId(header);
        if (userDB == null) {
            returnVal = false;
            newReturnObject.addError(singleError(errorCode, language));
        }
        return returnVal;
    }

//        //Audit History Insertion
//        public void createAuditHistory(AuditHistoryRepository auditHistoryRepository, String collectionName, String headerUserid, double creationDate, String headerOperationId, String headerAuthId, double approveDate, Object contentObj) {
//        AuditHistory auditHistory = new AuditHistory(collectionName, headerUserid, creationDate, headerAuthId, headerOperationId, approveDate);
//        auditHistory.addContent(contentObj);
//        auditHistoryRepository.insert(auditHistory);
//    }

        public boolean creatorHasPermission(String userid, NewReturnObject newReturnObject, String errorCode, String language, String functionid, String operationid, String bankId) {
        boolean isPermission = false;
        try {

            if (creatoridExist(userid, newReturnObject, errorCode, language)) {
                LookupOperation lookupOperation = LookupOperation.newLookup().
                        from("tm_role_function").
                        localField("user.bankRole").
                        foreignField("roleId").
                        as("user.bankRole");
                AggregationOperation userMatch = Aggregation.match(Criteria.where("user.officialEmailId").is(userid)); //userid/officialEmailId match data
                AggregationOperation functionMatch = Aggregation.match(Criteria.where("user.bankRole.permissions.functionId").is(functionid));// match role_function Id /roleid from role_function table
                AggregationOperation replaceRoot=Aggregation.replaceRoot("user");
                Aggregation aggregation = Aggregation.newAggregation(userMatch, lookupOperation, unwind("user.bankRole"), unwind("user.bankRole.permissions"), functionMatch,replaceRoot);
                List<UserRole> results = mongoTemplate.aggregate(aggregation, "tm_user", UserRole.class).getMappedResults();
                for (int i = 0; i < results.size(); i++) {
                    if (results.get(i).getBankRole().getTenantId().equals(bankId)) {
                        switch (operationid) {
                            case FnOptConfig.opFNew:
                                isPermission = results.get(i).getBankRole().getPermissions().getAction().ispNew();
                                break;
                            case FnOptConfig.opFEdit:
                                isPermission = results.get(i).getBankRole().getPermissions().getAction().ispEdit();
                                break;
                            case FnOptConfig.opFDelete:
                                isPermission = results.get(i).getBankRole().getPermissions().getAction().ispDelete();
                                break;
                            case FnOptConfig.opFAuth:
                                isPermission = results.get(i).getBankRole().getPermissions().getAction().ispAuth();
                                break;
                            case FnOptConfig.opFCancel:
                                isPermission = results.get(i).getBankRole().getPermissions().getAction().ispCancel();
                                break;
                            case FnOptConfig.opFReverse:
                                isPermission = results.get(i).getBankRole().getPermissions().getAction().ispReverse();
                                break;
                            case FnOptConfig.opFHold:
                                isPermission = results.get(i).getBankRole().getPermissions().getAction().ispHold();
                                break;
                            case FnOptConfig.opFView:
                                isPermission = results.get(i).getBankRole().getPermissions().getAction().ispView();
                                break;
                        }

                    }
                    if (isPermission) break;
                }
                if (!isPermission) {
                    //create a general error code for user not having permission
                    newReturnObject.addError(singleError("MUS013", language));
                }
            }
        } catch (Exception ex) {
            logger.error(ex);
            isPermission = false;
        }
            return isPermission;

    }

        public boolean creatorHasPermission(String userid, NewReturnObject newReturnObject, String errorCode, String language, String functionid, String operationid) {
        boolean isPermission = false;
        try {

            if (creatoridExist(userid, newReturnObject, errorCode, language)) {
                LookupOperation lookupOperation = LookupOperation.newLookup().
                        from("tm_role_function").
                        localField("user.bankRole").
                        foreignField("roleId").
                        as("user.bankRole");
                AggregationOperation userMatch = Aggregation.match(Criteria.where("user.officialEmailId").is(userid)); //userid/officialEmailId match data
                AggregationOperation functionMatch = Aggregation.match(Criteria.where("user.bankRole.permissions.functionId").is(functionid));// match role_function Id /roleid from role_function table
                AggregationOperation replaceRoot=Aggregation.replaceRoot("user");
                Aggregation aggregation = Aggregation.newAggregation(userMatch, lookupOperation, unwind("user.bankRole"), unwind("user.bankRole.permissions"), functionMatch,replaceRoot);
                List<UserRole> results = mongoTemplate.aggregate(aggregation, "tm_user", UserRole.class).getMappedResults();
                for (int i = 0; i < results.size(); i++) {
                    switch (operationid) {
                        case FnOptConfig.opFNew:
                            isPermission = results.get(i).getBankRole().getPermissions().getAction().ispNew();
                            break;
                        case FnOptConfig.opFEdit:
                            isPermission = results.get(i).getBankRole().getPermissions().getAction().ispEdit();
                            break;
                        case FnOptConfig.opFDelete:
                            isPermission = results.get(i).getBankRole().getPermissions().getAction().ispDelete();
                            break;
                        case FnOptConfig.opFAuth:
                            isPermission = results.get(i).getBankRole().getPermissions().getAction().ispAuth();
                            break;
                        case FnOptConfig.opFCancel:
                            isPermission = results.get(i).getBankRole().getPermissions().getAction().ispCancel();
                            break;
                        case FnOptConfig.opFReverse:
                            isPermission = results.get(i).getBankRole().getPermissions().getAction().ispReverse();
                            break;
                        case FnOptConfig.opFHold:
                            isPermission = results.get(i).getBankRole().getPermissions().getAction().ispHold();
                            break;
                        case FnOptConfig.opFView:
                            isPermission = results.get(i).getBankRole().getPermissions().getAction().ispView();
                            break;
                        case FnOptConfig.opFReactivate:
                            isPermission = results.get(i).getBankRole().getPermissions().getAction().ispReactivate();
                            break;

                    }

                    if (isPermission) break;
                }
                if (!isPermission) {
                    //create a general error code for user not having permission
                    newReturnObject.addError(singleError("MUS013", language));
                }
            }
        } catch (Exception ex) {
            isPermission = false;
            logger.error(ex);
        } finally {
            return isPermission;
        }
    }
    }

